VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrincipal 
   BackColor       =   &H80000016&
   Caption         =   "Portal de Neg�cios da Alian�a do Brasil"
   ClientHeight    =   8190
   ClientLeft      =   630
   ClientTop       =   2070
   ClientWidth     =   11400
   ForeColor       =   &H0000C000&
   LinkTopic       =   "Form1"
   ScaleHeight     =   8190
   ScaleWidth      =   11400
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer2 
      Interval        =   10
      Left            =   5250
      Top             =   5400
   End
   Begin VB.Timer Timer3 
      Interval        =   10
      Left            =   4800
      Top             =   5400
   End
   Begin VB.Timer Timer1 
      Interval        =   65535
      Left            =   4320
      Top             =   5400
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   7815
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7056
            MinWidth        =   7056
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7056
            MinWidth        =   7056
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   7056
            MinWidth        =   7056
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   5760
      Top             =   3480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":0000
            Key             =   "expandir"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":0454
            Key             =   "fechar"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":08A8
            Key             =   "sisbb"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":282C
            Key             =   "sair"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":2C80
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":30D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":3570
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":3BA2
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":4374
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":47C2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   390
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   688
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "ImageList2"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   10
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Expandir �rvore"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Fechar �rvore"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Acesso ao SISBB"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Trocar Empresa"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Ajuda"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Sair"
            ImageIndex      =   4
         EndProperty
      EndProperty
      OLEDropMode     =   1
      Begin VB.TextBox txtNomeEmpresa 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2790
         TabIndex        =   4
         Text            =   "txtNomeEmpresa"
         Top             =   90
         Width           =   8565
      End
      Begin VB.PictureBox Picture1 
         Height          =   135
         Left            =   1920
         ScaleHeight     =   135
         ScaleWidth      =   15
         TabIndex        =   2
         Top             =   0
         Width           =   15
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6840
      Top             =   4560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":4C4C
            Key             =   "NoAberto"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":50A0
            Key             =   "Programa"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":54F4
            Key             =   "NoFechado"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "menu.frx":5948
            Key             =   "Expandir"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView Tree 
      Height          =   7410
      Left            =   15
      TabIndex        =   0
      Top             =   405
      Width           =   3660
      _ExtentX        =   6456
      _ExtentY        =   13070
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      Style           =   7
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Begin VB.Menu mnuSisbb 
         Caption         =   "Acesso ao SISBB"
      End
      Begin VB.Menu MnExecutarAplicativo 
         Caption         =   "Executar Aplicativo"
      End
      Begin VB.Menu MnListaPermissoes 
         Caption         =   "Imprimir Lista de Permiss�es"
      End
      Begin VB.Menu MnRecarregarmenu 
         Caption         =   "Recarregar menu"
      End
      Begin VB.Menu MnLogin 
         Caption         =   "Trocar Empresa"
      End
      Begin VB.Menu separador 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "Sair"
      End
   End
   Begin VB.Menu mnuExibir 
      Caption         =   "&Exibir"
      Begin VB.Menu mnuExpandir 
         Caption         =   "Expandir �rvore"
      End
      Begin VB.Menu mnuResumir 
         Caption         =   "Resumir �rvore"
      End
   End
   Begin VB.Menu mnuAjuda 
      Caption         =   "Ajuda"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "&Sobre"
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim gcolInstancias As New Collection

Private lContador           As Long
Private dx                  As New DirectX7
Private di                  As DirectInput
Private diDeviceKeyb        As DirectInputDevice
Private diDeviceMouse       As DirectInputDevice
Private diDevEnum           As DirectInputEnumDevices
Private lTempoTimeout       As Long
Public gdHndJanelaPrincipal As Double
Public gdtUltimaDataHora    As Date
Public bCancelaLogin        As Boolean
Public sSenhaPortal         As String

Private Sub Form_Activate()

  'obtendo o handle da janela deste programa
  gdHndJanelaPrincipal = GetActiveWindow
  
End Sub

Private Sub LogMeIn()

  Call ChamarTelaLogin(oSABL0010)

  'cria ODBC
  'Call RegistrarFonteDados(oSABL0010, glAmbiente_id)
  Call RegistrarFonteDados(oSABL0010, glAmbiente_id_seg2)

  Call Inicializar_Vari�veis_Menu(oSABL0010)

  'Configura o Status Bar
  Call ConfigurarStatusBar
      
  If bCancelaLogin = False Then
  
    'Carrega a arvore
    Call CarregarArvore(Tree, ImageList1)
  
  End If

End Sub


Private Sub Form_Load()
  On Local Error GoTo TratarErro
  
  Timer1.Enabled = False
  Timer2.Enabled = False
  Timer3.Enabled = False

  glAmbiente_id = 3

  Set oSABL0010 = CreateObject(gsOBJETOAMBIENTE)
  
  'Conecta no Banco em produ��o
  Call Conexao(, , oSABL0010)
  glAmbiente_id_seg2 = glAmbiente_id
  
  Call ChamarTelaLogin(oSABL0010)

  Call Inicializar_Vari�veis_Menu(oSABL0010)
  
  'cria tabela tempor�ria para verifica��o de programas utilizados pelo usu�rio
  Call CriaTabelaTemporaria(oSABL0010)
  
  '-- TESTE
  ''''glAmbiente_id_seg2 = 2
  
  'cria ODBC
  'Call RegistrarFonteDados(oSABL0010, glAmbiente_id)
  Call RegistrarFonteDados(oSABL0010, glAmbiente_id_seg2)

  'Timer1.Interval = 30000
  Timer1.Interval = 65535
  Timer3.Interval = 10000
        
  Me.Top = 0
  Me.Left = 0
  Me.Height = Screen.Height - 400
  Me.Width = Screen.Width
  
  'Configura o Status Bar
  Call ConfigurarStatusBar
      
  'Carrega a arvore
  Call CarregarArvore(Tree, ImageList1)
  
  'dispara timer de atualizacao da colecao de programas abertos
  Timer3.Enabled = True
  
  '   DESABILITADO PARA EVITAR CONSULTAS PERI�DICAS AO BANCO DE DADOS,
  '   VISANDO A RESOLVER PROBLEMAS DE LENTID�O DO SISTEMA
  
  Timer3.Enabled = False
  
  Exit Sub
  
TratarErro:
  Call TrataErroGeral("Carregamento do Formul�rio", Me.name)
  Unload Me
End Sub

Function CarregarArvore(oArvore As TreeView, oImagens As ImageList)

'carrega a arvore de diretorios e programas

Dim sSql As String
Dim rs   As Recordset
Dim rsSistema   As Recordset
Dim lUsuarioId As Long
Dim lSistemaId As Long
Dim lIndice As Long
Dim sCodEmpresa As String

oArvore.Nodes.Clear

On Error GoTo Trata_Erro
  oArvore.ImageList = oImagens
  Call oArvore.Nodes.Add(, , "ALS0000", "PORTAL DE NEG�CIOS", "NoFechado")
  
  'L� o usu�rio para pegar o usuario_id
  'sSQL = " SELECT usuario_id " & vbNewLine
  'sSQL = sSQL & " FROM segab_db..usuario_tb " & vbNewLine
  'sSQL = sSQL & " WHERE situacao = 'A' " & vbNewLine
  'sSQL = sSQL & "   AND cpf = '" & gsCPF & "'" & vbNewLine
  'Set rs = oSABL0010.ExecutaAmbiente(sSQL)
  'If Not rs.EOF Then lUsuarioId = rs("usuario_id")
  'rs.Close
  lUsuarioId = glUsu�rio
  
  'lUsuarioId = 7903 usuario do Jorge
  'L� o sistema para pegar o sistema_id
  '''sSQL = " SELECT distinct sistema_id " & vbNewLine
  sSql = " SELECT sistema_id " & vbNewLine
  sSql = sSql & " FROM segab_db..sistema_tb " & vbNewLine
  sSql = sSql & " WHERE situacao = 'A' " & vbNewLine
  sSql = sSql & "   AND tipo = 1 " & vbNewLine
  sSql = sSql & "   AND subsistema_id IS NULL" & vbNewLine
  Set rsSistema = oSABL0010.ExecutaAmbiente(sSql)
  If Not rsSistema.EOF Then
  'Atribui a arvore as imagens
  
    Do While Not rsSistema.EOF
    'SETANDO A EMPRESA PARA EMPRESA LIBERADA PARA O PHILLIP NA QUALIDADE
    glEmpresa_id = 3911
    
        lSistemaId = rsSistema("sistema_id")
        'MsgBox lSistemaId
        'Pega a lista de programas e diret�rios para montar a �rvore
        sSql = " EXEC segab_db..monta_arvore_individual_usuario_sps " & lUsuarioId & " , " & lSistemaId & ", " & glEmpresa_id & ", " & glAmbiente_id_seg2
        Set rs = oSABL0010.ExecutaAmbiente(sSql)
        
        Do While Not rs.EOF
          
          'Se o menu pai for nulo ent�o � a raiz pois n�o tem pai
          If IsNull(rs("MENU_PAI_ID")) Then
            'Call oArvore.Nodes.Add(, , "ALS" & CStr(rs("MENU_ID")), rs("NOME_MENU"), "NoFechado")
            Call oArvore.Nodes.Add("ALS0000", tvwChild, "ALS" & rs("menu_id"), UCase(rs("NOME_MENU")), "NoFechado")
            Tree.Nodes.Item("ALS" & CStr(rs("MENU_ID"))).Expanded = True
          Else
            '� um n� n�o raiz
            'Se o recurso estiver nulo � um n� pai
            If IsNull(rs("RECURSO_ID")) Then
              Call oArvore.Nodes.Add("ALS" & rs("MENU_PAI_ID"), tvwChild, "ALS" & rs("menu_id"), UCase(rs("NOME_MENU")), "NoFechado")
              oArvore.Nodes("ALS" & rs("menu_id")).Tag = rs("SIGLA_RECURSO")
            Else
              '� um n� filho
              
              'Call oArvore.Nodes.Add("ALS" & rs("MENU_PAI_ID"), tvwChild, "ALS" & rs("menu_id"), UCase(rs("SIGLA_RECURSO") & " - " & rs("NOME_MENU")), "Programa")
              Call oArvore.Nodes.Add("ALS0000", tvwChild, "ALS" & rs("menu_id"), UCase(rs("SIGLA_RECURSO") & " - " & rs("NOME_MENU")), "Programa")
              
              'Call oArvore.Nodes.Add("ALS" & rs("MENU_PAI_ID"), tvwChild, "ALS" & rs("menu_id"), UCase(rs("SIGLA_RECURSO") & " - " & rs("NOME_MENU") & sCodEmpresa), "Programa")
              oArvore.Nodes("ALS" & rs("menu_id")).Tag = rs("SIGLA_RECURSO")
            End If
          End If
          lIndice = lIndice + 1
          rs.MoveNext
          If lIndice Mod 100 = 0 Then
                    DoEvents
          End If
        Loop
        rs.Close
        rsSistema.MoveNext
    Loop
  End If
  Exit Function

Trata_Erro:
  '  Call GravaMensagem(Err.Description, "MENU", "CARREGAR ARVORE", "SEGP0007")
    Resume Next
End Function

Private Sub ConfigurarStatusBar()
      
Dim sUsuario  As String
Dim sAmbiente As String
Dim sSql      As String
Dim rs        As Recordset

On Error GoTo Trata_Erro

  '''sSql = "SELECT * FROM segab_db..usuario_tb WHERE cpf = '" & gsCPF & "'"
  sSql = "SELECT nome FROM segab_db..usuario_tb WHERE cpf = '" & gsCPF & "'"
  Set rs = oSABL0010.ExecutaAmbiente(sSql)
  If Not rs.EOF Then
    sUsuario = rs("nome")
  End If
  
  '''sSql = "SELECT * FROM segab_db..ambiente_tb WHERE ambiente_id = " & glAmbiente_id
  '''sSQL = "SELECT nome FROM segab_db..ambiente_tb WHERE ambiente_id = " & glAmbiente_id
  sSql = "SELECT nome FROM segab_db..ambiente_tb WHERE ambiente_id = " & glAmbiente_id_seg2
  Set rs = oSABL0010.ExecutaAmbiente(sSql)
  If Not rs.EOF Then
    sAmbiente = rs("nome")
  End If
  
  StatusBar1.Panels(1).Text = "Dt. Movimento: " & Data_Sistema
  StatusBar1.Panels(1).Alignment = sbrCenter
  StatusBar1.Panels(1).Width = Me.Width / 4
  StatusBar1.Panels(2).Text = "CPF: " & gsCPF
  StatusBar1.Panels(2).Alignment = sbrCenter
  StatusBar1.Panels(2).Width = Me.Width / 4
  StatusBar1.Panels(3).Text = "Usu�rio: " & sUsuario
  StatusBar1.Panels(3).Alignment = sbrCenter
  StatusBar1.Panels(3).Width = Me.Width / 4
  StatusBar1.Panels(4).Text = "Ambiente: " & sAmbiente
  StatusBar1.Panels(4).Alignment = sbrCenter
  StatusBar1.Panels(4).Width = Me.Width / 4
  
  sSql = "       SELECT e.descricao, e.cod_empresa "
  sSql = sSql & "FROM controle_sistema_db..empresa_ambiente_tb a "
  sSql = sSql & "JOIN controle_sistema_db..empresa_tb e "
  sSql = sSql & "  ON e.cod_empresa = a.cod_empresa "
  sSql = sSql & "Where ambiente_destino_id = " & glAmbiente_id_seg2
  Set rs = oSABL0010.ExecutaAmbiente(sSql)
  If Not rs.EOF Then
     'frmPrincipal.Caption = "Portal de Neg�cios - " & rs("descricao")
     frmPrincipal.Caption = "Portal de Neg�cios da Alian�a do Brasil"
  
     txtNomeEmpresa = rs("descricao") & " - " & rs("cod_empresa")
     If glAmbiente_id <> glAmbiente_id_seg2 Or rs("cod_empresa") = "ABS" Then
        txtNomeEmpresa.ForeColor = &HFF&            ' VERMELHO
     Else
        txtNomeEmpresa.ForeColor = &HFF0000         ' AZUL
     End If
  Else
     frmPrincipal.Caption = "Portal de Neg�cios da Alian�a do Brasil"
  End If
        
  'txtNomeEmpresa.ForeColor = &HFF&         ' VERMELHO
  'txtNomeEmpresa.ForeColor = &HFF0000      ' AZUL
  'txtNomeEmpresa.ForeColor = &HFFFF&       ' AMARELO
  
  Exit Sub
  
Trata_Erro:
  Call TrataErroGeral("ConfigurarStatusBar", Me.name)
End Sub
 
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If MsgBox("Deseja fechar todos os programas do SEGBR?", vbYesNo, "SEGBR") = vbNo Then
    Cancel = 1
    Exit Sub
Else
    Form_Unload (0)
End If
End Sub

Private Sub Form_Resize()
If Me.Height <= 500 Then
    Exit Sub
End If
'StatusBar1.Width = Me.Width
Toolbar1.Width = Me.Width

With Tree
    .Width = Me.Width - 100
    .Height = Me.Height - 1200 - Toolbar1.Height - StatusBar1.Height
    .Left = 0
    .Top = StatusBar1.Height + Toolbar1.Height
End With

End Sub

Private Sub Form_Unload(Cancel As Integer)
  
On Local Error GoTo TrataErro
  
  Call VerificaProgramasAbertos(True, oSABL0010)
  Set oSABL0010 = Nothing
  Call TerminaSEGBR
  
  Exit Sub
TrataErro:
  Resume Next
End Sub

Private Sub MnExecutarAplicativo_Click()
  
  FrmAplicativo.Show vbModeless
  
End Sub

Private Sub MnLogin_Click()

    If MsgBox("Todos os programas ser�o fechados ao trocar a Empresa." & Chr(13) & Chr(13) & "Deseja Continuar ?", vbYesNo, "SEGBR") = vbYes Then
    
        Call LogMeIn

    End If

End Sub

Private Sub MnRecarregarmenu_Click()

    Call CarregarArvore(Tree, ImageList1)

End Sub

Private Sub mnuAjuda_Click()
Dim SQS      As String
Dim rs        As Recordset
Dim sTabTemp   As String
  sTabTemp = """##ajuda" & gsMac & "-componentes"""
 
  SQS = "CREATE TABLE " & sTabTemp & "("
  SQS = SQS & "  componente VARCHAR( 300) NOT NULL,"
  SQS = SQS & "  estado CHAR(1) NOT NULL"
  SQS = SQS & ")"
  'Call oSABL0010.ExecutaAmbiente(SQS)
 Set rs = oSABL0010.ExecutaAmbiente(SQS)
    Call ExecutaPrograma("SGSP0034.exe", "SGSP0034.exe", "", vbNormalFocus, oSABL0010)
 ' sTabTemp = """##ajuda" & gsMac & "-componentes"""
  SQS = "DROP TABLE " & sTabTemp
  Call oSABL0010.ExecutaAmbiente(SQS)
    
End Sub

Private Sub mnuExpandir_Click()
Dim TotalNo As Integer
Dim i As Integer
    TotalNo = Tree.Nodes.Count
    'expande os n�s
    For i = 1 To TotalNo
        If Tree.Nodes.Item(i).Children > 0 Then
            Tree.Nodes.Item(i).Expanded = True
        End If
    Next i
End Sub

Private Sub mnuResumir_Click()
    'fecha os n�s
    Tree.Nodes.Item(1).Expanded = False
End Sub

Private Sub mnuSair_Click()
  
  If MsgBox("Deseja fechar todos os programas do SEGBR?", vbYesNo, "SEGBR") = vbNo Then
    Exit Sub
  Else
    Call Form_Unload(0)
  End If

End Sub

Private Sub mnuSisbb_Click()

Dim sChamada As String

  MousePointer = vbHourglass
  'sChamada = Shell("C:\Arquivos de Programas\Internet Explorer\iexplore  https://www.aliancadobrasil.com.br/default.asp?proc01298=hdkahsdkasdhuas19382187637816238612983khadhaskhdad979701820381023lkjldkalk", vbMaximizedFocus)
  sChamada = Shell("C:\Arquivos de Programas\Internet Explorer\iexplore  https://www.aliancadobrasil.com.br/internet/serv/sisbb/default1.asp", vbMaximizedFocus)
  MousePointer = vbDefault

End Sub

Private Sub mnuSobre_Click()
    frmSobre.Show
End Sub

Private Sub Timer1_Timer()

Dim MouseState    As DIMOUSESTATE
Dim KeybState     As DIKEYBOARDSTATE
Dim sSql          As String
Dim bEncontrou    As Boolean
Dim iIndice       As Integer
Dim dhAtual       As Date

On Error GoTo Trata_Erro

  lContador = lContador + 1
    
  diDeviceMouse.GetDeviceStateMouse MouseState
  diDeviceKeyb.GetDeviceStateKeyboard KeybState
        
  For iIndice = 0 To 255
    If KeybState.Key(iIndice) <> 0 Then
      bEncontrou = True
      Exit For
    End If
  Next
  
  dhAtual = DataHoraAmbiente(, oSABL0010)
  If bEncontrou Or MouseState.x <> 0 Or MouseState.y <> 0 Or MouseState.buttons(0) <> 0 Or _
     MouseState.buttons(1) Or MouseState.buttons(2) Or KeybState.Key(66) <> 0 Then
    gdtUltimaDataHora = dhAtual
  End If
   
  If (dhAtual - gdtUltimaDataHora) * 24 * 60 >= lTempoTimeout Then
    gdtUltimaDataHora = dhAtual
    
   'chamando tela de login
    'desabilitando programas abertos do sistema
    Call HabilitarProgramas(False)
    
    'travando timer de tempo ocioso
    Timer1.Enabled = False
    'disparando o timer de espera ate o usuario focar um programa do sistema
    Timer2.Enabled = True
            
  End If
    
  Exit Sub

Trata_Erro:
  Call TrataErroGeral("Timer1_Timer", Me.name)
End Sub

Private Sub Timer2_Timer()

Dim objInstancias As Object
Dim dTelaAtiva As Double

On Error GoTo Trata_Erro

  Set objInstancias = gcolInstancias
    
  'verificando se o programa que esta sendo utilizado pelo usuario e do sistema
  dTelaAtiva = GetActiveWindow()
    
  If gdHndJanelaPrincipal = dTelaAtiva Then
    
    'chamando tela de login
    Call ChamarTelaLogin(oSABL0010)
  
  Else
      
    For Each objInstancias In gcolInstancias
        
        If VerificarJanelaAtiva(objInstancias.instancia) Then
          
          'chamando tela de login
          Call ChamarTelaLogin(oSABL0010)
          
          Exit For
      End If
    Next
        
  End If
        
  
  Exit Sub

Trata_Erro:
  Call TrataErroGeral("Timer2_Timer", Me.name)
End Sub
Private Sub Timer3_Timer()

Dim sSql As String
Dim rs As Recordset
Dim oInstancias As clsProgAbertos
    
    Set gcolInstancias = New Collection
        
    sSql = "       SELECT instancia FROM " & gsTabelaTempor�riaUsu�rio
    sSql = sSql & " WHERE instancia <> 0"
    Set rs = oSABL0010.ExecutaAmbiente(sSql)
    
    While Not rs.EOF
        
        Set oInstancias = New clsProgAbertos
        
        oInstancias.instancia = rs(0)
        Call gcolInstancias.Add(oInstancias)
        rs.MoveNext
        
    Wend
    
    Set rs = Nothing
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
Dim TotalNo As Integer
Dim chamada As Variant
       
    TotalNo = Tree.Nodes.Count
    Select Case Button.Index
    Case 1
        'expande os n�s
        mnuExpandir_Click
    Case 2
        'fecha os n�s
        mnuResumir_Click
    Case 4
        'CHAMA O SISBB
        mnuSisbb_Click
    Case 6
        Call MnLogin_Click
        '''Call LogMeIn
    Case 8
        mnuAjuda_Click
    Case 10
        mnuSair_Click
        
    End Select
End Sub

Private Sub Tree_DblClick()

Dim sStatusSistema As String
Dim sSistemaSistema As String
Dim sMenuID        As String

On Error GoTo Tratar_Erro
  
  If Not Tree.SelectedItem Is Nothing Then
    sMenuID = Mid(frmPrincipal.Tree.SelectedItem.Key, 4, Len(frmPrincipal.Tree.SelectedItem.Key) - 3)
  
    If BuscarSituacaoSistema(sMenuID, sStatusSistema, sSistemaSistema) = True Then
  
      Select Case UCase(sSistemaSistema)
  
        Case "L", "LC", "C"
          If sStatusSistema = "B" Then
            Call MsgBox("A op��o selecionada n�o pode ser executada com o sistema SEGBR bloqueado.")
            Exit Sub
          End If
  
        Case "B", "BC", "BL", "BLC"
  
      End Select
    End If
  
    'se for um n� pai n�o chama um programa
    If Tree.SelectedItem.Children > 0 Then Exit Sub
    
    'Executa o aplicativo selecionado
    Call ExecutarAplicativo(Mid(frmPrincipal.Tree.SelectedItem.Key, 4, Len(frmPrincipal.Tree.SelectedItem.Key) - 3), frmPrincipal.Tree.SelectedItem.Text)
    
  End If
  
  Exit Sub

Tratar_Erro:
    Call TrataErroGeral("Tree_DblClick", "frmPrincipal.frm")
End Sub

Private Sub Tree_Expand(ByVal Node As MSComctlLib.Node)
    'expande o n� selecionado
    Node.Image = "NoAberto"
End Sub

Private Sub Tree_Collapse(ByVal Node As MSComctlLib.Node)
    'fecha a pasta do n� selecionado
  
  'gsCPF = "74697420772" ' ascofano ' "60968826768" ' WRIBEIRO
  'glAmbiente_id = 2
  'glEmpresa_id = 109 ' 22
  
    Node.Image = "NoFechado"
End Sub

Sub HabilitarProgramas(bFlag As Boolean)

Dim objInstancias As Object

  Set objInstancias = gcolInstancias
    
  For Each objInstancias In gcolInstancias
        
    Call Habilitar_Todas_Janelas(objInstancias.instancia, bFlag)
                
  Next
        
End Sub

Sub ChamarTelaLogin(objAmbiente As Object)

Dim oSABL0011 As Object


  Timer2.Enabled = False
  
  'criando a instancia da dll e chamamdo a tela de login
  Set oSABL0011 = CreateObject("SABL0011.cls00010")

  'gsCPF = "13994926831" 'anacarvalho
  'glAmbiente_id = 3
  'glEmpresa_id = 30

  'gsCPF = "28384491844" 'fpimenta
  'glAmbiente_id = 2
  'glEmpresa_id = 30

  'gsCPF = "74697420772" ' ascofano
  'glAmbiente_id = 2
  'glEmpresa_id = 109 ' 22
  bCancelaLogin = False
  If Not oSABL0011.ChamarTela(gsCPF, glEmpresa_id, glAmbiente_id, glAmbiente_id_seg2, bCancelaLogin, sSenhaPortal) Then
    If bCancelaLogin = False Then
        Call VerificaProgramasAbertos(True, objAmbiente)
        Set oSABL0011 = Nothing
        Call TerminaSEGBR
    End If
  Else
    'habilitando novamente o acesso aos programas abertos do sistema
    If bCancelaLogin = False Then
        Call VerificaProgramasAbertos(True, objAmbiente)
        Call HabilitarProgramas(True)
    End If
    Set oSABL0011 = Nothing
  End If
  
  'dispara o timer de tempo ocioso para novo pedido de login
  Timer1.Enabled = True
  
End Sub

Private Function Inicializar_Vari�veis_Menu(objAmbiente As Object)
  On Local Error GoTo TrataExce��o
  Dim RX As ADODB.Recordset
  
  ' vari�veis que N�O dependem do ambiente escolhido pelo usu�rio
  
  SQS = "SELECT usuario_id"
  adSQS "FROM segab_db..usuario_tb"
  adSQS " WHERE situacao = 'A'"
  adSQS "   AND cpf = '" & gsCPF & "'"
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  If Not RX.EOF Then
    glUsu�rio = RX("usuario_id")
  End If
  RX.Close
  Set RX = Nothing

  gsTabelaTempor�riaUsu�rio = TabTempUsr(, , objAmbiente)
  
  gdtUltimaDataHora = DataHoraAmbiente(, objAmbiente)



  'Para o controle do timeout
  Set di = dx.DirectInputCreate
  Set diDeviceMouse = di.CreateDevice("GUID_SysMouse")
  diDeviceMouse.SetCommonDataFormat DIFORMAT_MOUSE
  diDeviceMouse.SetCooperativeLevel hwnd, DISCL_NONEXCLUSIVE Or DISCL_BACKGROUND
  diDeviceMouse.Acquire
  Set diDevEnum = di.GetDIEnumDevices(DIDEVTYPE_KEYBOARD, DIEDFL_ATTACHEDONLY)
  Set diDeviceKeyb = di.CreateDevice("GUID_SysKeyboard")
  diDeviceKeyb.SetCommonDataFormat DIFORMAT_KEYBOARD
  diDeviceKeyb.SetCooperativeLevel hwnd, DISCL_NONEXCLUSIVE Or DISCL_BACKGROUND
  diDeviceKeyb.Acquire
  
  ' vari�veis que dependem do ambiente escolhido pelo usu�rio
  
  Call inicializa_variaveis_ambiente(objAmbiente, glAmbiente_id_seg2)
  
  lTempoTimeout = "0" & objAmbiente.RetornaValorAmbiente("SEGBR", "OPERACIONAL", "TIMEOUT", gsCHAVESISTEMA, glAmbiente_id)
  
  If lTempoTimeout = 0 Then
    lTempoTimeout = 5
  End If

  Exit Function
TrataExce��o:
  Call TrataErroGeral("Inicializar_Vari�veis_Menu", Me.name)
  Call TerminaSEGBR
End Function
