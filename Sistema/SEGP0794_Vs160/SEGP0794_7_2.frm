VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form SEGP0794_7_2 
   Caption         =   "Question�rio de Sinistro"
   ClientHeight    =   7335
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10875
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7335
   ScaleWidth      =   10875
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frBotoes 
      Height          =   975
      Left            =   240
      TabIndex        =   1
      Top             =   6120
      Width           =   10335
      Begin VB.CommandButton btnAplicar 
         Caption         =   "Aplicar"
         Height          =   360
         Left            =   9000
         TabIndex        =   2
         Top             =   360
         Width           =   990
      End
   End
   Begin VB.Frame frQuestionario 
      Caption         =   "Question�rio"
      Height          =   5655
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   10335
      Begin VB.ComboBox cmbResposta 
         Height          =   315
         Left            =   1665
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1395
         Width           =   1935
      End
      Begin MSFlexGridLib.MSFlexGrid flexQuest 
         Height          =   5055
         Left            =   270
         TabIndex        =   3
         Top             =   405
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   8916
         _Version        =   393216
         Cols            =   5
         FormatString    =   "Seq | Pergunta | Resposta | Codigo"
      End
   End
End
Attribute VB_Name = "SEGP0794_7_2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private iTecnicoId As Integer
Private rsQuest As Recordset

Public questionario_id As Integer
Public Evento_sinistro_id As String
Public sinistro_id As String



Private Sub btnAplicar_Click()

    AplicarQuestoesSinistro

End Sub

Private Sub Form_Load()

    Evento_sinistro_id = SEGP0794_3.cboEvento.ItemData(SEGP0794_3.cboEvento.ListIndex)
    sinistro_id = SEGP0794_7.sinistro_id
    SEGP0794_7_1.form_quest = "sin"
  ' gHW = flexQuest.hwnd
   'Hook   ' Start checking messages.
    flexQuest.AllowUserResizing = flexResizeNone
    flexQuest.Cols = 5
              
    flexQuest.RowHeightMin = cmbResposta.Height
    cmbResposta.Visible = False
    cmbResposta.ZOrder (0)
    cmbResposta.Width = flexQuest.CellWidth
    
    ' Load the ComboBox's list.
    cmbResposta.AddItem "V"
    cmbResposta.AddItem "F"
      
    
    ConsultarQuestionario
    
End Sub



Private Sub ConsultarQuestionario()

        Dim SQL As String
        Dim rsConsultarQuestionario As ADODB.Recordset
        Dim rsPerguntas As ADODB.Recordset
        Dim j As Integer
        Dim k As Integer
        Dim linhas As New Collection
        Dim l As Integer
        Dim totalLinha As Integer
        Dim i As Integer
        
        j = 1
        k = 0
        
        SQL = "exec SEGS9411_SPS " & CInt(SEGP0794_7.produto_id) & ", " & CDbl(Evento_sinistro_id)
        
                
        Set rsConsultarQuestionario = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQL, _
                                                True)
                                                
                                                        
        Do While Not rsConsultarQuestionario.EOF
               
        
            
        
            questionario_id = rsConsultarQuestionario(0)
                    
            SQL = "exec SEGS9064_SPS " & questionario_id
            
            Set rsPerguntas = ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                SQL, _
                                                True)
            
              
            If Not rsPerguntas.EOF Then
                                       
                
                i = 0
                
                Do While Not rsPerguntas.EOF
                
                    rsPerguntas.MoveNext
                    i = i + 1
                Loop
                
                rsPerguntas.MoveFirst
                
                
                
                linhas.Add (i + 1)
                
                For l = 0 To k
                
                    If k = 0 Then
                    
                        totalLinha = totalLinha + CInt(linhas(k + 1))
                    
                    Else
                    
                        totalLinha = totalLinha + CInt(linhas(k + 1)) - 1
                    
                    End If
                    
                    
                    
                    l = k
                    
                Next
                                
                l = 0
                
                flexQuest.Rows = totalLinha
                                        
                
                Do While Not rsPerguntas.EOF
                
    
                       flexQuest.TextMatrix(j, 0) = j
                       flexQuest.TextMatrix(j, 1) = rsPerguntas("TEXTO_PERGUNTA")
                       flexQuest.TextMatrix(j, 2) = ""
                       flexQuest.TextMatrix(j, 3) = rsPerguntas("pergunta_id")
                       flexQuest.TextMatrix(j, 4) = rsConsultarQuestionario("questionario_id")
                       flexQuest.WordWrap = True
                       flexQuest.RowHeight(j) = 700
                             
                       j = j + 1
                       rsPerguntas.MoveNext
                Loop
                
                flexQuest.ColWidth(1) = 8480
                flexQuest.ColWidth(3) = 0
                flexQuest.ColWidth(4) = 0
                
                
                
            End If
            k = k + 1
            rsConsultarQuestionario.MoveNext
        Loop
        
        
               
    End Sub
    
  
Private Sub AplicarQuestoesSinistro()
              
        Dim i As Integer
        If flexQuest.Rows - 1 > 0 Then
        
          For i = 1 To flexQuest.Rows - 1
        
                      
                Dim rsQuest As New ADODB.Recordset
                Dim sSQL As String
                
                
            If flexQuest.TextMatrix(i, 2) = "" Then
                MsgBox ("Favor Responder a Pergunta de n�mero " & i & "!")
                Exit Sub
            End If

                sSQL = "exec SEGS9412_SPI "
                sSQL = sSQL & flexQuest.TextMatrix(i, 4)
                sSQL = sSQL & ", " & flexQuest.TextMatrix(i, 3)
                sSQL = sSQL & ", " & SEGP0794_7.sinistro_id 'sinistro_id - Esta vari�vel n�o esta sendo atualizada corretamente, provocando erro de PK.
                sSQL = sSQL & ", '" & flexQuest.TextMatrix(i, 2) & "'"
                sSQL = sSQL & ", '" & cUserName & "'"

                ExecutarSQL gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSQL, _
                                           False
        
            Next
        
             
        End If
        
        For i = 1 To flexQuest.Rows - 1
        
            flexQuest.TextMatrix(i, 2) = ""
                
        Next
            
        Me.Hide
        
        With SEGP0794_fim
            .stbQtd.SimpleText = SEGP0794_7.GridPropostaAfetada.Rows - 1 & " sinistro(s) avisado(s)."
            .grdPropAvis.Rows = 1
            For i = 1 To SEGP0794_7.GridPropostaAfetada.Rows - 1
                .grdPropAvis.Rows = .grdPropAvis.Rows + 1
                .grdPropAvis.TextMatrix(i, 0) = IIf(Not IsNull(Proposta_Avisada(i).sinistro_id), Proposta_Avisada(i).sinistro_id, "")
                .grdPropAvis.TextMatrix(i, 1) = IIf(Not IsNull(Proposta_Avisada(i).proposta_id), Proposta_Avisada(i).proposta_id, "")
                .grdPropAvis.TextMatrix(i, 2) = IIf(Not IsNull(Proposta_Avisada(i).produto), Proposta_Avisada(i).produto, "")
            Next i
            
            .Show
        End With
        
    End Sub
    
    

     
      Private Sub flexQuest_MouseUp(Button As Integer, _
         Shift As Integer, x As Single, y As Single)
         
          Static CurrentWidth As Single
          ' Check to see if the Cell's width has changed.
          If flexQuest.CellWidth <> CurrentWidth Then
              cmbResposta.Width = flexQuest.CellWidth
              CurrentWidth = flexQuest.CellWidth
              
          End If
      End Sub
      
      Private Sub flexQuest_Click()
             
          'ConsultarQuestionario
          cmbResposta.Width = flexQuest.CellWidth
          cmbResposta.Left = flexQuest.CellLeft + flexQuest.Left
          cmbResposta.Top = flexQuest.CellTop + flexQuest.Top
         
          
            If flexQuest.CellLeft = "8925" Then
            
                cmbResposta.Visible = True
            
            Else
                
                cmbResposta.Visible = False
            
            End If
        
      End Sub

Private Sub cmbResposta_Click()
          ' Place the selected item into the Cell and hide the ComboBox.
          flexQuest.Text = cmbResposta.Text
          cmbResposta.Visible = False
          
End Sub



