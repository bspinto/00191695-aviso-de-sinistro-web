--trava_saida
BEGIN TRAN
declare @dt smalldatetime = '2019-11-18'
     update a
       set msg_id = 'BSP'
       , situacao = 'x'
       , dt_coa = GETDATE()
       ,dt_cod = GETDATE()
       , dt_alteracao = @dt
     from interface_db.dbo.saida_gtr_atual_tb a with (nolock)
     where msg_id is null
     and dt_COA is null
     and dt_COD is null
     and situacao = 'N'
     AND saida_gtr_id not IN (23110376,23110377)
     
     update a
       set msg_id = 'BSP_T'
       , situacao = 'x'
       , dt_coa = GETDATE()
       ,dt_cod = GETDATE()
       , dt_alteracao = @dt
     from interface_db.dbo.saida_gtr_atual_tb a with (nolock)
     where msg_id is null
     and dt_COA is null
     and dt_COD is null
     and situacao = 'T'
      AND saida_gtr_id not IN (23110376,23110377)

     