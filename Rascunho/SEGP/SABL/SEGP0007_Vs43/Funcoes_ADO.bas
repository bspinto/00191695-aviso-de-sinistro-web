Attribute VB_Name = "Funcoes_ADO"
Option Explicit

Global rdocn      As New Connection
Global rdoCnWEB   As New Connection
'
Private Const SYNCHRONIZE = &H100000
Private Const INFINITE = -1&
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheriHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMiliseconds As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" (ByVal IpExistingFileName As String, ByVal IpNewFileName As String, ByVal bFailifExists As Boolean) As Long


Sub ConexaoEvento()

Dim SIS_Server_Evento As String
Dim Sis_Banco_Evento As String
Dim SIS_Usuario_Evento As String
Dim SIS_Senha_Evento As String
Dim objAmbiente As Object

Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)

'Call inicializa_variaveis_ambiente(ObjAmbiente)

SIS_Server_Evento = objAmbiente.RetornaValorAmbiente("SEGBR", "EVENTO", "Servidor", gsCHAVESISTEMA, glAmbiente_id)
Sis_Banco_Evento = objAmbiente.RetornaValorAmbiente("SEGBR", "EVENTO", "Banco", gsCHAVESISTEMA, glAmbiente_id)
SIS_Usuario_Evento = objAmbiente.RetornaValorAmbiente("SEGBR", "EVENTO", "Usuario", gsCHAVESISTEMA, glAmbiente_id)
SIS_Senha_Evento = objAmbiente.RetornaValorAmbiente("SEGBR", "EVENTO", "Senha", gsCHAVESISTEMA, glAmbiente_id)

If Trim(rdocn.ConnectionString) <> "" Then
   rdocn.Close
   Set rdocn = New Connection
End If

With rdocn
  .ConnectionString = "UID=" & SIS_Usuario_Evento & ";PWD=" & UCase(SIS_Senha_Evento) & ";server=" & SIS_Server_Evento & ";driver={SQL Server};database=" & Sis_Banco_Evento & ";"
  .ConnectionTimeout = 300000
  .CommandTimeout = 300000
  .Open
End With

Set objAmbiente = Nothing

End Sub

Function Retorna_Dt_Inicio_Vigencia_Sub_ramo(ByVal ramo_id As String, ByVal gSubramo_id As Integer, ByVal vDt_Auxiliar As String) As String

    Dim OSQL As String
    Dim rc As Recordset
    
    If vDt_Auxiliar < "19990101" Then
       vDt_Auxiliar = "19990101"
    End If
    

    OSQL = "SELECT dt_inicio_vigencia_sbr FROM subramo_tb "
    OSQL = OSQL & "WHERE ramo_id=" & ramo_id & " AND "
    OSQL = OSQL & "subramo_id=" & gSubramo_id & " AND "
    OSQL = OSQL & "dt_inicio_vigencia_sbr <= '" & vDt_Auxiliar & "'"
    
    Set rc = rdocn.Execute(OSQL)
    
    If Not rc.EOF Then
        Retorna_Dt_Inicio_Vigencia_Sub_ramo = Format$(rc(0), "yyyymmdd")
    Else
        MsgBox "Proposta muito antiga. N�o foi poss�vel obter a " & vbNewLine _
             & "data de in�cio de vig�ncia do sub-ramo." & vbNewLine _
             & "O programa ser� encerrado."
        Call TrataErroGeral("Retorna_Dt_Inicio_Vigencia_Sub_ramo", "Funcoes_ADO")
        Call TerminaSEGBR
    End If
    
    rc.Close

End Function
Function VerificaExistencia(Tabela As String, CpChave As String, ValCpChave As Variant, TpCpChave As String, CpDesc As String, ValCpDesc As Variant, TpCpDesc As String) As Integer
    Dim SQLINI, OSQL, SqlTpCpDesc, SqlTpCpChave, sqlWhere, sqlAnd
    '
    ' . Somente para tabelas com chave prim�ria simples
    ' . Esta fun��o recebe uma tabela,
    ' o arquivo do campo chave, seu valor e tipo,
    ' e(ou) um campo descri��o, seu valor e tipo e retorna :
    ' "0" (zero)
    '1. j� exista a chave, n�o deve ser informada a descri��o
    '2. j� exista a descri��o, n�o deve ser informada a chave
    '3. j� exista a descri��o para uma chave diferente da informada
    ' "1" (um)
    '1. n�o existe
    ' "2" (dois)
    '1. processo abortado
    '
    ' TpCp... = "N"umber ou "C"har
    '
    Dim rc As Recordset
    
    On Error GoTo TrataErro
    '
    ' Testa valores obrigat�rios
    '
    If Tabela = "" Then
        MsgBox "O arquivo da Tabela � dado obrigat�rio !"
        VerificaExistencia = 2
        Exit Function
    End If
    If CpChave <> "" Then
        If ValCpChave = "" Or TpCpChave = "" Then
            MsgBox "O Valor e o Tipo da chave s�o dados obrigat�rios !"
            VerificaExistencia = 2
            Exit Function
        End If
    End If
    If CpDesc <> "" Then
        If ValCpDesc = "" Or TpCpDesc = "" Then
            MsgBox "O Valor e o Tipo da descri��o s�o dados obrigat�rios !"
            VerificaExistencia = 2
            Exit Function
        End If
    End If
    If CpChave = "" And CpDesc = "" Then
        MsgBox "A chave ou a descri��o s�o dados obrigat�rios !"
        VerificaExistencia = 2
        Exit Function
    End If
    
    SQLINI = "Select * "
    SQLINI = SQLINI & "from " & Tabela
    If UCase(TpCpChave) = "C" Then
        SqlTpCpChave = "'" & ValCpChave & "' "
    ElseIf UCase(TpCpChave) = "N" Then
        SqlTpCpChave = " " & ValCpChave & " "
    End If
    If UCase(TpCpDesc) = "C" Then
        SqlTpCpDesc = "'" & ValCpDesc & "' "
    ElseIf UCase(TpCpChave) = "N" Then
        SqlTpCpDesc = "" & ValCpDesc & " "
    End If
    If CpChave <> "" And CpDesc <> "" Then
        sqlWhere = " where " & CpDesc & " = "
        sqlWhere = sqlWhere & SqlTpCpDesc
        sqlAnd = "and " & CpChave & " <> "
        sqlWhere = sqlWhere & sqlAnd & SqlTpCpChave
    ElseIf CpChave <> "" Then
        sqlWhere = " where " & CpChave & " = "
        sqlWhere = sqlWhere & SqlTpCpChave
    Else
        sqlWhere = " where " & CpDesc & " = "
        sqlWhere = sqlWhere & SqlTpCpDesc
    End If
    OSQL = SQLINI & sqlWhere
    Set rc = rdocn.Execute(OSQL)
    If Not rc.EOF Then
        VerificaExistencia = 0
        MsgBox "Item j� cadastrado !"
    Else
        VerificaExistencia = 1
    End If
    rc.Close
    
Exit Function

TrataErro:
    Call TrataErroGeral("VerificaExistencia", "Funcoes_ADO")
    Screen.MousePointer = vbDefault
End Function
Function ProximoDiaUtil(ByVal DataBase, ByVal NumDias) As Variant
    Dim OSQL
    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe uma data e o n� de dias a serem somados a esta
    ' data como par�metros
    ' Retorna :
    ' Data calculada e formatada
    ' "-1" processo abortado
    '
    Dim RsFeriados As Recordset
    '
    ' Testa Dados B�sicos
    '
    If Not IsNumeric(NumDias) Then
        MsgBox NumDias & " n�o � um valor num�rico v�lido !", 16  ' vbAbortRetryIgnore
        ProximoDiaUtil = -1
        Exit Function
    End If
    '
    ' Soma n� de dias � data informada
    '
    DataBase = Format(CVDate(DataBase) + NumDias, "dd/mm/yyyy")
    '
    ' Verifica se data encontrada � s�bado ou domingo
    '
    If Weekday(DataBase) = 1 Then     ' Domingo
        DataBase = CVDate(DataBase) + 1
    ElseIf Weekday(DataBase) = 7 Then ' S�bado
        DataBase = CVDate(DataBase) + 2
    End If
    DataBase = Format(DataBase, "dd/mm/yyyy")
    '
    ' Verifica se data encontrada � feriado
    '
    OSQL = "Select nome "
    OSQL = OSQL & "from FERIADO_TB "
    OSQL = OSQL & "where DATA = " & FormataData(DataBase, "S", "S") & " "
    Set RsFeriados = rdocn.Execute(OSQL)
    If Not RsFeriados.EOF Then
        Do While Not RsFeriados.EOF Or (Weekday(CVDate(DataBase)) = 7 Or Weekday(CVDate(DataBase)) = 1)
            DataBase = Format(CVDate(DataBase) + 1, "dd/mm/yyyy")
            OSQL = "Select nome "
            OSQL = OSQL & "from FERIADO_TB "
            OSQL = OSQL & "where DATA = " & FormataData(DataBase, "S", "S") & " "
            Set RsFeriados = rdocn.Execute(OSQL)
        Loop
    End If
    RsFeriados.Close
    
    ProximoDiaUtil = DataBase
    
End Function
Function VerificaFeriado(ByVal DataBase As Variant) As Boolean
    Dim OSQL
    '
    ' Respons�vel : Carlos Rosa
    ' Dt Desenv : 09/04/98
    '
    ' Recebe uma data como par�metro
    ' Retorna :
    ' "true" para feriado
    ' "false" para n�o feriado
    '
    Dim RsFeriados As Recordset
    '
    ' Verifica se data � feriado
    '
    OSQL = "Select nome "
    OSQL = OSQL & "from FERIADO_TB "
    OSQL = OSQL & "where DATA = " & FormataData(DataBase, "S", "S") & ""
    Set RsFeriados = rdocn.Execute(OSQL)
    If Not RsFeriados.EOF Then
        MsgBox "Esta data j� est� cadastrada como feriado de " & RsFeriados(0) & " !"
        VerificaFeriado = True
    Else
        VerificaFeriado = False
    End If
    RsFeriados.Close
    
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
'
' retorna a data e a hora do banco de dados MSSQL
Public Function DataHoraBanco(Optional ByVal objAmbiente As Object, _
                              Optional ByVal AForma As String = "dd/mm/yyyy hh:nn:ss") As Date
  On Local Error GoTo TrataErro
  Dim RX As Object, Criou As Boolean
  
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  Set RX = objAmbiente.ExecutaAmbiente("SELECT CONVERT(DATETIME, GETDATE())")
  DataHoraBanco = Format(RX(0), AForma)
  RX.Close
  Set RX = Nothing
  
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Function
TrataErro:
  Call TrataErroGeral("DataHoraBanco", "Funcoes_ADO.bas")
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Call TerminaSEGBR
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' libera a conex�o, pois ela foi criada
Public Sub LiberaConexao(ByVal FechaConexaoCriada As Boolean, ConexaoCriada As Connection)
  On Local Error GoTo TrataErro
  If FechaConexaoCriada Then
    ConexaoCriada.Close
    Set ConexaoCriada = Nothing
  End If
  Exit Sub
TrataErro:
  MsgBox "Erro na rotina LiberaConexao!" & vbCrLf & Err.Description
End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' retorna a cobran�a original de uma parcela que foi cancelada e depois estornada
Public Function RetornaCobrancaAtualParcelaOriginal(ByVal AConexao As Connection, _
  ByVal AProposta As Long, ByVal OEndosso As Long, ByVal AParcelaOriginal As Long) As Long
  On Local Error GoTo TrataErro
  Dim RX As Recordset
  SQS = "SELECT ace.num_cobranca CobranEst, ace.num_parcela_endosso ParcEst"
  adSQS "FROM agendamento_cobranca_tb ac"
  adSQS "LEFT JOIN cancelamento_proposta_tb cp"
  adSQS "  ON cp.proposta_id = ac.proposta_id"
  adSQS " AND not cp.endosso_id is null"
  adSQS "LEFT JOIN cancelamento_endosso_tb cea"
  adSQS "  ON cea.proposta_id     = ac.proposta_id"
  adSQS " AND cea.endosso_id      = ac.canc_endosso_id"
  adSQS " AND cea.canc_endosso_id = ac.num_endosso"
  adSQS "LEFT JOIN cancelamento_endosso_tb cep"
  adSQS "  ON cep.proposta_id = ac.proposta_id"
  adSQS " AND cep.canc_endosso_id = ISNULL( cea.endosso_id, cp.endosso_id)"
  adSQS "LEFT JOIN agendamento_cobranca_tb ace"
  adSQS "  ON ace.proposta_id = ac.proposta_id"
  adSQS " AND ace.num_endosso = cep.endosso_id"
  adSQS "WHERE NOT ac.canc_endosso_id IS NULL"
  adSQS "  AND ac.situacao            <> 'a'"
  adSQS "  AND ac.proposta_id         = " & AProposta
  adSQS "  AND ac.num_endosso         = " & OEndosso
  adSQS "  AND ac.num_parcela_endosso = " & AParcelaOriginal
  adSQS "  AND ac.num_parcela_endosso = ace.num_parcela_endosso +"
  adSQS "                              (SELECT ISNULL( MAX( x.num_parcela_endosso), 0)"
  adSQS "                               FROM agendamento_cobranca_tb x"
  adSQS "                               WHERE x.canc_endosso_id IS NULL"
  adSQS "                                 AND x.num_endosso = ac.num_endosso"
  adSQS "                                 AND x.proposta_id = ac.proposta_id)"
  Set RX = AConexao.Execute(SQS)
  If RX.EOF Then
    RetornaCobrancaAtualParcelaOriginal = 0
    RX.Close
  Else
    Dim CobrancaAtual As Long, ParcelaAtual As Long
    CobrancaAtual = Val(RX("CobranEst"))
    ParcelaAtual = Val(RX("ParcEst"))
    RX.Close
    RetornaCobrancaAtualParcelaOriginal = Maior(CobrancaAtual, _
      RetornaCobrancaAtualParcelaOriginal(AConexao, AProposta, OEndosso, ParcelaAtual))
  End If
  
  Exit Function
TrataErro:
  MsgBox "erro na rotina RetornaCobrancaAtualParcelaOriginal"
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' auxilia v�rias fun��es para definir qual deve ser a conex�o
' cria uma conex�o usando a padr�o (rdocn) se a conex�o passada por par�metro n�o existir
' �til quando se est� em uma transa��o, onde se deve passar a conex�o da transa��o
Public Sub AssociaConexao(FechaConexaoCriada As Boolean, CriaNovaConexao As Connection, _
                          Optional ByVal AConexaoPassada)
  On Local Error GoTo TrataErro
  If IsMissing(AConexaoPassada) Then
    If Trim(rdocn.ConnectionString) = "" Then 'Alterado por fpimenta / Imar�s - 07/08/2003 - (DE: Connect PARA: ConnectionString)
      MsgBox "A conex�o RDOCN n�o foi estabelecida." & vbCrLf & "Programa terminar�."
      End
    End If
    FechaConexaoCriada = True
    With CriaNovaConexao
      .ConnectionString = rdocn.ConnectionString
      .ConnectionTimeout = 3600
      .Open
    End With
    CriaNovaConexao.Errors.Clear
  Else
    FechaConexaoCriada = False
    Set CriaNovaConexao = AConexaoPassada
  End If
  Exit Sub
  Resume
TrataErro:
  MsgBox "Erro na rotina AssociaConexao!" & vbCrLf & Err.Description
End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' esta rotina n�o deve ser mais usada
Public Sub RegistraArquivoSEGBR(OPrograma As String)
  On Local Error GoTo TrataErro
  Dim AChamada As String
  If Left(OPrograma, 2) = "\\" Then
    If InStr(".OCX .DLL", UCase(Right(OPrograma, 4))) > 0 Then
      AChamada = PegaRegistro(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Windows\CurrentVersion\Setup\SysDir")
      Inc AChamada, "\RegSvr32.exe /s " & OPrograma
      Call Shell(AChamada)
    End If
  Else
    Dim AInstancia As Double
    ' exibe um AVI
    AInstancia = Shell(gsPastaServidorSegbr & "CopiaArquivoAVI.exe " & OPrograma, vbNormalFocus)
    ' copia arquivo
    FileCopy gsPastaServidorSegbr & OPrograma, gsPastaLocalSegbr & OPrograma
'    Pause 1
    ' retira a exibi��o do AVI
    Call Fecha_Aplicativos(AInstancia)
    ' registra se for biblioteca
    If InStr(".OCX .DLL", UCase(Right(OPrograma, 4))) > 0 Then
      AChamada = PegaRegistro(HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Windows\CurrentVersion\Setup\SysDir")
      Inc AChamada, "\RegSvr32.exe /s " & gsPastaLocalSegbr & OPrograma
      Call Shell(AChamada)
    End If
  End If
  Exit Sub
TrataErro:
  Call TrataErroGeral("RegistraArquivoSEGBR", "Funcoes_ADO.bas")
  Call TerminaSEGBR
End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' retorna o valor de um par�metro
Public Function BuscaParametro(ByVal OParametro As String, Optional ByVal AConexao) As Variant
  Dim NovaConexao As New Connection
  Dim RX As Recordset, FechaConexao As Boolean
  On Local Error GoTo TrataErro
  AssociaConexao FechaConexao, NovaConexao, AConexao
  ' busca valor associado ao par�metro
  SQS = "SELECT val_parametro FROM ps_parametro_tb"
  adSQS "WHERE parametro = '" & Trim(OParametro) & "' "
  Set RX = NovaConexao.Execute(SQS)
  If RX.EOF Then
    BuscaParametro = Null
  Else
    BuscaParametro = RX(0)
  End If
  RX.Close
  Set RX = Nothing
  LiberaConexao FechaConexao, NovaConexao
  Exit Function
TrataErro:
  MsgBox "Erro na rotina BuscaParametro!" & vbCrLf & Err.Description
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' retorna o valor da reserva de um documento passando o dia base ou
' passando os valores para o c�lculo
Public Function CalculaReserva(ByVal ODiaBase As Date, _
                               Optional ByVal AProposta As Long, _
                               Optional ByVal OEndosso As Long, _
                               Optional ByVal TipoReserva_OU_Conexao, _
                               Optional ByVal OPremioLiquido, _
                               Optional ByVal OInicioVigencia, _
                               Optional ByVal QtdDiasVigencia) As Variant
  Dim NovaConexao As New Connection
  Dim RX As Recordset, FechaConexao As Boolean
  On Local Error GoTo TrataErro
  ' se passar um tipo de reserva, calcula numericamente a reserva
  If VarType(TipoReserva_OU_Conexao) = vbString Then
    If OPremioLiquido = 0 Or IsNull(OPremioLiquido) Then
      CalculaReserva = 0
    Else
      Dim OPercentual
      If TipoReserva_OU_Conexao = "A" Then
        ' a decorrer
        OPercentual = 1 - DateDiff("d", OInicioVigencia, ODiaBase) / QtdDiasVigencia
        OPercentual = IIf(OPercentual > 0, OPercentual, 0)
      Else
        ' decorrida
        OPercentual = 0.5
      End If
      CalculaReserva = TruncaDecimal(OPercentual * OPremioLiquido)
    End If
    Exit Function
  End If
  
  AssociaConexao FechaConexao, NovaConexao, TipoReserva_OU_Conexao
  
  Dim AMoedaAtual, ODiaBaseF
  AMoedaAtual = BuscaParametro("MOEDA ATUAL", NovaConexao)
  ODiaBaseF = "'" & Format(ODiaBase, "yyyymmdd") & "'" 'FormatoSQL(ODiaBase)
  ' busca reserva
  If OEndosso = 0 Or IsNull(OEndosso) Then
    SQS = "SELECT rp.cod_tp_reserva, rp.dt_inicio_vigencia, rp.dias_vigencia,"
    adSQS "       rp.val_premio - rp.val_premio_co_seguro - rp.val_premio_re_seguro Liquido,"
    adSQS "       CASE WHEN rp.moeda_id = " & AMoedaAtual
    adSQS "            THEN 1 ELSE pr.val_paridade_moeda END val_paridade_moeda"
    adSQS "FROM ps_reserva_proposta_tb rp"
    adSQS "LEFT JOIN paridade_tb pr"
    adSQS "  ON pr.dt_conversao = " & ODiaBaseF
    adSQS " AND pr.destino_moeda_id = " & AMoedaAtual
    adSQS " AND pr.origem_moeda_id  = rp.moeda_id"
    If AProposta = 0 Or IsNull(AProposta) Then
      ' verifica a reserva para o dia base na tabela reserva_proposta_tb
      adSQS "WHERE rp.dt_inicio_vigencia <= " & ODiaBaseF
    Else
      ' verifica a reserva para a proposta no dia base
      adSQS "WHERE rp.proposta_id = " & AProposta
      adSQS "  AND rp.dt_inicio_vigencia <= " & ODiaBaseF
    End If
    adSQS "  AND rp.dt_emissao <= " & ODiaBaseF
    adSQS "  AND ("
    adSQS "       ((rp.cod_tp_reserva = 'D') AND"
    adSQS "        (CONVERT( VARCHAR( 6), rp.dt_emissao, 112) = CONVERT( VARCHAR( 6), " & ODiaBaseF & ", 112)))"
    adSQS "    OR ((rp.cod_tp_reserva = 'A') AND"
    adSQS "        (DATEADD( DD, rp.dias_vigencia, rp.dt_inicio_vigencia) >= " & ODiaBaseF & "))"
    adSQS "      )"
    adSQS "  AND (rp.dt_cancelamento > " & ODiaBaseF
    adSQS "    OR rp.dt_cancelamento IS NULL)"
  Else
    SQS = "SELECT re.cod_tp_reserva, re.dt_inicio_vigencia, re.dias_vigencia,"
    adSQS "       re.val_premio - re.val_premio_co_seguro - re.val_premio_re_seguro Liquido,"
    adSQS "       CASE WHEN re.moeda_id = " & AMoedaAtual
    adSQS "            THEN 1 ELSE pr.val_paridade_moeda END val_paridade_moeda"
    adSQS "FROM ps_reserva_endosso_tb re"
    adSQS "LEFT JOIN paridade_tb pr"
    adSQS "  ON pr.dt_conversao = " & ODiaBaseF
    adSQS " AND pr.destino_moeda_id = " & AMoedaAtual
    adSQS " AND pr.origem_moeda_id  = re.moeda_id"
    If AProposta = 0 Or IsNull(AProposta) Then
      ' verifica a reserva para o dia base na tabela reserva_endosso_tb
      adSQS "WHERE re.dt_inicio_vigencia <= " & ODiaBaseF
    Else
      ' verifica a reserva para o endosso no dia base
      adSQS "WHERE re.proposta_id = " & AProposta
      adSQS "  AND re.endosso_id  = " & OEndosso
      adSQS "  AND re.dt_inicio_vigencia <= " & ODiaBaseF
    End If
    adSQS "  AND re.dt_emissao <= " & ODiaBaseF
    adSQS "  AND ("
    adSQS "       ((re.cod_tp_reserva = 'D') AND"
    adSQS "        (CONVERT( VARCHAR( 6), re.dt_emissao, 112) = CONVERT( VARCHAR( 6), " & ODiaBaseF & ", 112)))"
    adSQS "    OR ((re.cod_tp_reserva = 'A') AND"
    adSQS "        (DATEADD( DD, re.dias_vigencia, re.dt_inicio_vigencia) >= " & ODiaBaseF & "))"
    adSQS "      )"
    adSQS "  AND (re.dt_cancelamento > " & ODiaBaseF
    adSQS "    OR re.dt_cancelamento IS NULL)"
  End If
  Set RX = NovaConexao.Execute(SQS)
  If RX.EOF Then
    CalculaReserva = Null
  Else
    Dim TR, INI, QTD, PL, PAR, TotalReserva
    TotalReserva = 0
    While Not RX.EOF
      TR = RX("cod_tp_reserva")
      INI = RX("dt_inicio_vigencia")
      QTD = Val(RX("dias_vigencia"))
      PL = Val(RX("Liquido"))
      PAR = Val(RX("val_paridade_moeda"))
      If PAR <> 1 Then PL = PAR * PL
      Inc TotalReserva, CalculaReserva(ODiaBase, , , TR, PL, INI, QTD)
      RX.MoveNext
    Wend
    CalculaReserva = TotalReserva
  End If
  RX.Close
  Set RX = Nothing
  LiberaConexao FechaConexao, NovaConexao
  Exit Function
TrataErro:
  MsgBox "Erro na rotina CalculaReserva!" & vbCrLf & Err.Description
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' retorna a execu��o de um comando SQL em uma nova conex�o
Public Function ExecutaSQLNovaConexao(ByVal OSQL As String, AConexaoBase As Connection) _
                    As Recordset
  On Local Error GoTo TrataErro
  Dim AConexao As Connection
  Set AConexao = New Connection
  With AConexao
    .ConnectionString = AConexaoBase.ConnectionString
    .ConnectionTimeout = AConexaoBase.ConnectionTimeout
    .Open
  End With
  AConexao.Errors.Clear
  Set ExecutaSQLNovaConexao = AConexao.Execute(OSQL)
  AConexao.Close
  Set AConexao = Nothing
  Exit Function
TrataErro:
  Call TrataErroGeral("ExecutaSQLNovaConexao", "Funcoes_ADO.bas")
  Call TerminaSEGBR
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' retorna se o documento cancelou outro
Public Function DocumentoCancelou(ByVal AProposta, ByVal OEndosso, Optional ByVal AConexao) As Boolean
  Dim NovaConexao As New Connection
  Dim RX As Recordset, FechaConexao As Boolean
  On Local Error GoTo TrataErro
  AssociaConexao FechaConexao, NovaConexao, AConexao
  ' verifica se � cancelamento
  SQS = "SELECT * FROM cancelamento_endosso_tb"
  adSQS "WHERE proposta_id = " & AProposta
  adSQS "  AND endosso_id  = " & OEndosso
  Set RX = NovaConexao.Execute(SQS)
  If RX.EOF Then
    RX.Close
    SQS = "SELECT * FROM cancelamento_proposta_tb"
    adSQS "WHERE proposta_id = " & AProposta
    adSQS "  AND endosso_id  = " & OEndosso
    Set RX = NovaConexao.Execute(SQS)
    DocumentoCancelou = Not RX.EOF
  Else
    DocumentoCancelou = True
  End If
  RX.Close
  Set RX = Nothing
  LiberaConexao FechaConexao, NovaConexao
  Exit Function
TrataErro:
  MsgBox "Erro na rotina DocumentoCancelou!" & vbCrLf & Err.Description
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' retorna o CNPJ da seguradora
Public Function Buscar_CNPJ(ByVal SUSEP, Optional ByVal AConexao) As Variant
  Dim NovaConexao As New Connection
  Dim RX As Recordset, FechaConexao As Boolean
  On Local Error GoTo TrataErro
  AssociaConexao FechaConexao, NovaConexao, AConexao
  ' busca CNPJ de uma seguradora
  SQS = "SELECT cgc FROM seguradora_tb"
  adSQS "WHERE seguradora_cod_susep = " & SUSEP
  Set RX = NovaConexao.Execute(SQS)
  If RX.EOF Then
    Buscar_CNPJ = Null
  Else
    Buscar_CNPJ = RX(0)
  End If
  RX.Close
  Set RX = Nothing
  LiberaConexao FechaConexao, NovaConexao
  Exit Function
TrataErro:
  MsgBox "Erro na rotina Buscar_CNPJ!" & vbCrLf & Err.Description
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' calcula IS
Public Function Calcula_IS(ByVal AProposta, ByVal OEndosso, Optional ByVal Resseguro As Boolean, Optional ByVal AConexao) As Double
  Dim NovaConexao As New Connection
  Dim RX As Recordset, FechaConexao As Boolean
  On Local Error GoTo TrataErro
  AssociaConexao FechaConexao, NovaConexao, AConexao
  AProposta = Val(AProposta)
  OEndosso = Val(OEndosso)
  ' pega o valor do documento
  SQS = "EXEC apoio_soma_is_sps"
  adSQS FormatoSQL(AProposta, OEndosso)
  adSQS IIf(Resseguro, ", 1", "")
  Set RX = NovaConexao.Execute(SQS)
  Calcula_IS = Val(RX(0))
  RX.Close
  Set RX = Nothing
  LiberaConexao FechaConexao, NovaConexao
  Exit Function
TrataErro:
  MsgBox "Erro na rotina Calcula_IS!" & vbCrLf & Err.Description
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' acerta IS de um documento
Public Function Acerta_Val_IS_Endosso(ByVal AProposta, ByVal OEndosso, Optional ByVal AConexao) As Boolean
  Dim val_is, FechaConexao As Boolean
  Dim NovaConexao As New Connection
  On Local Error GoTo TrataErro
  Acerta_Val_IS_Endosso = False
  AssociaConexao FechaConexao, NovaConexao, AConexao
  AProposta = Val(AProposta)
  OEndosso = Val(OEndosso)
  val_is = Calcula_IS(AProposta, OEndosso, False, NovaConexao)
  ' pega o valor do documento
  SQS = "EXEC  endosso_financeiro_is_spu"
  adSQS FormatoSQL(AProposta, OEndosso, val_is)
  NovaConexao.Execute SQS
  LiberaConexao FechaConexao, NovaConexao
  Acerta_Val_IS_Endosso = True
  Exit Function
TrataErro:
  'MsgBox "Erro na rotina Acerta_Val_IS_Endosso!" & vbCrLf & err.Description
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' verifica se a ap�lice est� encerrada
Public Function VerificaApoliceEncerrada(AProposta As Long, Optional DataReferencia As Date, Optional ByVal AConexao) As Boolean
  Dim NovaConexao As New Connection
  Dim RX As Recordset, FechaConexao As Boolean
  On Local Error GoTo TrataErro
  AssociaConexao FechaConexao, NovaConexao, AConexao
  ' verifica cancelamento
  SQS = "SELECT endosso_id endosso "
  adSQS "FROM cancelamento_proposta_tb "
  adSQS "WHERE proposta_id = " & AProposta
  If DataReferencia = "00:00:00" Then
    adSQS "  AND dt_fim_cancelamento IS NULL"
  Else
    adSQS "  AND dt_inicio_cancelamento <= " & DataReferencia
    adSQS "  AND ISNULL( dt_fim_cancelamento, " & DataReferencia & ") >= " & DataReferencia
  End If
  Set RX = NovaConexao.Execute(SQS)
  If RX.EOF Then
    VerificaApoliceEncerrada = False
  Else
    VerificaApoliceEncerrada = IsNull(RX("endosso"))
  End If
  RX.Close
  Set RX = Nothing
  LiberaConexao FechaConexao, NovaConexao
  Exit Function
TrataErro:
  MsgBox "Erro na rotina VerificaApoliceEncerrada!" & vbCrLf & Err.Description
End Function
Sub BuscaStatusSistema(Optional objAmbiente As Object)
    
  On Local Error GoTo TrataErro
  Dim Criou As Boolean
  
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  Dim RX As Recordset
  
  SQS = "SELECT dt_operacional,"
  adSQS "  status_sistema,"
  adSQS "  dt_contabilizacao"
  adSQS "  from seguros_db..parametro_geral_tb"
  
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  
  Data_Sistema = Format(RX(0), "dd/mm/yyyy")

  If IsNull(RX(2)) Then
    Data_Contabilizacao = CDate("01/01/2000")
  Else
    Data_Contabilizacao = Format(RX(2), "dd/mm/yyyy")
  End If

  If RX(1) = "B" Then
    Status_Sistema = "Bloqueado"
  End If
  If RX(1) = "L" Then
    Status_Sistema = "Dispon�vel"
  End If
  If RX(1) = "C" Then
    Status_Sistema = "Consulta"
  End If
    
  RX.Close
  Set RX = Nothing
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Sub
TrataErro:
  TrataErroGeral "BuscaStatusSistema", "Funcoes_ADO.bas"
  Set RX = Nothing
  If Criou Then
    Set objAmbiente = Nothing
  End If
End Sub
Sub Conexao(Optional TestaBDLiberado As Boolean = False, Optional FechaConexao As Boolean = False, Optional objAmbiente As Object)
  
  On Local Error GoTo TrataErro

  Dim Criou As Boolean
  
  ' extrai os par�metros passados ao programa
  Call Trata_Parametros(Command, objAmbiente)

  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If

  Call inicializa_variaveis_ambiente(objAmbiente)

  With rdocn
    .ConnectionString = "UID=" & SIS_usuario & ";PWD=" & UCase(SIS_senha) & ";server=" & SIS_server & ";driver={SQL Server};database=" & SIS_banco & ";"
    .ConnectionTimeout = 300000
    .CommandTimeout = 300000
    .Open
  End With

  rdocn.Errors.Clear
      
  Call BuscaStatusSistema(objAmbiente)

  If TestaBDLiberado Then
    If Status_Sistema = "Bloqueado" Then
      Call MensagemBatch("Sistema Bloqueado. Tente mais tarde", vbCritical)
      Call TerminaSEGBR
    End If
  End If

  If FechaConexao Then
    rdocn.Close
    Set rdocn = Nothing
  End If
    
  If Criou Then
    Set objAmbiente = Nothing
  End If

Exit Sub
TrataErro:
  Call TrataErroGeral("Conexao", "Funcoes_ADO.bas")
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Call TerminaSEGBR
End Sub
Sub identifica(username As String)
Dim rs As Recordset, sqlstmt, tela

On Local Error GoTo TrataErro

With rdoCnWEB
  .ConnectionString = "UID=" & INT_usuario & ";PWD=" & INT_senha & ";server=" & INT_server & ";driver={SQL Server};database=" & INT_banco & ";"
  .ConnectionTimeout = 15
  .Open
End With

rdoCnWEB.Errors.Clear

sqlstmt = "SELECT funcoes_tb.endereco "
sqlstmt = sqlstmt + "FROM   permissoes_tb,funcoes_tb  "
sqlstmt = sqlstmt + "WHERE permissoes_tb.Funcoes_Id = funcoes_tb.Id and   permissoes_tb.Status = 's' and permissoes_tb.status = 's' and (permissoes_tb.nome  = '" & username & "' or permissoes_tb.nome in "
sqlstmt = sqlstmt + "(SELECT grupo from membros_tb where username = '" & username & "'))"

Set rs = rdoCnWEB.Execute(sqlstmt)
'montando a string de permiss�es

If Not rs.EOF Then
 
 Do While Not rs.EOF

    If InStr(cpermissao, "estrut/SISSEG") > 0 Then
        tela = UCase(Mid(rs!Endereco, 15)) ' /estrut/sisseg/ = possui 15 caracteres
    Else 'pegar somente o endereco completo do relatorio que ser� apresentado na WEB.
        tela = UCase(rs!Endereco)
    End If
  cpermissao = cpermissao & tela & ","
  rs.MoveNext
 Loop
End If

rs.Close
rdoCnWEB.Close

Exit Sub

TrataErro:
  Call TrataErroGeral("identifica", "Funcoes_ADO")
  Call MensagemBatch("Conex�o com a " & INT_server & " indispon�vel.", vbCritical)
  Call TerminaSEGBR
End Sub
Function Seguranca_Old(ByVal prog As String, funcao As String) As Boolean
    
   Seguranca_Old = True
   lpUserName = String(140, " ")
   'nameu = GetUserName(lpUserName, Len(lpUserName))
   nameu = WNetGetUser("BRCAPFS", lpUserName, Len(lpUserName))
   cUserName = Trim(lpUserName)
   cUserName = Left(cUserName, Len(cUserName) - 1)
    
   identifica (cUserName)
    
    If InStr(cpermissao, prog) = 0 Then
        mensagem_erro 6, "Usuario " & cUserName & " n�o tem acesso a fun��o " & funcao
        Seguranca_Old = False
        End
    End If
End Function
Public Sub TrataErroGeral(NmRotina As String, Optional NmArquivo As String)
  Dim DescricaoErro As String, NumErro As Long
  Dim InstanciouErro As Boolean, Continua As Boolean
  
  NumErro = Err.Number
  DescricaoErro = Err.Description
  InstanciouErro = True
  Continua = False
  
  On Local Error GoTo TrataErro
  
  Dim Erro As Object
  
  Set Erro = CreateObject("Erro.log")
  Continua = True
  
  If InstanciouErro Then
    Erro.Usuario = IIf(Trim(cUserName) = "", gsIp & "~" & gsHost, cUserName)
    Erro.NmProjeto = gsSIGLASISTEMA
    Erro.NmAplicacao = App.EXEName
    Erro.NmForm = NmArquivo
    'Erro.NmArquivo = NmArquivo
    Erro.NmRotina = NmRotina
  End If
  
  Dim agenda_diaria_id As Long
  ' caso para a produ��o
  If goProducao Is Nothing Then
    If Obtem_agenda_diaria_id(Command) > 0 Then
      agenda_diaria_id = Obtem_agenda_diaria_id(Command)
    Else
      agenda_diaria_id = 0
    End If
  Else
    agenda_diaria_id = Obtem_agenda_diaria_id(Command)
  End If
 
  Dim posic
  If rdocn.Errors.Count > 0 Then
    If rdocn.Errors.Count = 1 Then
      Select Case rdocn.Errors(0).Number
      Case 66666, 55555
        posic = ultimaString(rdocn.Errors(0).Description, "]")
        gsMensagem = Mid$(rdocn.Errors(0).Description, posic + 1)
        Call MensagemBatch(gsMensagem, vbExclamation)
        rdocn.RollbackTrans
      Case Else
        gsMensagem = Erro.loga(rdocn.Errors, NumErro, DescricaoErro, agenda_diaria_id)
        Call MensagemBatch(gsMensagem, vbExclamation, , False)
      End Select
    Else
      rdocn.RollbackTrans
      If rdocn.Errors(1).Number = 2627 Or rdocn.Errors(1).Number = 2601 Then  'Registro duplicado na tabela principal
        'gsMensagem = "Item " & NmRotina & " n�o selecionado!"
        gsMensagem = "Erro de Chave Duplicada - " & NmRotina
        Call MensagemBatch(gsMensagem, vbExclamation)
      ElseIf rdocn.Errors(1).Number = 10054 Then
        gsMensagem = "Perda de Conex�o com o Banco de Dados - " & NmRotina
        Call MensagemBatch(gsMensagem, vbExclamation)
      Else
        gsMensagem = Erro.loga(rdocn.Errors, NumErro, DescricaoErro, agenda_diaria_id)
        Call MensagemBatch(gsMensagem, vbExclamation, , False)
      End If
    End If
  Else
    gsMensagem = Erro.loga(rdocn.Errors, NumErro, DescricaoErro, agenda_diaria_id)
    Call MensagemBatch(gsMensagem, vbExclamation, , False)
  End If
  
  rdocn.Errors.Clear
  If Not InstanciouErro Then
    InstanciouErro = True
    Err.Raise 429, , "A classe para tratamento de ocorr�ncias n�o p�de ser instanciada!"
  End If
  Exit Sub
  
TrataErro:
  If Not Continua Then
    InstanciouErro = False
  End If
  If Not InstanciouErro Then
    Resume Next
  End If
  
ErroAnterior:
  gsMensagem = "A fun��o TrataErroGeral fez a seguinte an�lise:" & vbNewLine & vbNewLine & _
               "Descri��o ==> " & Err.Description & vbNewLine & vbNewLine & _
               "Arquivo ==> " & NmArquivo & vbNewLine & vbNewLine & _
               "Rotina ==> " & NmRotina & vbNewLine & vbNewLine & _
               "An�lise da rotina ==> (" & NumErro & ") " & DescricaoErro
  Call MensagemBatch(gsMensagem, vbCritical)
End Sub

Public Sub ErroIntReferencial(ByRef Tabela As String, ByRef Coluna As String)
    Dim pos, pos1
    If rdocn.Errors.Count > 1 Then
        If Mid(rdocn.Errors(1).SQLState, 1, 2) <> "01" Then
            If rdocn.Errors(1).Number = 547 Then  'Registro conflitando com foreigns keys
                If InStr(rdocn.Errors(1).Description, "COLUMN REFERENCE") <> 0 Then
                   pos = InStr(rdocn.Errors(1).Description, "table")
                   If pos <> 0 Then
                      pos = pos + 7
                      pos1 = InStr(pos, rdocn.Errors(1).Description, "'")
                      Tabela = "'" & Mid(rdocn.Errors(1).Description, pos, pos1 - pos) & "'"
                   Else
                      Tabela = " 'N�o fornecida' "
                   End If
                   pos = InStr(rdocn.Errors(1).Description, "column")
                   If pos <> 0 Then
                      pos = pos + 8
                      pos1 = InStr(pos, rdocn.Errors(1).Description, "'")
                      Coluna = "'" & Mid(rdocn.Errors(1).Description, pos, pos1 - pos) & "'"
                   Else
                      Coluna = " 'N�o fornecida' "
                   End If
                Else
                   pos = InStr(100, rdocn.Errors(1).Description, "table")
                   If pos <> 0 Then
                      pos = pos + 7
                      pos1 = InStr(pos, rdocn.Errors(1).Description, "'")
                      Tabela = "'" & Mid(rdocn.Errors(1).Description, pos, pos1 - pos) & "'"
                   Else
                      Tabela = " 'N�o fornecida' "
                   End If
                   Coluna = " 'N�o Fornecida '"
                End If
                rdocn.Errors.Clear
            End If
        End If
    End If
End Sub
Function Seguranca(ByVal prog As String, funcao As String, _
                   Optional ByVal ValidaVariasConexoes As Boolean = False, _
                   Optional ByVal objAmbiente As Object) As Boolean
  
  Dim RX           As Object
  Dim oSABL0001    As Object

  Dim StrSQL       As String
  Dim Ss           As Recordset
  Dim Criou        As Boolean

  On Local Error GoTo TrataErro
  
  Seguranca = False
  
  'Retira os parametros do Command
  Call Trata_Parametros(Command)
  
  If Len(Command) > 1 Then
    If InStrRev(Command, " ") = 0 Then
      gsParamAplicacao = Command
    Else
      gsParamAplicacao = Left(Command, InStrRev(Command, " "))
    End If
  Else
      gsParamAplicacao = Command
  End If
  'pega ap�s o �ltimo espa�o em branco, onde est� a cadeia de criptografia
  gsParamAplicacao = Trim(gsParamAplicacao)
    
  'VALIDA A SEGURAN�A COM A DLL DE SEGURAN�A
  Set oSABL0001 = CreateObject("SABL0001.Seguranca")
  If oSABL0001.ValidarPermissaoSegbr2("SEGBR", gsCPF, glEmpresa_id, glAmbiente_id, prog) = False Then Error glERRO_USUARIO_SEM_PERMISSAO
 
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  If ValidaVariasConexoes Then
    SQS = "SELECT name FROM tempdb..sysobjects"
    adSQS "WHERE name LIKE '##$" & cUserName & "$%'"
    Set RX = objAmbiente.ExecutaAmbiente(SQS)
    If Not RX.EOF Then
      Dim Computador As String
      Computador = RX("name")
      RX.Close
      Set RX = Nothing
      SQS = "SELECT computador FROM " & Computador
      Set RX = objAmbiente.ExecutaAmbiente(SQS)
      Computador = RX("computador")
      RX.Close
      oSABL0001.id_funcao = "PERMITEVARIASCONEXOES"
      If oSABL0001.acesso <> "True" Then Error glERRO_USUARIO_COM_VARIAS_CONEXOES
    End If
    Set RX = Nothing
  End If
  
  If Not rdoCnWEB = "" Then rdoCnWEB.Close

NaoValidaSeguranca:
  Seguranca = True

  If Criou Then
    Set objAmbiente = Nothing
  End If
Exit Function

TrataErro:
  Select Case Err
    Case glERRO_VALIDA_PARAMETRO
      MontaErro gsERRO_VALIDA_PARAMETRO, App.EXEName
    Case glERRO_USUARIO_SEM_PERMISSAO
      MontaErro gsERRO_USUARIO_SEM_PERMISSAO, funcao
    Case glERRO_FUNCAO_INDISPONIVEL
      MontaErro gsERRO_FUNCAO_INDISPONIVEL, funcao
    Case glERRO_USUARIO_COM_VARIAS_CONEXOES
      MontaErro gsERRO_USUARIO_COM_VARIAS_CONEXOES, cUserName, Computador
  End Select
  Call TrataErroGeral("Seguranca", "Funcoes_ADO.bas")
  Set RX = Nothing
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Call TerminaSEGBR
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' verifica se um programa j� est� cadastrado na tabela, retornando a inst�ncia
Public Function ProgramaAtivado(ByVal OCodPrograma As String, ByVal OParametro As String, _
                                OManipulador As Double, _
                                Optional objAmbiente) As Boolean
  On Local Error GoTo TrataErro
  Dim RX As Object, AInstancia As Double
  Dim sTabTempUsr As String, Criou As Boolean
  
  If IsMissing(objAmbiente) Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  sTabTempUsr = TabTempUsr(, , objAmbiente)

  SQS = "IF OBJECT_ID('TempDB.." & sTabTempUsr & "') IS NOT NULL"
  adSQS "  SELECT instancia"
  adSQS "  FROM """ & sTabTempUsr & """"
  adSQS "  WHERE id_aplicacao = " & FormatoSQL(OCodPrograma)
  adSQS "    AND parametro_aplicacao = " & FormatoSQL(OParametro)
  adSQS "ELSE"
  adSQS "  SELECT 0"
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  If RX.EOF Then
    AInstancia = 0
  Else
    AInstancia = RX(0)
  End If
  RX.Close
  Set RX = Nothing
  If AInstancia = 0 Then
    ProgramaAtivado = False
  Else
    If AplicacaoAtiva(AInstancia) Then
      ProgramaAtivado = True
      OManipulador = AInstancia
    Else
      ProgramaAtivado = False
      ' remove a inst�ncia que n�o est� ativa
      SQS = "DELETE """ & sTabTempUsr & """"
      adSQS "WHERE instancia = " & AInstancia
      Call objAmbiente.ExecutaAmbiente(SQS)
    End If
  End If
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Function
TrataErro:
  Call TrataErroGeral("ProgramaAtivado", "Funcoes_ADO.bas")
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Call TerminaSEGBR
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' inclui registro na tabela tempor�ria do usu�rio
Public Sub IncluiRegTemp(ByVal OCodAplicacao As String, ByVal OParametro As String, _
                         ByVal AAplicacao As String, ByVal AInstancia As Double, _
                         ByVal OUsuario, ByVal OComputador, ByVal OIp, ByVal OMac, _
                         Optional ByVal OInicioAplicacao, _
                         Optional ByVal objAmbiente As Object)
  On Local Error GoTo TrataErro
  Dim Criou As Boolean
  
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  SQS = "INSERT """ & TabTempUsr(, , objAmbiente) & """"
  adSQS "VALUES ( " & FormatoSQLF(",", OUsuario, OComputador)
  adSQS FormatoSQLF(",", OIp, OMac, AAplicacao, OCodAplicacao, OParametro)
  If IsMissing(OInicioAplicacao) Then
    adSQS "GETDATE(),"
  Else
    adSQS FormatoSQLF(",", OInicioAplicacao)
  End If
  adSQS FormatoSQLF(")", AInstancia, 0)

  Call objAmbiente.ExecutaAmbiente(SQS)
  
  If glAmbiente_id = 2 Then
    ' ambiente de produ��o
    SQS = "EXEC segab_db..registro_acesso_spi"
    adSQS FormatoSQLF(",", , glUsu�rio, glEmpresa_id, glRecursoAcionado)
    If IsMissing(OInicioAplicacao) Then
      adSQS FormatoSQLF(",", Now)
    Else
      adSQS FormatoSQLF(",", OInicioAplicacao)
    End If
    adSQS FormatoSQLF(",", OUsuario, glAmbiente_id)
    adSQS FormatoSQLF(",", gsCPF, OComputador, OIp, OMac)
    adSQS FormatoSQL(OCodAplicacao, OParametro)
    
    Call objAmbiente.ExecutaAmbiente(SQS)
  End If
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Sub
TrataErro:
  Call TrataErroGeral("IncluiRegTemp", "Funcoes_ADO.bas")
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Call TerminaSEGBR
End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' remove o programa da tabela tempor�ria do usu�rio
Public Sub TerminaSEGBR(ParamArray OsFormularios())
  On Local Error GoTo TrataErro
  Dim objAmbiente As Object, RX As Object, sTabTempUsr As String
  Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
  sTabTempUsr = TabTempUsr(, , objAmbiente)
  SQS = "SELECT OBJECT_ID('TempDB.." & sTabTempUsr & "')"
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  If Not IsNull(RX(0)) Then
    SQS = "DELETE """ & sTabTempUsr & """"
    adSQS "WHERE id_aplicacao = " & FormatoSQL(gsCodAplicacao)
    adSQS "  AND parametro_aplicacao = " & FormatoSQL(gsParamAplicacao)
    
    Set objAmbiente = Nothing
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Call objAmbiente.ExecutaAmbiente(SQS)
  End If
  RX.Close
  Set RX = Nothing
  Set objAmbiente = Nothing
  
  If gsCodAplicacao = gsCODPRINCIPAL Then
    Call DescarregaBandeja(traySegbr)
    Call VerificaProgramasAbertos(True)
  End If

  Dim Formulario
  For Each Formulario In OsFormularios
    Unload Formulario
  Next Formulario

  rdocn.Close
  Set rdocn = Nothing
  End
  Exit Sub
TrataErro:
 ' Call GravaMensagem(Err.Description, "VB", "TerminaSEGBR", "Funcoes_ADO.bas")
  Resume Next
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' usado para a inicializa��o das vari�veis de conex�o
Public Sub inicializa_variaveis_ambiente(Optional ByVal objAmbiente As Object)
  On Local Error GoTo TrataErro
  Dim NaoUsado As String, Criou As Boolean, RX As ADODB.Recordset
  Dim lCifrado As Boolean
  Const sEXCE��O = "EXCECAO_PRODUCAO_PATH"
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
   
  Dim Dominio As String
  Dominio = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Sistema", "Dominio", gsCHAVESISTEMA, glAmbiente_id)
  PegaInfoSistema gsHost, gsMac, gsIp, NaoUsado, NaoUsado, cUserName, Dominio
  
  SQS = "SELECT sigla_sistema, secao, valor, 'igual' forma, len( secao) tamanho"
  adSQS "FROM parametro_tb (NOLOCK)"
  adSQS "WHERE campo = '" & sEXCE��O & "'"
  adSQS "  AND '" & gsIp & "' = secao"
  adSQS "UNION ALL"
  adSQS "SELECT sigla_sistema, secao, valor, 'diferente', len( secao)"
  adSQS "FROM parametro_tb (NOLOCK)"
  adSQS "WHERE campo = '" & sEXCE��O & "'"
  adSQS "  AND '" & gsIp & "' like secao + '%'"
  adSQS "ORDER BY forma DESC, tamanho DESC, valor"
  
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  
  If RX.EOF Then
    gsPastaServidorSegbr = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "PRODUCAO", "PRODUCAO_PATH", gsCHAVESISTEMA, glAmbiente_id)
  Else
    gsPastaServidorSegbr = RX("valor")
  End If
  
  RX.Close
  Set RX = Nothing
  
  gsPastaLocalSegbr = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "PRODUCAO", "LOCAL_PATH", gsCHAVESISTEMA, glAmbiente_id)
  
  SIS_server = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Sistema", "Servidor", gsCHAVESISTEMA, glAmbiente_id)
  SIS_banco = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Sistema", "Banco", gsCHAVESISTEMA, glAmbiente_id)
  SIS_usuario = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Sistema", "Usuario", gsCHAVESISTEMA, glAmbiente_id)
  SIS_senha = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Sistema", "Senha", gsCHAVESISTEMA, glAmbiente_id)
  
  INT_server = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Intranet", "Servidor", gsCHAVESISTEMA, glAmbiente_id)
  INT_banco = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Intranet", "Banco", gsCHAVESISTEMA, glAmbiente_id)
  INT_usuario = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Intranet", "Usuario", gsCHAVESISTEMA, glAmbiente_id)
  INT_senha = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Intranet", "Senha", gsCHAVESISTEMA, glAmbiente_id)
  
  SP_usuario = objAmbiente.RetornaValorAmbiente(gsSIGLASISTEMA, "Store Procedures", "Ambiente", gsCHAVESISTEMA, glAmbiente_id)
  Ambiente = SP_usuario
  
  If Len(Command) > 1 Then
   If InStrRev(Command, " ") = 0 Then
      gsParamAplicacao = Command
   Else
      gsParamAplicacao = Left(Command, InStrRev(Command, " "))
   End If
  Else
      gsParamAplicacao = Command
  End If
    
  ' pega ap�s o �ltimo espa�o em branco, onde est� a cadeia de criptografia
  gsParamAplicacao = Trim(gsParamAplicacao)

  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Sub
TrataErro:
  Call TrataErroGeral("inicializa_variaveis_ambiente", "Funcoes_ADO.bas")
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Call TerminaSEGBR
End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' verifica que aplica��es SEGBR est�o sendo executadas
Public Sub VerificaProgramasAbertos(Optional ByVal FechaTudo As Boolean = False, _
           Optional objAmbiente)
  
  On Local Error GoTo TrataErro
  Const sSEPARADOR = "$$$$$$XXXX$$$$$$======botafogo-----"
  Dim RX As Object, AInstanciaProcesso As Double
  Dim sTabTempUsr As String, Criou As Boolean
  
  If IsMissing(objAmbiente) Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  sTabTempUsr = TabTempUsr(, , objAmbiente)
  
  SQS = "SELECT OBJECT_ID('TempDB.." & sTabTempUsr & "')"
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  If IsNull(RX(0)) Then GoTo Fim
  RX.Close
  Set RX = Nothing
  
  ' pega inst�ncias
  SQS = "SELECT instancia"
  adSQS "FROM " & sTabTempUsr
  adSQS "WHERE id_aplicacao <> " & FormatoSQL(gsCODPRINCIPAL)
  
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  SQS = ""
  While Not RX.EOF
    AInstanciaProcesso = Val(RX(0))
    If FechaTudo Then
      Call Fecha_Aplicativos(AInstanciaProcesso)
    ElseIf Not AplicacaoAtiva(AInstanciaProcesso) Then
      ' remove o registro da tabela uma vez que a inst�ncia n�o est� mais ativa
      adSQS "DELETE " & sTabTempUsr
      adSQS "WHERE instancia = " & AInstanciaProcesso & sSEPARADOR
    End If
    RX.MoveNext
  Wend
  RX.Close
  Set RX = Nothing
  If SQS <> "" Then
    Dim vProgramas, i�ndice
    vProgramas = Split(SQS, sSEPARADOR)
    For i�ndice = LBound(vProgramas) To UBound(vProgramas)
      If CStr(vProgramas(i�ndice)) <> "" Then
        Call objAmbiente.ExecutaAmbiente(CStr(vProgramas(i�ndice)))
      End If
    Next i�ndice
  End If
  GoTo Fim
TrataErro:
  Call TrataErroGeral("VerificaProgramasAbertos", "Funcoes_ADO.bas")
Fim:
  Set RX = Nothing
  If Criou Then
    Set objAmbiente = Nothing
  End If
End Sub
'----------------------------------------------------------------------------------------------
'
' Rotinas que chamam outras que precisam de RDO
'
'----------------------------------------------------------------------------------------------
'
'
'
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' copia um arquivo exibindo um AVI
Public Function CopiaArquivo(ByVal Origem As String, ByVal Destino As String, _
                             Optional ByVal PastaAVI, Optional ByVal RegistraPrograma = False)
  On Local Error GoTo TrataErro
  Dim sArquivoResultado As String, sResultado As String
  Dim AInstancia As Double, sMensagem As String
  Dim sArquivo As String, sPastaOrigem As String, sPastaDestino As String
  CopiaArquivo = False
  AInstancia = 0
  sArquivo = Right(Origem, Len(Origem) - InStrRev("\" & Origem, "\") + 1)
  sPastaOrigem = Left(Origem, Len(Origem) - Len(sArquivo))
  sPastaDestino = Left(Destino, Len(Destino) - Len(sArquivo))
  sArquivoResultado = sPastaDestino & sArquivo & ".copiou.txt"
  If IsMissing(PastaAVI) Or (PastaAVI = "") Then PastaAVI = App.PATH & "\"
  ' exibe um AVI
  AInstancia = Shell(PastaAVI & "CopiaArquivoAVI.exe " & sArquivo, vbNormalFocus)
  ' copia arquivo
  'FileCopy Origem, Destino
  sMensagem = "c;" & sArquivo & ";" & sPastaOrigem & ";" & _
              sPastaDestino & ";" & sArquivoResultado
  sResultado = CStr(EncaminharMensagem(sMensagem, "localhost", glPortaSABL0102))
  If sResultado = "0" Then ' mensagem encaminhada com sucesso
    sResultado = ExtrairResultado(sArquivoResultado, "opera��o", True)
    If sResultado = "n" Then Error gsERRO_ATUALIZA_ARQUIVO
    If RegistraPrograma Then Call RegistraArquivo(sPastaDestino & sArquivo)
    'Pause 1
    CopiaArquivo = (sResultado <> "n")
  Else
    CopiaArquivo = False
  End If
  ' retira a exibi��o do AVI
  Call Fecha_Aplicativos(AInstancia)
  Exit Function
TrataErro:
  Select Case Err.Number
    Case 53
      Call MontaErro(gsERRO_ARQUIVO_NAO_ENCONTRADO, Origem)
    Case Else
      Call MontaErro(gsERRO_ATUALIZA_ARQUIVO, sPastaDestino & sArquivo)
  End Select
  Call TrataErroGeral("CopiaArquivo", "Funcoes_ADO.bas")
  ' retira a exibi��o do AVI
  If AInstancia <> 0 Then Call Fecha_Aplicativos(AInstancia)
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' executa um programa atualizando as depend�ncias
Public Function ExecutaPrograma(ByVal OCodPrograma As String, ByVal OPrograma As String, _
                                ByVal OParametro As String, ByVal OEstilo As Integer, _
                                ParamArray ArquivosDependentes()) As Double
  On Local Error GoTo TrataErro
  Dim AInstancia As Double, AJanela As Long, OArquivoParaAtualizacao As String
  Dim lComAmbiente As Boolean, lProgramaAtivado As Boolean, OIndice
  ExecutaPrograma = 0
  Screen.MousePointer = vbHourglass
  lComAmbiente = False
  OIndice = LBound(ArquivosDependentes)
  If OIndice = 0 Then
    lComAmbiente = IsObject(ArquivosDependentes(0))
  End If
  If LCase(App.Comments) <> "chamador" Then
    If lComAmbiente Then
      ' O primeiro par�metro � o ambiente
      Call VerificaProgramasAbertos(, ArquivosDependentes(0))
      lProgramaAtivado = ProgramaAtivado(OCodPrograma, OParametro, AInstancia, ArquivosDependentes(0))
    Else
      Call VerificaProgramasAbertos
      lProgramaAtivado = ProgramaAtivado(OCodPrograma, OParametro, AInstancia)
    End If
  End If
  ' se a inst�ncia estiver em execu��o, ativa o programa
  If lProgramaAtivado Then
    Call AppActivate(AInstancia)
    AJanela = PegaJanela(AInstancia)
    If IsIconic(AJanela) <> 0 Then Call OpenIcon(AJanela)
    'Public Const WM_QUERYOPEN = &H13
    'Call SendMessage(AInstancia, &H13, 0, 0)
    'Call SendMessage(AJanela, &H13, 0, 0)
    'Call OpenIcon(AJanela)
    'Call ShowWindow(AJanela, SW_RESTORE)
    'Call SetForegroundWindow(AJanela)
    'Call OpenIcon(AJanela)
  ElseIf InStr(OPrograma, "\") > 0 Then ' n�o atualiza vers�o
    OArquivoParaAtualizacao = OPrograma
    ' iniciar o programa
    AInstancia = Shell(OPrograma & " " & OParametro & " " & Monta_Parametros(OCodPrograma), OEstilo)
    ' adicionar na tabela do usu�rio a inst�ncia da aplica��o
    If LCase(App.Comments) <> "chamador" Then
      If lComAmbiente Then
        ' O primeiro par�metro � o ambiente
        Call IncluiRegTemp(OCodPrograma, OParametro, OPrograma, AInstancia, cUserName, gsHost, gsIp, gsMac, gsDHEntrada, ArquivosDependentes(0))
      Else
        Call IncluiRegTemp(OCodPrograma, OParametro, OPrograma, AInstancia, cUserName, gsHost, gsIp, gsMac, gsDHEntrada)
      End If
    End If
  Else
    Call AtualizarComponentes(Left(OPrograma & ".", InStr(OPrograma, ".") - 1), gsPastaServidorSegbr, gsPastaLocalSegbr)
'    If AtualizaVersao(OPrograma, gsPastaServidorSegbr, gsPastaLocalSegbr) Then
'    ' usado para atualizar outros arquivos como .RPT
'    For OIndice = LBound(ArquivosDependentes) To UBound(ArquivosDependentes)
'      If Not IsObject(ArquivosDependentes(OIndice)) Then
'        If Not AtualizaVersao(ArquivosDependentes(OIndice), gsPastaServidorSegbr, gsPastaLocalSegbr) Then
'          OArquivoParaAtualizacao = ArquivosDependentes(OIndice)
'          Call MensagemBatch("O componente " & OArquivoParaAtualizacao & " n�o p�de ser atualizado", vbInformation, "Atualiza��o de componente")
'        End If
'      End If
'    Next OIndice
    ' iniciar o programa
    If lComAmbiente Then
      ' O primeiro par�metro � o ambiente
      AInstancia = Shell(gsPastaLocalSegbr & OPrograma & " " & OParametro & " " & _
                   Monta_Parametros(OCodPrograma, ArquivosDependentes(0)), OEstilo)
    Else
      AInstancia = Shell(gsPastaLocalSegbr & OPrograma & " " & OParametro & " " & Monta_Parametros(OCodPrograma), OEstilo)
    End If
    ' adicionar na tabela do usu�rio a inst�ncia da aplica��o
    If LCase(App.Comments) <> "chamador" Then
      ' no chamador n�o existe a tabela tempor�ria
      If lComAmbiente Then
        ' O primeiro par�metro � o ambiente
        Call IncluiRegTemp(OCodPrograma, OParametro, OPrograma, AInstancia, cUserName, gsHost, gsIp, gsMac, gsDHEntrada, ArquivosDependentes(0))
      Else
        Call IncluiRegTemp(OCodPrograma, OParametro, OPrograma, AInstancia, cUserName, gsHost, gsIp, gsMac, gsDHEntrada)
      End If
    End If
  End If
  Screen.MousePointer = vbDefault
  ExecutaPrograma = AInstancia
  Exit Function
TrataErro:
  If Err.Number = glERRO_ATUALIZA_ARQUIVO Then
    MontaErro gsERRO_ATUALIZA_ARQUIVO, OArquivoParaAtualizacao
  End If
  Call TrataErroGeral("ExecutaPrograma", "Funcoes_ADO.bas")
  Screen.MousePointer = vbDefault
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2005
'
' registra um arquivo
Public Sub RegistraArquivo(OPrograma As String)
  On Local Error GoTo TrataExce��o
  Dim sPasta As String, sArquivo As String, sMensagem As String
  sPasta = Left(OPrograma, InStrRev(OPrograma, "\"))
  sArquivo = Right(OPrograma, Len(OPrograma) - Len(sPasta))
  If Len(sPasta) = 0 Then
    sPasta = gsPastaLocalSegbr
  End If
  sMensagem = "r;" & sArquivo & ";" & sPasta
  Call EncaminharMensagem(sMensagem, "localhost", glPortaSABL0102)
  Exit Sub
TrataExce��o:
  Call TrataErroGeral("RegistraArquivo", "Funcoes_ADO.bas")
  Call TerminaSEGBR
End Sub
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick e Raphael Gagliardi
' Dt Desenv : 16/11/2000
'
' monta os par�metros cifrados que (Nome da m�quina, IP, Usu�rio, Data-Hora)
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' fecha o(s) aplicativo(s) gerenciado(s) pelo SEGBR
Public Function Fecha_Aplicativos(vInstance As Variant) As Integer

Dim hProcess As Long
Dim lErro As Integer
Dim lExitCode As Long
Dim iIndice As Integer
Dim lHandle As Long
 
On Local Error GoTo TrataErro

    'Se for Num�rico (Long)
    If VarType(vInstance) = vbDouble Then
        'Pega o Processo e Fecha o Processo
        'hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, True, CLng(vInstance))
        hProcess = OpenProcess(PROCESS_TERMINATE, True, CLng(vInstance))
        lHandle = TerminateProcess(hProcess, lExitCode)
        CloseHandle (lHandle)
    ElseIf VarType(vInstance) = vbArray + vbDouble Then
        For iIndice = LBound(vInstance) To UBound(vInstance)
            lErro = Fecha_Aplicativos(vInstance(iIndice))
            If lErro <> 0 Then Error glERRO_FECHA_APLICATIVO
        Next
    Else
        Error glERRO_TIPO_NAO_CORRETO
    End If
        
    Fecha_Aplicativos = 0
    
    Exit Function
    
TrataErro:
    Select Case Err
      Case glERRO_FECHA_APLICATIVO
        MontaErro gsERRO_FECHA_APLICATIVO, "n�o identificado"
      Case glERRO_TIPO_NAO_CORRETO
        MontaErro gsERRO_TIPO_NAO_CORRETO, TypeName(vInstance)
    End Select
    Fecha_Aplicativos = Err
    Call TrataErroGeral("Fecha_Aplicativos", "Funcoes_ADO.bas")
    Call TerminaSEGBR
End Function
'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 16/11/2000
'
' atualiza vers�o de um programa e suas depend�ncias
Public Function AtualizaVersao(ByVal OPrograma As String, ByVal PastaOrigem As String, _
                               ByVal PastaDestino As String, Optional ByVal PastaAVI, _
                               Optional ByVal RegistraPrograma As Boolean = False) As Boolean
On Error GoTo Trata_Erro
  Dim AOrigem As String, ODestino  As String
  Dim objFSO As Object
  
  Set objFSO = CreateObject("Scripting.FileSystemObject")
  If OPrograma = "" Then
    AtualizaVersao = True
    Exit Function
  Else
    AtualizaVersao = False
  End If
  ''If Dir(PastaOrigem) = "" Then
  If Not objFSO.FolderExists(PastaOrigem) Then
    If InStr(OPrograma, "\") > 0 Then
      ' optou-se por registrar o arquivo que est� no servidor
      If RegistraPrograma Then Call RegistraArquivo(OPrograma)
      AtualizaVersao = True
    End If
  Else
    If IsMissing(PastaAVI) Then PastaAVI = PastaOrigem
    If PastaAVI = "" Then PastaAVI = PastaOrigem
    If Not objFSO.FolderExists(PastaDestino) Then Call MkDir(PastaDestino)
    AOrigem = PastaOrigem & OPrograma
    ODestino = PastaDestino & OPrograma
    If Not objFSO.FolderExists(ODestino) Then
      Call CopiaArquivo(AOrigem, ODestino, PastaAVI, RegistraPrograma)
    Else
      If Abs(DateDiff("s", FileDateTime(AOrigem), FileDateTime(ODestino))) > 90 Then
        If DateDiff("s", FileDateTime(AOrigem), FileDateTime(ODestino)) > 90 Then
          If MsgBox("O arquivo " & AOrigem & _
                    " possui uma vers�o anterior que a do arquivo local (" & _
                    PastaDestino & ")" & vbNewLine & "Atualiza?", vbYesNo) = vbNo Then
            AtualizaVersao = True
            Exit Function
          End If
        End If
        Call CopiaArquivo(AOrigem, ODestino, PastaAVI, RegistraPrograma)
      End If
    End If
    AtualizaVersao = True
    If RegistraPrograma Then Call RegistraArquivo(OPrograma)
  End If
  
  Set objFSO = Nothing
  Exit Function
  
Trata_Erro:
  Call TrataErroGeral("AtualizaVersao", "Funcoes_ADO")
End Function

Sub Finaliza_Objeto(objObjeto As Object)
  If Not objObjeto Is Nothing Then Set objObjeto = Nothing
End Sub

Sub MensagemBatch(ByVal PMensagem As String, Optional PBotao As VbMsgBoxStyle = vbOKOnly, _
                  Optional PTitulo As String = "", Optional ByVal PGravaMensagem As Boolean = True, _
                  Optional PFormulario As Form)
  If Len(Trim(PTitulo)) = 0 Then PTitulo = gsSIGLASISTEMA
  
  If PGravaMensagem Then
    Call GravaMensagem(PMensagem, "PROC")
  End If
  
  If goProducao Is Nothing Then
    If Obtem_agenda_diaria_id(Command) = 0 And PMensagem <> "" Then
      MsgBox PMensagem, PBotao, PTitulo
    End If
  ElseIf Not goProducao.automatico Then
    If PMensagem <> "" Then
      MsgBox PMensagem, PBotao, PTitulo
    End If
  ElseIf Not PFormulario Is Nothing Then
    Call TerminaSEGBR(PFormulario)
  End If
End Sub
Sub InicializaParametrosExecucaoBatch(PObjeto As Object)

  On Local Error GoTo TrataErro
  'Instanciando objeto produ��o '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  Set goProducao = CreateObject("Producao.ClasseProducao")
  goProducao.Usuario = cUserName
  
  'Obtendo no n�mero do processo agendado para o programa. Caso o programa esteja agen-
  'dado para ser executado automaticamente, a fun��o Obtem_agenda_diaria_id retornar�
  'um valor maior que 0 '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Dim lagenda_diaria_id As Long
  lagenda_diaria_id = Obtem_agenda_diaria_id(Command)
  
  If lagenda_diaria_id > 0 Then
     
    'Gravando os dados iniciais do processamento, esta��o, hora de in�cio etc. ''''''''''
    
    If Not goProducao.InicializaAutomatico(lagenda_diaria_id) Then
      Call TerminaSEGBR(PObjeto)
    End If
    
    'Preenchendo automaticamente os par�metros necess�rios para a execu��o ''''''''''''''
    
    If Not goProducao.LeParametros(PObjeto) Then
      Call TerminaSEGBR(PObjeto)
    End If
  
  Else
  
    'Gravando dados dados iniciais do processamento esta��o, hora de in�cio etc. ''''''''
    Dim OArquivo
    OArquivo = App.PATH & "\" & App.EXEName & ".exe"
    If Not goProducao.InicializaManual(OArquivo, Data_Sistema) Then
       Call MensagemBatch("N�o foi poss�vel inicializar manualmente.", vbCritical)
       Call TerminaSEGBR(PObjeto)
    End If
  
    'Gravando os par�metros informados pelo usu�rio '''''''''''''''''''''''''''''''''''''

    Call goProducao.GravaParametros(PObjeto)
    
  End If
  Exit Sub
TrataErro:
  Call TrataErroGeral("InicializaParametrosExecucaoBatch", "Funcoes_ADO.bas")
End Sub

'------------------------------------------------------------------------------------------
'
' Respons�vel : Jo�o Mac-Cormick
' Dt Desenv : 03/06/2000
' convertido para ado
' retorna a �ltima posi��o da busca na cadeia
Function Valida_Grupo_Intranet(ByVal prog As String, funcao As String) As Boolean
  Dim rs As Recordset, sqlstmt
  On Local Error GoTo TrataErro
  
  With rdoCnWEB
    .Connect = "UID=" & INT_usuario & ";PWD=" & INT_senha & ";server=" & INT_server & ";driver={SQL Server};database=" & INT_banco & ";"
    .LoginTimeout = 15
    .CursorLocation = adUseServer
    .Open
  End With
        
  sqlstmt = "SELECT funcoes_tb.endereco "
  sqlstmt = sqlstmt & "FROM   permissoes_tb,funcoes_tb  "
  sqlstmt = sqlstmt & "WHERE permissoes_tb.Funcoes_Id = funcoes_tb.Id "
  sqlstmt = sqlstmt & "  AND permissoes_tb.Status = 's' "
  sqlstmt = sqlstmt & "  AND funcoes_tb.endereco LIKE '%/" & Trim(prog) & "/'"
  sqlstmt = sqlstmt & "  AND (permissoes_tb.nome  = '" & cUserName & "' "
  sqlstmt = sqlstmt & "       or permissoes_tb.nome in (SELECT grupo from membros_tb where username = '" & cUserName & "'))"
  
  Set rs = rdoCnWEB.Execute(sqlstmt)
  
  If rs.EOF Then
    Valida_Grupo_Intranet = False
  Else
    Valida_Grupo_Intranet = True
  End If
  
  rs.Close
  Set rs = Nothing
  rdoCnWEB.Close
Exit Function

TrataErro:
  gsMensagem = "N�o foi poss�vel a conex�o com o banco " & INT_banco
  Call MensagemBatch(gsMensagem)
  Call TerminaSEGBR
End Function

Sub Verifica_Existencia_Arquivo(ByVal arquivo1 As String, cmdOk As Object, lblarq As Object, lblver As Object, lblstatus As Object, flag As Boolean, Optional UnloadForm As Object = Nothing)
    
    Dim versao As Integer, max_versao As Integer
    Dim rc As ADODB.Recordset, SQL As String
    Dim Arquivo As String

    'Captura nome do arquivo correspondente
    versao = Val(Mid(arquivo1, InStr(1, arquivo1, ".") + 1))
    Arquivo = Mid(arquivo1, 1, InStr(1, arquivo1, ".") - 1)
    
    'teste do arquivo
    '------------------------------------
    
    SQL = "      SELECT"
    SQL = SQL & "   max(versao) "
    SQL = SQL & "FROM"
    SQL = SQL & "   controle_proposta_db..arquivo_versao_recebido_tb "
    SQL = SQL & "WHERE"
    SQL = SQL & "   layout_id = (SELECT"
    SQL = SQL & "                   layout_id "
    SQL = SQL & "                FROM"
    SQL = SQL & "                   controle_proposta_db..layout_tb "
    SQL = SQL & "                WHERE"
    SQL = SQL & "                   nome = '" & Arquivo & "') AND"
    SQL = SQL & "   dt_ent_segbr is not null"
    Set rc = rdocn.Execute(SQL)
    
    max_versao = IIf(IsNull(rc(0)), 0, rc(0))
    
    Set rc = Nothing
        
    If max_versao = versao Then 'J� foi importado
        
        lblarq.Caption = arquivo1
        lblver.Caption = versao
        lblstatus.Caption = "Conclu�do"
        Call MensagemBatch("Arquivo j� importado!")
        
        '------------------------------------------------------------------------------
        ' joconceicao - 29maio2001
        ' encerra a execu��o se for autom�tica
        If Not goProducao Is Nothing Then
            If Not UnloadForm Is Nothing Then
                Call Unload(UnloadForm)
            End If
            Call TerminaSEGBR
        Else
            cmdOk.Enabled = False
        End If
            
    Else
            
        If max_versao = versao - 1 Then 'N�o foi importado
        
            cmdOk.Enabled = True
            flag = True
            lblarq.Caption = arquivo1
            lblver.Caption = versao
            lblstatus.Caption = "N�o processado"
            
        Else 'Arquivo fora de ordem
                
            Call MensagemBatch("Arquivo fora de ordem!" & Chr(10) & Chr(13) & _
            "Arquivo esperado: " & Arquivo & "." & Format((max_versao + 1), "0000"), vbCritical)
            
            '------------------------------------------------------------------------------
            ' joconceicao - 29maio2001
            ' encerra a execu��o se for autom�tica
            If Not goProducao Is Nothing Then
                If goProducao.automatico Then
                    If Not UnloadForm Is Nothing Then
                        Call Unload(UnloadForm)
                    End If
                    Call TerminaSEGBR
                End If
            End If
                
            flag = False
            lblarq.Caption = arquivo1
            lblver.Caption = versao
            lblstatus.Caption = "Fora de ordem"
                
        End If
                
    End If
    
End Sub

Sub Verifica_Controle_Arquivo(ByVal arquivo1 As String, cmdOk As Object, lblarq As Object, lblver As Object, lblstatus As Object, flag As Boolean, Optional UnloadForm As Object = Nothing)
    
Dim versao As Integer, max_versao As Integer
Dim rc As New Recordset, SQL As String
Dim Arquivo As String

On Error GoTo Trata_Erro

  'Captura nome do arquivo correspondente
  versao = Val(Mid(arquivo1, InStr(1, arquivo1, ".") + 1))
  Arquivo = Mid(arquivo1, 1, InStr(1, arquivo1, ".") - 1)
    
  'teste do arquivo
  '------------------------------------
  SQL = "      SELECT"
  SQL = SQL & "   max(versao) "
  SQL = SQL & "FROM"
  SQL = SQL & "   controle_proposta_db..arquivo_versao_recebido_tb "
  SQL = SQL & "WHERE"
  SQL = SQL & "   layout_id = (SELECT"
  SQL = SQL & "                   layout_id "
  SQL = SQL & "                FROM"
  SQL = SQL & "                   controle_proposta_db..layout_tb "
  SQL = SQL & "                WHERE"
  SQL = SQL & "                   nome = '" & Arquivo & "') AND"
  SQL = SQL & "   dt_ent_segbr is not null"

  Call rc.Open(SQL, rdocn, adOpenStatic, adLockReadOnly)
    
  max_versao = IIf(IsNull(rc(0)), 0, rc(0))
    
  Set rc = Nothing
        
  If max_versao = versao Then 'J� foi importado
      
    lblarq.Caption = arquivo1
    lblver.Caption = versao
    lblstatus.Caption = "Conclu�do"
    Call MensagemBatch("Arquivo j� importado!")
    
    '------------------------------------------------------------------------------
    ' joconceicao - 29maio2001
    ' encerra a execu��o se for autom�tica
    If Not goProducao Is Nothing Then
        If Not UnloadForm Is Nothing Then
            Call Unload(UnloadForm)
        End If
        Call TerminaSEGBR
    Else
        cmdOk.Enabled = False
    End If
          
  Else
          
    If max_versao = versao - 1 Then 'N�o foi importado
        
        cmdOk.Enabled = True
        flag = True
        lblarq.Caption = arquivo1
        lblver.Caption = versao
        lblstatus.Caption = "N�o processado"
        
    Else 'Arquivo fora de ordem
            
        Call MensagemBatch("Arquivo fora de ordem!" & Chr(10) & Chr(13) & _
        "Arquivo esperado: " & Arquivo & "." & Format((max_versao + 1), "0000"), vbCritical)
        
        '------------------------------------------------------------------------------
        ' joconceicao - 29maio2001
        ' encerra a execu��o se for autom�tica
        If Not goProducao Is Nothing Then
            If goProducao.automatico Then
                If Not UnloadForm Is Nothing Then
                    Call Unload(UnloadForm)
                End If
                Call TerminaSEGBR
            End If
        End If
            
        flag = False
        lblarq.Caption = arquivo1
        lblver.Caption = versao
        lblstatus.Caption = "Fora de ordem"
            
    End If
              
    SQL = "      SELECT"
    SQL = SQL & "     dt_ent_segbr "
    SQL = SQL & "FROM"
    SQL = SQL & "     controle_proposta_db..arquivo_versao_recebido_tb "
    SQL = SQL & "WHERE"
    SQL = SQL & "     versao = " & versao & " AND"
    SQL = SQL & "     layout_id = (SELECT"
    SQL = SQL & "                       layout_id "
    SQL = SQL & "                  FROM"
    SQL = SQL & "                       controle_proposta_db..layout_tb "
    SQL = SQL & "                  WHERE"
    SQL = SQL & "                       nome = '" & Arquivo & "')"
    
    Call rc.Open(SQL, rdocn, adOpenStatic, adLockReadOnly)

    If rc.EOF Then
            
      'Insere em arquivo_versao_recebido_tb
      SQL = "exec controle_proposta_db..inclui_arquivos_versao_spi "
      SQL = SQL & "null, null,'" & Arquivo & "'," & versao & ",'"
      SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "','"
      SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "',0,'" & cUserName & "'"
      
      Call rdocn.Execute(SQL)
    
    Else
      
      If Not IsNull(rc("dt_ent_segbr")) Then
        Call MensagemBatch("Arquivo j� importado anteriormente!", vbCritical)
        cmdOk.Enabled = False
        lblstatus.Caption = "Fora de ordem - Conclu�do"
      End If
    End If
  End If
      
  Exit Sub
  
Trata_Erro:
  Call TrataErroGeral("Verifica_Controle_Arquivo", "Funcoes_ADO")
End Sub

Sub Atualiza_Controle_Arquivo(ByVal sArquivo As String, lVersao As Long, lLinhas As Long, lRegistros As Long)

Dim sSQL As String
   
On Error GoTo Trata_Erro

  'Captura nome do sArquivo correspondente
  sArquivo = Mid(sArquivo, 1, InStr(1, sArquivo, ".") - 1)
   
  'Se registro est� vazio ent�o qtd.lLinhas = qtd.lRegistros
  If IsEmpty(lRegistros) Or lRegistros = 0 Then lRegistros = lLinhas - 2
   
  'Atualiza dados de importacao em sArquivo_detalhe_tb
  sSQL = "EXEC controle_proposta_db..controle_Arquivos_import_spu "
  sSQL = sSQL & "'" & sArquivo & "'," & lVersao & "," & lLinhas & ",'"
  sSQL = sSQL & Format(Data_Sistema, "yyyymmdd") & "'," & lRegistros & ",'"
  sSQL = sSQL & cUserName & "'"
   
  Call rdocn.Execute(sSQL)
       
  Exit Sub

Trata_Erro:
  Call TrataErroGeral("Atualiza_Controle_Arquivo", "Funcoes_ADO")
End Sub

Public Function MontaListViewADO(ByRef lRec As Object, _
                        ByRef LView As Object, _
                        Optional ByRef bGridLines As Boolean = False, _
                        Optional ByRef bCheckBoxes As Boolean = False) As Boolean
'Funcao p/ montar listview a partir de um rdoresultser, utilizando os nomes dos
'atributos como cabechalhos, e podendo optar por ter ou nao checkbox e linhas de grid
'joconceicao - 28-jun-01
' esta funcao n�o deve ser chamada antes do load do form completo
Dim Itm As Object
Dim Colunas As Long
Dim TamCol() As Long
    Colunas = lRec.Fields.Count - 1
    ReDim TamCol(0 To Colunas)
    For Colunas = 0 To UBound(TamCol)
        TamCol(Colunas) = 0
    Next Colunas
    With LView
       .Visible = False
       .GridLines = bGridLines
       .Checkboxes = bCheckBoxes
       .FullRowSelect = True
       .View = 3                 'lvwReport
       .LabelEdit = 1            ' lvwManual
       .ColumnHeaders.Clear
       For Colunas = 0 To lRec.Fields.Count - 1
           .ColumnHeaders.Add , , lRec(CLng(Colunas)).name
       Next Colunas
       .ListItems.Clear
       If lRec.EOF Then
          .Visible = True
          Exit Function
       End If
       Do
             If lRec(0).Type = 7 Then
                Set Itm = .ListItems.Add(, , Format(lRec(0), "dd/mm/yyyy"))
             Else
                Set Itm = .ListItems.Add(, , lRec(0) & "")
             End If
             If TamCol(0) < Screen.ActiveForm.TextWidth(CStr(.ListItems.Item(.ListItems.Count))) Then
                   TamCol(0) = Screen.ActiveForm.TextWidth(.ListItems.Item(.ListItems.Count))
             End If
             For Colunas = 1 To lRec.Fields.Count - 1
                If IsNull(lRec(Colunas)) Then
                   Itm.SubItems(Colunas) = " "
                ElseIf lRec(Colunas).Type = 11 Then
                       If lRec(Colunas).Value = True Then
                          Itm.SubItems(Colunas) = "S"
                       Else
                          Itm.SubItems(Colunas) = "N"
                       End If
                ElseIf lRec(Colunas).Type = 7 Then
                       If lRec(Colunas).Value = Null Then
                          Itm.SubItems(Colunas) = ""
                       Else
                          Itm.SubItems(Colunas) = Format(lRec(Colunas), "dd/mm/yyyy")
                       End If
                ElseIf InStr(UCase(CStr(lRec(Colunas).name)), "CPF") And _
                       Len(Trim(lRec(Colunas).Value)) = 11 > 0 Then
                       Itm.SubItems(Colunas) = MasCPF(lRec(Colunas))
                ElseIf InStr(UCase(CStr(lRec(Colunas).name)), "CNPJ") > 0 Or _
                       InStr(UCase(CStr(lRec(Colunas).name)), "CGC") > 0 Then
                       Itm.SubItems(Colunas) = MasCGC(lRec(Colunas))
                ElseIf InStr(UCase(CStr(lRec(Colunas).name)), "CEP") Or _
                       InStr(UCase(CStr(lRec(Colunas).name)), "C.E.P") Then
                       Itm.SubItems(Colunas) = MasCEP(lRec(Colunas))
                Else
                    Itm.SubItems(Colunas) = lRec(Colunas) & " "
                End If
                If TamCol(Colunas) < Screen.ActiveForm.TextWidth(Trim(.ListItems.Item(.ListItems.Count).SubItems(Colunas))) Then
                   TamCol(Colunas) = Screen.ActiveForm.TextWidth(Trim(.ListItems.Item(.ListItems.Count).SubItems(Colunas)))
                End If
                 
             Next Colunas
            lRec.MoveNext
       Loop Until lRec.EOF
       For Colunas = 1 To .ColumnHeaders.Count
           If TamCol(Colunas - 1) > 1000 Then
              .ColumnHeaders.Item(Colunas).Width = (TamCol(Colunas - 1) * 1.5)
           Else
              .ColumnHeaders.Item(Colunas).Width = 1500
           End If
       Next Colunas
       .Visible = True
  End With
End Function


 
Public Function Obtem_Numero_do_Convenio(ByRef iProduto As Integer, _
                                          ByRef iRamo As Integer, _
                                          ByRef PParcela As Integer, _
                                          ByRef iMoeda_id As Integer) As String
'------------------------------------------------------------------------------
' Fun��o para obter o numero do convenio - JoConceicao
'        criada em 07-Aug-2002 - a moeda id passou a fazer parte da chave
'
 
Dim rc As Recordset, SQL As String
 
    On Error GoTo Erro
 
 
    SQL = "SELECT num_convenio=isNull(num_convenio, '0') " _
        & " FROM tp_movimentacao_financ_tb " _
        & " WHERE ramo_id = " & iRamo _
        & "   and produto_id = " & iProduto _
        & "   and tp_operacao_financ_id =  "
        Select Case PParcela
            Case 1
                 SQL = SQL & " 1 "
            Case Else
                 SQL = SQL & " 2 "
        End Select
    SQL = SQL & " and moeda_id = " & iMoeda_id
        
    Set rc = rdocn.Execute(SQL)
 
    If Not rc.EOF Then
       Obtem_Numero_do_Convenio = rc(0)
    Else
       MensagemBatch "Convenio n�o cadastrado para o produto " & iProduto
       Call TerminaSEGBR
    End If
 
    rc.Close
 
Exit Function
 
Erro:
     TrataErroGeral "Fun��o: Obtem_Conv�nio" & vbCrLf & "Produto : " & iProduto & _
                                               vbCrLf & "Ramo    : " & iRamo & _
                                               vbCrLf & "Oper    : " & PParcela & _
                                               vbCrLf & "Moeda   : " & iMoeda_id, Screen.ActiveForm.name
     Call TerminaSEGBR
 
End Function

'
' Respons�vel : Edson F. Yoshino
' Dt Desenv : 19/08/2002
'
' atualiza vers�o de um programa e suas depend�ncias
Public Function AtualizaVersao2(ByVal OPrograma As String, ByVal PastaOrigem As String, _
                               ByVal PastaDestino As String, Optional ByVal PastaAVI, _
                               Optional ByVal Acao As String = "", _
                               Optional ByVal Comando As String = "", _
                               Optional ByVal Parametros As String = "") As Boolean
  On Error GoTo Trata_Erro
  Dim AOrigem As String, ODestino As String
  Dim sMensagem As String, sArquivoResultado As String, sResultado As String
  Dim objFSO As Object
  
  Set objFSO = CreateObject("Scripting.FileSystemObject")
  If OPrograma = "" Then
    AtualizaVersao2 = True
    Exit Function
  End If
  AtualizaVersao2 = False
  
  If IsMissing(PastaAVI) Then PastaAVI = PastaOrigem
  If PastaAVI = "" Then PastaAVI = PastaOrigem
  AOrigem = PastaOrigem & OPrograma
  ODestino = PastaDestino & OPrograma
  If Not objFSO.FolderExists(ODestino) Then
    Call CopiaArquivo(AOrigem, ODestino, PastaAVI)
  Else
    If Abs(DateDiff("s", FileDateTime(AOrigem), FileDateTime(ODestino))) > 90 Then
      If DateDiff("s", FileDateTime(AOrigem), FileDateTime(ODestino)) > 90 Then
        If MsgBox("O arquivo " & AOrigem & _
                  " possui uma vers�o anterior que a do arquivo local (" & _
                  PastaDestino & ")" & vbNewLine & "Atualiza?", vbYesNo) = vbNo Then
          Exit Function
        End If
      End If
      Call CopiaArquivo(AOrigem, ODestino, PastaAVI)
    End If
  End If
  If Acao = "SHELL" Then
    sArquivoResultado = ODestino & ".executou.txt"
    sMensagem = "e;" & Comando & ";" & ODestino & " " & Parametros & ";" & sArquivoResultado
    sResultado = CStr(EncaminharMensagem(sMensagem, "localhost", glPortaSABL0102))
    If sResultado = "0" Then ' mensagem encaminhada com sucesso
      Call ExtrairResultado(sArquivoResultado, "opera��o", True)
    End If
  End If
  AtualizaVersao2 = True
  Exit Function
  
Trata_Erro:
  Call TrataErroGeral("AtualizaVersao2", "Funcoes_ADO")

End Function

Public Sub TratarErro(NmRotina As String, Optional NmArquivo As String)
  
  Dim DescricaoErro As String
  Dim NumErro As Long
  Dim InstanciouErro As Boolean
  Dim agenda_diaria_id As Long
  Dim Erro As Object
        
  'Obtendo dados do erro ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  NumErro = Err.Number
  DescricaoErro = Err.Description
      
  'Instanciando objeto erro''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  Set Erro = CreateObject("Erro.log")
  
  Erro.Usuario = IIf(Trim(cUserName) = "", gsIp & "~" & gsHost, cUserName)
  Erro.NmProjeto = gsSIGLASISTEMA
  Erro.NmAplicacao = App.EXEName
  Erro.NmForm = NmArquivo
  'Erro.NmArquivo = NmArquivo
  Erro.NmRotina = NmRotina
    
  'Obtendo c�digo da agenda di�ria, caso exista''''''''''''''''''''''''''''''''''''
  
  If goProducao Is Nothing Then
    agenda_diaria_id = 0
  Else
    agenda_diaria_id = Obtem_agenda_diaria_id(Command)
  End If
 
  'Logando mensagem de erro'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   
  gsMensagem = Erro.loga(rdocn.Errors, NumErro, DescricaoErro, agenda_diaria_id)
  Call MensagemBatch(gsMensagem, vbExclamation, , False)
   
  Exit Sub
 
End Sub


Sub ObterDataSistema(ByVal sSiglaSistema As String)

Dim objDadosBasicos As Object
  
    ' Obtendo a data operacional ''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Set objDadosBasicos = CreateObject("SEGL0022.cls00118")
      
    Data_Sistema = objDadosBasicos.ObterDtOperacional(sSiglaSistema, App.Title, App.FileDescription, glAmbiente_id)
      
    Set objDadosBasicos = Nothing

End Sub

'agin - 10/05/2005
Public Function ObterNossoNumero(ByVal sNumConvenio As String) As String

Dim rs As Recordset
Dim sSQL As String
Dim iNumCarteira As Integer
Dim sSequencial As String

On Error GoTo Trata_Erro

    sSQL = "      SELECT seq_nosso_numero, num_carteira = IsNull(num_carteira, '0') "
    sSQL = sSQL & " FROM convenio_tb "
    sSQL = sSQL & "WHERE num_convenio= '" & sNumConvenio & "'"

    Set rs = rdocn.Execute(sSQL)

    If Not rs.EOF Then
        If rs("num_carteira") = 18 Then
            iNumCarteira = 18
        End If
        
        If (Len(Trim(sNumConvenio)) = 4 And Val(rs("seq_nosso_numero")) >= 9999999) Or _
           (Len(Trim(sNumConvenio)) = 6 And Val(rs("seq_nosso_numero")) >= 99999) Or _
           (Len(Trim(sNumConvenio)) = 7 And Val(rs("seq_nosso_numero")) >= 9999999999#) Then
            
            sSQL = "exec " & Ambiente & ".convenio_estorno_chave_spu "
            sSQL = sSQL & "'" & sNumConvenio & "'"
            sSQL = sSQL & ", " & 1
            sSQL = sSQL & ", '" & cUserName & "'"
            
            rs.Close
            Set rs = rdocn.Execute(sSQL)
        Else
            sSQL = "exec " & Ambiente & ".convenio_chave_spu "
            sSQL = sSQL & "'" & sNumConvenio & "'"
            sSQL = sSQL & ", '" & cUserName & "'"
            
            rs.Close
            Set rs = rdocn.Execute(sSQL)
        End If
    Else
        rs.Close
        Set rs = Nothing
        GoTo Trata_Erro
    End If
    
    If Not rs.EOF Then
        If iNumCarteira = 18 Then
            sNumConvenio = Format(sNumConvenio, "0000000")
            sSequencial = Format(rs("seq_nosso_numero"), "0000000000")
        Else
            If Len(Trim(sNumConvenio)) = 4 Then
                sNumConvenio = Format(sNumConvenio, "0000")
                sSequencial = Format(rs("seq_nosso_numero"), "0000000")
            ElseIf Len(Trim(sNumConvenio)) = 6 Then
                sNumConvenio = Format(sNumConvenio, "000000")
                sSequencial = Format(rs("seq_nosso_numero"), "00000")
            End If
        End If
    Else
        rs.Close
        Set rs = Nothing
        GoTo Trata_Erro
    End If
    
    rs.Close
    Set rs = Nothing
    ObterNossoNumero = sNumConvenio & sSequencial
    
    Exit Function
    
Trata_Erro:
    Call TrataErroGeral("ObterNossoNumero", "Funcoes_ADO")
    Call TerminaSEGBR
End Function
'---

'agin - 17/05/2005
Public Function MontarCodigoBarrasLinhaDigitavel(ByVal sDtAgendamento As String, _
                                                 ByVal dValCobranca As Double, _
                                                 ByVal sCarteira As String, _
                                                 ByVal sConvenio As String, _
                                                 ByVal sNossoNumero As String, _
                                                 ByVal sAgencia As String, _
                                                 ByVal sCodigoCedente As String, _
                                                 ByRef sCodigoBarras As String, _
                                                 ByRef sLinhaDigitavel As String)
 
Dim sParte1        As String
Dim sParte2        As String
Dim sParte3        As String
Dim sDv1           As String
Dim sDv2           As String
Dim sDv3           As String
Dim sDvGeral       As String
Dim sCodigoBarras1 As String
Dim sCodigoBarras2 As String
Dim sCodigoBarras3 As String
Dim vFator         As Variant
    
On Error GoTo Trata_Erro
       
    If sNossoNumero = "" Then
        Exit Function
    End If
    
    'c�digo banco + dv
    'acrescentado a diferen�a de dias entre a data de vencimento e (7/10/97)
    'para a forma��o da linha digit�vel -- Jo�o Mac-Cormick em 19/3/2001
    sCodigoBarras1 = "0019"

    vFator = Format(DateDiff("d", "07/10/1997", sDtAgendamento), "0000")
    sCodigoBarras2 = vFator & Format(dValCobranca * 100, "0000000000")
    
    If sCarteira = "18" Then
        If Len(sConvenio) = 7 Then
            sCodigoBarras3 = "000000" & Format(sNossoNumero, "00000000000000000") & Format(sCarteira, "00")
        Else
            sCodigoBarras3 = Format(sConvenio, "000000") & Format(sNossoNumero, "00000000000000000") & "21"
        End If
    Else
        sCodigoBarras3 = Format(sNossoNumero, "00000000000") & Left(sAgencia, 4) & Left(sCodigoCedente, 8) & Format(sCarteira, "00")
    End If
        
    sCodigoBarras = sCodigoBarras1 & " " & sCodigoBarras2 & sCodigoBarras3
    sDvGeral = CalculaMod11(sCodigoBarras)
    
    sCodigoBarras = Left(sCodigoBarras1 & sDvGeral & sCodigoBarras2 & sCodigoBarras3 & Space(44), 44)
    
    sParte1 = Mid(sCodigoBarras, 1, 3) & Mid(sCodigoBarras, 4, 1) & Mid(sCodigoBarras, 20, 5)
    sDv1 = CalculaMod10(sParte1)
    sParte1 = Mid(sParte1, 1, 5) & "." & Mid(sParte1, 6, 4) & sDv1
    
    sParte2 = Mid(sCodigoBarras, 25, 10)
    sDv2 = CalculaMod10(sParte2)
    sParte2 = Mid(sParte2, 1, 5) & "." & Mid(sParte2, 6, 5) & sDv2
    
    sParte3 = Mid(sCodigoBarras, 35, 10)
    sDv3 = CalculaMod10(sParte3)
    sParte3 = Mid(sParte3, 1, 5) & "." & Mid(sParte3, 6, 5) & sDv3
    
    sLinhaDigitavel = Right(Space(54) & sParte1 & " " & sParte2 & " " & sParte3 _
                      & " " & sDvGeral & " " & vFator & _
                      Format(dValCobranca * 100, "0000000000"), 54)
                      
    Exit Function

Trata_Erro:
    Call TrataErroGeral("MontarLinhaDigitavel", "Funcoes_ADO")
    Call TerminaSEGBR
End Function
'---

'agin - 17/05/2005
Private Function CalculaMod10(ByVal sParte As String) As String

Dim i As Integer
Dim lDv As Long
Dim iPeso As Integer
Dim iSoma As Integer
Dim iParcela As Integer
    
On Error GoTo Trata_Erro
    
    iPeso = 2
    iSoma = 0
    
    For i = Len(sParte) To 1 Step -1
        iParcela = iPeso * Val(Mid(sParte, i, 1))
        
        If iParcela > 9 Then
            iParcela = Val(Mid(Format(iParcela, "00"), 1, 1)) + Val(Mid(Format(iParcela, "00"), 2, 1))
        End If
      
        iSoma = iSoma + iParcela
        If iPeso = 2 Then iPeso = 1 Else iPeso = 2
    Next i
    
    lDv = 10 - (iSoma Mod 10)
    If lDv > 9 Then lDv = 0
    CalculaMod10 = Format(lDv, "0")

    Exit Function
    
Trata_Erro:
    Call TrataErroGeral("CalculaMod10", "Funcoes_ADO")
    Call TerminaSEGBR
End Function
'---

'agin - 17/05/2005
Private Function CalculaMod11(ByVal Parte As String) As String

Dim i As Integer
Dim iDv As Integer
Dim iPeso As Integer
Dim iSoma As Integer
Dim iParcela As Integer

On Error GoTo Trata_Erro
     
    iPeso = 2
    iSoma = 0
     
    For i = Len(Parte) To 1 Step -1
        If i <> 5 Then
            iParcela = iPeso * Val(Mid(Parte, i, 1))
            iSoma = iSoma + iParcela
            iPeso = iPeso + 1
            If iPeso > 9 Then iPeso = 2
        End If
    Next i
    
    iDv = 11 - (iSoma Mod 11)
    If iDv = 10 Or iDv = 11 Then iDv = 1
    CalculaMod11 = Format(iDv, "0")

    Exit Function

Trata_Erro:
    Call TrataErroGeral("CalculaMod11", "Funcoes_ADO")
    Call TerminaSEGBR
End Function
'---
Private Sub AtualizarComponentes(OCodPrograma As String, sPastaOrigem As String, sPastaDestino As String)
  On Local Error GoTo TrataExce��o
  Dim sArquivoResultado As String, sMensagem As String, sResultado As String
  'sArquivoResultado = "%temp%\x.txt"
  sMensagem = "a;" & OCodPrograma & ";" & sPastaOrigem & ";" & _
              sPastaDestino & ";" & sArquivoResultado
  If EncaminharMensagem(sMensagem, "localhost", glPortaSABL0102) <> "0" Then
    Err.Raise glERRO_ATUALIZA_ARQUIVO
  End If
  Exit Sub
TrataExce��o:
  Call TrataErroGeral("AtualizarComponentes", "Funcoes_ADO")
End Sub
