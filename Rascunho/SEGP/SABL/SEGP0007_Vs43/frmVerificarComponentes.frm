VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmVerificarComponentes 
   ClientHeight    =   4485
   ClientLeft      =   2070
   ClientTop       =   2160
   ClientWidth     =   4365
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   4365
   Begin VB.CommandButton cmdFechar 
      Caption         =   "&Fechar"
      Default         =   -1  'True
      Height          =   495
      Left            =   1560
      TabIndex        =   5
      Top             =   3840
      Width           =   1335
   End
   Begin VB.Frame fraComponentesAtualizados 
      Caption         =   "Componente(s) atualizado(s)"
      Height          =   2295
      Left            =   60
      TabIndex        =   3
      Top             =   2160
      Width           =   4250
      Begin VB.TextBox txtComponentesAtualizados 
         Height          =   1935
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   4
         Text            =   "frmVerificarComponentes.frx":0000
         Top             =   240
         Width           =   4000
      End
   End
   Begin VB.Frame fraVerificandoComponentes 
      Height          =   1335
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   4250
      Begin VB.Timer tmrTemporizador 
         Enabled         =   0   'False
         Interval        =   60000
         Left            =   3480
         Top             =   120
      End
      Begin MSComCtl2.Animation anmArquivo 
         Height          =   615
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   4000
         _ExtentX        =   7064
         _ExtentY        =   1085
         _Version        =   393216
         AutoPlay        =   -1  'True
         FullWidth       =   267
         FullHeight      =   41
      End
      Begin VB.Label lblArquivo 
         Caption         =   "Nome do componente"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   4050
      End
   End
End
Attribute VB_Name = "frmVerificarComponentes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim bTemporizadorAcionado As Boolean

Private Sub Form_Activate()
  If Not cmdFechar.Visible Then
    lblArquivo.Caption = "Verificando componentes para " & gsComponentesAtualizados
  End If
End Sub

Private Sub Form_Load()
  On Local Error GoTo TrataExceção
  Dim sArquivo As String
  bTemporizadorAcionado = False
  Me.Height = 1865
  Me.Caption = App.Title
  cmdFechar.Visible = False
  tmrTemporizador.Enabled = False
  tmrTemporizador.Interval = 10000 '60000
  sArquivo = App.PATH & "\filecopy.avi"
  lblArquivo.Caption = "Verificando componentes para " & gsComponentesAtualizados
  anmArquivo.Open sArquivo
  Exit Sub
TrataExceção:
  Select Case Err.Number
    Case 53:
      MsgBox "Arquivo " & sArquivo & " não encontrado!", vbCritical
    Case Else
  End Select
End Sub

Private Sub Form_QueryUnload(Cancela As Integer, Solicitação As Integer)
  Cancela = (Solicitação = vbFormControlMenu) And Not cmdFechar.Visible
  If Not Cancela Then
    Call EnviarFoco(0 & Me.Tag)
  End If
  
'vbFormControlMenu 0
'  User has chosen Close command from the Control-menu box on a form
'vbFormCode 1
'  Unload method invoked from code.
'vbAppWindows 2
'  Current Windows session ending
'vbAppTaskManager 3
'  Windows Task Manager is closing the application.
'vbFormMDIForm 4
'  MDI child form is closing because the MDI form is closing
'vbFormOwner 5
'  Form is closing because its owner is closing.
End Sub

Private Sub cmdFechar_Click()
  Unload Me
End Sub

Private Sub tmrTemporizador_Timer()
  bTemporizadorAcionado = True
  Unload Me
End Sub

Private Sub EnviarFoco(dInstância As Double)
  On Local Error GoTo TrataExceção
  Dim lJanela As Long
  If dInstância = 0 Then
    Exit Sub
  End If
  Call AppActivate(dInstância)
  lJanela = PegaJanela(dInstância)
  If IsIconic(lJanela) <> 0 Then
    Call OpenIcon(lJanela)
  End If
  Exit Sub
TrataExceção:

End Sub
