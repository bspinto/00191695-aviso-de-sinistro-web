Attribute VB_Name = "ModPrincipal"
Option Explicit

Public sMenus(18)               As String
Public sDataHoraMensagem        As String
Public lQtdAplicacoesAbertas    As Long
Public bUsuarioSolicitaTermino  As Boolean
Public Sub AssociaMenus()
  sMenus(0) = "SEGP0006.exe" 'Administra��o
  sMenus(1) = "SEGP0001.exe" 'Central de Atendimento
  sMenus(2) = "SEGP0002.exe" 'Comercial
  sMenus(3) = "SEGP0003.exe" 'Contabilidade
  sMenus(4) = "SEGP0004.exe" 'Cosseguro
  sMenus(5) = "SEGP0005.exe" 'Dados B�sicos
  sMenus(6) = "SEGP0008.exe" 'Emiss�o
  sMenus(7) = "SEGP0009.exe" 'Financeiro
  sMenus(8) = "SEGP0010.exe" 'Produ��o
  sMenus(9) = "SEGP0012.exe" 'Produto
  sMenus(10) = "SEGP0011.exe" 'Relat�rio
  sMenus(11) = "SEGP0013.exe" 'Resseguro
  sMenus(12) = "SEGP0016.exe" 'Scheduler
  sMenus(13) = "SEGP0014.exe" 'Sinistro
  sMenus(14) = "SEGP0015.exe" 'T�cnica
End Sub
'----------------------------------------------------------------------------------------------
' cria tabela tempor�ria do usu�rio
Public Sub CriaTabelaTemporaria(Optional ByVal Ambiente As Variant)
  On Local Error GoTo TrataErro
  
  If IsMissing(Ambiente) Then
    Set Ambiente = CreateObject("Ambiente.ClasseAmbiente")
  End If
    
  SQS = "CREATE TABLE " & TabTempUsr & "("
  adSQS "  usuario VARCHAR( 20) NULL,"
  adSQS "  computador VARCHAR( 15) NULL,"
  adSQS "  ip VARCHAR( 15) NULL,"
  adSQS "  mac CHAR( 12) NULL,"
  adSQS "  aplicacao VARCHAR( 50) NOT NULL,"
  adSQS "  id_aplicacao VARCHAR( 40) NOT NULL,"
  adSQS "  parametro_aplicacao VARCHAR( 250) NOT NULL"
  adSQS "    CONSTRAINT C" & Mid(cUserName & gsMac, 1, 29) & _
               " UNIQUE (id_aplicacao, parametro_aplicacao),"
  adSQS "  dt_inicio_aplicacao SMALLDATETIME NOT NULL,"
  adSQS "  instancia INT NOT NULL, "
'  adSQS "    CONSTRAINT I" & Mid(cUserName & gsMac, 1, 29) & " UNIQUE,"
  adSQS "  fecha_aplicacao BIT"
  adSQS ")"
  
  Call Ambiente.ExecutaAmbiente(SQS)
  
  IncluiRegTemp gsCODPRINCIPAL, "", gsSIGLASISTEMA, 0, cUserName, gsHost, gsIp, gsMac, , Ambiente
  Exit Sub
TrataErro:
  TrataErroGeral "CriaTabelaTemporaria", "modPrincipal.bas"
End Sub
'----------------------------------------------------------------------------------------------
' verifica se existe alguma mensagem na tabela
Public Sub VerificaMensagem(Optional ByVal ADataReferencia = "dt_inclusao", _
                            Optional ByVal Ambiente As Variant)
  On Local Error GoTo TrataErro
  Dim RX As Object
  
  If IsMissing(Ambiente) Then
    Set Ambiente = CreateObject("Ambiente.ClasseAmbiente")
  End If
  
  SQS = "SELECT dt_inclusao, dt_validade, mensagem,"
  adSQS "       usuario, CONVERT(DATETIME, GETDATE()) hora_atual"
  adSQS "FROM mensagem_tb"
  adSQS "WHERE DATEDIFF( MINUTE, " & FormatoSQL(sDataHoraMensagem) & _
                              ", " & ADataReferencia & ") > 0"
  adSQS "ORDER BY dt_inclusao" '& ADataReferencia
  
  Set RX = Ambiente.ExecutaAmbiente(SQS)
  
  If Not RX.EOF Then
    Dim EstavaVisivel
    EstavaVisivel = frmPrincipal.Visible
    frmPrincipal.WindowState = vbMaximized
    frmPrincipal.Visible = True
    frmPrincipal.SetFocus
    While Not RX.EOF
      Mensagem cUserName & "," & vbNewLine & vbNewLine & _
               UCase(RX("usuario")) & ", em " & Format(RX("dt_inclusao"), "dd/mm/yyyy") & _
               " �s " & Format(RX("dt_inclusao"), "hh:nn") & "h" & _
               ", informa:" & vbNewLine & vbNewLine & _
               RX("mensagem") & vbNewLine & vbNewLine & _
               "Mensagem v�lida at� " & Format(RX("dt_validade"), "dd/mm/yyyy hh:nn") & _
               "h" & vbNewLine & vbNewLine & "Hora atual: " & RX("hora_atual") & "h"
      sDataHoraMensagem = RX("dt_inclusao") 'ADataReferencia)
      RX.MoveNext
    Wend
    frmPrincipal.Visible = EstavaVisivel
  End If
  RX.Close
  Exit Sub
TrataErro:
  TrataErroGeral "VerificaMensagem", "modPrincipal.bas"
End Sub
'----------------------------------------------------------------------------------------------
' verifica se deve fechar alguma a aplica��o SEGBR
Public Function FechaProgramaSolicitado(Optional ByVal Ambiente As Variant) As Long
  On Local Error GoTo TrataErro
  Dim RX As Object, AInstanciaProcesso As Long
  ' pega inst�ncias
  
  If IsMissing(Ambiente) Then
    Set Ambiente = CreateObject("Ambiente.ClasseAmbiente")
  End If
  
  SQS = "SELECT instancia"
  adSQS "FROM " & TabTempUsr
  adSQS "WHERE id_aplicacao = " & FormatoSQL(gsCODPRINCIPAL)
  adSQS "  AND fecha_aplicacao = 1"
  Set RX = Ambiente.ExecutaAmbiente(SQS)
  If Not RX.EOF Then
    Call Ambiente.ExecutaAmbiente("DELETE " & TabTempUsr & " WHERE id_aplicacao = " & FormatoSQL(gsCODPRINCIPAL))
    'Call ExecutaSQLNovaConexao("DELETE " & TabTempUsr & " WHERE id_aplicacao = " & FormatoSQL(gsCODPRINCIPAL), grdoConexaoAmbiente)
    Unload frmPrincipal
    End
  End If
  RX.Close
  SQS = "SELECT instancia"
  adSQS "FROM " & TabTempUsr
  adSQS "WHERE id_aplicacao <> " & FormatoSQL(gsCODPRINCIPAL)
  adSQS "  AND fecha_aplicacao = 1"
  Set RX = Ambiente.ExecutaAmbiente(SQS)
  While Not RX.EOF
    AInstanciaProcesso = Val(RX(0))
    Call Fecha_Aplicativos(AInstanciaProcesso)
    RX.MoveNext
  Wend
  RX.Close
  Call VerificaProgramasAbertos(False, Ambiente)
  SQS = "SELECT COUNT(*)"
  adSQS "FROM " & TabTempUsr
  adSQS "WHERE id_aplicacao <> " & FormatoSQL(gsCODPRINCIPAL)
  Set RX = Ambiente.ExecutaAmbiente(SQS)
  FechaProgramaSolicitado = RX(0)
  RX.Close
  Exit Function
TrataErro:
  TrataErroGeral "FechaProgramaSolicitado", "modPrincipal.bas"
End Function
Public Sub RegisterDataSource(ByVal ObjAmbiente As Object)
  On Local Error GoTo TrataErro
  Dim strAttribs As String
  Dim GDS_server As String
  
  GDS_server = LerArquivoIni("GDS", "GDS_SERVER")
  
  strAttribs = "Description="
  Inc strAttribs, "Conex�o com o GDS_DB"
  Inc strAttribs, Chr$(13) & "OemToAnsi=No"
  Inc strAttribs, Chr$(13) & "SERVER=" & GDS_server
  Inc strAttribs, Chr$(13) & "Database=gds_db"
  rdoEngine.rdoRegisterDataSource "Brasilseg", "SQL Server", True, strAttribs
         
  strAttribs = "Description="
  Inc strAttribs, "Conex�o com o SEGBR"
  Inc strAttribs, Chr$(13) & "OemToAnsi=No"
  Inc strAttribs, Chr$(13) & "SERVER=" & SIS_server
  Inc strAttribs, Chr$(13) & "Database=" & SIS_banco
  rdoEngine.rdoRegisterDataSource "RPT_SEGBR", "SQL Server", True, strAttribs
  
  Exit Sub
  
TrataErro:
  Call TrataErroGeral("RegisterDataSource", "modPrincipal.bas")
End Sub
Public Sub PosicionaMoldura(Formulario As Form, Moldura As Object)
  'frmPrincipal.fraIcones.Top = 5000
  'MsgBox "Largura " & Formulario.Width _
  & " Altura " & Formulario.Height
  Moldura.Top = Formulario.Height - Moldura.Height - 400
  Moldura.Left = (Formulario.Width - Moldura.Width) \ 2
  Formulario.BackColor = Moldura.BackColor
End Sub
