VERSION 5.00
Begin VB.Form FrmAplicativo 
   Caption         =   "Executar Aplicativo"
   ClientHeight    =   1875
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   2865
   LinkTopic       =   "Form1"
   ScaleHeight     =   1875
   ScaleWidth      =   2865
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtParametro 
      Height          =   345
      Left            =   120
      TabIndex        =   4
      Top             =   930
      Width           =   2595
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   405
      Left            =   180
      TabIndex        =   3
      Top             =   1410
      Width           =   1245
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Height          =   405
      Left            =   1545
      TabIndex        =   1
      Top             =   1410
      Width           =   1245
   End
   Begin VB.TextBox txtSiglaAplicativo 
      Height          =   345
      Left            =   120
      TabIndex        =   0
      Top             =   330
      Width           =   2595
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Digite o par�metro do aplicativo:"
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   720
      Width           =   2280
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Digite a sigla do aplicativo:"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1890
   End
End
Attribute VB_Name = "FrmAplicativo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdOk_Click()

Dim rs   As Recordset
Dim sParametro As String
Dim sEstadoSistema As String, sEstadoAplica��o As String

'''On Erro GoTo Trata_Erro
  
On Local Error GoTo Trata_Erro
  
  If Len(Trim(txtSiglaAplicativo.Text)) = 0 Then
    Call MsgBox("Aplicativo n�o preenchido.")
    txtSiglaAplicativo.SetFocus
    Exit Sub
  End If
  
  '-**- Parte cpf
  SQS = "SELECT TOP 1 m.menu_id, m.nome_menu"
  adSQS "FROM segab_db..verifica_permissao_tb vp WITH (NOLOCK) "
  adSQS "INNER JOIN segab_db..sistema_recurso_tb sr  WITH (NOLOCK) "
  adSQS "   ON sr.recurso_id = vp.recurso_id"
  adSQS "INNER JOIN segab_db..menu_tb m  WITH (NOLOCK) "
  adSQS "   ON m.sistema_id = sr.sistema_id"
  adSQS "  AND m.recurso_id = sr.recurso_id"
  adSQS "INNER JOIN segab_db..usuario_tb u  WITH (NOLOCK) "
  adSQS "   ON u.usuario_id = vp.usuario_id"
  adSQS "WHERE sr.situacao = 'A'"
  adSQS "  AND m.nome_executavel = '" & txtSiglaAplicativo.Text & "'"
  adSQS "  AND LTRIM( RTRIM( ISNULL( m.parametro, ''))) = '" & Trim(txtParametro.Text) & "'"
  adSQS "  AND vp.unidade_usuario_id = " & glEmpresa_id
  adSQS "  AND vp.ambiente_id = " & glAmbiente_id_seg2
  adSQS "  AND u.cpf = '31684170842'"
  adSQS "  AND u.situacao = 'A'"
  Set rs = rdocn.Execute(SQS)
    
  If rs.EOF Then
    sParametro = ""
    SQS = "SELECT DISTINCT m.parametro"
    adSQS "FROM segab_db..verifica_permissao_tb vp  WITH (NOLOCK) "
    adSQS "INNER JOIN segab_db..sistema_recurso_tb sr  WITH (NOLOCK) "
    adSQS "   ON sr.recurso_id = vp.recurso_id"
    adSQS "INNER JOIN segab_db..menu_tb m  WITH (NOLOCK) "
    adSQS "   ON m.sistema_id = sr.sistema_id"
    adSQS "  AND m.recurso_id = sr.recurso_id"
    adSQS "INNER JOIN segab_db..usuario_tb u  WITH (NOLOCK) "
    adSQS "   ON u.usuario_id = vp.usuario_id"
    adSQS "WHERE sr.situacao = 'A'"
    adSQS "  AND m.nome_executavel = '" & txtSiglaAplicativo.Text & "'"
    adSQS "  AND vp.unidade_usuario_id = " & glEmpresa_id
    adSQS "  AND vp.ambiente_id = " & glAmbiente_id_seg2
    adSQS "  AND u.cpf = '" & gsCPF & "'"
    adSQS "  AND u.situacao = 'A'"
    Set rs = rdocn.Execute(SQS)
    Do While Not rs.EOF
      Inc sParametro, rs("parametro") & " ou "
      rs.MoveNext
    Loop
    If InStr(sParametro, "ou") > 0 Then
      sParametro = Mid(sParametro, 1, Len(sParametro) - 4)
    End If
    If sParametro = "" Then
      Call MsgBox("Execut�vel n�o encontrado ou usu�rio sem permiss�o.")
    Else
      Call MsgBox("Utilize um dos par�metros: " & sParametro & ".")
    End If
    Exit Sub
  End If
  
  Call BuscarSituacaoSistema(rs("menu_id"), sEstadoSistema, sEstadoAplica��o)
  If InStr(LCase(sEstadoSistema + sEstadoAplica��o), "l") = 0 Then
'  Select Case lCase(sEstadoAplica��o)
'    Case "l", "lc", "c"
'      If sEstadoSistema = "b" Then
        Call MsgBox("O sistema SEGBR ou o programa " & txtSiglaAplicativo.Text & _
                    " est� bloqueado.")
        Exit Sub
'      End If
'    Case "b", "bc", "bl", "blc"
'  End Select
  End If
  
  Unload Me
      
  Call ExecutarAplicativo(rs("menu_id"), rs("nome_menu"))
  
  Exit Sub
  
Trata_Erro:
  Call TrataErroGeral("cmdOk_Click", Me.name)
End Sub

