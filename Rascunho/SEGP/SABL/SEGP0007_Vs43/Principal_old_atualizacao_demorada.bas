Attribute VB_Name = "Principal"
Option Explicit

Public oSABL0010           As Object
Public gsTabelaTempor�riaUsu�rio As String
Public gsComponentesAtualizados  As String

Public Function ExecutarAplicativo(lMenuId As Long, sMenu As String)
'Executa os aplicativos do menu

  On Error GoTo Trata_Erro
  Const lMAX_EXPIRA��O = 30
  Dim sSQL       As String
  Dim rs         As Recordset
  Dim sParametro As String
  Dim sTabTemp   As String
  Dim lExpira��o As Integer
  
  If frmVerificarComponentes.Visible Then
    frmVerificarComponentes.SetFocus
    Exit Function
  End If
  lExpira��o = 0
  sTabTemp = """##" & gsMac & "-componentes"""
  SQS = "CREATE TABLE " & sTabTemp & "("
  adSQS "  componente VARCHAR( 20) NOT NULL,"
  adSQS "  estado CHAR(1) NOT NULL"
  adSQS ")"
  Call oSABL0010.ExecutaAmbiente(SQS)
  
  '''sSQL = "       SELECT * " & vbNewLine
  sSQL = "       SELECT 1 " & vbNewLine
  sSQL = sSQL & "  FROM segab_db..menu_tb (nolock) " & vbNewLine
  sSQL = sSQL & " WHERE menu_pai_id = " & lMenuId & vbNewLine
  Set rs = oSABL0010.ExecutaAmbiente(sSQL)
  If Not rs.EOF Then
    Set rs = Nothing
    gsComponentesAtualizados = "SEGPMO001"
    frmVerificarComponentes.Show vbModeless
    frmVerificarComponentes.SetFocus
    
    SQS = "INSERT " & sTabTemp
    adSQS "VALUES ( " & FormatoSQL("SEGPMO001", "v")
    Call oSABL0010.ExecutaAmbiente(SQS)
    frmVerificarComponentes.Tag = ExecutaPrograma("SEGPMO001.exe", "SEGPMO001.exe", "", vbNormalFocus, oSABL0010)
    GoTo ApagaTempor�ria
  End If
  
  Set rs = Nothing
    
  sSQL = "       SELECT nome_executavel, sigla_recurso, parametro, recurso_id "
  'LTRIM(RTRIM(nome_executavel)) nome_executavel, sigla_recurso, "
  ''sSql = sSql & "       ISNULL(parametro, '') parametro "
  sSQL = sSQL & "  FROM segab_db..menu_tb (nolock) "
  sSQL = sSQL & " WHERE menu_id = " & lMenuId
  Set rs = oSABL0010.ExecutaAmbiente(sSQL)
    
  If Not rs.EOF Then
  
    'Se houver a op��o mas o programa n�o estiver cadastrado enviar mensagem
    If IsNull(rs("nome_executavel")) Then
      Call MsgBox("Programa para a op��o " & sMenu & " n�o cadastrado." & vbNewLine & "Favor entrar em contato com a Tecnologia.")
      GoTo ApagaTempor�ria
    End If
    
    glRecursoAcionado = rs("recurso_id")
    If IsNull(rs("parametro")) Then
      sParametro = " "
    Else
      sParametro = rs("parametro")
    End If

    gsComponentesAtualizados = rs("NOME_EXECUTAVEL")
    frmVerificarComponentes.Show vbModeless
    frmVerificarComponentes.SetFocus
    SQS = "INSERT " & sTabTemp
    adSQS "VALUES ( " & FormatoSQLF(")", rs("NOME_EXECUTAVEL"), "v")
    Call oSABL0010.ExecutaAmbiente(SQS)
    
    'executa o programa solicitado
    'Call BuscaObjetosDoPrograma(rs("NOME_EXECUTAVEL"))
           
    'If Trim(UCase(rs("NOME_EXECUTAVEL"))) = "SGDI_1" Then
    '  Call Shell("\\sisab005\aplicacoes\sisbr\sisbr\" & rs("NOME_EXECUTAVEL") & ".exe " & sParametro, vbNormalFocus)
    'Else
      If ProgramaAtivado(rs("NOME_EXECUTAVEL") & ".exe", sParametro, vbNormalFocus, oSABL0010) Then
        Unload frmVerificarComponentes
        lExpira��o = lMAX_EXPIRA��O
      End If
      frmVerificarComponentes.Tag = ExecutaPrograma(rs("NOME_EXECUTAVEL") & ".exe", rs("NOME_EXECUTAVEL") & ".exe", sParametro, vbNormalFocus, oSABL0010)
    'End If

  End If
  
  Set rs = Nothing
  If lExpira��o = 0 Then
    frmVerificarComponentes.SetFocus
  End If

ApagaTempor�ria:
  
  SQS = "SELECT componente"
  adSQS "FROM " & sTabTemp
  adSQS "WHERE estado = 't'" ' terminou a verifica��o
  Do
    Set rs = Nothing
    DoEvents
    Set rs = oSABL0010.ExecutaAmbiente(SQS)
    Pause 0.5 'segundo
    Inc lExpira��o
  Loop While rs.EOF And lExpira��o < lMAX_EXPIRA��O
  Set rs = Nothing
  SQS = "SELECT componente"
  adSQS "FROM " & sTabTemp
  adSQS "WHERE estado = 'a'" ' atualizou componentes
  Set rs = oSABL0010.ExecutaAmbiente(SQS)
  gsComponentesAtualizados = ""
  While Not rs.EOF
    Inc gsComponentesAtualizados, rs("componente") & vbCrLf
    rs.MoveNext
  Wend
  Set rs = Nothing
  If gsComponentesAtualizados = "" Then
    Unload frmVerificarComponentes
  Else
    frmVerificarComponentes.SetFocus
    frmVerificarComponentes.cmdFechar.Visible = True
    frmVerificarComponentes.fraComponentesAtualizados.Top = 1320
    frmVerificarComponentes.anmArquivo.AutoPlay = False
    frmVerificarComponentes.lblArquivo = "Os componentes j� foram verificados."
    frmVerificarComponentes.txtComponentesAtualizados = gsComponentesAtualizados
    frmVerificarComponentes.tmrTemporizador.Enabled = True
    frmVerificarComponentes.Height = 5000
  End If
  SQS = "DROP TABLE " & sTabTemp
  Call oSABL0010.ExecutaAmbiente(SQS)
  
  Exit Function
Trata_Erro:
  Set rs = Nothing
  Call TrataErroGeral("ExecutarAplicativo", "Principal.bas")
  SQS = "DROP TABLE " & sTabTemp
  Call oSABL0010.ExecutaAmbiente(SQS)
End Function

'Verifica se existe alguma mensagem na tabela
Public Sub VerificaMensagem(Optional ByVal ADataReferencia = "dt_inclusao", _
                            Optional objAmbiente As Object)

  On Local Error GoTo TrataErro
  Dim sDataHoraMensagem As String
  Dim RX As Object, Criou As Boolean
  
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  SQS = "SELECT dt_inclusao, dt_validade, mensagem,"
  adSQS "       usuario, CONVERT(DATETIME, GETDATE()) hora_atual"
  adSQS "FROM mensagem_tb"
  adSQS "WHERE DATEDIFF( MINUTE, " & FormatoSQL(sDataHoraMensagem) & _
                              ", " & ADataReferencia & ") > 0"
  adSQS "ORDER BY dt_inclusao" '& ADataReferencia
  
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  
  If Not RX.EOF Then
    Dim EstavaVisivel
    EstavaVisivel = frmPrincipal.Visible
    frmPrincipal.WindowState = vbMaximized
    frmPrincipal.Visible = True
    frmPrincipal.SetFocus
    While Not RX.EOF
      Mensagem cUserName & "," & vbNewLine & vbNewLine & _
               UCase(RX("usuario")) & ", em " & Format(RX("dt_inclusao"), "dd/mm/yyyy") & _
               " �s " & Format(RX("dt_inclusao"), "hh:nn") & "h" & _
               ", informa:" & vbNewLine & vbNewLine & _
               RX("mensagem") & vbNewLine & vbNewLine & _
               "Mensagem v�lida at� " & Format(RX("dt_validade"), "dd/mm/yyyy hh:nn") & _
               "h" & vbNewLine & vbNewLine & "Hora atual: " & RX("hora_atual") & "h"
      sDataHoraMensagem = RX("dt_inclusao") 'ADataReferencia)
      RX.MoveNext
    Wend
    frmPrincipal.Visible = EstavaVisivel
  End If
  RX.Close
  Set RX = Nothing
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Sub
TrataErro:
  If Criou Then
    Set objAmbiente = Nothing
  End If
  TrataErroGeral "VerificaMensagem", "SEGP0007"
End Sub
'----------------------------------------------------------------------------------------------
' verifica se deve fechar alguma a aplica��o SEGBR
Public Function FechaProgramaSolicitado(Optional objAmbiente As Object) As Long
  On Local Error GoTo TrataErro
  Dim RX As Object, AInstanciaProcesso As Long, Criou As Boolean
  ' pega inst�ncias
  
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  SQS = "SELECT instancia"
  adSQS "FROM " & gsTabelaTempor�riaUsu�rio
  adSQS "WHERE id_aplicacao = " & FormatoSQL(gsCODPRINCIPAL)
  adSQS "  AND fecha_aplicacao = 1"
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  If Not RX.EOF Then
    Call objAmbiente.ExecutaAmbiente("DELETE " & gsTabelaTempor�riaUsu�rio & _
                                     " WHERE id_aplicacao = " & FormatoSQL(gsCODPRINCIPAL))
     Unload frmPrincipal
    End
  End If
  RX.Close
  Set RX = Nothing
  SQS = "SELECT instancia"
  adSQS "FROM " & gsTabelaTempor�riaUsu�rio
  adSQS "WHERE id_aplicacao <> " & FormatoSQL(gsCODPRINCIPAL)
  adSQS "  AND fecha_aplicacao = 1"
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  While Not RX.EOF
    AInstanciaProcesso = Val(RX(0))
    Call Fecha_Aplicativos(AInstanciaProcesso)
    RX.MoveNext
  Wend
  RX.Close
  Set RX = Nothing
  Call VerificaProgramasAbertos(False, objAmbiente)
  SQS = "SELECT COUNT(*)"
  adSQS "FROM " & gsTabelaTempor�riaUsu�rio
  adSQS "WHERE id_aplicacao <> " & FormatoSQL(gsCODPRINCIPAL)
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  FechaProgramaSolicitado = RX(0)
  RX.Close
  Set RX = Nothing
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Function
TrataErro:
  If Criou Then
    Set objAmbiente = Nothing
  End If
  TrataErroGeral "FechaProgramaSolicitado", "SEGP0007"
End Function
Public Sub BuscaObjetosDoPrograma(Programa As String)
On Error GoTo TratarErro
Dim SQL As String
Dim rc As Recordset
Dim vAmbiente As String

    SQL = ""
    SQL = SQL & " SELECT op.programa, op.objeto, "
    '''SQL = SQL & "        acao = isnull(tob.acao,''), comando = isnull(tob.comando, ''), parametros = isnull(tob.parametros, '') "
    SQL = SQL & "        tob.acao, tob.comando, tob.parametros "
    SQL = SQL & " FROM segab_db..objeto_programa_tb op "
    SQL = SQL & "      LEFT JOIN segab_db..tipo_objeto_tb tob "
    SQL = SQL & "      ON tob.tipo_objeto_id = op.tipo_objeto_id "
    SQL = SQL & " WHERE op.programa = '" & Programa & "' "
    Set rc = rdocn.Execute(SQL)
    
    If Not rc.EOF Then
        Do While Not rc.EOF
            If Not AtualizaVersao2(rc!OBJETO, gsPastaServidorSegbr, gsPastaLocalSegbr, "", _
                rc!Acao, rc!Comando, rc!Parametros) Then GoTo TratarErro
            rc.MoveNext
        Loop
    End If
    
    Exit Sub
    Resume
TratarErro:
    Call TrataErroGeral("BuscaObjetosDoPrograma", "SEGP0007")
End Sub

Public Sub RegistrarFonteDados(objAmbiente As Object)
  'cria todas as fontes de ODBC na conta do usu�rio
  On Local Error GoTo TrataExce��o
  Dim RX As ADODB.Recordset, sAtributos As String, sSistema
  Dim sServidor As String, sBanco As String, sNomeFonte As String
  
  SQS = "SELECT sigla_sistema, valor"
  adSQS "FROM parametro_tb"
  adSQS "WHERE secao = 'ODBC'"
  adSQS "  AND campo = 'Fonte'"
  Set RX = objAmbiente.ExecutaAmbiente(SQS)
  While Not RX.EOF
    sSistema = Trim(RX("sigla_sistema"))
    sServidor = objAmbiente.RetornaValorAmbiente(sSistema, "Conexao", "Servidor", gsCHAVESISTEMA, glAmbiente_id)
    sBanco = objAmbiente.RetornaValorAmbiente(sSistema, "Conexao", "Banco", gsCHAVESISTEMA, glAmbiente_id)
    sNomeFonte = Trim(RX("valor"))
    sAtributos = "Description="
    Inc sAtributos, "Conex�o com o " & sSistema
    Inc sAtributos, Chr$(13) & "OemToAnsi=No"
    Inc sAtributos, Chr$(13) & "SERVER=" & sServidor
    Inc sAtributos, Chr$(13) & "Database=" & sBanco
    rdoEngine.rdoRegisterDataSource sNomeFonte, "SQL Server", True, sAtributos
    RX.MoveNext
  Wend
  RX.Close
  Set RX = Nothing
  Exit Sub
TrataExce��o:
  Set RX = Nothing
  Call TrataErroGeral("RegistrarFonteDados", "SEGP0007")
End Sub

Public Function BuscarSituacaoSistema(sMenuID As String, sStatusSistema As String, sSituacaoSistema As String) As Boolean
'verifica qual a situacao que o n� do menu poder� ser executado
'b  - bloqueado
'l  - liberado
'lb - bloqueado/liberado

Dim sSQL As String
Dim rs   As Recordset

On Error GoTo Trata_Erro

  BuscarSituacaoSistema = False
        
  sSQL = "      SELECT status_sistema "
  sSQL = sSQL & " FROM seguros_db..parametro_geral_tb "
  Set rs = rdocn.Execute(sSQL)
  'Set rs = .ExecutaAmbiente(sSQL)
  If Not rs.EOF Then
    
    sStatusSistema = rs("status_sistema")
    
    '''sSQL = "       SELECT segab_db..menu_tb.* "
    sSQL = "       SELECT segab_db..menu_tb.depend_execucao "
    sSQL = sSQL & "  FROM segab_db..menu_tb (nolock) "
    sSQL = sSQL & " WHERE menu_id = " & sMenuID
                
    Set rs = rdocn.Execute(sSQL)
    
    If Not rs.EOF Then
      BuscarSituacaoSistema = True
      sSituacaoSistema = rs("depend_execucao")
    Else
      sSituacaoSistema = ""
    End If
  End If
    
  Exit Function

Trata_Erro:
  Call TrataErroGeral("BuscarSituacaoSistema", "Principal.bas")
End Function

'----------------------------------------------------------------------------------------------
' cria tabela tempor�ria do usu�rio
Public Sub CriaTabelaTemporaria(Optional objAmbiente As Object)
  On Local Error GoTo TrataErro
  Dim Criou As Boolean
  
  If objAmbiente Is Nothing Then
    Set objAmbiente = CreateObject(gsOBJETOAMBIENTE)
    Criou = True
  Else
    Criou = False
  End If
  
  If Criou Then
    Set objAmbiente = Nothing
  End If
    
  SQS = "CREATE TABLE " & gsTabelaTempor�riaUsu�rio & "("
  adSQS "  usuario VARCHAR( 20) NULL,"
  adSQS "  computador VARCHAR( 15) NULL,"
  adSQS "  ip VARCHAR( 15) NULL,"
  adSQS "  mac CHAR( 12) NULL,"
  adSQS "  aplicacao VARCHAR( 50) NOT NULL,"
  adSQS "  id_aplicacao VARCHAR( 40) NOT NULL,"
  adSQS "  parametro_aplicacao VARCHAR( 250) NOT NULL"
  adSQS "    CONSTRAINT C" & Mid(cUserName & gsMac, 1, 29) & _
               " UNIQUE (id_aplicacao, parametro_aplicacao),"
  adSQS "  dt_inicio_aplicacao SMALLDATETIME NOT NULL,"
  adSQS "  instancia INT NOT NULL, "
  adSQS "  fecha_aplicacao BIT"
  adSQS ")"
  
  'set objAmbiente = Nothing
  Call objAmbiente.ExecutaAmbiente(SQS)
  
  IncluiRegTemp gsCODPRINCIPAL, "", gsSIGLASISTEMA, 0, cUserName, gsHost, gsIp, gsMac, , objAmbiente
  
  If Criou Then
    Set objAmbiente = Nothing
  End If
  Exit Sub
TrataErro:
  If Criou Then
    Set objAmbiente = Nothing
  End If
  TrataErroGeral "CriaTabelaTemporaria", "Principal.bas"
  MsgBox "Programa j� est� aberto."
  TerminaSEGBR
End Sub

Public Function fprCarregarTree(pai As Integer) As Boolean
On Error GoTo TratarErro
Dim rc As Recordset
Dim SQL As String
Dim a As Variant
fprCarregarTree = False
Dim parametro As String
Dim OBJETO As String
Dim Programa As String

 Set rc = Nothing
 

'carrega o treeview de acordo com a tabela segab_db..menu_tb
'sistema_id = 3 - sistema SEGBR
SQL = "SELECT   DISTINCT MENU_PAI_ID, MENU_ID, NOME_MENU, ISNULL(LTRIM(RTRIM(NOME_EXECUTAVEL)), '') " _
    & "         NOME_EXECUTAVEL, M.SIGLA_RECURSO " _
    & "FROM     SEGAB_DB..MENU_TB M (nolock) LEFT JOIN SEGAB_DB..RECURSO_TB R " _
    & "ON       M.RECURSO_ID = R.RECURSO_ID " _
    & "WHERE    SISTEMA_ID IN(1,3) " _
    & "AND      MENU_PAI_ID IS NOT NULL " _
    & "AND      MENU_PAI_ID = " & pai & " " _
    & "ORDER BY MENU_PAI_ID, NOME_MENU"
    '& "AND      (R.SITUACAO ='A' " _
    '& "OR       MENU_ID IN (SELECT MENU_PAI_ID FROM SEGAB_DB..MENU_TB)) "


Set rc = rdocn.Execute(SQL)
If Not rc.EOF Then
    Do While Not rc.EOF
        Programa = ""
        OBJETO = ""
        If rc!nome_executavel <> "" Then
            'artificio para modifica��o do nome do exe
            'retirar ap�s o acerto da base
            If Not IsNull(rc!sigla_recurso) Then
                
                If Len(rc!sigla_recurso) <> 0 Then
                    
                    Select Case Mid(rc!sigla_recurso, Len(rc!sigla_recurso) - 1, 2)
                    Case ".1"
                        'inclus�o
                        Programa = Mid(rc!nome_executavel, 1, 4) & "9" & Mid(rc!nome_executavel, 6, 3)
                    Case ".2"
                        'altera��o
                        Programa = Mid(rc!nome_executavel, 1, 4) & "8" & Mid(rc!nome_executavel, 6, 3)
                    Case ".3"
                        'exclus�o
                        Programa = Mid(rc!nome_executavel, 1, 4) & "7" & Mid(rc!nome_executavel, 6, 3)
                    Case Else
                        'consulta
                        Programa = rc!nome_executavel
                    
                    End Select
                Else
                    Programa = rc!nome_executavel
                End If
            Else
                Programa = rc!nome_executavel
            End If
           
            frmPrincipal.Tree.Nodes.Add "SEGBR" & rc!MENU_PAI_ID, tvwChild, "SEGBR" & rc!MENU_ID, UCase(Programa & " - " & rc!nome_menu), "Programa"
        Else
            frmPrincipal.Tree.Nodes.Add "SEGBR" & rc!MENU_PAI_ID, tvwChild, "SEGBR" & rc!MENU_ID, UCase(rc!nome_menu), "NoFechado"
        End If
    
        rc.MoveNext
    Loop
Else
    '''SQL = "SELECT * FROM SEGAB_DB..MENU_TB (nolock) WHERE MENU_PAI_ID = " & pai
    SQL = "SELECT 1 FROM SEGAB_DB..MENU_TB (nolock) WHERE MENU_PAI_ID = " & pai
    Set rc = rdocn.Execute(SQL)
    If Not rc.EOF Then
        'a = ExecutaPrograma("SEGPMO001.exe", "SEGPMO001.exe", "", vbNormalFocus)
        Exit Function
    End If
    
    SQL = "SELECT LTRIM(RTRIM(NOME_EXECUTAVEL)) NOME_EXECUTAVEL, ISNULL(PARAMETRO,'') PARAMETRO FROM " _
        & "SEGAB_DB..MENU_TB (nolock) WHERE MENU_ID = " _
        & Mid(frmPrincipal.Tree.SelectedItem.Key, 6, Len(frmPrincipal.Tree.SelectedItem.Key) - 5)
    Set rc = rdocn.Execute(SQL)
    If Not rc.EOF Then
        'se houver a op��o mas o programa n�o estiver cadastrado enviar mensagem
        If IsNull(rc!nome_executavel) Then
            'MsgBox "Programa para a op��o " & Tree.SelectedItem.Text & " n�o cadastrado." & vbNewLine _
            & "Favor entrar em contato com a Tecnologia."
            Exit Function
        If IsNull(rc!parametro) Then
        End If
            parametro = " "
        Else
            parametro = rc!parametro
        End If
        'executa o programa solicitado
        'BuscaObjetosDoPrograma (rc!nome_executavel)
           
        If Trim(UCase(rc!nome_executavel)) = "SGDI_1" Then
            'a = Shell("\\sisab005\aplicacoes\sisbr\sisbr\" & rc!nome_executavel & ".exe " & parametro, vbNormalFocus)
        Else
            'a = ExecutaPrograma(rc!nome_executavel & ".exe", rc!nome_executavel & ".exe", parametro, vbNormalFocus)
        End If
    End If

End If
Exit Function
Resume
TratarErro:
    fprCarregarTree = False
    TrataErroGeral "fprCarregarTree", "frmPrincipal.frm"
End Function
