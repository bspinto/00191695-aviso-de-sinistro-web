VERSION 5.00
Begin VB.Form frmSair 
   Caption         =   "SEGBR"
   ClientHeight    =   1635
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   1635
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdNao 
      Caption         =   "N�o"
      Height          =   375
      Left            =   3360
      TabIndex        =   2
      Top             =   1200
      Width           =   1335
   End
   Begin VB.CommandButton cmdSim 
      Caption         =   "Sim"
      Height          =   375
      Left            =   1920
      TabIndex        =   1
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      Begin VB.Label lblMensagem 
         Caption         =   "lblMensagem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   735
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   4455
      End
   End
End
Attribute VB_Name = "frmSair"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdNao_Click()
    frmPrincipal.SetFocus
    Unload Me
End Sub

Private Sub cmdSim_Click()
    On Local Error GoTo TrataErro
    Call VerificaProgramasAbertos(True, GAmbienteGlobal)
    Set GAmbienteGlobal = Nothing
    Call TerminaSEGBR
    Exit Sub
TrataErro:
    Resume Next
End Sub

Private Sub Form_Load()
    lblMensagem.Caption = "Deseja fechar todas as aplica��es do SEGBR?"
End Sub

