﻿Imports System.Configuration
Imports System.Collections.Specialized

'Structs do layout
'Obs: No construtor das structs, informar o nome da chave do app.config que 
'possui sua devida configuração, o nome das chaves sao as constantes da classe LayoutHelper
#Region "Codigo da Transacao"
Public Structure CodTransacao
    Dim posicao_inicial As Integer
    Dim qtd_caracteres As Integer

    Public Sub SetValues()
        Me.posicao_inicial = 0
        Me.qtd_caracteres = 4
    End Sub
End Structure
#End Region

#Region "Evento BB"
Public Structure EventoBB
    Dim posicao_inicial As Integer
    Dim qtd_caracteres As Integer
    Public Sub SetValues()
        Me.posicao_inicial = 17
        Me.qtd_caracteres = 4
    End Sub
End Structure
#End Region

#Region "Quantidade de Registros"
Public Structure QtdRegistros
    Dim posicao_inicial As Integer
    Dim qtd_caracteres As Integer
    Public Sub SetValues()
        Me.posicao_inicial = 4
        Me.qtd_caracteres = 5
    End Sub
End Structure
#End Region