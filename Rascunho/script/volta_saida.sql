--volta_saida
declare @dt smalldatetime = '2019-11-18'
     update a
       set msg_id = NULL
       , situacao = 'N'
       , dt_coa = null
       ,dt_cod = null
       , dt_alteracao = null
     from interface_db.dbo.saida_gtr_atual_tb a with (nolock)
     where msg_id = 'BSP'
     and situacao = 'x'
     and saida_gtr_id not IN (23110376,23110377)
     
     update a
       set msg_id = NULL
       , situacao = 'T'
       , dt_coa = null
       ,dt_cod = null
       , dt_alteracao = null
     from interface_db.dbo.saida_gtr_atual_tb a with (nolock)
     where msg_id = 'BSP_T'
     and situacao = 'x'     
     and saida_gtr_id not IN (23110376,23110377)