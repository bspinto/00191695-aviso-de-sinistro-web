VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmHistoricoProposta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Hist�rico Proposta"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   420
   ClientWidth     =   9420
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   9420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fmeHistoricoProposta 
      Caption         =   "Hist�rico Proposta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3570
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   9225
      Begin RichTextLib.RichTextBox txtDetalheHistorico 
         Height          =   1455
         Left            =   120
         TabIndex        =   2
         Top             =   1920
         Width           =   8895
         _ExtentX        =   15690
         _ExtentY        =   2566
         _Version        =   393217
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         TextRTF         =   $"frmHistoricoProposta.frx":0000
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdHistorico 
         Height          =   1545
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   8835
         _ExtentX        =   15584
         _ExtentY        =   2725
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   0
         SelectionMode   =   1
         FormatString    =   $"frmHistoricoProposta.frx":0080
      End
   End
   Begin VB.CommandButton btnSair 
      Caption         =   "Voltar"
      Height          =   375
      Left            =   8280
      TabIndex        =   0
      Top             =   3840
      Width           =   1095
   End
End
Attribute VB_Name = "frmHistoricoProposta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Paulo Pelegrini - MU-2017-063260 - 16/07/2018

Private sTodasDescricoes() As String
Public proposta_hist As Long


Private Sub btnSair_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call CentraFrm(Me)

    Call SetWindowPos(frmHistoricoProposta.hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)

    FrmConsultaSinistrocon.Enabled = False

    With grdHistorico
        .ColWidth(2) = 0
        .ColWidth(4) = 0
    End With

    CarregarHistoricoProposta

End Sub


Private Sub Form_Unload(Cancel As Integer)
    FrmConsultaSinistrocon.Enabled = True
End Sub


Private Function CarregarHistoricoProposta() As Boolean

    Dim oHistorico As Object
    Dim rsHistorico As Recordset
    Dim lCont As Long
    
    On Error GoTo TrataErro
    
    CarregarHistoricoProposta = True
    
    grdHistorico.Rows = 2
    
    lCont = 1

    Set oHistorico = CreateObject("SEGL0026.cls00406")
        
    
    Set rsHistorico = oHistorico.ConsultarComentarioProposta(gsSIGLASISTEMA, App.Title, App.FileDescription, glAmbiente_id, 3, proposta_hist, cUserName, 0, 0, 0, False, , True)
    
    If Not rsHistorico.EOF Then
    
        grdHistorico.Rows = 1
        
        While Not rsHistorico.EOF
        
            grdHistorico.AddItem lCont & vbTab & rsHistorico("assunto") & vbTab & rsHistorico("comentario_id") & vbTab & Format(rsHistorico("dt_inclusao"), "dd/mm/yyyy") & vbTab
                                 
            Call MontarTodasDescricoes(rsHistorico, lCont)
        
            rsHistorico.MoveNext
            
            lCont = lCont + 1
        
        Wend

    Else
    
        grdHistorico.HighLight = flexHighlightNever
    
    End If
    
    Set oHistorico = Nothing
    Set rsHistorico = Nothing
    
    Exit Function
    
TrataErro:
    CarregarHistoricoProposta = False
    Call TratarErro("CarregarHistoricoProposta", Me.Name)
    Call FinalizarAplicacao
    
End Function



Private Sub MontarTodasDescricoes(ByRef rsDescricao As Recordset, _
                                  ByVal lNumComentario As Long)

    Dim sDescricao As String

    On Error GoTo TrataErro
    
    'Cabe�alho da descricao
    sDescricao = ""
    
    sDescricao = sDescricao & "Coment�rio: " & lNumComentario & vbNewLine & vbNewLine
    
    sDescricao = sDescricao & "Assunto: " & rsDescricao("assunto") & vbNewLine & vbNewLine
    
    sDescricao = sDescricao & "Data de Inclus�o: " & Format(rsDescricao("dt_inclusao"), "dd/mm/yyyy") & vbNewLine & vbNewLine
    
    sDescricao = sDescricao & "Usu�rio de Inclus�o: " & rsDescricao("nome") & vbNewLine & vbNewLine
    
    If LCase(rsDescricao("restrito")) = "s" Then
        sDescricao = sDescricao & "Restri��o: Restrito" & vbNewLine & vbNewLine
    Else
        sDescricao = sDescricao & "Restri��o: N�o restrito" & vbNewLine & vbNewLine
    End If
        
    'Descri��o
    sDescricao = sDescricao & "Descri��o:" & vbNewLine & vbNewLine
        
    If UCase(Trim(rsDescricao("restrito"))) = "S" _
    And UCase(Trim(rsDescricao("usuario_unidade_permitida"))) = "N" Then
        sDescricao = sDescricao & "Coment�rio restrito" & vbNewLine & vbNewLine
    Else
        sDescricao = sDescricao & rsDescricao("descricao") & vbNewLine & vbNewLine
    End If
    
    ReDim Preserve sTodasDescricoes(lNumComentario + 1) As String
    
    sTodasDescricoes(lNumComentario) = sDescricao
    
    Exit Sub
    
TrataErro:
    Call TratarErro("MontarTodasDescricoes", Me.Name)
    Call FinalizarAplicacao
End Sub


Private Sub grdHistorico_Click()
    If grdHistorico.TextMatrix(grdHistorico.Row, 0) <> "" Then
        grdHistorico.HighLight = flexHighlightAlways
        txtDetalheHistorico.Text = sTodasDescricoes(grdHistorico.TextMatrix(grdHistorico.Row, 0))
    End If
End Sub

Private Sub grdHistorico_SelChange()
    If grdHistorico.Row <> grdHistorico.RowSel Then
        grdHistorico.RowSel = grdHistorico.Row
    End If
End Sub

