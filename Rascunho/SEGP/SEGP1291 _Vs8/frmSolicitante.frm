VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form FrmSolicitante 
   Caption         =   "SEGP1291 - Consulta de Solicitante - VIDA"
   ClientHeight    =   5205
   ClientLeft      =   4740
   ClientTop       =   3150
   ClientWidth     =   10890
   LinkTopic       =   "frmSolicitante"
   ScaleHeight     =   5205
   ScaleWidth      =   10890
   Begin VB.TextBox TxtAuxiliar 
      Height          =   285
      Left            =   240
      TabIndex        =   62
      Text            =   "Cancelado"
      Top             =   4800
      Width           =   1095
   End
   Begin VB.CommandButton btnAplicar 
      Caption         =   "Aplicar"
      Height          =   375
      Left            =   7800
      TabIndex        =   30
      Top             =   4680
      Width           =   1452
   End
   Begin VB.CommandButton btnVoltar 
      Caption         =   "Voltar"
      Height          =   375
      Left            =   9360
      TabIndex        =   31
      Top             =   4680
      Width           =   1452
   End
   Begin VB.Frame fmeDadosGerais_Solicitante 
      Caption         =   "Solicitante"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4455
      Left            =   120
      TabIndex        =   32
      Top             =   120
      Width           =   10635
      Begin MSMask.MaskEdBox mskSolicitante_Sinistro_Cep 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "00###-###"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         Height          =   315
         Left            =   9240
         TabIndex        =   7
         Top             =   1680
         Width           =   1140
         _ExtentX        =   2011
         _ExtentY        =   556
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   10
         Mask            =   "##.###-###"
         PromptChar      =   " "
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Email 
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Top             =   1080
         Width           =   2535
      End
      Begin VB.ComboBox cmbSolicitante_Sinistro_Estado 
         Height          =   315
         Left            =   5040
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1680
         Width           =   840
      End
      Begin VB.ComboBox cmbSolicitante_Sinistro_Parentesco 
         Height          =   315
         ItemData        =   "frmSolicitante.frx":0000
         Left            =   6480
         List            =   "frmSolicitante.frx":0007
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   3900
      End
      Begin VB.ComboBox cmbSolicitante_Municipio_Nome 
         Height          =   315
         Left            =   6120
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1680
         Width           =   3000
      End
      Begin VB.Frame fmeSolicitante_Sinistro_Telefone 
         Caption         =   "Contatos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2175
         Left            =   240
         TabIndex        =   33
         Top             =   2040
         Width           =   10125
         Begin VB.TextBox txtSolicitante_Sinistro_Ramal4 
            Height          =   330
            Left            =   7560
            MaxLength       =   4
            TabIndex        =   22
            Top             =   1080
            Width           =   915
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Ramal2 
            Height          =   330
            Left            =   7560
            MaxLength       =   4
            TabIndex        =   14
            Top             =   480
            Width           =   915
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Ramal5 
            Height          =   330
            Left            =   2520
            MaxLength       =   4
            TabIndex        =   26
            Top             =   1680
            Width           =   915
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Ramal3 
            Height          =   330
            Left            =   2520
            MaxLength       =   4
            TabIndex        =   18
            Top             =   1080
            Width           =   915
         End
         Begin VB.TextBox txtSolicitante_Sinistro_Ramal1 
            Height          =   330
            Left            =   2520
            MaxLength       =   4
            TabIndex        =   10
            Top             =   480
            Width           =   915
         End
         Begin VB.ComboBox cmbTp_Telefone5 
            Height          =   315
            ItemData        =   "frmSolicitante.frx":0012
            Left            =   3600
            List            =   "frmSolicitante.frx":0019
            Style           =   2  'Dropdown List
            TabIndex        =   27
            Top             =   1680
            Width           =   1380
         End
         Begin VB.ComboBox cmbTp_Telefone4 
            Height          =   315
            ItemData        =   "frmSolicitante.frx":0025
            Left            =   8640
            List            =   "frmSolicitante.frx":002C
            Style           =   2  'Dropdown List
            TabIndex        =   23
            Top             =   1080
            Width           =   1380
         End
         Begin VB.ComboBox cmbTp_Telefone2 
            Height          =   315
            ItemData        =   "frmSolicitante.frx":003B
            Left            =   8640
            List            =   "frmSolicitante.frx":0042
            Style           =   2  'Dropdown List
            TabIndex        =   15
            Top             =   480
            Width           =   1380
         End
         Begin VB.ComboBox cmbTp_Telefone3 
            Height          =   315
            ItemData        =   "frmSolicitante.frx":004F
            Left            =   3600
            List            =   "frmSolicitante.frx":0056
            Style           =   2  'Dropdown List
            TabIndex        =   19
            Top             =   1080
            Width           =   1380
         End
         Begin VB.ComboBox cmbTp_Telefone1 
            Height          =   315
            HelpContextID   =   1
            ItemData        =   "frmSolicitante.frx":0064
            Left            =   3600
            List            =   "frmSolicitante.frx":006B
            Style           =   2  'Dropdown List
            TabIndex        =   11
            Top             =   480
            Width           =   1380
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_Telefone1 
            Height          =   315
            Left            =   1080
            TabIndex        =   9
            Top             =   480
            Width           =   1190
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   9
            Mask            =   "#########"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_DDD1 
            Height          =   315
            Left            =   240
            TabIndex        =   8
            Top             =   480
            Width           =   620
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_Telefone3 
            Height          =   315
            Left            =   1080
            TabIndex        =   17
            Top             =   1080
            Width           =   1190
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   9
            Mask            =   "#########"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_DDD3 
            Height          =   315
            Left            =   240
            TabIndex        =   16
            Top             =   1080
            Width           =   620
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_Telefone2 
            Height          =   315
            Left            =   6240
            TabIndex        =   13
            Top             =   480
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   9
            Mask            =   "#########"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_DDD2 
            Height          =   315
            Left            =   5400
            TabIndex        =   12
            Top             =   480
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_Telefone4 
            Height          =   315
            Left            =   6240
            TabIndex        =   21
            Top             =   1080
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   9
            Mask            =   "#########"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_DDD4 
            Height          =   315
            Left            =   5400
            TabIndex        =   20
            Top             =   1080
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_Telefone5 
            Height          =   315
            Left            =   1080
            TabIndex        =   25
            Top             =   1680
            Width           =   1190
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   9
            Mask            =   "#########"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_DDD5 
            Height          =   315
            Left            =   240
            TabIndex        =   24
            Top             =   1680
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_Telefone_Fax 
            Height          =   315
            Left            =   6240
            TabIndex        =   29
            Top             =   1680
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   9
            Mask            =   "#########"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox mskSolicitante_Sinistro_DDD_Fax 
            Height          =   315
            Left            =   5400
            TabIndex        =   28
            Top             =   1680
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   2
            Mask            =   "##"
            PromptChar      =   " "
         End
         Begin VB.Label lblRamal 
            AutoSize        =   -1  'True
            Caption         =   "Ramal 4"
            Height          =   195
            Index           =   3
            Left            =   7560
            TabIndex        =   61
            Top             =   840
            Width           =   585
         End
         Begin VB.Label lblRamal 
            AutoSize        =   -1  'True
            Caption         =   "Ramal 2"
            Height          =   195
            Index           =   1
            Left            =   7560
            TabIndex        =   60
            Top             =   240
            Width           =   585
         End
         Begin VB.Label lblRamal 
            AutoSize        =   -1  'True
            Caption         =   "Ramal 5"
            Height          =   195
            Index           =   4
            Left            =   2520
            TabIndex        =   59
            Top             =   1440
            Width           =   585
         End
         Begin VB.Label lblRamal 
            AutoSize        =   -1  'True
            Caption         =   "Ramal 3"
            Height          =   195
            Index           =   2
            Left            =   2520
            TabIndex        =   58
            Top             =   840
            Width           =   585
         End
         Begin VB.Label lblRamal 
            AutoSize        =   -1  'True
            Caption         =   "Ramal 1"
            Height          =   195
            Index           =   0
            Left            =   2520
            TabIndex        =   57
            Top             =   240
            Width           =   585
         End
         Begin VB.Label LblNumero_Fax_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "Fax  N�mero:"
            Height          =   255
            Index           =   9
            Left            =   6240
            TabIndex        =   55
            Top             =   1440
            Width           =   975
         End
         Begin VB.Label LblDDD_Fax_Solicitante 
            Caption         =   "Fax DDD:"
            Height          =   255
            Index           =   8
            Left            =   5400
            TabIndex        =   54
            Top             =   1440
            Width           =   735
         End
         Begin VB.Label LblTp_Telefone5_Solicitante 
            Caption         =   "Tipo de Telefone 5"
            Height          =   255
            Index           =   4
            Left            =   3600
            TabIndex        =   53
            Top             =   1440
            Width           =   1455
         End
         Begin VB.Label LblNumero_Telefone5_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "5� N�mero:"
            Height          =   255
            Index           =   7
            Left            =   1080
            TabIndex        =   52
            Top             =   1440
            Width           =   855
         End
         Begin VB.Label LblDDD_Telefone5_Solicitante 
            Caption         =   "5� DDD:"
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   51
            Top             =   1440
            Width           =   615
         End
         Begin VB.Label LblTp_Telefone4_Solicitante 
            Caption         =   "Tipo de Telefone 4"
            Height          =   255
            Index           =   3
            Left            =   8640
            TabIndex        =   50
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label LblTp_Telefone2_Solicitante 
            Caption         =   "Tipo de Telefone 2"
            Height          =   255
            Index           =   2
            Left            =   8640
            TabIndex        =   49
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label LblNumero_Telefone2_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "2� N�mero:"
            Height          =   255
            Index           =   5
            Left            =   6240
            TabIndex        =   48
            Top             =   240
            Width           =   855
         End
         Begin VB.Label LblDDD_Telefone2_Solicitante 
            Caption         =   "2� DDD:"
            Height          =   255
            Index           =   4
            Left            =   5400
            TabIndex        =   47
            Top             =   240
            Width           =   615
         End
         Begin VB.Label LblNumero_Telefone4_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "4� N�mero:"
            Height          =   255
            Index           =   1
            Left            =   6240
            TabIndex        =   46
            Top             =   840
            Width           =   855
         End
         Begin VB.Label LblDDD_Telefone4_Solicitante 
            Caption         =   "4� DDD:"
            Height          =   255
            Index           =   0
            Left            =   5400
            TabIndex        =   45
            Top             =   840
            Width           =   615
         End
         Begin VB.Label LblTp_Telefone3_Solicitante 
            Caption         =   "Tipo de Telefone 3"
            Height          =   255
            Index           =   1
            Left            =   3600
            TabIndex        =   44
            Top             =   840
            Width           =   1455
         End
         Begin VB.Label LblTp_Telefone1_Solicitante 
            Caption         =   "Tipo de Telefone 1"
            Height          =   255
            Index           =   0
            Left            =   3600
            TabIndex        =   43
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label LblDDD_Telefone3_Solicitante 
            Caption         =   "3� DDD:"
            Height          =   255
            Index           =   3
            Left            =   240
            TabIndex        =   42
            Top             =   840
            Width           =   615
         End
         Begin VB.Label LblNumero_Telefone3_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "3� N�mero:"
            Height          =   255
            Index           =   2
            Left            =   1080
            TabIndex        =   41
            Top             =   840
            Width           =   855
         End
         Begin VB.Label LblDDD_Telefone1_Solicitante 
            Caption         =   "1� DDD:"
            Height          =   255
            Index           =   25
            Left            =   240
            TabIndex        =   35
            Top             =   240
            Width           =   615
         End
         Begin VB.Label LblNumero_Telefone1_Solicitante 
            Alignment       =   1  'Right Justify
            Caption         =   "1� N�mero:"
            Height          =   255
            Index           =   26
            Left            =   1080
            TabIndex        =   34
            Top             =   240
            Width           =   855
         End
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Nome 
         Height          =   315
         Left            =   240
         MaxLength       =   60
         TabIndex        =   0
         Top             =   480
         Width           =   6015
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Endereco 
         Height          =   315
         Left            =   3000
         MaxLength       =   60
         TabIndex        =   3
         Top             =   1080
         Width           =   7335
      End
      Begin VB.TextBox txtSolicitante_Sinistro_Bairro 
         Height          =   315
         Left            =   240
         MaxLength       =   30
         TabIndex        =   4
         Top             =   1680
         Width           =   4575
      End
      Begin VB.Label LblEmail_Solicitante 
         Caption         =   "Email:"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   64
         Top             =   840
         Width           =   855
      End
      Begin VB.Label LblCep_Solicitante 
         Caption         =   "CEP:"
         Height          =   255
         Index           =   0
         Left            =   9240
         TabIndex        =   63
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label LblParentesco_Solicitante 
         Caption         =   "Grau de Parentesco"
         Height          =   255
         Left            =   6480
         TabIndex        =   56
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label LblEstado_Solicitante 
         Caption         =   "Estado:"
         Height          =   255
         Index           =   21
         Left            =   5040
         TabIndex        =   40
         Top             =   1440
         Width           =   615
      End
      Begin VB.Label LblNome_Solicitante 
         Caption         =   "Nome:"
         Height          =   255
         Index           =   17
         Left            =   240
         TabIndex        =   39
         Top             =   240
         Width           =   615
      End
      Begin VB.Label LblEndereco_Solicitante 
         Caption         =   "Endere�o:"
         Height          =   255
         Index           =   19
         Left            =   3000
         TabIndex        =   38
         Top             =   840
         Width           =   855
      End
      Begin VB.Label LblBairro_Solicitante 
         Caption         =   "Bairro:"
         Height          =   255
         Index           =   18
         Left            =   240
         TabIndex        =   37
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label LblMunicipio_Solicitante 
         Caption         =   "Munic�pio:"
         Height          =   255
         Index           =   20
         Left            =   6120
         TabIndex        =   36
         Top             =   1440
         Width           =   1095
      End
   End
End
Attribute VB_Name = "FrmSolicitante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Conexao
Private lConexaoLocal As Integer

Private bytModoOperacao As Byte         '1-Inclusao / 2-Alteracao/Exclus�o / 3-Consulta
Private intTipoInterface As Integer
Private dSinistro_ID As String
Private lSolicitante_ID As Long
Public vInclui_Solicitante As Boolean

Private Sub btnAplicar_Click()

    On Error GoTo Trata_Erro

    
    If Valida_ManutencaoSolicitante() Then    ' verifica se os bot�es obrigat�rios est�o preenchidos
        Aplicar_Solicitante
    End If

    Exit Sub

Trata_Erro:
    TrataErroGeral "btnAplicar_Click"
    Inclui_Solicitante = False

End Sub

Private Sub Aplicar_Solicitante()

    Dim sSQL As String
    Dim rSql As String
    Dim rsRecordSet As ADODB.Recordset

    On Error GoTo Trata_Erro

    AbrirTransacao (lConexaoLocal)

    Select Case bytModoOperacao

    Case 1    ' Incluir solicitante

        rSql = "SELECT max(solicitante_sinistro_tb.solicitante_id) FROM solicitante_sinistro_tb WITH (NOLOCK) "

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              rSql, _
                                              lConexaoLocal, True)

        lSolicitante_ID = IIf(IsNull(rsRecordSet(0)), 1, rsRecordSet(0) + 1)

        sSQL = "" & vbNewLine
        '            sSQL = "EXEC seguro_db.dbo.SEGS10348_SPI " & lSolicitante_ID & ",'" & txtSolicitante_Sinistro_Nome & "'" & vbNewLine   'AKIO.OKUNO - 23/09/2012
        sSQL = "EXEC seguros_db.dbo.SEGS10348_SPI " & lSolicitante_ID & ",'" & MudaAspaSimples(txtSolicitante_Sinistro_Nome) & "'" & vbNewLine
        'sSQL = "EXEC desenv_db.dbo.SEGS10348_SPI " & lSolicitante_ID & ",'" & txtSolicitante_Sinistro_Nome & "'" & vbNewLine

        sSQL = sSQL & ",'" & MudaAspaSimples(txtSolicitante_Sinistro_Endereco) & "','" & MudaAspaSimples(txtSolicitante_Sinistro_Bairro) & "'," & cmbSolicitante_Municipio_Nome.ItemData(cmbSolicitante_Municipio_Nome.ListIndex) & "" & vbNewLine
        sSQL = sSQL & ",'" & cmbSolicitante_Municipio_Nome & "','" & IIf(cmbSolicitante_Sinistro_Estado <> "--", cmbSolicitante_Sinistro_Estado, "XX") & "'" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD1 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD1) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone1 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone1) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal1 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal1) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone1.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone1.ItemData(cmbTp_Telefone1.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD2 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD2) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone2 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone2) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal2 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal2) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone2.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone2.ItemData(cmbTp_Telefone2.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD3 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD3) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone3 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone3) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal3 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal3) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone3.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone3.ItemData(cmbTp_Telefone3.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD4 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD4) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone4 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone4) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal4 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal4) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone4.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone4.ItemData(cmbTp_Telefone4.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD5 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD5) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone5 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone5) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal5 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal5) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone5.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone1.ItemData(cmbTp_Telefone1.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD_Fax <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD_Fax) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone_Fax <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone_Fax) & "'", "''") & "" & vbNewLine

        If cmbSolicitante_Sinistro_Parentesco.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbSolicitante_Sinistro_Parentesco.ItemData(cmbSolicitante_Sinistro_Parentesco.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & ",'" & cUserName & "'" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Cep <> "", "'" & mskSolicitante_Sinistro_Cep & "'", "''") & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Email <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Email) & "'", "''") & vbNewLine

    Case 2    'Alterar Solicitante

        sSQL = "" & vbNewLine
        '            sSQL = "EXEC seguro_db.dbo.SEGS10347_SPU " & lSolicitante_ID & ",'" & txtSolicitante_Sinistro_Nome & "'" & vbNewLine
        'sSQL = "EXEC desenv_db.dbo.SEGS10347_SPU " & lSolicitante_ID & ",'" & txtSolicitante_Sinistro_Nome & "'" & vbNewLine
        sSQL = "EXEC seguros_db.dbo.SEGS10347_SPU " & lSolicitante_ID & ",'" & MudaAspaSimples(txtSolicitante_Sinistro_Nome) & "'" & vbNewLine       'AKIO.OKUNO - 23/09/2012

        sSQL = sSQL & ",'" & MudaAspaSimples(txtSolicitante_Sinistro_Endereco) & "','" & MudaAspaSimples(txtSolicitante_Sinistro_Bairro) & "'," & cmbSolicitante_Municipio_Nome.ItemData(cmbSolicitante_Municipio_Nome.ListIndex) & "" & vbNewLine
        sSQL = sSQL & ",'" & cmbSolicitante_Municipio_Nome & "','" & IIf(cmbSolicitante_Sinistro_Estado <> "--", cmbSolicitante_Sinistro_Estado, "XX") & "'" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD1 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD1) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone1 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone1) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal1 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal1) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone1.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone1.ItemData(cmbTp_Telefone1.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD2 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD2) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone2 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone2) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal2 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal2) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone2.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone2.ItemData(cmbTp_Telefone2.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD3 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD3) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone3 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone3) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal3 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal3) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone3.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone3.ItemData(cmbTp_Telefone3.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD4 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD4) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone4 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone4) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal4 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal4) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone4.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone4.ItemData(cmbTp_Telefone4.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD5 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD5) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone5 <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone5) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Ramal5 <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Ramal5) & "'", "''") & "" & vbNewLine

        If cmbTp_Telefone5.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbTp_Telefone1.ItemData(cmbTp_Telefone1.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_DDD_Fax <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_DDD_Fax) & "'", "''") & "" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Telefone_Fax <> "", "'" & MudaAspaSimples(mskSolicitante_Sinistro_Telefone_Fax) & "'", "''") & "" & vbNewLine

        If cmbSolicitante_Sinistro_Parentesco.ListIndex = -1 Then
            sSQL = sSQL & ",''" & vbNewLine
        Else
            sSQL = sSQL & ",'" & cmbSolicitante_Sinistro_Parentesco.ItemData(cmbSolicitante_Sinistro_Parentesco.ListIndex) & "'" & vbNewLine
        End If

        sSQL = sSQL & ",'" & cUserName & "'" & vbNewLine
        sSQL = sSQL & "," & IIf(mskSolicitante_Sinistro_Cep <> "", "'" & mskSolicitante_Sinistro_Cep & "'", "''") & vbNewLine
        sSQL = sSQL & "," & IIf(txtSolicitante_Sinistro_Email <> "", "'" & MudaAspaSimples(txtSolicitante_Sinistro_Email) & "'", "''") & vbNewLine
    End Select

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)


    Set rsRecordSet = Nothing

    'cristovao.rodrigues 28/08/2012


    btnAplicar.Enabled = False

    btnVoltar.Enabled = False
    
    ConfirmarTransacao (lConexaoLocal)    'msalema - MP - 15/07/2013
    
    'MsgBox " Processo Efetuado com Sucesso", vbExclamation, App.Title
    Select Case bytModoOperacao
    Case 1    'Incluir
        MsgBox " Solicitante inclu�do com sucesso!", vbExclamation, App.Title

    Case 2    ' Alterar
        MsgBox " Solicitante alterado com sucesso!", vbExclamation, App.Title
    End Select
    
    'ConfirmarTransacao (lConexaoLocal)    'msalema - MP - 15/07/2013

    TxtAuxiliar.Text = lSolicitante_ID

    vInclui_Solicitante = True

    btnVoltar_Click    'cristovao.rodrigues 28/08/2012

    Exit Sub
    Resume
Trata_Erro:
    TrataErroGeral "Aplicar Solicitante"
    RetornarTransacao (lConexaoLocal)
    TxtAuxiliar.Text = "Cancelado"
    Inclui_Solicitante = False
End Sub

Private Function Valida_ManutencaoSolicitante()
    Dim i As Integer
    Dim blnEmail As Boolean

    Valida_ManutencaoSolicitante = True
    On Error GoTo Trata_Erro_Solicitante

    If Trim(txtSolicitante_Sinistro_Nome) = "" Then
        MsgBox "Nome de Solicitante Vazio", vbExclamation, App.Title
        Valida_ManutencaoSolicitante = False
        txtSolicitante_Sinistro_Nome.SetFocus
        Exit Function
    End If

    If cmbSolicitante_Sinistro_Estado = "" Or cmbSolicitante_Sinistro_Estado.ListIndex = -1 Then
        MsgBox "Escolha um Estado", vbExclamation, App.Title
        Valida_ManutencaoSolicitante = False
        cmbSolicitante_Sinistro_Estado.SetFocus
        Exit Function
    End If

    If cmbSolicitante_Municipio_Nome = "" Or cmbSolicitante_Municipio_Nome.ListIndex = -1 Then
        Valida_ManutencaoSolicitante = False
        MsgBox "Escolha um Munic�pio", vbExclamation, App.Title
        cmbSolicitante_Municipio_Nome.SetFocus
        Exit Function
    End If

    Exit Function

Trata_Erro_Solicitante:
    TrataErroGeral "Valida_ManutencaoSolicitante"
    Inclui_Solicitante = False
End Function


Private Sub cmbSolicitante_Sinistro_Estado_Click()
    With cmbSolicitante_Sinistro_Estado
        If .ListIndex >= 0 Then
            If cmbSolicitante_Sinistro_Estado <> cmbSolicitante_Sinistro_Estado.Tag Then
                Screen.MousePointer = vbHourglass

                CarregaDadosCombo_Municipio

                cmbSolicitante_Sinistro_Estado.Tag = cmbSolicitante_Sinistro_Estado

                Screen.MousePointer = vbNormal
            End If
        End If
    End With
End Sub
Private Sub Form_Load()
    On Error GoTo Erro

    InicializaVariaveisLocal

    IniciarConexao
    
    InicializaInterfaceLocal

    Exit Sub

Erro:
    Call TrataErroGeral("Form_Load", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosTela()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String

    On Error GoTo Erro

    MousePointer = vbHourglass

    If bytModoOperacao <> 1 Then
    'Msalema - MP - 15/07/2013
            
'        SelecionaDados
'
'                sSQL = ""
'                sSQL = sSQL & "Select * " & vbCrLf
'                sSQL = sSQL & "  From #Sinistro_Solicitante"
        sSQL = "set nocount on" & vbCrLf
        sSQL = sSQL & "declare @Sinistro_Solicitante  Table( Sinistro_ID    Numeric(11)                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_ID                                Int                                                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Nome                     VarChar(60)                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 VarChar(60)                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   VarChar(30)                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                VarChar(60)                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Municipio_Nome                    VarChar(60)                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     VarChar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                VarChar(9)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     VarChar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                VarChar(9)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     VarChar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                VarChar(9)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     VarChar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                VarChar(9)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     VarChar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                VarChar(9)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Grau_Parentesco          int                                                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)                                                             " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)                                                             " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)                                                             " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)                                                             " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)                                                             " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   varchar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   varchar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   varchar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   varchar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   varchar(4)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   Char(2)                                                             " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      varchar(8)                                                          " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Email                    varchar(60)                                                         " & vbCrLf
        sSQL = sSQL & "     )                                                                                                                   " & vbCrLf
        sSQL = sSQL & " " & vbCrLf
        sSQL = sSQL & "Insert into @Sinistro_Solicitante                                                                                        " & vbCrLf
        sSQL = sSQL & "          ( Sinistro_ID                                                                                                  " & vbCrLf
        sSQL = sSQL & "          , Solicitante_ID                                                                                               " & vbCrLf
        sSQL = sSQL & "          )                                                                                                              " & vbCrLf
        sSQL = sSQL & "Select Sinistro_ID                                                                                                       " & vbCrLf
        sSQL = sSQL & "     , Solicitante_ID                                                                                                    " & vbCrLf
        sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb WITH (NOLOCK)                                                " & vbCrLf
        sSQL = sSQL & " Where Sinistro_ID = " & gbldSinistro_ID & vbNewLine
        sSQL = sSQL & " " & vbCrLf
        sSQL = sSQL & "Update @Sinistro_Solicitante                                                                                             " & vbCrLf
        sSQL = sSQL & "   Set Solicitante_Sinistro_Nome                     = Solicitante_Sinistro_Tb.Nome                                      " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 = Solicitante_Sinistro_Tb.Endereco                                  " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   = Solicitante_Sinistro_Tb.Bairro                                    " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             = Solicitante_Sinistro_Tb.Municipio_ID                              " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                = Solicitante_Sinistro_Tb.Municipio                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     = Solicitante_Sinistro_Tb.DDD                                       " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                = Solicitante_Sinistro_Tb.Telefone                                  " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     = Solicitante_Sinistro_Tb.DDD1                                      " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                = Solicitante_Sinistro_Tb.Telefone1                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     = Solicitante_Sinistro_Tb.DDD2                                      " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                = Solicitante_Sinistro_Tb.Telefone2                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     = Solicitante_Sinistro_Tb.DDD3                                      " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                = Solicitante_Sinistro_Tb.Telefone3                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     = Solicitante_Sinistro_Tb.DDD4                                      " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                = Solicitante_Sinistro_Tb.Telefone4                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             = Solicitante_Sinistro_Tb.Tp_Telefone                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             = Solicitante_Sinistro_Tb.Tp_Telefone1                              " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             = Solicitante_Sinistro_Tb.Tp_Telefone2                              " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             = Solicitante_Sinistro_Tb.Tp_Telefone3                              " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             = Solicitante_Sinistro_Tb.Tp_Telefone4                              " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   = Solicitante_Sinistro_Tb.Ramal                                     " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   = Solicitante_Sinistro_Tb.Ramal1                                    " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   = Solicitante_Sinistro_Tb.Ramal2                                    " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   = Solicitante_Sinistro_Tb.Ramal3                                    " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   = Solicitante_Sinistro_Tb.Ramal4                                    " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Grau_Parentesco          = Solicitante_Sinistro_Tb.Grau_Parentesco_ID                        " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   = Case Solicitante_Sinistro_Tb.Estado                               " & vbCrLf
        sSQL = sSQL & "                                                            When 'XX' then ''                                            " & vbCrLf
        sSQL = sSQL & "                                                            When '--' Then ''                                            " & vbCrLf
        sSQL = sSQL & "                                                                      Else Solicitante_Sinistro_Tb.Estado                " & vbCrLf
        sSQL = sSQL & "                                                       End                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  = Solicitante_Sinistro_Tb.DDD_Fax                                   " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             = Solicitante_Sinistro_Tb.Telefone_Fax                              " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      = Solicitante_Sinistro_Tb.cep                                       " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Email                    = Solicitante_Sinistro_Tb.email                                     " & vbCrLf
        sSQL = sSQL & "  From Seguros_Db.Dbo.Solicitante_Sinistro_Tb        Solicitante_Sinistro_Tb WITH (NOLOCK, Index = PK_solicitante_sinistro)   " & vbCrLf
        sSQL = sSQL & "  Join @Sinistro_Solicitante                    Sinistro_Solicitante_TB                                                  " & vbCrLf
        sSQL = sSQL & "    on Sinistro_Solicitante_TB.Solicitante_ID          = Solicitante_Sinistro_Tb.Solicitante_ID                          " & vbCrLf
        sSQL = sSQL & " " & vbCrLf
        sSQL = sSQL & "Update @Sinistro_Solicitante                                                                                             " & vbCrLf
        sSQL = sSQL & "   set Solicitante_Municipio_Nome                    = Municipio_Tb.Nome                                                 " & vbCrLf
        sSQL = sSQL & "  From @Sinistro_Solicitante                     Sinistro_Solicitante_TB                                                 " & vbCrLf
        sSQL = sSQL & "  Join Seguros_Db.Dbo.Municipio_Tb                   Municipio_Tb WITH (NOLOCK, Index = PK_municipio)                         " & vbCrLf
        sSQL = sSQL & "    on Municipio_Tb.Municipio_ID                     = Sinistro_Solicitante_TB.Solicitante_Sinistro_Municipio_ID         " & vbCrLf
        sSQL = sSQL & "   and Municipio_Tb.Estado                           = Sinistro_Solicitante_TB.Solicitante_Sinistro_Estado               " & vbCrLf
        sSQL = sSQL & "  " & vbCrLf
        sSQL = sSQL & "Select Sinistro_ID                                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_ID                                                                                            " & vbCrLf
        sSQL = sSQL & "  , Solicitante_Sinistro_Nome                                                                                    " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                                                                             " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID                                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                                                                            " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                                                                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                                                                            " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                                                                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                                                                            " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                                                                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                                                                            " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                                                                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                                                                            " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                                                                                 " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                                                                            " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1                                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2                                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3                                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4                                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5                                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Grau_Parentesco                                                                      " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Estado                                                                               " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                                                                              " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax                                                                         " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_CEP                                                                                  " & vbCrLf
        sSQL = sSQL & "     , Solicitante_Sinistro_Email                                                                                " & vbCrLf
        sSQL = sSQL & "  , Solicitante_Municipio_Nome                                                                                   " & vbCrLf
        sSQL = sSQL & " From @Sinistro_Solicitante                                                                                      " & vbCrLf

        'Msalema - MP - 15/07/2013
            
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, True)
    
        With rsRecordSet
               If Not .EOF Then
                   'Dados do Frame: Solicitante
                   txtSolicitante_Sinistro_Nome = .Fields!Solicitante_Sinistro_Nome & ""
    
                    'Email
                    If Not IsNull(.Fields!Solicitante_Sinistro_Email) Then
                        txtSolicitante_Sinistro_Email = .Fields!Solicitante_Sinistro_Email & ""
                    Else
                        txtSolicitante_Sinistro_Email = ""
                    End If
                    txtSolicitante_Sinistro_Endereco = .Fields!Solicitante_Sinistro_Endereco & ""
                    txtSolicitante_Sinistro_Bairro = .Fields!Solicitante_Sinistro_Bairro & ""
                    If LenB(Trim(.Fields!Solicitante_Sinistro_Estado)) <> 0 Then
                        cmbSolicitante_Sinistro_Estado = .Fields!Solicitante_Sinistro_Estado & ""
                    End If
    
                    CarregaDadosCombo_Municipio
    
                    'Ricardo Toledo (Confitec) : 02/01/2017 : INC000005286818 : INI
                    'Al�m do erro "Text property is read-only", a l�gica tamb�m estava estava errada e portanto foi comentada
                    'If LenB(Trim(.Fields!Solicitante_Sinistro_Municipio)) <> 0 Then
                    '    cmbSolicitante_Municipio_Nome = IIf(IsNull(.Fields!Solicitante_Sinistro_Municipio), "INDEFINIDO", .Fields!Solicitante_Sinistro_Municipio & "")
                    'Else
                        'mathayde
                        'cmbSolicitante_Municipio_Nome = "INDEFINIDO"
                        cmbSolicitante_Municipio_Nome.ListIndex = -1
                    'End If
                    'Ricardo Toledo (Confitec) : 02/01/2017 : INC000005286818 : FIM
    
                    'CEP
                    If Not IsNull(.Fields!Solicitante_Sinistro_Cep) Then
                        mskSolicitante_Sinistro_Cep = MasCEP(.Fields!Solicitante_Sinistro_Cep)
                    End If
    
                    'Telefone 1
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD1) Then
                        mskSolicitante_Sinistro_DDD1 = .Fields!Solicitante_Sinistro_DDD1
                    Else
                        mskSolicitante_Sinistro_DDD1 = ""
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone1) Then
                        mskSolicitante_Sinistro_Telefone1 = .Fields!Solicitante_Sinistro_Telefone1
                    Else
                        mskSolicitante_Sinistro_Telefone1 = ""
                    End If
                    'Telefone 2
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD2) Then
                        mskSolicitante_Sinistro_DDD2 = .Fields!Solicitante_Sinistro_DDD2
                    Else
                        mskSolicitante_Sinistro_DDD2 = ""
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone2) Then
                        mskSolicitante_Sinistro_Telefone2 = .Fields!Solicitante_Sinistro_Telefone2
                    Else
                        mskSolicitante_Sinistro_Telefone2 = ""
                    End If
                    'Telefone 3
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD3) Then
                        mskSolicitante_Sinistro_DDD3 = .Fields!Solicitante_Sinistro_DDD3
                    Else
                        mskSolicitante_Sinistro_DDD3 = ""
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone3) Then
                        mskSolicitante_Sinistro_Telefone3 = .Fields!Solicitante_Sinistro_Telefone3
                    Else
                        mskSolicitante_Sinistro_Telefone3 = ""
                    End If
                    'Telefone 4
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD4) Then
                        mskSolicitante_Sinistro_DDD4 = .Fields!Solicitante_Sinistro_DDD4
                    Else
                        mskSolicitante_Sinistro_DDD4 = ""
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone4) Then
                        mskSolicitante_Sinistro_Telefone4 = .Fields!Solicitante_Sinistro_Telefone4
                    Else
                        mskSolicitante_Sinistro_Telefone4 = ""
                    End If
                    'Telefone 5
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD5) Then
                        mskSolicitante_Sinistro_DDD5 = .Fields!Solicitante_Sinistro_DDD5
                    Else
                        mskSolicitante_Sinistro_DDD5 = ""
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone5) Then
                        mskSolicitante_Sinistro_Telefone5 = .Fields!Solicitante_Sinistro_Telefone5
                    Else
                        mskSolicitante_Sinistro_Telefone5 = ""
                    End If
                    'Fax
                    If Not IsNull(.Fields!Solicitante_Sinistro_DDD_Fax) Then
                        mskSolicitante_Sinistro_DDD_Fax = .Fields!Solicitante_Sinistro_DDD_Fax
                    Else
                        mskSolicitante_Sinistro_DDD_Fax = ""
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Telefone_Fax) Then
                        mskSolicitante_Sinistro_Telefone_Fax = .Fields!Solicitante_Sinistro_Telefone_Fax
                    Else
                        mskSolicitante_Sinistro_Telefone_Fax = ""
                    End If
                    txtSolicitante_Sinistro_Ramal1 = .Fields!Solicitante_Sinistro_Ramal1 & ""
                    txtSolicitante_Sinistro_Ramal2 = .Fields!Solicitante_Sinistro_Ramal2 & ""
                    txtSolicitante_Sinistro_Ramal3 = .Fields!Solicitante_Sinistro_Ramal3 & ""
                    txtSolicitante_Sinistro_Ramal4 = .Fields!Solicitante_Sinistro_Ramal4 & ""
                    txtSolicitante_Sinistro_Ramal5 = .Fields!Solicitante_Sinistro_Ramal5 & ""
                    If Not IsNull(.Fields!Solicitante_Sinistro_Tp_Telefone1) Then
                        cmbTp_Telefone1 = Seleciona_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone1 & "")
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Tp_Telefone2) Then
                        cmbTp_Telefone2 = Seleciona_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone2 & "")
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Tp_Telefone3) Then
                        cmbTp_Telefone3 = Seleciona_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone3 & "")
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Tp_Telefone4) Then
                        cmbTp_Telefone4 = Seleciona_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone4 & "")
                    End If
                    If Not IsNull(.Fields!Solicitante_Sinistro_Tp_Telefone5) Then
                        cmbTp_Telefone5 = Seleciona_Tp_Telefone(.Fields!Solicitante_Sinistro_Tp_Telefone5 & "")
                    End If
    
                    If LenB(Trim(ValidaDados_Grau_Parentesco("0" & .Fields!Solicitante_Sinistro_Grau_Parentesco))) <> 0 Then
                        cmbSolicitante_Sinistro_Parentesco = ValidaDados_Grau_Parentesco(.Fields!Solicitante_Sinistro_Grau_Parentesco)
                    Else
                        cmbSolicitante_Sinistro_Parentesco.ListIndex = -1
                    End If
    
    
                    InicializaToolTipTextLocal
                End If
            End With
    End If
    MousePointer = vbNormal

    Set rsRecordSet = Nothing
    
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosTela", Me.name)
    Call FinalizarAplicacao

End Sub

''Msalema - MP - Inicio - 15/07/2013
'Private Sub SelecionaDados()
'    Dim sSQL As String
'
'    On Error GoTo Erro
'
'    MousePointer = vbHourglass
'
'
'    sSQL = ""
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Solicitante'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Sinistro_Solicitante" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Create Table #Sinistro_Solicitante "
'    sSQL = sSQL & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
'    sSQL = sSQL & "     , Solicitante_ID                                Int                 " & vbNewLine  ' -- Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Nome                     VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   VarChar(30)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Municipio_Nome                    VarChar(60)         " & vbNewLine  ' -- Municipio_Tb (Nome) (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    '    sSql = sSql & "     , Solicitante_Sinistro_Grau_Parentesco          VarChar(1)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)  'AKIO.OKUNO - 28/09/2012
'    sSQL = sSQL & "     , Solicitante_Sinistro_Grau_Parentesco          int                 " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   varchar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   varchar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   varchar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   varchar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   varchar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   Char(2)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      varchar(8)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     , Solicitante_Sinistro_Email                    varchar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
'    sSQL = sSQL & "     ) " & vbNewLine
'    sSQL = sSQL & "     " & vbNewLine
'    sSQL = sSQL & "Create Index PK_Sinistro_ID                          on #Sinistro_Solicitante (Sinistro_ID, Solicitante_ID)" & vbNewLine
'    sSQL = sSQL & "" & vbNewLine
''AKIO.OKUNO - MP - INICIO - 15/02/2013
''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
''    sSQL = ""
''AKIO.OKUNO - MP - FIM - 15/02/2013
'    Select Case bytModoOperacao
'    Case 1      'Inclusao
'    Case 2, 3   'Altera��o/Exclus�o e Consulta
'        sSQL = sSQL & "Insert into #Sinistro_Solicitante " & vbNewLine
'        sSQL = sSQL & "          ( Sinistro_ID                      " & vbNewLine
'        sSQL = sSQL & "          , Solicitante_ID                   " & vbNewLine
'        sSQL = sSQL & "          )" & vbNewLine
'        sSQL = sSQL & "Select Sinistro_ID                               " & vbNewLine
'        sSQL = sSQL & "     , Solicitante_ID                            " & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb WITH (NOLOCK)" & vbNewLine
'        sSQL = sSQL & " Where Sinistro_ID = " & gbldSinistro_ID & vbNewLine
'        sSQL = sSQL & "" & vbNewLine
'        '-- Solicitante                                                                                                                                                                                                                          & vbNewLine
'        sSQL = sSQL & "Update #Sinistro_Solicitante" & vbNewLine
'        sSQL = sSQL & "   Set Solicitante_Sinistro_Nome                     = Solicitante_Sinistro_Tb.Nome" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Endereco                 = Solicitante_Sinistro_Tb.Endereco" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Bairro                   = Solicitante_Sinistro_Tb.Bairro" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio_ID             = Solicitante_Sinistro_Tb.Municipio_ID" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Municipio                = Solicitante_Sinistro_Tb.Municipio " & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_DDD1                     = Solicitante_Sinistro_Tb.DDD" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone1                = Solicitante_Sinistro_Tb.Telefone" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_DDD2                     = Solicitante_Sinistro_Tb.DDD1" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone2                = Solicitante_Sinistro_Tb.Telefone1" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_DDD3                     = Solicitante_Sinistro_Tb.DDD2" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone3                = Solicitante_Sinistro_Tb.Telefone2" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_DDD4                     = Solicitante_Sinistro_Tb.DDD3" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone4                = Solicitante_Sinistro_Tb.Telefone3" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_DDD5                     = Solicitante_Sinistro_Tb.DDD4" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone5                = Solicitante_Sinistro_Tb.Telefone4" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone1             = Solicitante_Sinistro_Tb.Tp_Telefone" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone2             = Solicitante_Sinistro_Tb.Tp_Telefone1" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone3             = Solicitante_Sinistro_Tb.Tp_Telefone2" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone4             = Solicitante_Sinistro_Tb.Tp_Telefone3" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Tp_Telefone5             = Solicitante_Sinistro_Tb.Tp_Telefone4" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal1                   = Solicitante_Sinistro_Tb.Ramal" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal2                   = Solicitante_Sinistro_Tb.Ramal1" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal3                   = Solicitante_Sinistro_Tb.Ramal2" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal4                   = Solicitante_Sinistro_Tb.Ramal3" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Ramal5                   = Solicitante_Sinistro_Tb.Ramal4" & vbNewLine
'        '            sSql = sSql & "     , Solicitante_Sinistro_Grau_Parentesco          = Solicitante_Sinistro_Tb.Grau_Parentesco" & vbNewLine     'AKIO.OKUNO - 29/09/2012
'        sSQL = sSQL & "     , Solicitante_Sinistro_Grau_Parentesco          = Solicitante_Sinistro_Tb.Grau_Parentesco_ID " & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Estado                   = Case Solicitante_Sinistro_Tb.Estado " & vbNewLine
'        sSQL = sSQL & "                                                            When 'XX' then '' " & vbNewLine
'        sSQL = sSQL & "                                                            When '--' Then '' " & vbNewLine
'        sSQL = sSQL & "                                                                      Else Solicitante_Sinistro_Tb.Estado" & vbNewLine
'        sSQL = sSQL & "                                                       End" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_DDD_Fax                  = Solicitante_Sinistro_Tb.DDD_Fax" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Telefone_Fax             = Solicitante_Sinistro_Tb.Telefone_Fax" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_CEP                      = Solicitante_Sinistro_Tb.cep" & vbNewLine
'        sSQL = sSQL & "     , Solicitante_Sinistro_Email                    = Solicitante_Sinistro_Tb.email" & vbNewLine
'        sSQL = sSQL & "  From Seguros_Db.Dbo.Solicitante_Sinistro_Tb        Solicitante_Sinistro_Tb WITH (NOLOCK, Index = PK_solicitante_sinistro)" & vbNewLine
'        sSQL = sSQL & "  Join #Sinistro_Solicitante                         " & vbNewLine
'        sSQL = sSQL & "    on #Sinistro_Solicitante.Solicitante_ID          = Solicitante_Sinistro_Tb.Solicitante_ID" & vbNewLine
'        sSQL = sSQL & "" & vbNewLine
'        '-- Dados do municipio
'        sSQL = sSQL & "Update #Sinistro_Solicitante" & vbNewLine
'        sSQL = sSQL & "   set Solicitante_Municipio_Nome                    = Municipio_Tb.Nome" & vbNewLine
'        sSQL = sSQL & "  From #Sinistro_Solicitante                         " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Municipio_Tb                   Municipio_Tb WITH (NOLOCK, Index = PK_municipio)" & vbNewLine
'        sSQL = sSQL & "    on Municipio_Tb.Municipio_ID                     = #Sinistro_Solicitante.Solicitante_Sinistro_Municipio_ID" & vbNewLine
'        sSQL = sSQL & "   and Municipio_Tb.Estado                           = #Sinistro_Solicitante.Solicitante_Sinistro_Estado"
'
'        sSQL = sSQL & "" & vbNewLine
'    End Select
'
'    If sSQL <> "" Then
'
'        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                 glAmbiente_id, _
'                                 App.Title, _
'                                 App.FileDescription, _
'                                 sSQL, _
'                                 lConexaoLocal, _
'                                 False)
'    End If
'
'    Exit Sub
'
'Erro:
'    Call TrataErroGeral("SelecionaDados", Me.name)
'    Call FinalizarAplicacao
'End Sub
''Msalema - MP - Fim - 15/07/2013


Private Sub InicializaToolTipTextLocal()
    On Error GoTo Erro

    InicializaToolTipText Me

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaToolTipTextLocal", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub InicializaVariaveisLocal()
    Dim sSQL As String
    Dim aParametros() As String
    Dim bItemParametro As Byte
    Dim sParametroLocal As String


    If Not Trata_Parametros(Command) Or InStr(1, Command, " ") = 0 Then
        FinalizarAplicacao
    Else

        'glAmbiente_id = 3
        Ambiente = ConvAmbiente(glAmbiente_id)

        sParametroLocal = Left(Command, InStrRev(Command, " "))

        aParametros = Split(sParametroLocal, ".")


        bytTipoRamo = aParametros(0)
        gsParamAplicacao = aParametros(1)
        bytModoOperacao = Trim(gsParamAplicacao)


        If UBound(aParametros) >= 2 Then
            gbldSinistro_ID = aParametros(2)
        Else
            gbldSinistro_ID = 0
        End If

        If UBound(aParametros) >= 3 Then
            lSolicitante_ID = aParametros(3)
        Else
            lSolicitante_ID = 0
        End If
    End If
End Sub

Private Sub InicializaInterfaceLocal()
    On Error GoTo Erro

    Screen.MousePointer = vbHourglass

    If ValidaSegurancaLocal Then

        If strTipoProcesso = strTipoProcesso_Avaliar_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Avisar_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Consulta_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro Or _
           strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Or _
           sOperacaoCosseguro = "C" Then

            bytModoOperacao = 3

        End If

        Call CentraFrm(Me)

        LimpaCamposTelaLocal Me

        InicializaInterfaceLocal_Form

        CarregaDadosCombo_UF
        CarregaDadosCombo_GrauParentesco
        CarregaDadosCombo_TipoTelefone

        CarregaDadosTela

        InicializaModoOperacaoLocal

    Else

        FinalizarAplicacao

    End If

    Screen.MousePointer = vbNormal
    Exit Sub

Erro:
    Call TrataErroGeral("InicializaInterfaceLocal", Me.name)
    Call FinalizarAplicacao

End Sub


Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao As String

    TxtAuxiliar.Visible = False
    TxtAuxiliar.Text = "Cancelado"

    btnAplicar.Visible = (bytModoOperacao <> 3)
    btnAplicar.Enabled = (bytModoOperacao <> 3)

    Select Case bytModoOperacao

    Case 1, 2

        btnVoltar.Caption = "Cancelar"
        sTipoProcesso_Descricao = "Manuten��o"

    Case 3

        btnVoltar.Caption = "Voltar"
        sTipoProcesso_Descricao = "Consulta"

    End Select


    Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " Solicitante - " & Ambiente


End Sub

Private Function ValidaSegurancaLocal() As Boolean
    ValidaSegurancaLocal = True
End Function

Private Sub InicializaModoOperacaoLocal()
    On Error GoTo Erro

    InicializaModoOperacao Me, bytModoOperacao

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaModoOperacao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Function ValidaDados_Grau_Parentesco(iTp_Componente_ID As Integer) As String
    Dim sSQL As String
    Dim rsRecordSet As Recordset

    On Error GoTo Erro

    MousePointer = vbHourglass

    ValidaDados_Tecnico = True

    If iTp_Componente_ID <> 0 Then
        sSQL = ""
        sSQL = sSQL & "Select Nome" & vbNewLine
        '        sSql = sSql & "  From Tp_Componente_ID                  Tp_Componente_ID WITH (NOLOCK) " & vbNewLine
        '        sSql = sSql & " Where Tp_Componente_ID                  = iTp_Componente_ID)"          'AKIO.OKUNO - 28/09/2012
        sSQL = sSQL & "  From Tp_Componente_Tb                  Tp_Componente_Tb WITH (NOLOCK) " & vbNewLine
        sSQL = sSQL & " Where Tp_Componente_ID                  = " & iTp_Componente_ID
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, True)
        With rsRecordSet
            If .EOF Then

                ValidaDados_Grau_Parentesco = ""
            Else
                ValidaDados_Grau_Parentesco = .Fields!Nome
            End If
        End With

    End If

    MousePointer = vbNormal

    Set rsRecordSet = Nothing

    Exit Function

Erro:
    Call TrataErroGeral("ValidaDados_Grau_Parentesco", Me.name)
    Call FinalizarAplicacao

End Function

Private Sub LimpaCamposTelaLocal(objObjeto As Object)
    On Error GoTo Erro

    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("LimpaCamposTelaLocal", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosCombo_TipoTelefone()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim iCampos As Integer

    On Error GoTo Erro

    With FrmSolicitante
        For iCampos = 0 To .Controls.Count - 1
            If TypeOf .Controls(iCampos) Is ComboBox And Mid(.Controls(iCampos).name, 1, 14) = "cmbTp_Telefone" Then
                .Controls(iCampos).Clear
                .Controls(iCampos).AddItem " "
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 0
                .Controls(iCampos).AddItem "Residencial"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 1
                .Controls(iCampos).AddItem "Comercial"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 2
                .Controls(iCampos).AddItem "Celular"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 3
                .Controls(iCampos).AddItem "Fax"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 4
                .Controls(iCampos).AddItem "Recado"
                .Controls(iCampos).ItemData(.Controls(iCampos).NewIndex) = 5
            End If
        Next
    End With
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosCombo_TipoTelefone", Me.name)
    Call FinalizarAplicacao

End Sub

Public Function Seleciona_Tp_Telefone(Tp_Telefone As String) As String

    Select Case Tp_Telefone
    Case "1"
        Seleciona_Tp_Telefone = "Residencial"
    Case "2"
        Seleciona_Tp_Telefone = "Comercial"
    Case "3"
        Seleciona_Tp_Telefone = "Celular"
    Case "4"
        Seleciona_Tp_Telefone = "Fax"
    Case "5"
        Seleciona_Tp_Telefone = "Recado"
    Case Else
        Seleciona_Tp_Telefone = " "
    End Select

End Function

Private Sub CarregaDadosCombo_Municipio()
    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset

    On Error GoTo Erro

    sSQL = ""
    sSQL = sSQL & "Select Municipio_ID " & vbNewLine
    sSQL = sSQL & "     , Estado " & vbNewLine
    sSQL = sSQL & "     , Nome" & vbNewLine
    sSQL = sSQL & "  From Municipio_Tb WITH (NOLOCK)" & vbNewLine
    With cmbSolicitante_Sinistro_Estado
        If .ListIndex >= 0 Then
            sSQL = sSQL & "Where Estado = '" & .Text & "'" & vbNewLine
        End If
    End With
    sSQL = sSQL & " Order By Nome"

    Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  True)

    With rsRecordSet
        cmbSolicitante_Municipio_Nome.Clear
        While Not .EOF
            cmbSolicitante_Municipio_Nome.AddItem Trim(.Fields(2))
            cmbSolicitante_Municipio_Nome.ItemData(cmbSolicitante_Municipio_Nome.NewIndex) = .Fields(0)
            .MoveNext
        Wend
        .Close
    End With

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosCombo_Municipio", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosCombo_UF()
    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset

    On Error GoTo Erro

    sSQL = ""
    sSQL = sSQL & " Select Distinct Estado " & vbNewLine
    sSQL = sSQL & "   From Municipio_Tb WITH (NOLOCK) " & vbNewLine
    sSQL = sSQL & "  Order By Estado"

    Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  True)

    cmbSolicitante_Sinistro_Estado.Clear

    With rsRecordSet
        While Not .EOF
            cmbSolicitante_Sinistro_Estado.AddItem .Fields(0)
            .MoveNext
        Wend
        .Close
    End With

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosCombo_UF", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub CarregaDadosCombo_GrauParentesco()
    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset

    On Error GoTo Erro

    sSQL = ""
    sSQL = sSQL & " Select Distinct Tp_Componente_ID" & vbNewLine
    sSQL = sSQL & "      , Nome " & vbNewLine
    sSQL = sSQL & "   From Tp_Componente_Tb WITH (NOLOCK) " & vbNewLine
    sSQL = sSQL & "  Order by Nome "

    Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  True)

    With rsRecordSet
        cmbSolicitante_Sinistro_Parentesco.Clear
        While Not .EOF
            cmbSolicitante_Sinistro_Parentesco.AddItem .Fields(1)
            cmbSolicitante_Sinistro_Parentesco.ItemData(cmbSolicitante_Sinistro_Parentesco.NewIndex) = .Fields(0)
            .MoveNext
        Wend
        .Close
    End With

    Set rst_Grau = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosCombo_GrauParentesco", Me.name)
    Call FinalizarAplicacao

End Sub



Private Sub IniciarConexao()
    On Error GoTo Erro



    '    vNunCx = 0

    'AKIO.OKUNO - INICIO - 23/09/2012
    On Error GoTo Erro
    Select Case EstadoConexao(lConexaoLocal)
    Case adStateClosed
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    Case adStateOpen
        bConex = FecharConexao(lConexaoLocal)
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    End Select
    
    '    If EstadoConexao(lConexaoLocal) = adStateClosed Then
    '        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    '    End If

    '    vNunCx = lConexaoLocal
    'AKIO.OKUNO - FIM - 23/09/2012


    Exit Sub

Erro:
    Call TrataErroGeral("IniciarConexao", Me.name)
    Call FinalizarAplicacao

End Sub

Private Sub btnVoltar_Click()
    Dim Retorno As Boolean
    On Error GoTo Erro

    If btnVoltar.Caption = "Cancelar" And vInclui_Solicitante = False Then

        MsgBox "Opera��o Cancelada!"
        TxtAuxiliar.Text = "Cancelado"

    End If

    If EstadoConexao(lConexaoLocal) = adStateOpen Then
        Retorno = FecharConexao(lConexaoLocal)
    End If

    'cristovao.rodrigues 28/08/2012
    'envia mensagem para aplica��o master
    If vInclui_Solicitante Then

        Retorno = EnvioSaidaAuxiliar(TxtAuxiliar.Text, FrmSolicitante)

    End If

    End

Erro:
    Call TrataErroGeral("btnVoltar", Me.name)
    Call FinalizarAplicacao


End Sub


Private Sub mskSolicitante_Sinistro_Cep_GotFocus()
    mskSolicitante_Sinistro_Cep.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_DDD_Fax_GotFocus()
    mskSolicitante_Sinistro_DDD_Fax.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_DDD1_GotFocus()
    mskSolicitante_Sinistro_DDD1.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_DDD2_GotFocus()
    mskSolicitante_Sinistro_DDD2.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_DDD3_GotFocus()
    mskSolicitante_Sinistro_DDD3.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_DDD4_GotFocus()
    mskSolicitante_Sinistro_DDD4.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_DDD5_GotFocus()
    mskSolicitante_Sinistro_DDD5.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_Telefone_Fax_GotFocus()
    mskSolicitante_Sinistro_Telefone_Fax.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_Telefone1_GotFocus()
    mskSolicitante_Sinistro_Telefone1.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_Telefone2_GotFocus()
    mskSolicitante_Sinistro_Telefone2.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_Telefone3_GotFocus()
    mskSolicitante_Sinistro_Telefone3.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_Telefone4_GotFocus()
    mskSolicitante_Sinistro_Telefone4.SelStart = 0
End Sub

Private Sub mskSolicitante_Sinistro_Telefone5_GotFocus()
    mskSolicitante_Sinistro_Telefone5.SelStart = 0
End Sub

Private Sub txtSolicitante_Sinistro_Bairro_Click()
    Dim sCodigo As String

    sCodigo = UCase(txtSolicitante_Sinistro_Bairro.Text)
    txtSolicitante_Sinistro_Bairro.Text = ""
    txtSolicitante_Sinistro_Bairro.Text = sCodigo

End Sub

Private Sub txtSolicitante_Sinistro_Bairro_LostFocus()
    Dim sCodigo As String

    sCodigo = UCase(txtSolicitante_Sinistro_Bairro.Text)
    txtSolicitante_Sinistro_Bairro.Text = ""
    txtSolicitante_Sinistro_Bairro.Text = sCodigo

End Sub

Private Sub txtSolicitante_Sinistro_Endereco_Click()
    Dim sCodigo As String

    sCodigo = UCase(txtSolicitante_Sinistro_Endereco.Text)
    txtSolicitante_Sinistro_Endereco.Text = ""
    txtSolicitante_Sinistro_Endereco.Text = sCodigo

End Sub

Private Sub txtSolicitante_Sinistro_Endereco_LostFocus()
    Dim sCodigo As String

    sCodigo = UCase(txtSolicitante_Sinistro_Endereco.Text)
    txtSolicitante_Sinistro_Endereco.Text = ""
    txtSolicitante_Sinistro_Endereco.Text = sCodigo

End Sub

Private Sub txtSolicitante_Sinistro_Nome_click()

    Dim sCodigo As String

    sCodigo = UCase(txtSolicitante_Sinistro_Nome.Text)
    txtSolicitante_Sinistro_Nome.Text = ""
    txtSolicitante_Sinistro_Nome.Text = sCodigo

End Sub

Private Sub txtSolicitante_Sinistro_Nome_LostFocus()
    Dim sCodigo As String

    sCodigo = UCase(txtSolicitante_Sinistro_Nome.Text)
    txtSolicitante_Sinistro_Nome.Text = ""
    txtSolicitante_Sinistro_Nome.Text = sCodigo
End Sub
