VERSION 5.00
Begin VB.Form frmTelaApresent 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   0  'None
   Caption         =   "Portal de Neg�cios da Alian�a do Brasil"
   ClientHeight    =   4605
   ClientLeft      =   2835
   ClientTop       =   1950
   ClientWidth     =   7305
   DrawStyle       =   5  'Transparent
   Icon            =   "frmTelaApresent.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   7305
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   2000
      Left            =   6450
      Top             =   240
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   1455
      Left            =   420
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      Text            =   "frmTelaApresent.frx":030A
      Top             =   2400
      Width           =   6195
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Neg�cios"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   3690
      TabIndex        =   1
      Top             =   810
      Width           =   3165
   End
   Begin VB.Label lblVersion 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Version"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   6090
      TabIndex        =   6
      Top             =   4140
      Width           =   615
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Copyright Alian�a do Brasil"
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   450
      TabIndex        =   5
      Top             =   4110
      Width           =   1995
   End
   Begin VB.Line Line1 
      BorderColor     =   &H0000FFFF&
      BorderWidth     =   2
      X1              =   420
      X2              =   6810
      Y1              =   2160
      Y2              =   2160
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Portal de"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3690
      TabIndex        =   4
      Top             =   480
      Width           =   3165
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "da"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3690
      TabIndex        =   3
      Top             =   1140
      Width           =   3165
   End
   Begin VB.Label Label4 
      BackStyle       =   0  'Transparent
      Caption         =   "Alian�a do Brasil"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3690
      TabIndex        =   2
      Top             =   1470
      Width           =   3165
   End
   Begin VB.Image Image1 
      Height          =   1605
      Left            =   420
      Picture         =   "frmTelaApresent.frx":045B
      Stretch         =   -1  'True
      Top             =   360
      Width           =   3150
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00C0C0C0&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00400000&
      Height          =   4425
      Left            =   90
      Top             =   90
      Width           =   7125
   End
End
Attribute VB_Name = "frmTelaApresent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Form_Load()
  lblVersion.Caption = "Vers�o 1.5 " '  & App.Major & "." & App.Minor & "." & App.Revision
  
End Sub



Private Sub Timer1_Timer()
  Unload Me
  frmPrincipal.Show
  
End Sub
