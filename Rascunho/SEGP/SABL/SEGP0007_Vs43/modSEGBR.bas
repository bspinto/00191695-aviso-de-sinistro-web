Attribute VB_Name = "modSEGBR"
Option Explicit

Private Sub AtualizaCFG()
  On Local Error GoTo TrataExce��o
  Dim sArquivoParaAtualiza��o As String ', sArquivoResultado As String
  Dim sMensagem As String, sResultado As String
  'sArquivoResultado = "%temp%\x.txt"
  sArquivoParaAtualiza��o = App.EXEName & ".cfg"
  sMensagem = "i;" & sArquivoParaAtualiza��o & ";" & App.PATH '& ";" & sArquivoParaAtualiza��o
  If EncaminharMensagem(sMensagem, "localhost", glPortaSABL0102) <> "0" Then
    Err.Raise glERRO_ATUALIZA_ARQUIVO
  End If
  Pause 2
  
  Exit Sub
TrataExce��o:
  If Err.Number = glERRO_ATUALIZA_ARQUIVO Then
    Call MontaErro(gsERRO_ATUALIZA_ARQUIVO, sArquivoParaAtualiza��o)
  Else
    Err.Description = Err.Description & " (" & sArquivoParaAtualiza��o & ")"
  End If
  Call TrataErroGeral("AtualizaCFG", "modSEGBR.bas")
  Call TerminaSEGBR
End Sub

Sub Main()
  On Local Error GoTo TrataExce��o
  Dim oSABL0010 As Object, RX As ADODB.Recordset
  Dim OProgramaPrincipal As String, OArquivoParaAtualizacao As String

  Call AtivaInstanciaCriada
  
  OProgramaPrincipal = gsCODPRINCIPAL & ".exe"
  'TituloJanelaSEGBR = "SEGBR"
  'Finaliza = True
  
  ' tenta atualizar as configura��es de acesso ao banco
  ' config.ini, sabl0010.dll e sabl0100.dll
  Call AtualizaCFG
  
  Set oSABL0010 = CreateObject("SABL0010.cls00009")
  glAmbiente_id = 2
  Call inicializa_variaveis_ambiente(oSABL0010)
  SQS = "select name from tempdb..sysobjects where name = '" & TabTempUsr & "'"
  Set RX = oSABL0010.ExecutaAmbiente(SQS)
  If Not RX.EOF Then
    Call MsgBox("O segbr j� est� aberto.")
    Set RX = Nothing
    Set oSABL0010 = Nothing
    Call TerminaSEGBR
  Else
    Set RX = Nothing
  End If
  
  ' tenta atualizar a vers�o do programa principal
  OArquivoParaAtualizacao = gsPastaServidorSegbr & "\" & OProgramaPrincipal & ".exe"
  Call ExecutaPrograma(OProgramaPrincipal, OProgramaPrincipal, _
                       Command, vbNormalFocus, oSABL0010)
  Set oSABL0010 = Nothing
  Exit Sub

TrataExce��o:
  MsgBox "Erro na fun��o Principal. " & _
         "Entre em contato com o suporte." & vbNewLine & vbNewLine & _
         "Descri��o ==> " & Err.Description & vbNewLine & vbNewLine & _
         "Erro na rotina ==> (" & Err.Number & ")"
  
  If Err.Number = glERRO_ATUALIZA_ARQUIVO Then
    Call MontaErro(gsERRO_ATUALIZA_ARQUIVO, OArquivoParaAtualizacao)
  End If
  
  Call TrataErroGeral("Principal", "modSEGBR.bas")
  Call TerminaSEGBR
End Sub
