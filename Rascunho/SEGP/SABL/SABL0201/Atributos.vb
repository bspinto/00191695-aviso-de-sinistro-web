Public Class Atributos
    Public Function ReadAssemblyDescription(ByVal AssemblyInfo As System.Reflection.Assembly) As String
        Try
            Dim m_description As System.Reflection.AssemblyDescriptionAttribute
            m_description = CType(AssemblyInfo.GetCustomAttributes(GetType(System.Reflection.AssemblyDescriptionAttribute), False)(0), System.Reflection.AssemblyDescriptionAttribute)
            Return m_description.Description.ToString
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ReadAssemblyTitle(ByVal AssemblyInfo As System.Reflection.Assembly) As String
        Try
            Dim m_Title As System.Reflection.AssemblyTitleAttribute
            m_Title = CType(AssemblyInfo.GetCustomAttributes(GetType(System.Reflection.AssemblyTitleAttribute), False)(0), System.Reflection.AssemblyTitleAttribute)
            Return m_Title.Title.ToString
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ReadAssemblyProduct(ByVal AssemblyInfo As System.Reflection.Assembly) As String
        Try
            Dim m_Product As System.Reflection.AssemblyProductAttribute
            m_Product = CType(AssemblyInfo.GetCustomAttributes(GetType(System.Reflection.AssemblyProductAttribute), False)(0), System.Reflection.AssemblyProductAttribute)
            Return m_Product.Product.ToString
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class
