Imports Alianca.Seguranca.BancoDados
Imports Microsoft.Win32
Namespace Alianca
    Namespace Seguranca
        Namespace Web
            Public Class ControleAmbiente
                Private _Usuario_ID As Integer
                Private _AssemblInfo As System.Reflection.Assembly
                Private _ambiente As cCon.Ambientes
                Private _bancodados As String
                Private _post As String
                Private Shared _URL As String
                Private Shared _ambiente_id As Boolean
                Public Function ObterAmbiente(ByVal URL As String) As cCon.Ambientes
                    Dim v_sUrl As String = URL.ToUpper
                    Dim v_sAmbiente As String
                    v_sUrl = v_sUrl.Replace("HTTP://", "")
                    v_sUrl = v_sUrl.Replace("HTTPS://", "")
                    v_sAmbiente = Left(v_sUrl, v_sUrl.IndexOf("/"))
                    Dim sAns As String

                    Dim sErr As String = ""

                    Dim ConnectionString As String

                    sAns = RegValue(RegistryHive.LocalMachine, _
                      "SOFTWARE\AliancaBrasil\SABL0201", _
                      "Conexao", sErr)
                    If sAns <> "" Then
                        ConnectionString = sAns
                    Else
                        ConnectionString = sErr
                    End If

                    ConnectionString = ConnectionString.Replace("Provider=SQLNCLI;Persist Security Info=False;User ID=", "uid=")

                    ConnectionString = ConnectionString.Replace("Initial Catalog=", "database=")

                    ConnectionString = ConnectionString.Replace("Data Source=", "server=")

                    ConnectionString = ConnectionString & ";Pooling=false;Application Name=SABL0201;Connect Timeout=0;"

                    Dim conexao As New SqlConnection(ConnectionString)

                    '                    configura_conexao()
                    Dim Ds As New DataSet
                    Dim Da As New SqlDataAdapter("exec controle_sistema_db..ambiente_web_sps '" & v_sAmbiente & "'", conexao)

                    Da.Fill(Ds)

                    _ambiente = CType(Ds.Tables(0).Rows(0).Item(0), cCon.Ambientes)
                    _post = CType(ds.Tables(0).Rows(0).Item("post"), String)

                    Return CType(_ambiente, cCon.Ambientes)
                End Function

                Private Sub configura_conexao()
                    If cCon.configurado Then
                        _AssemblInfo = cCon.AssemblyInfo
                        _ambiente = cCon.Ambiente
                        _bancodados = cCon.BancodeDados
                        If cCon.Ambiente <> cCon.Ambientes.Produ��o And cCon.Ambiente <> cCon.Ambientes.PRODUCAO_ABS Then
                            cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cCon.Ambientes.Produ��o)
                        End If
                    Else
                        cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cCon.Ambientes.Produ��o)
                    End If
                End Sub
                Private Sub reload_conexao()
                    If Not _AssemblInfo Is Nothing Then
                        If cCon.Ambiente <> _ambiente Then
                            cCon.Reset(_AssemblInfo, _ambiente)
                        End If
                        cCon.BancodeDados = _bancodados
                    Else
                        cCon.Reset()
                    End If
                End Sub
                Public ReadOnly Property Ambiente() As cCon.Ambientes
                    Get
                        Ambiente = _ambiente
                    End Get
                End Property
                Public ReadOnly Property Post() As String
                    Get
                        Post = _post
                    End Get
                End Property
                Public Shared Function RegValue(ByVal Hive As RegistryHive, _
    ByVal Key As String, ByVal ValueName As String, _
    Optional ByRef ErrInfo As String = "") As String

                    'DEMO USAGE



                    Dim objParent As RegistryKey
                    Dim objSubkey As RegistryKey
                    Dim sAns As String
                    Select Case Hive
                        Case RegistryHive.ClassesRoot
                            objParent = Registry.ClassesRoot
                        Case RegistryHive.CurrentConfig
                            objParent = Registry.CurrentConfig
                        Case RegistryHive.CurrentUser
                            objParent = Registry.CurrentUser
                        Case RegistryHive.DynData
                            objParent = Registry.DynData
                        Case RegistryHive.LocalMachine
                            objParent = Registry.LocalMachine
                        Case RegistryHive.PerformanceData
                            objParent = Registry.PerformanceData
                        Case RegistryHive.Users
                            objParent = Registry.Users
                    End Select
                    Try
                        objSubkey = objParent.OpenSubKey(Key)
                        If Not objSubkey Is Nothing Then
                            sAns = Convert.ToString((objSubkey.GetValue(ValueName)))
                        End If
                    Catch ex As Exception
                        ErrInfo = ex.Message
                    Finally
                        If ErrInfo = "" And sAns = "" Then
                            ErrInfo = _
                               "No value found for requested registry key"
                        End If
                    End Try
                    Return sAns
                End Function
            End Class
        End Namespace
    End Namespace
End Namespace
