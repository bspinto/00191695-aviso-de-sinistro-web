Attribute VB_Name = "Module1"
'funcoes de uso local
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function GetProcessHeap Lib "kernel32" () As Long
Private Declare Function HeapAlloc Lib "kernel32" (ByVal hHeap As Long, ByVal dwFlags As Long, ByVal dwBytes As Long) As Long
Private Declare Function HeapFree Lib "kernel32" (ByVal hHeap As Long, ByVal dwFlags As Long, lpMem As Any) As Long
Private Declare Sub CopyMemoryWrite Lib "kernel32" Alias "RtlMoveMemory" (ByVal Destination As Long, Source As Any, ByVal Length As Long)
Private Declare Sub CopyMemoryRead Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, ByVal Source As Long, ByVal Length As Long)



' Gera certificado auto-assinado X509V3 a partir de um csr.
' Parametros:
'    arqCSR - arquivo csr de entrada
'    arqCER - arquivo certificado de saida
'    arqCSRPrivKey - nome do arquivo contendo a chave privada do CSR;
'    senha - senha usada para armazenar a chave
' Retorno:
'             0 - tudo OK!
'             1 - parametro(s) invalido(s)
'             2 - Erro na abertura do csr
'             3 - Erro ao processar csr
'             4 - Erro ao gravar certificado
'             5 - Erro na abertura da chave privada
'             6 - Erro ao assinar o certificado
'
Public Declare Function gerarCertAutoAssinado Lib "msgicp.dll" (ByVal arqCSR As String, ByVal arqCER As String, ByVal arqCSRPrivKey As String, ByVal senha As String) As Long



' Gera certificado X509V3 a partir de um csr.
' Se os parametros relativos a chave privada
' de assinatura forem nulos um certificado
' auto-assinado serah gerado entao.
'
' Parametros:
'    arqCSR - arquivo csr de entrada
'    arqCER - arquivo certificado de saida
'    arqCSRPrivKey - nome do arquivo contendo a chave privada do CSR;
'    csrSenha - senha usada para acessar a chave privada do CSR
'    arqAssCER - arquivo certificado do assinante
'    arqAssPrivKey - nome do arquivo contendo a chave privada do assinante do certificado;
'    assSenha - senha usada para acessar a chave privada do assinante
' Retorno:
'             0 - tudo OK!
'             1 - parametro(s) invalido(s)
'             2 - Erro na abertura do csr
'             3 - Erro ao processar csr
'             4 - Erro ao gravar certificado
'             5 - Erro na abertura da chave privada
'             6 - Erro ao assinar o certificado
'/
Public Declare Function gerarCert Lib "msgicp.dll" (ByVal arqCSR As String, ByVal arqCER As String, ByVal arqCSRPrivKey As String, ByVal csrSenha As String, ByVal arqAssCER As String, ByVal arqAssPrivKey As String, ByVal assSenha As String) As Long


' Recep��o de Certificado
' Cria um repositorio seguro para um certificado novo.
'Alias do repositorio: CertificadoSpbLite
'
' Parametros:
' arqCER - nome do arquivo de certificado;
' arqCSRPrivKey - nome do arquivo contendo a chave privada do CSR;
' csrSenha - senha de acesso aa chave privada do CSR;
' arqRepos - nome do reposit�rio original da CSR;
' repSenha - senha de acesso ao reposit�rio.
' Retorna:
' 0 - ok
Public Declare Function criarRepositorio Lib "msgicp.dll" (ByVal arqCER As String, ByVal arqCSRPrivKey As String, ByVal csrSenha As String, ByVal arqRepos As String, ByVal repSenha As String) As Long


' Muda a senha de um repositorio.
'
' Parametros:
' arqRepos - nome do reposit�rio
' repSenhaAtual - senha de acesso ao reposit�rio.
' repSenhaNova - nova senha de acesso ao reposit�rio.
' Retorna:
' 0 - ok
' 1 - nao foi possivel abrir o arquivo
' 2 - o arquivo nao eh um PKCS 12 valido
' 3 - falha ao mudar a senha, verifique se a senha estah correta
Public Declare Function mudarSenhaRepositorio Lib "msgicp.dll" (ByVal arqRepos As String, ByVal repSenhaAtual As String, ByVal repSenhaNova As String) As Long


' Gera��o e formata��o de mensagem - formatos SPB
' Esta funcionalidade recebe uma mensagem e devolve
' como resultado uma mensagem no formato solicitado,
' criptografada e assinada.
' Use a funcao de calculo para alocar a memoria
' necessaria a geracao de mensagem - int calcularTamMsgSPB(int dadosTam).
'
' Parametros:
' nmArqCertificado - nome do certificado de destino;
' msg - mensagem original;
' tmMsg - tamanho da mensagem original;
' msgEnv - mensagem envelopada (assinada e cifrada);
' arqRepos - arquivo de repositorio do par de chaves a ser utilizado;
' senha - senha de acesso ao reposit�rio.
' Retorna:
' 0 - ok
' X - erro
' 1 - nao foi possivel abrir o arquivo do certificado ou repositorio
' 2 - o arquivo nao eh um PKCS 12 valido ou falha na cifragem 3DES
' 3 - erro ao extrair as infomacoes do arquivo PKCS 12
' 99- Erro ao gerar assinatura RSA
Public Declare Function gerarMsg Lib "msgicp.dll" (ByVal nmArqCertificado As String, ByVal msg As String, ByVal tmMsg As Long, ByVal msgEnv As String, ByVal arqRepos As String, ByVal senha As String) As Long
Private Declare Function gerarMsgPtr Lib "msgicp.dll" Alias "gerarMsg" (ByVal nmArqCertificado As String, ByVal msg As Long, ByVal tmMsg As Long, ByVal msgEnv As Long, ByVal arqRepos As String, ByVal senha As String) As Long

' Esta funcionalidade recebe uma mensagem envelopada
' (cifrada e assinada), e devolve como resultado uma
' mensagem em texto claro.
' Use a funcao de calculo para alocar a memoria
' necessaria para recuperar a mensagem original - int calcularTamMsgOriginal(int tamMsgSPB).
'
' Parametros:
' nmArqCertificadoAss - nome do arquivo do certificado do assinante
' msgEnv - mensagem envelopada (assinada e cifrada);
' tmMsgEnv - tamanho da mensagem envelopada;
' msg - mensagem original a ser recuperada;
' arqRepos - nome do reposit�rio do par de chaves a ser utilizado;
' senha - senha de acesso ao reposit�rio.
' Retorna
' 0 - ok
' X - erro
' 99 - assinatura nao confere
Public Declare Function abrirMsg Lib "msgicp.dll" (ByVal nmArqCertificadoAss As String, ByVal msgEnv As String, ByVal tmMsgEnv As Long, ByVal msg As String, ByVal arqRepos As String, ByVal senha As String, ByVal verificarAssinatura As Long) As Long
Private Declare Function abrirMsgPtr Lib "msgicp.dll" Alias "abrirMsg" (ByVal nmArqCertificadoAss As String, ByVal msgEnv As Long, ByVal tmMsgEnv As Long, ByVal msg As Long, ByVal arqRepos As String, ByVal senha As String, ByVal verificarAssinatura As Long) As Long


' Gera��o de CSR
' Esta funcionalidade permite gerar uma requisi��o de
' certificado, baseada no arquivo de configura��o.
' O arquivo de configura��o deve ser alterado conforme
' os dados da empresa. � fornecido um arquivo modelo (csr.conf)
'
' Parametros:
' arqCFG - nome do arquivo de configura��o;
' arqCSR - nome do arquivo de saida CSR;
' arqCSRPrivKey - nome do arquivo contendo a chave privada do CSR;
' repSenha - senha de acesso ao reposit�rio.
' Retorno:
' 0 - OK
' X - Erro
Public Declare Function gerarCSR Lib "msgicp.dll" (ByVal arqCFG As String, ByVal arqCSR As String, ByVal arqCSRPrivKey As String, ByVal repSenha As String) As Long


' Gera��o de CSR de Teste
' Esta funcionalidade permite gerar uma requisi��o de
' certificado, baseada no arquivo de configura��o.
' O arquivo de configura��o deve ser alterado conforme
' os dados da empresa. � fornecido um arquivo modelo (csr.conf)
' Se o campo Propriet�rio(Subject) for especificado o
' arquivo de configura��o pode ser nulo.
' O CSR pode ser gerado com extensoes ICP(SubjectAlternativeName),
' o que n�o � correto uma vez que cabe a AC esta tarefa, por�m �
' apenas testes.
'
'
' Parametros:
' arqCFG - nome do arquivo de configura��o;
' subj - Assunto/Propriet�rio ex: "CN=Empresa Teste:12345678901234, OU=ISPB-00000000, OU=SISBACEN-00000, O=ICP-Brasil, L=Brasilia, ST=DF, C=BR";
' subjAltName - pode ser nulo, extensao SubjectAtlternativeName ex: "2.16.76.1.3.4=01011942005995228870000000000000004276129SSPSP, 2.16.76.1.3.3=12345678901234, 2.16.76.1.3.2=Jos� da Silva";
' arqCSR - nome do arquivo de saida CSR;
' arqCSRPrivKey - nome do arquivo contendo a chave privada do CSR;
' repSenha - senha de acesso ao reposit�rio.
' Retorno:
' 0 - OK
' 2  - falha na inicializacao ou criacao de arquivo do CSR
' 3  - PARAMETRO INVALIDO - verificar os parametros passados, se nulo,etc
' 4  - falha ao assinar o csr
' 5  - falha ao ler o arquivo de configuracao
' 6  - falha ao setar subjectDN ou atributos
' 7  - falha ao processar subjAltName extension
Public Declare Function gerarCSR_EXT3_ICP Lib "msgicp.dll" (ByVal arqCFG As String, ByVal subj As String, ByVal subjAltName As String, ByVal arqCSR As String, ByVal arqCSRPrivKey As String, ByVal repSenha As String) As Long


' Calcula o tamanho da mensagem envelopada a ser gerada.
Public Declare Function calcularTamMsgEnv Lib "msgicp.dll" (ByVal tamMsg As Long) As Long


' Calcula o tamanho da mensagem desenvelopada.
Public Declare Function calcularTamMsg Lib "msgicp.dll" (ByVal tamMsgEnv As Long) As Long


' Inicializa a biblioteca.
' Precisa ser feito apenas uma vez.
Public Declare Sub inicializarCryptoICP Lib "msgicp.dll" ()


' Finaliza a biblioteca.
Public Declare Sub finalizarCryptoICP Lib "msgicp.dll" ()


'implementacao
Public Function gerarArqCifrado(ByVal nmArqCertificado As String, ByRef msgArq As String, ByRef msgEnvArq As String, ByVal arqRepos As String, ByVal senha As String) As Long
'Public Function gerarArqCifrado(ByVal nmArqCertificado As String, ByVal msgArq As String, ByVal msgEnvArq As String, ByVal arqRepos As String, ByVal senha As String) As Long

Dim ptrMsg As Long
Dim ptrMsgEnv As Long
Dim tamMsg As Long
Dim tamMsgEnv As Long

'codigo de retorno
Dim ret As Long
ret = 1

'verifica se o arquivo existe
tamMsg = FileLen(msgArq)
If tamMsg < 1 Then
    MsgBox "O arquivo inv�lido: " & msgArq
    GoTo fim
End If

'aloca memoria necessaria
tamMsgEnv = calcularTamMsgEnv(tamMsg)
Dim hHeap As Long
hHeap = GetProcessHeap()
'arquivo mensagem
ptrMsg = HeapAlloc(hHeap, 0, tamMsg)
If ptrMsg = Null Then
    MsgBox "Mem�ria insuficiente, tamanho necess�rio: " & CStr(tamMsg)
    GoTo fim
End If
'arquivo mensagem cifrada
ptrMsgEnv = HeapAlloc(hHeap, 0, tamMsgEnv)
If ptrMsgEnv = Null Then
    MsgBox "Mem�ria insuficiente, tamanho necess�rio para cifrar mensagem: " & CStr(tamMsgEnv)
    GoTo fim
End If

'le arquivo na memoria
Dim Arq As Long
Arq = FreeFile
Open msgArq For Binary Access Read Lock Read Write As #Arq
lerArquivoMemoria ptrMsg, Arq, tamMsg
Close Arq

'gera mensagem
ret = gerarMsgPtr(nmArqCertificado, ptrMsg, tamMsg, ptrMsgEnv, arqRepos, senha)

'grava arquivo se tudo ok
If ret = 0 Then
    'If Not IsMissing(msgEnvArq) Then
    '    Kill (msgEnvArq)
    'End If
    Arq = FreeFile
    Open msgEnvArq For Binary Access Write As #Arq
    gravarArquivoMemoria ptrMsgEnv, Arq, tamMsgEnv
    Close Arq
End If

fim:
'libera ponteiros se necessario
If ptrMsg <> Null Then
    HeapFree GetProcessHeap(), 0, ptrMsg
End If
If ptrMsgEnv <> Null Then
    HeapFree GetProcessHeap(), 0, ptrMsgEnv
End If

gerarArqCifrado = ret
End Function


Public Function abrirArqCifrado(ByVal nmArqCertificadoAss As String, ByVal msgEnvArq As String, ByVal msgArqRecup As String, ByVal arqRepos As String, ByVal senha As String, ByVal verificarAssinatura As Boolean) As Long

'processa parametro
Dim verificarAss As Long
If verificarAssinatura Then
    verificarAss = 1
Else
    verificarAss = 0
End If

Dim ptrMsg As Long
Dim ptrMsgEnv As Long
Dim tamMsg As Long
Dim tamMsgEnv As Long

'codigo de retorno
Dim ret As Long
ret = 1

'verifica se o arquivo existe
tamMsgEnv = FileLen(msgEnvArq)
If tamMsgEnv < 1 Then
    MsgBox "O arquivo inv�lido: " & msgEnvArq
    GoTo fim
End If

'aloca memoria necessaria
tamMsg = calcularTamMsg(tamMsgEnv)
Dim hHeap As Long
hHeap = GetProcessHeap()
'arquivo mensagem
ptrMsgEnv = HeapAlloc(hHeap, 0, tamMsgEnv)
If ptrMsgEnv = Null Then
    MsgBox "Mem�ria insuficiente, tamanho necess�rio: " & CStr(tamMsgEnv)
    GoTo fim
End If
'arquivo mensagem cifrada
ptrMsg = HeapAlloc(hHeap, 0, tamMsg)
If ptrMsg = Null Then
    MsgBox "Mem�ria insuficiente, tamanho necess�rio para recuperar a mensagem: " & CStr(tamMsg)
    GoTo fim
End If

'le arquivo na memoria
Dim Arq As Long
Arq = FreeFile
Open msgEnvArq For Binary Access Read Lock Read Write As #Arq
lerArquivoMemoria ptrMsgEnv, Arq, tamMsgEnv
Close Arq

'abre mensagem
ret = abrirMsgPtr(nmArqCertificadoAss, ptrMsgEnv, tamMsgEnv, ptrMsg, arqRepos, senha, verificarAss)

'grava arquivo se tudo ok
If ret = 0 Then
    'If Not IsMissing(msgArqRecup) Then
    '    Kill (msgArqRecup)
    'End If
    Arq = FreeFile
    Open msgArqRecup For Binary Access Write As #Arq
    gravarArquivoMemoria ptrMsg, Arq, tamMsg
    Close Arq
End If

fim:
'libera ponteiros se necessario
If ptrMsg <> Null Then
    'HeapFree GetProcessHeap(), 0, ptrMsg
End If
If ptrMsgEnv <> Null Then
    'HeapFree GetProcessHeap(), 0, ptrMsgEnv
End If

abrirArqCifrado = ret
End Function

Private Sub lerArquivoMemoria(ptrDest As Long, Arq As Long, tam As Long)

Dim dado As Byte
Dim count As Long
count = 0

'ReDim dado(tam)
'Get #Arq, , dado
'CopyMemoryWrite ptrDest, dado, tam

While count < tam
    Get #Arq, , dado
    CopyMemoryWrite ptrDest + count, dado, 1
    count = count + 1
Wend

End Sub


Private Sub gravarArquivoMemoria(ptrDest As Long, Arq As Long, tam As Long)

Dim dado As Byte
Dim count As Long
count = 0

'ReDim dado(tam)
'CopyMemoryRead dado, ptrDest, tam
'Put Arq, , dado

While count < tam
    CopyMemoryRead dado, ptrDest + count, 1
    Put Arq, , dado
    count = count + 1
Wend


End Sub



Public Function cifra(texto As String) As String
Dim DADOSTXT As String
'registro = cifra(registro)
inicializarCryptoICP

DADOSTXT = "PP10TESTE MSG"
tamDados = Len(texto)
tamMsgSPB = calcularTamMsgEnv(tamDados)
Dim textoCifrado As String
textoCifrado = String(tamMsgSPB, vbNullChar)

ret = gerarMsg("BBDesenv.cer", DADOSTXT, tamDados, textoCifrado, "01-Alianca.p12", "1234")
cifra = textoCifrado
End Function


Public Function decifra(msg As String) As String
Dim arqRep, arqCER As String
Dim textoDescifrado As String
Dim ret, tamMsgSPB, tamDadosRecup, verificarAssinatura As Integer
arqCER = "BBDesenv.cer"
arqRep = "01-Alianca.p12"
senhaRep = "1234"

ret = abrirArqCifrado(arqCER, msg, textoDescifrado, arqRep, senhaRep, True)
decifra = textoDescifrado
End Function

