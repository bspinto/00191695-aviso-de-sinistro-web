CREATE PROCEDURE dbo.solicitante_sinistro_spi (
	@solicitante_id INT
	,@nome VARCHAR(60)
	,@endereco VARCHAR(60)
	,@bairro VARCHAR(60)
	,@municipio_id NUMERIC
	,@municipio VARCHAR(60)
	,@estado CHAR(2)
	,@ddd VARCHAR(4)
	,@telefone VARCHAR(9)
	,@ddd_fax VARCHAR(4)
	,@fax VARCHAR(9)
	,@usuario VARCHAR(20)
	,@cep VARCHAR(8) = NULL --Stefanini (Maur�cio), em 19/04/2005 
	,@CPF VARCHAR(11) = NULL -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB   	
	,@grau_parentesco_id INT = NULL -- cvao 26/03/2012 incluido campos	
	,@ramal VARCHAR(4) = NULL
	,@tp_telefone CHAR(1) = NULL
	,@ddd1 VARCHAR(4) = NULL
	,@telefone1 VARCHAR(9) = NULL
	,@ramal1 VARCHAR(4) = NULL
	,@tp_telefone1 CHAR(1) = NULL
	,@ddd2 VARCHAR(4) = NULL
	,@telefone2 VARCHAR(9) = NULL
	,@ramal2 VARCHAR(4) = NULL
	,@tp_telefone2 CHAR(1) = NULL
	,@ddd3 VARCHAR(4) = NULL
	,@telefone3 VARCHAR(9) = NULL
	,@ramal3 VARCHAR(4) = NULL
	,@tp_telefone3 CHAR(1) = NULL
	,@ddd4 VARCHAR(4) = NULL
	,@telefone4 VARCHAR(9) = NULL -- Cleber Sardo - alterando o tamanho da vari�vel - 10/12/2015 - INC000004785059
	,@ramal4 VARCHAR(4) = NULL
	,@tp_telefone4 CHAR(1) = NULL
	,@grau_parentesco CHAR(1) = NULL
	,@email VARCHAR(60) = NULL
	)
AS
/*
	Desenvolvedor: Marcia Endler Kranen- 25/03/99
	Demanda: Cria��o da procedure	
	Descri��o: Procedure de inclusao de solicitante de sinistro, retorna valor 1
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN  
    DECLARE @solicitante_id int
    select @solicitante_id = MAX(solicitante_id)+1 from seguros_db.dbo.solicitante_sinistro_tb with (nolock)
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.solicitante_sinistro_spi 
      @solicitante_id 
	    ,@nome = 'nome'
	    ,@endereco = 'endereco'
	    ,@bairro = 'bairro'
	    ,@municipio_id = 1
	    ,@municipio = 'municipio'
	    ,@estado = 'RJ'
	    ,@ddd = '24'
	    ,@telefone = '00000000'
	    ,@ddd_fax = null
	    ,@fax = null
	    ,@usuario = 'teste'
	    ,@cep  = NULL
	    ,@CPF  = NULL -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB   
	    ,@grau_parentesco_id= NULL
	    ,@ramal  = NULL
	    ,@tp_telefone  = NULL
	    ,@ddd1  = NULL
	    ,@telefone1 = NULL
	    ,@ramal1  = NULL
	    ,@tp_telefone1  = NULL
	    ,@ddd2  = NULL
	    ,@telefone2 = NULL
	    ,@ramal2  = NULL
	    ,@tp_telefone2  = NULL
	    ,@ddd3  = NULL
	    ,@telefone3 = NULL
	    ,@ramal3  = NULL
	    ,@tp_telefone3  = NULL
	    ,@ddd4  = NULL
	    ,@telefone4 = NULL
	    ,@ramal4  = NULL
	    ,@tp_telefone4  = NULL
	    ,@grau_parentesco  = NULL
	    ,@email  = NULL
    ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  */
BEGIN
	SET NOCOUNT ON

	BEGIN TRY
		INSERT INTO seguros_db.dbo.solicitante_sinistro_tb (
			solicitante_id
			,nome
			,endereco
			,bairro
			,municipio_id
			,municipio
			,estado
			,ddd
			,telefone
			,ramal
			,tp_telefone
			,ddd_fax
			,telefone_fax
			,ddd1
			,telefone1
			,ramal1
			,tp_telefone1
			,ddd2
			,telefone2
			,ramal2
			,tp_telefone2
			,ddd3
			,telefone3
			,ramal3
			,tp_telefone3
			,ddd4
			,telefone4
			,ramal4
			,tp_telefone4
			,CEP
			,grau_parentesco_id
			,grau_parentesco
			,email
			,usuario -- cvao 26/03/2012 incluidos campos 
			,solicitante_CPF -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB 
			)
		VALUES (
			@solicitante_id
			,@nome
			,@endereco
			,@bairro
			,@municipio_id
			,@municipio
			,@estado
			,@ddd
			,@telefone
			,@ramal
			,@tp_telefone
			,@ddd_fax
			,@fax
			,@ddd1
			,@telefone1
			,@ramal1
			,@tp_telefone1
			,@ddd2
			,@telefone2
			,@ramal2
			,@tp_telefone2
			,@ddd3
			,@telefone3
			,@ramal3
			,@tp_telefone3
			,@ddd4
			,@telefone4
			,@ramal4
			,@tp_telefone4
			,@CEP
			,@grau_parentesco_id
			,@grau_parentesco
			,@email
			,@usuario -- cvao 26/03/2012 incluidos campos  
			,@CPF -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB   
			)

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


