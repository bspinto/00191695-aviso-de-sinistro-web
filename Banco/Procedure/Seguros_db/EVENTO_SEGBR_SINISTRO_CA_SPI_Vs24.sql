CREATE PROCEDURE DBO.evento_SEGBR_sinistro_CA_spi (
  @sinistro_id numeric(11,0),                
  @apolice_id numeric(9,0),                
  @seguradora_id numeric(5,0),                
  @sucursal_id numeric(5,0),                
  @ramo_id int,                
  @evento_SEGBR_id int,                
  @evento_bb_id int,                
  @dt_ocorrencia_sinistro smalldatetime,                     
  @entidade_id smallint,                
  @localizacao varchar(10),                
  @usuario UD_usuario,                
  @produto_id int,                
  @proposta_id int,                
  @proposta_bb numeric(9,0),                
  @agencia_aviso numeric(4,0),                
  @segurado_cliente_id int,                
  @segurado_nome_cliente varchar(60),                
  @segurado_CPF varchar(14),                
  @segurado_sexo char(1),                
  @segurado_dt_nascimento smalldatetime,                
  @sinistrado_nome_cliente varchar(60),                
  @sinistrado_cpf varchar(11),                
  @sinistrado_sexo varchar(1),                
  @sinistrado_dt_nascimento smalldatetime,                
  @solicitante_nome varchar(60),                
  @solicitante_endereco varchar(60),                
  @solicitante_bairro varchar(30),                
  @solicitante_CEP varchar(8),                  
  @solicitante_municipio varchar(60),                
  @solicitante_municipio_id numeric(3,0) = null,                
  @solicitante_estado char(2),                
  @solicitante_grau_parentesco char(2),                
  @ddd1 varchar(4)= null,                
  @ddd2 varchar(4) = null,                
  @ddd3 varchar(4) = null,                
  @ddd4 varchar(4) = null,                
  @ddd5 varchar(4) = null,                   
  @telefone1 varchar(10) = null,                
  @telefone2 varchar(9) = null,                
  @telefone3 varchar(9) = null,                
  @telefone4 varchar(9) = null,                
  @telefone5 varchar(9) = null,                     
  @ramal1 varchar(4) = null,                     
  @ramal2 varchar(4) = null,                     
  @ramal3 varchar(4) = null,                     
  @ramal4 varchar(4) = null,                     
  @ramal5 varchar(4) = null,                     
  @tp_telefone1 varchar(1) = null,                     
  @tp_telefone2 varchar(1) = null,                
  @tp_telefone3 varchar(1) = null,                
  @tp_telefone4 varchar(1) = null,                
  @tp_telefone5 varchar(1) = null,                
  @solicitante_email varchar(60),                
  @dt_aviso_sinistro smalldatetime,                
  @evento_sinistro_id int,                
  @descricao_evento varchar(70),                
  @ind_reanalise char(1),                
  @enviado_eMail_tecnico char(1),                 
  @banco_aviso_id numeric(3,0),                
  @num_remessa char(16),                
  @num_proc_remessa char(4),                
  @processa_reintegracao_is char(1),                
  @canal_comunicacao char(1),                
  @ind_tp_sinistrado char(1),                
  @situacao_aviso char(1),                
  @moeda_id numeric(3,0),                
  @endosso_id int,                
  @sinistro_id_lider numeric(30,0),  --numeric(13,0), EF_18229361-Sinistro Cosseguro Aceito SEGBR 07/07/2014              
  @cod_ramo numeric(9,0),                
  @sinistro_bb numeric(13,0),               
  @hora_ocorrencia_sinistro varchar(4) = null,              
  @endereco_sinistro varchar(60) = null ,            
  @bairro_sinistro varchar(30) = null,            
  @municipio_id_sinistro numeric(3,0) = null,            
  @municipio_sinistro varchar(60) = null,            
  @estado_sinistro varchar(2) = null,            
  @cep_sinistro  varchar(8) = null,            
  @tp_ramo_id tinyint = null,            
  @CD_PRD smallint = null,            
  @CD_MDLD smallint = null,            
  @CD_ITEM_MDLD int = null,            
  @num_contr_seguro int =null,    
  @num_ver_endosso int = null,          
  -- Cleber Sardo - INC000004228559 - O Valor ultrapassou o tamanho do campo -  12/12/2013        
  --@Val_Informado numeric(9,2) = null, 
  @Val_Informado numeric(12,2) = null,        
  -- Cleber Sardo - INC000004228559 - O Valor ultrapassou o tamanho do campo -  12/12/2013      
  @subevento_sinistro_id int = null,        
  @tp_log_sinistro_id int = null,        
  @sub_grupo_id int = null,        
  @motivo_reanalise_sinistro_id INT = NULL  ,        
  @dt_ocorrencia_sinistro_fim smalldatetime = NULL,    
  @dt_ocorrencia_sinistro_inicio smalldatetime = NULL,  
  @flagCredito char(1) = NULL,
  @num_protocolo varchar(30) = NULL, 
  @solicitante_CPF varchar(11) = NULL -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB
)  
AS
/*
	Kleber Roberto Nisa Tragante 	- 15/01/2005
	Demanda: Altera��o da Procedure evento_SEGBR_sinistro_CA_spi
	Descri��o: procedure para gravar sinistro.
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*  
  BEGIN TRAN
  
    IF @@TRANCOUNT > 0 EXEC desenv_db.dbo.evento_SEGBR_sinistro_CA_spi 
      @sinistro_id = 1,                
      @apolice_id = 1,                
      @seguradora_id = 1 ,                
      @sucursal_id = 1,                
      @ramo_id = 1,                
      @evento_SEGBR_id = 1,                
      @evento_bb_id = 1,                
      @dt_ocorrencia_sinistro = '2019-01-01',                     
      @entidade_id = 1,                
      @localizacao = '',                
      @usuario = 'teste',                
      @produto_id = 1198,                
      @proposta_id = 1,                
      @proposta_bb = 0,                
      @agencia_aviso = 6865,                
      @segurado_cliente_id = 1,                
      @segurado_nome_cliente = 'teste_cli',                
      @segurado_CPF = '11111111111',                
      @segurado_sexo = 'M',                
      @segurado_dt_nascimento = '2019-01-01',                
      @sinistrado_nome_cliente = 'teste_sin',                
      @sinistrado_cpf = '11111111111',                
      @sinistrado_sexo = 'M',                
      @sinistrado_dt_nascimento = '2019-01-01',                
      @solicitante_nome = 'teste_sol',                
      @solicitante_endereco = 'teste_end',                
      @solicitante_bairro = 'teste_bai',                
      @solicitante_CEP = '25870000',                
      @solicitante_municipio = 'teste_mun',                
      @solicitante_municipio_id  = 1,                
      @solicitante_estado = 'RJ',                
      @solicitante_grau_parentesco = '',                
      @ddd1 = null,                
      @ddd2  = null,                
      @ddd3  = null,                
      @ddd4  = null,                
      @ddd5  = null,                   
      @telefone1  = null,                
      @telefone2  = null,                
      @telefone3  = null,                
      @telefone4  = null,                
      @telefone5  = null,                     
      @ramal1  = null,                     
      @ramal2  = null,                     
      @ramal3  = null,                     
      @ramal4  = null,                     
      @ramal5  = null,                     
      @tp_telefone1  = null,                     
      @tp_telefone2  = null,                
      @tp_telefone3  = null,                
      @tp_telefone4  = null,                
      @tp_telefone5  = null,                
      @solicitante_email = 'teste_ema',                
      @dt_aviso_sinistro = '2019-01-01',                
      @evento_sinistro_id = 1,                
      @descricao_evento = 'descricao_evento',                
      @ind_reanalise = '0',                
      @enviado_eMail_tecnico = '0',                 
      @banco_aviso_id = 0,                
      @num_remessa = '0',                
      @num_proc_remessa = '0000',                
      @processa_reintegracao_is = '0',                
      @canal_comunicacao = '0',                
      @ind_tp_sinistrado = '0',                
      @situacao_aviso = '0',                
      @moeda_id = 1,                
      @endosso_id = 1,                
      @sinistro_id_lider = 1,  --numeric(13,0), EF_18229361-Sinistro Cosseguro Aceito SEGBR 07/07/2014              
      @cod_ramo = 1,                
      @sinistro_bb = 1,               
      @hora_ocorrencia_sinistro  = null,              
      @endereco_sinistro = null ,            
      @bairro_sinistro  = null,            
      @municipio_id_sinistro  = null,            
      @municipio_sinistro  = null,            
      @estado_sinistro  = null,            
      @cep_sinistro   = null,            
      @tp_ramo_id  = null,            
      @CD_PRD  = null,            
      @CD_MDLD  = null,            
      @CD_ITEM_MDLD  = null,            
      @num_contr_seguro  =null,    
      @num_ver_endosso  = null,                
      --@Val_Informado  = null, -- Cleber Sardo - INC000004228559 - O Valor ultrapassou o tamanho do campo -  12/12/2013        
      @Val_Informado  = null,  -- Cleber Sardo - INC000004228559 - O Valor ultrapassou o tamanho do campo -  12/12/2013                  
      @subevento_sinistro_id  = null,        
      @tp_log_sinistro_id  = null,        
      @sub_grupo_id = null,        
      @motivo_reanalise_sinistro_id = NULL  ,        
      @dt_ocorrencia_sinistro_fim  = NULL,    
      @dt_ocorrencia_sinistro_inicio  = NULL,  
      @flagCredito  = NULL,
      @num_protocolo = NULL,
      @solicitante_CPF = '11111111111' -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB 
        
    ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON
  Declare                 
   @produto_bb int,                
   @dt_contratacao smalldatetime,                
   @cod_transacao varchar(4),    
   @num_recibo numeric(3)          

	BEGIN TRY               
    SELECT  @dt_contratacao = dt_contratacao                
    FROM   seguros_db.dbo.proposta_tb WITH (NOLOCK)              
    WHERE   proposta_id = @proposta_id                
                    
    if ( isnull(@produto_id,0) > 999 AND (isnull(@CD_PRD,0) = 0 OR isnull(@CD_MDLD,0) = 0 OR isnull(@CD_ITEM_MDLD,0) = 0 OR isnull(@num_contr_seguro,0) = 0 OR isnull(@num_ver_endosso,0) = 0) )
      BEGIN
	      SELECT @CD_PRD = cod_produto
		       , @CD_MDLD = cod_modalidade
		       , @CD_ITEM_MDLD = cod_item_modalidade
		       , @num_contr_seguro = num_contrato_seguro
		       , @num_ver_endosso = num_versao_endosso
	      FROM seguros_db.dbo.PROPOSTA_PROCESSO_SUSEP_TB WITH (NOLOCK)
	      WHERE proposta_id = @proposta_id
      END

    if  ( isnull(@produto_id,0) > 999 )
    BEGIN
      SET @cod_transacao = 'ST10'            
    END
    else            
    BEGIN
      SET @cod_transacao = 'SN10'            
    END
      
    IF  @produto_id is null                
    BEGIN                
       SELECT @produto_bb = 100006                
    END                
    ELSE             
    BEGIN                
     SELECT  @produto_bb = produto_bb                 
     FROM  seguros_db.dbo.produto_bb_sinistro_tb WITH (NOLOCK)              
     WHERE  produto_id = @produto_id                
     AND  dt_inicio_vigencia <= @dt_contratacao                
     AND  (dt_fim_vigencia >= @dt_contratacao OR dt_fim_vigencia is null)                
    END      
        
    --- FLOW 205983 - Leandro A. Souza - Stefanini IT - 08/03/2007    
    ----------------------------------------------------------------------------------------------------------    
    IF (@solicitante_municipio_id IS NOT NULL) AND (@solicitante_municipio IS NOT NULL) AND (@solicitante_estado IS NULL)    
    BEGIN    
      SELECT @solicitante_estado = ESTADO FROM seguros_db.dbo.MUNICIPIO_TB WITH (NOLOCK)    
      WHERE MUNICIPIO_ID = @solicitante_municipio_id    
      AND NOME = @solicitante_municipio    
    END    
        
    --Anderson Fiuza - GPTI 12/02/2010 -  Estava gerando o numero de recibo como NULL.    
    --Banco s� aceita o cancelamento de sinistro como recibo 0.    
    --17/02/2010 -- Incluso para o evento_bb_id  = 1120  
        
    IF(@EVENTO_BB_ID = 1121 OR @EVENTO_BB_ID = 1120)    
    BEGIN     
       SET @NUM_RECIBO =0    
    END   

    -- rodrigo.moreira - Nova Consultoria - 19/11/2013 
    -- 11228568 - Implantar Modelo de Gest�o de Sinistro (Firstone) 
    -- Busca do evento_bb_id caso esse seja nulo. 
    /* �nicio */
    IF(@EVENTO_BB_ID IS NULL)      
    BEGIN
        SELECT @EVENTO_BB_ID = evento_bb_id
          FROM seguros_db.dbo.evento_segbr_tb WITH (NOLOCK)
         WHERE evento_segbr_id = @evento_SEGBR_id
    END     
    /* Fim */
      
    ----------------------------------------------------------------------------------------------------------    
                
    /******************************************************************************************************************/                
    /* Procedure de inclus�o na tabela evento_SEGBR_sinistro_tb para sinistro pela CA                                */                
    /******************************************************************************************************************/                
        
    INSERT INTO seguros_db.dbo.evento_SEGBR_sinistro_atual_tb (
	    sinistro_id
	    ,apolice_id
	    ,seguradora_cod_susep
	    ,sucursal_seguradora_id
	    ,ramo_id
	    ,evento_SEGBR_id
	    ,evento_bb_id
	    ,dt_ocorrencia_sinistro
	    ,entidade_id
	    ,localizacao
	    ,usuario
	    ,produto_id
	    ,proposta_id
	    ,num_recibo
	    ,proposta_bb
	    ,agencia_aviso_id
	    ,cliente_id
	    ,nome_segurado
	    ,cpf_cgc_segurado
	    ,sexo_segurado
	    ,dt_nascimento_segurado
	    ,nome_sinistrado
	    ,cpf_sinistrado
	    ,sexo_sinistrado
	    ,dt_nascimento_sinistrado
	    ,nome_solicitante
	    ,endereco_solicitante
	    ,solicitante_bairro
	    ,solicitante_CEP
	    ,solicitante_cpf -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB              
	    ,solicitante_municipio 
	    ,solicitante_municipio_id
	    ,solicitante_estado
	    ,solicitante_grau_parentesco
	    ,ddd_solicitante
	    ,ddd1
	    ,ddd2
	    ,ddd3
	    ,ddd4
	    ,telefone_solicitante
	    ,telefone1
	    ,telefone2
	    ,telefone3
	    ,telefone4
	    ,ramal
	    ,ramal1
	    ,ramal2
	    ,ramal3
	    ,ramal4
	    ,tp_telefone
	    ,tp_telefone1
	    ,tp_telefone2
	    ,tp_telefone3
	    ,tp_telefone4
	    ,solicitante_email
	    ,dt_aviso_sinistro
	    ,evento_sinistro_id
	    ,descricao_evento
	    ,dt_inclusao
	    ,dt_evento
	    ,ind_reanalise
	    ,enviado_eMail_tecnico
	    ,banco_aviso_id
	    ,num_remessa
	    ,cod_produto_bb
	    ,num_proc_remessa
	    ,processa_reintegracao_is
	    ,canal_comunicacao
	    ,ind_tp_sinistrado
	    ,situacao_aviso
	    ,moeda_id
	    ,endosso_id
	    ,sinistro_id_lider
	    ,cod_ramo
	    ,sinistro_bb
	    ,hora_ocorrencia_sinistro
	    ,endereco_sinistro
	    ,bairro_sinistro
	    ,municipio_id_sinistro
	    ,municipio_sinistro
	    ,estado_sinistro
	    ,cep_sinistro
	    ,tp_ramo_id
	    ,CD_PRD
	    ,CD_MDLD
	    ,CD_ITEM_MDLD
	    ,NR_CTC_SGRO
	    ,NR_VRS_EDS
	    ,cod_transacao
	    ,VAL_INFORMADO
	    ,subevento_sinistro_id
	    ,tp_log_sinistro_id
	    ,sub_grupo_id
	    ,motivo_reanalise_sinistro_id
	    ,dt_ocorrencia_sinistro_fim
	    ,dt_ocorrencia_sinistro_inicio
	    ,autoriza_credito_em_conta --rsilva 25/06/2009  
	    ,NR_PTC_AVISO --aricardo 25/11/2010
	    ) 
    VALUES (
	    @sinistro_id
	    ,@apolice_id
	    ,@seguradora_id
	    ,@sucursal_id
	    ,@ramo_id
	    ,@evento_SEGBR_id
	    ,@evento_bb_id
	    ,@dt_ocorrencia_sinistro
	    ,@entidade_id
	    ,@localizacao
	    ,@usuario
	    ,@produto_id
	    ,@proposta_id
	    ,@num_recibo
	    ,@proposta_bb
	    ,@agencia_aviso
	    ,@segurado_cliente_id
	    ,@segurado_nome_cliente
	    ,@segurado_CPF
	    ,@segurado_sexo
	    ,@segurado_dt_nascimento
	    ,@sinistrado_nome_cliente
	    ,@sinistrado_cpf
	    ,@sinistrado_sexo
	    ,@sinistrado_dt_nascimento
	    ,@solicitante_nome
	    ,@solicitante_endereco
	    ,@solicitante_bairro
	    ,@solicitante_CEP
	    ,@solicitante_CPF -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB
	    ,@solicitante_municipio
	    ,@solicitante_municipio_id
	    ,@solicitante_estado
	    ,@solicitante_grau_parentesco
	    ,@ddd1
	    ,@ddd2
	    ,@ddd3
	    ,@ddd4
	    ,@ddd5
	    ,@telefone1
	    ,@telefone2
	    ,@telefone3
	    ,@telefone4
	    ,@telefone5
	    ,@ramal1
	    ,@ramal2
	    ,@ramal3
	    ,@ramal4
	    ,@ramal5
	    ,@tp_telefone1
	    ,@tp_telefone2
	    ,@tp_telefone3
	    ,@tp_telefone4
	    ,@tp_telefone5
	    ,@solicitante_email
	    ,@dt_aviso_sinistro
	    ,@evento_sinistro_id
	    ,@descricao_evento
	    ,getdate()
	    ,getdate()
	    ,@ind_reanalise
	    ,@enviado_eMail_tecnico
	    ,@banco_aviso_id
	    ,@num_remessa
	    ,@produto_bb
	    ,@num_proc_remessa
	    ,@processa_reintegracao_is
	    ,@canal_comunicacao
	    ,@ind_tp_sinistrado
	    ,@situacao_aviso
	    ,@moeda_id
	    ,@endosso_id
	    ,@sinistro_id_lider
	    ,@cod_ramo
	    ,@sinistro_bb
	    ,@hora_ocorrencia_sinistro
	    ,@endereco_sinistro
	    ,@bairro_sinistro
	    ,@municipio_id_sinistro
	    ,@municipio_sinistro
	    ,@estado_sinistro
	    ,@cep_sinistro
	    ,@tp_ramo_id
	    ,@CD_PRD
	    ,@CD_MDLD
	    ,@CD_ITEM_MDLD
	    ,@num_contr_seguro
	    ,@num_ver_endosso
	    ,@cod_transacao
	    ,@Val_Informado
	    ,@subevento_sinistro_id
	    ,@tp_log_sinistro_id
	    ,@sub_grupo_id
	    ,@motivo_reanalise_sinistro_id
	    ,@dt_ocorrencia_sinistro_fim
	    ,@dt_ocorrencia_sinistro_inicio
	    ,@flagCredito
	    ,@num_protocolo
	    )
     
     declare @endosso_id_resseg int
     ----------------------------------------------------------------------------------------------------------------    
    -- INICIO - MU-2017-042360 buscar endosso do resseguro vigente no momento do sinistro -- Rog�rio Melo 17/09/2018
          SELECT @endosso_id_resseg = ISNULL(MAX(endosso_tb.endosso_id),0) 
          FROM seguros_db.dbo.evento_SEGBR_sinistro_atual_tb AS evento_SEGBR_sinistro_atual_tb WITH (NOLOCK)
         INNER JOIN seguros_db.dbo.endosso_tb AS endosso_tb WITH(NOLOCK)
            ON endosso_tb.proposta_id = evento_SEGBR_sinistro_atual_tb.proposta_id 
         INNER JOIN seguros_db.dbo.endosso_financeiro_tb as endosso_financeiro_tb with (nolock)
            on endosso_tb.proposta_id = endosso_financeiro_tb.proposta_id 
	       and endosso_tb.endosso_id = endosso_financeiro_tb.endosso_id 
         INNER JOIN resseg_db.dbo.contrato_apolice_tb  AS contrato_apolice_tb WITH(NOLOCK)
	        ON contrato_apolice_tb.apolice_id = evento_segbr_sinistro_atual_tb.apolice_id
	       AND contrato_apolice_tb.ramo_id = evento_segbr_sinistro_atual_tb.ramo_id 
         WHERE evento_SEGBR_sinistro_atual_tb.evento_bb_id = 1100 
           AND endosso_tb.dt_pedido_endosso <= evento_SEGBR_sinistro_atual_tb.dt_ocorrencia_sinistro
           AND ENDOSSO_TB.proposta_id = @PROPOSTA_ID
           AND evento_SEGBR_sinistro_atual_tb.sinistro_id = @SINISTRO_ID

       UPDATE evento_SEGBR_sinistro_atual_tb
          SET endosso_id_resseg = @endosso_id_resseg 
         FROM seguros_db.dbo.evento_SEGBR_sinistro_atual_tb as evento_SEGBR_sinistro_atual_tb WITH (NOLOCK) 
        WHERE evento_SEGBR_sinistro_atual_tb.evento_bb_id = 1100 
          AND evento_SEGBR_sinistro_atual_tb.proposta_id = @PROPOSTA_ID
          AND evento_SEGBR_sinistro_atual_tb.sinistro_id = @SINISTRO_ID
      -- FIM - MU-2017-042360 buscar endosso do resseguro vigente no momento do sinistro -- Rog�rio Melo 17/09/2018
     --------------------------------------------------------------------------------------------------------------   
                    
    SELECT SCOPE_Identity()
                          
		SET NOCOUNT OFF
        
    RETURN SCOPE_Identity()

	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO




