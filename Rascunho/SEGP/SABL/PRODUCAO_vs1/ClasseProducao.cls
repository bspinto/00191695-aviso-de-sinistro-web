VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClasseProducao"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'Fun��o API que retorna o nome simplificado do exe
Private Declare Function GetShortPathName Lib "kernel32" Alias "GetShortPathNameA" (ByVal lpszLongPath As String, ByVal lpszShortPath As String, ByVal cchBuffer As Long) As Long
Private Declare Function GetFullPathName Lib "kernel32" Alias "GetFullPathNameA" (ByVal lpFileName As String, ByVal nBufferLength As Long, ByVal lpBuffer As String, ByVal lpFilePart As String) As Long

'log_diario_manual_spi OK
'processo_sps OK
'agenda_spi OK
'agenda_diaria_ordem_sps (n�o faz mais sentido)
'agenda_diaria_spi OK
'agenda_diaria_processando_spu OK
'parametro_processo_sps OK
'parametro_diario_agenda_sps OK
'parametro_diario_spi ok

'Mantem a conec��o com o banco atrav�s da Tabela tempor�ria
'##tabela_temp + agenda_diaria_id
Private objAmb_Temp As Object

'Propriedades publicas
Private mvarAgenda_Diaria_id As Long
Private mvarLocalizacao      As String
Private mvarUsuario          As String

'propriedades locais
Private mvarexecutavel_id    As Integer
Private mvarTarefa_id        As Integer
Private mvarTp_Agenda        As String

Private mFlag_Libera_tudo    As Boolean
Friend Property Let Tp_Agenda(ByVal vData As String)
    mvarTp_Agenda = vData
End Property

'Propriedade Interna da Classe
Friend Property Get Tp_Agenda() As String
    Tp_Agenda = mvarTp_Agenda
End Property

'Propriedade Interna da Classe
Friend Property Let Tarefa_id(ByVal vData As Integer)
    mvarTarefa_id = vData
End Property
Friend Property Get Tarefa_id() As Integer
    Tarefa_id = mvarTarefa_id
End Property

'Propriedade Interna da Classe
Friend Property Let executavel_id(ByVal vData As Integer)
    mvarexecutavel_id = vData
End Property
Friend Property Get executavel_id() As Integer
    executavel_id = mvarexecutavel_id
End Property

'Propriedade Publica da Classe
Public Property Let Usuario(ByVal vData As String)
    mvarUsuario = vData
End Property
Public Property Get Usuario() As String
    Usuario = mvarUsuario
End Property

'Propriedade Publica da Classe mas Read-only
Private Sub S_Localizacao(ByVal vData As String)
    mvarLocalizacao = vData
End Sub
Public Property Get Localizacao() As String
    Localizacao = mvarLocalizacao
End Property
'Propriedade Publica da Classe mas Read-only
Private Sub S_Agenda_Diaria_id(ByVal vData As Long)
    mvarAgenda_Diaria_id = vData
End Sub
Public Property Get Agenda_Diaria_id() As Long
    Agenda_Diaria_id = mvarAgenda_Diaria_id
End Property

'Metodos Externos:
Public Function InicializaAutomatico(ByVal P_Agenda_diaria_id As Long) As Boolean

  'Atribui o parametro passado Agenda_diaria_id
  S_Agenda_Diaria_id P_Agenda_diaria_id
  'Seta flag como automatico
  Me.Tp_Agenda = "P"
  'Seta o processo como "em processamento"
  S_Atualiza_Processado "P", False
  
  Set objAmb_Temp = CreateObject("Ambiente.ClasseAmbiente")
  'Maur�cio(Stefanini), em 26/01/2006 - FLOWBR 119709 - Dropar a tabela caso existir, evitando erro ao re-executar tarefa
  Call objAmb_Temp.ExecutaAmbiente("IF (SELECT COUNT(1) FROM tempdb..sysobjects WHERE NAME LIKE '##TABELA_TEMP" & Me.Agenda_Diaria_id & "%') > 0 DROP TABLE ##TABELA_TEMP" & Me.Agenda_Diaria_id)
  Call objAmb_Temp.ExecutaAmbiente("CREATE TABLE ##TABELA_TEMP" & Me.Agenda_Diaria_id & "  (agenda_diaria_id int) ")
  
  InicializaAutomatico = True

End Function

Public Function InicializaManual(ByVal P_Localizacao As String, ByVal P_Data_Sistema As Date) As Boolean

Dim sNome_exe As String
Dim SS        As Object
Dim SQL       As String
Dim ObjAmb    As Object
Dim ObjAmb_aux As Object
    
On Error GoTo Error_Label

  Set ObjAmb = CreateObject("Ambiente.ClasseAmbiente")
  
  sNome_exe = Obtem_Nome_Executavel_Completo(P_Localizacao)
  
  'Chama sub para atribuir a propriedade
  'de execucao manual o valor inicializado
  S_Localizacao sNome_exe
  
  'Seta flag como Manual
  Me.Tp_Agenda = "M"
  
  'monta query para pegar o processo_id
  SQL = "Exec executavel_sps NULL , '" & sNome_exe & "'"
  Set SS = ObjAmb.ExecutaAmbiente(SQL)
  'verifica a existencia do processo-id,
  'caso n�o exista o programa deve aborta-lo
  If Not SS.EOF Then
    Me.executavel_id = SS("executavel_id")
    InicializaManual = True
  Else
    InicializaManual = True
    mFlag_Libera_tudo = True
    Exit Function
  End If
   
  SS.Close
  Set SS = Nothing
  
  'verifica se existe registro em Agenda_tb para processamento Manual
  SQL = "Agenda_sps '" & Format(P_Data_Sistema, "yyyymmdd") & "', '" & Me.Tp_Agenda & "'"
  Set SS = ObjAmb.ExecutaAmbiente(SQL)
  
  'Se n�o existir, cria registro em agenda_tb
  If SS.EOF Then
    SQL = "Exec agenda_spi '" & Format(P_Data_Sistema, "yyyymmdd") & "','" & Me.Tp_Agenda & "'," & Me.Usuario
    
    Set ObjAmb_aux = CreateObject("Ambiente.ClasseAmbiente")
    ObjAmb_aux.ExecutaAmbiente SQL
    Set ObjAmb_aux = Nothing
  
  End If
  
  SS.Close
  Set SS = Nothing
  
  'Monta Insert da agenda_diaria_tb
  SQL = "Exec agenda_diaria_spi "
  SQL = SQL & "NULL, "
  SQL = SQL & "'" & Format(P_Data_Sistema, "yyyymmdd") & "'"
  SQL = SQL & ", 'M'"
  SQL = SQL & ", " & Me.executavel_id
  SQL = SQL & ", NULL"
  SQL = SQL & ", NULL "
  SQL = SQL & ", 'P'"
  SQL = SQL & ", 'N'"
  SQL = SQL & ", '" & F_HoraAgora() & "'"
  SQL = SQL & ", NULL"
  SQL = SQL & ", NULL"
  SQL = SQL & "," & Me.Usuario
  
  'grava agenda_diaria_tb e pega agenda_diaria_id
  Set SS = ObjAmb.ExecutaAmbiente(SQL)
  S_Agenda_Diaria_id SS("agenda_diaria_id")
    
  Set objAmb_Temp = CreateObject("Ambiente.ClasseAmbiente")
  'Maur�cio(Stefanini), em 26/01/2006 - FLOWBR 119709 - Dropar a tabela caso existir, evitando erro ao re-executar tarefa
  Call objAmb_Temp.ExecutaAmbiente("IF (SELECT COUNT(1) FROM tempdb..sysobjects WHERE NAME LIKE '##TABELA_TEMP" & Me.Agenda_Diaria_id & "%') > 0 DROP TABLE ##TABELA_TEMP" & Me.Agenda_Diaria_id)
  Call objAmb_Temp.ExecutaAmbiente("CREATE TABLE ##TABELA_TEMP" & Me.Agenda_Diaria_id & "  (agenda_diaria_id int) ")
  
  SS.Close
  Set SS = Nothing
  
  Exit Function

Error_Label:

    InicializaManual = False
  
End Function

Public Function Finaliza() As Boolean

If mFlag_Libera_tudo Then Exit Function

  S_Atualiza_Processado "S", True

  Finaliza = True

End Function

Public Function Cancelar() As Boolean

  S_Atualiza_Processado "C", True

  Cancelar = True

End Function

Private Sub S_Atualiza_Processado(PProcessado As String, ByVal pFim As Boolean)
Dim SQL    As String
Dim ObjAmb As Object

  Set ObjAmb = CreateObject("Ambiente.ClasseAmbiente")
  
  SQL = "Exec agenda_diaria_processando_spu "
  SQL = SQL & Me.Agenda_Diaria_id
  SQL = SQL & ", '" & PProcessado & "'"
  SQL = SQL & ", " & Me.Usuario
  If pFim Then
    SQL = SQL & ",'" & F_HoraAgora() & "'"
  End If
  
  ObjAmb.ExecutaAmbiente SQL

End Sub

Public Function LeParametros(ByRef PFrm As Object) As Boolean

Dim Contr  As Object
Dim SValor As String

LeParametros = True
If mFlag_Libera_tudo Then Exit Function

For Each Contr In PFrm
  If IsNumeric(Contr.Tag) Then
    SValor = ""
    If F_PegaValorParametro(Contr.Tag, SValor) Then
      Select Case TypeName(Contr)
        
        Case "UserControl1"
          Contr = SValor
        
        Case "ConMask2S"
          Contr = SValor
        
        Case "TextBox"
          Contr = SValor
        
        Case "OptionButton"
          If UCase(SValor) = "S" Then
            Contr = True
          Else
            Contr = False
          End If
        
        Case "MaskEdBox"
          Contr.PromptInclude = False
          Contr = SValor
          Contr.PromptInclude = True
        
        Case "ComboBox"
          S_PosicionaCMB Contr, SValor, 0
        
        Case "CheckBox"
          If UCase(SValor) = "S" Then
            Contr = 1
          Else
            Contr = 0
          End If
        
        Case "ListBox"
          S_PosicionaLST Contr, SValor, 0
        
        Case Else
          LeParametros = False
          Exit For
        
      End Select
    
    Else
      LeParametros = False
      Exit For
    End If
  End If
Next

End Function

Private Function F_PegaValorParametro(P_Parametro_diario_id As Integer, ByRef Valor As String) As Boolean
Dim ObjAmb  As Object
Dim SS      As Object
Dim SQL     As String

  On Error GoTo Error_Label

  Set ObjAmb = CreateObject("Ambiente.ClasseAmbiente")

  F_PegaValorParametro = False
  
  SQL = "Exec parametro_diario_sps " & Me.Agenda_Diaria_id & ","
  SQL = SQL & P_Parametro_diario_id
  
  Set SS = ObjAmb.ExecutaAmbiente(SQL)
  
  If Not SS.EOF Then
    F_PegaValorParametro = True
    Valor = SS("Valor")
  End If
  
  SS.Close
  Set SS = Nothing
  
Exit Function
Error_Label:
  
  F_PegaValorParametro = False
  
End Function

Public Function GravaParametros(PFrm As Object) As Boolean

Dim Contr  As Object

GravaParametros = True

If mFlag_Libera_tudo Then Exit Function

For Each Contr In PFrm
  If Contr.Tag <> "" Then
    If F_ParametroValido(Contr.Tag) Then
    
      Select Case TypeName(Contr)
        
        Case "UserControl1"
          S_GravaParametro Contr.Tag, CStr(Contr)
        
        Case "TextBox"
          S_GravaParametro Contr.Tag, CStr(Contr)
        
        Case "OptionButton"
          If Contr.Value Then
            S_GravaParametro Contr.Tag, "S"
          Else
            S_GravaParametro Contr.Tag, "N"
          End If
        
        Case "MaskEdBox"
            S_GravaParametro Contr.Tag, CStr(Contr)
        
        Case "ComboBox"
            S_GravaParametro Contr.Tag, CStr(Contr)
        
        Case "CheckBox"
          If Contr = 1 Then
            S_GravaParametro Contr.Tag, "S"
          Else
            S_GravaParametro Contr.Tag, "N"
          End If
        
        Case "ListBox"
          S_GravaParametro Contr.Tag, CStr(Contr.List(Contr.ListIndex))
        
        Case Else
          GravaParametros = False
          Exit For
        
      End Select
    
    Else
      GravaParametros = False
      Exit For
    End If
  End If
Next


End Function

Private Function F_ParametroValido(P_Parametro_executavel_id As Integer) As Boolean
Dim ObjAmb  As Object
Dim SS      As Object
Dim SQL     As String

  Set ObjAmb = CreateObject("Ambiente.ClasseAmbiente")

  F_ParametroValido = True

  SQL = "Exec parametro_exec_sps " & Me.executavel_id & ", " & P_Parametro_executavel_id
  Set SS = ObjAmb.ExecutaAmbiente(SQL)

  If SS.EOF Then
    F_ParametroValido = False
    
    SS.Close
    Set SS = Nothing
    
    SQL = "Exec Executavel_sps " & Me.executavel_id & ", NULL"
    Set SS = ObjAmb.ExecutaAmbiente(SQL)
    
    If UCase(SS("Liberado")) = "N" Then
      F_ParametroValido = True
      mFlag_Libera_tudo = True
    End If
           
    SS.Close
    Set SS = Nothing
           
  End If

End Function

Public Function AdicionaLog(ByVal P_Log_id As Integer, ByVal P_Valor As String, Optional ByVal P_Seq_execucao As Integer = 1) As Boolean
Dim SQL     As String
Dim ObjAmb  As Object
Dim SS      As Object

On Error GoTo Error_Label

Set ObjAmb = CreateObject("Ambiente.ClasseAmbiente")

AdicionaLog = True

If mFlag_Libera_tudo Then Exit Function

SQL = "EXEC log_diario_manual_spi " & Me.Agenda_Diaria_id & ", "
SQL = SQL & P_Log_id & ", '"
SQL = SQL & P_Valor & "' , '"
SQL = SQL & Me.Usuario & "', "
SQL = SQL & P_Seq_execucao

Set SS = ObjAmb.ExecutaAmbiente(SQL)

If SS("ERRO") = "S" Then
  AdicionaLog = False
End If

  SS.Close
  Set SS = Nothing

Exit Function
Error_Label:
  AdicionaLog = False
End Function

Private Function F_HoraAgora() As String
Dim ObjAmb  As Object
Dim SS      As Object

  Set ObjAmb = CreateObject("Ambiente.ClasseAmbiente")

  Set SS = ObjAmb.ExecutaAmbiente("Select getdate()")

  F_HoraAgora = Format(SS(0), "yyyymmdd hh:mm:ss")

  SS.Close
  Set SS = Nothing

End Function

Private Sub S_PosicionaCMB(CMB As Object, Valor, Tipo As Integer)
    
    'Tipo=1 foi passado o valor do itemdata
    'Tipo<>1 foi passada a descri��o
    
    CMB.ListIndex = -1
    Dim I As Integer
    Dim FIM As Integer
    FIM = CMB.ListCount - 1
    If Tipo = 1 Then
       For I = 0 To FIM
           If UCase(CMB.ItemData(I)) = UCase(Valor) Then
              CMB.ListIndex = I
              Exit For
           End If
       Next I
    Else
       For I = 0 To FIM
           If UCase(Trim(CMB.List(I))) = UCase(Trim(Valor)) Then
              CMB.ListIndex = I
              Exit For
           End If
       Next I
    End If
End Sub
Private Sub S_PosicionaLST(LST As ListBox, Valor, Tipo As Integer)
    
    'Tipo=1 foi passado o valor do itemdata
    'Tipo<>1 foi passada a descri��o
    
    LST.ListIndex = -1
    Dim I As Integer
    Dim FIM As Integer
    FIM = LST.ListCount - 1
    If Tipo = 1 Then
       For I = 0 To FIM
           LST.Selected(I) = False
           If UCase(LST.ItemData(I)) = UCase(Valor) Then
              LST.Selected(I) = True
              LST.ListIndex = I
              Exit For
           End If
       Next I
    Else
       For I = 0 To FIM
           LST.Selected(I) = False
           If UCase(Trim(LST.List(I))) = UCase(Trim(Valor)) Then
              LST.Selected(I) = True
              LST.ListIndex = I
              Exit For
           End If
       Next I
    End If
End Sub



Private Sub S_GravaParametro(P_Parametro_diario_id As Integer, P_Valor As String)
Dim SQL As String
Dim ObjAmb As Object

  Set ObjAmb = CreateObject("Ambiente.ClasseAmbiente")
  
  SQL = "Exec parametro_diario_spi " & Agenda_Diaria_id & " , "
  SQL = SQL & P_Parametro_diario_id & ",'"
  SQL = SQL & P_Valor & "', '"
  SQL = SQL & Me.Usuario & "'"
  
  ObjAmb.ExecutaAmbiente SQL
  

End Sub



Public Function Automatico() As Boolean
  
  If Me.Tp_Agenda = "M" Then
    Automatico = False
  ElseIf Me.Tp_Agenda = "P" Then
    Automatico = True
  End If
  
End Function

Public Function Obtem_Nome_Executavel_Completo(sLocalizacao As String) As String
'Passa o Nome do Execut�vel e recebe o nome completo sem o path

Dim lPosicaoTill         As Long
Dim sExeName             As String
Dim sPathFileSemTill     As String
Dim lPosicao             As Long
Dim lPosicaoUltimaBarra  As Long
Dim lPosicao2            As Long
Dim lPosicaoUltimaBarra2 As Long
Dim length               As Long
Dim short_path           As String
Dim file_name            As String
Dim sPath                As String
Dim sPathTill            As String
Dim sFilePart            As String
Dim lErro As Long

    lPosicao = 1
    Do While lPosicao <> 0
        lPosicao = lPosicao + 1
        lPosicaoUltimaBarra = lPosicao
        lPosicao = InStr(lPosicao, sLocalizacao, "\")
    Loop

    'Procura a posicao do till
    lPosicaoTill = InStr(lPosicaoUltimaBarra, sLocalizacao, "~")

    sPath = Mid(sLocalizacao, 1, lPosicaoUltimaBarra - 1)
    
    If lPosicaoTill > 0 Then
        sPathFileSemTill = Mid(sLocalizacao, 1, lPosicaoTill - 1)
        sExeName = Dir(sPathFileSemTill & "*.exe")
        short_path = Space$(1024)
        length = GetShortPathName(sPath & sExeName, short_path, Len(short_path))
        short_path = Left$(short_path, length)
        
        lPosicao2 = 1
        Do While lPosicao2 <> 0
            lPosicao2 = lPosicao2 + 1
            lPosicaoUltimaBarra2 = lPosicao2
            lPosicao2 = InStr(lPosicao2, short_path, "\")
        Loop
        
        If InStr(1, Mid(sLocalizacao, lPosicaoUltimaBarra), Mid(short_path, lPosicaoUltimaBarra2), vbTextCompare) = 0 Then
            Do While Len(Trim(sExeName)) > 0
                sExeName = Dir
                length = GetShortPathName(sExeName, short_path, Len(short_path))
                short_path = Left$(short_path, length)
                lPosicao2 = 1
                Do While lPosicao2 <> 0
                    lPosicao2 = lPosicao2 + 1
                    lPosicaoUltimaBarra2 = lPosicao2
                    lPosicao2 = InStr(lPosicao2, short_path, "\")
                Loop
                If InStr(1, Mid(sLocalizacao, lPosicaoUltimaBarra), Mid(short_path, lPosicaoUltimaBarra2), vbTextCompare) = 0 Then Exit Do
            Loop
        End If
    Else
        sExeName = Mid(sLocalizacao, lPosicaoUltimaBarra)
    End If
    Obtem_Nome_Executavel_Completo = sExeName
End Function

Private Sub Class_Initialize()
  mFlag_Libera_tudo = False
End Sub


