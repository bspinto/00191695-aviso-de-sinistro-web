VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form SEGP0794_4_aviso_micro_seguro 
   Caption         =   "SEGP0794 - Aviso de Sinistro pela CA"
   ClientHeight    =   2565
   ClientLeft      =   5445
   ClientTop       =   4635
   ClientWidth     =   5745
   LinkTopic       =   "Form1"
   ScaleHeight     =   2565
   ScaleWidth      =   5745
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   420
      Left            =   4320
      TabIndex        =   7
      Top             =   2040
      Width           =   1275
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   420
      Left            =   2880
      TabIndex        =   6
      Top             =   2040
      Width           =   1275
   End
   Begin VB.ComboBox cboSinistrado 
      Height          =   315
      ItemData        =   "SEGP0794_4_aviso_micro_seguro.frx":0000
      Left            =   3240
      List            =   "SEGP0794_4_aviso_micro_seguro.frx":000D
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   2415
   End
   Begin VB.ComboBox cboSexo 
      Height          =   315
      ItemData        =   "SEGP0794_4_aviso_micro_seguro.frx":002B
      Left            =   4080
      List            =   "SEGP0794_4_aviso_micro_seguro.frx":0038
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1560
      Width           =   1575
   End
   Begin VB.TextBox txtNomeSinistrado 
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   5535
   End
   Begin MSMask.MaskEdBox mskCPF 
      Height          =   330
      Left            =   120
      TabIndex        =   3
      Top             =   1560
      Width           =   1770
      _ExtentX        =   3122
      _ExtentY        =   582
      _Version        =   393216
      MaxLength       =   14
      Mask            =   "###.###.###-##"
      PromptChar      =   "_"
   End
   Begin MSMask.MaskEdBox mskDtNascimento 
      Height          =   330
      Left            =   2160
      TabIndex        =   4
      Top             =   1560
      Width           =   1650
      _ExtentX        =   2910
      _ExtentY        =   582
      _Version        =   393216
      MaxLength       =   10
      Mask            =   "##/##/####"
      PromptChar      =   "_"
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Data de nascimento:"
      Height          =   195
      Left            =   2160
      TabIndex        =   11
      Top             =   1320
      Width           =   1470
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "CPF:"
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   1320
      Width           =   345
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Sexo:"
      Height          =   195
      Left            =   4080
      TabIndex        =   9
      Top             =   1320
      Width           =   405
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Nome:"
      Height          =   195
      Left            =   120
      TabIndex        =   8
      Top             =   600
      Width           =   465
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Dados do Sinistrado:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2910
   End
End
Attribute VB_Name = "SEGP0794_4_aviso_micro_seguro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Tiago Vignoli - Nova Consultoria - maio/2013; 16794492 - BB Microsseguro - Mudan�a de Escopo Sinistro.
'Formul�rio criado para informar o sinistrado no momento de avisar um microsseguro, produto 1218.

Private Sub cboSinistrado_Click()
    If cboSinistrado.ListIndex = 0 Then 'Titular
        txtNomeSinistrado.Text = SEGP0794_3.txtNomeSinistrado.Text
        
        'cristovao.rodrigues 24/04/2014 - 17919477
        If Len(SEGP0794_3.mskCPF.Text) > 14 Then
            mskCPF.Mask = "##.###.###/####-##"
        Else
            mskCPF.Mask = "###.###.###-##"
        End If
        mskCPF.Text = SEGP0794_3.mskCPF.Text
        cboSexo.ListIndex = SEGP0794_3.cboSexo.ListIndex
        mskDtNascimento.Text = SEGP0794_3.mskDtNascimento.Text
        
        txtNomeSinistrado.Enabled = False
        mskCPF.Enabled = False
        cboSexo.Enabled = False
        mskDtNascimento.Enabled = False
        
        txtNomeSinistrado.TabStop = False
        mskCPF.TabStop = False
        cboSexo.TabStop = False
        mskDtNascimento.TabStop = False
    Else
        txtNomeSinistrado.Text = ""
        mskCPF.Text = "___.___.___-__"
        cboSexo.ListIndex = -1
        mskDtNascimento.Text = "__/__/____"
        
        txtNomeSinistrado.Enabled = True
        mskCPF.Enabled = True
        cboSexo.Enabled = True
        mskDtNascimento.Enabled = True
        
        txtNomeSinistrado.TabStop = True
        mskCPF.TabStop = True
        cboSexo.TabStop = True
        mskDtNascimento.TabStop = True
    End If
End Sub


Private Sub Form_Load()
    CentraFrm Me
    cboSinistrado.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    cmdSair_Click
End Sub

Private Sub cmdOK_Click()

    If cboSinistrado.ListIndex = -1 Then
        MsgBox "Indique a qualifica��o do Sinistrado.", vbCritical, App.Title
        cboSinistrado.SetFocus
        Exit Sub
    End If

   If txtNomeSinistrado.Text = "" Then
        MsgBox "Indique o nome do Sinistrado.", vbCritical, App.Title
        txtNomeSinistrado.SetFocus
        Exit Sub
    End If
    
    If mskCPF.Text = "___.___.___-__" Then
        MsgBox "Indique o CPF do Sinistrado.", vbCritical, App.Title
         mskCPF.SetFocus
        Exit Sub
    End If
    
    If mskDtNascimento.Text = "__/__/____" Then
        MsgBox "Indique a Data de nascimento do Sinistrado.", vbCritical, App.Title
         mskDtNascimento.SetFocus
        Exit Sub
    End If
    
    If cboSexo.ListIndex = -1 Then
        MsgBox "Indique o sexo do Sinistrado.", vbCritical, App.Title
        cboSexo.SetFocus
        Exit Sub
    End If

    Select Case cboSinistrado.Text
        Case "Titular"
                Me.Hide
                SEGP0794_4.Show
                Call SEGP0794_4.Continuar
        
        Case "C�njuge"
            If (Valida_conjuge_micro_seguro(mskCPF.ClipText)) Then
                Me.Hide
                SEGP0794_4.Show
                Call SEGP0794_4.Continuar
            Else
                MsgBox "Sinistrado informado inv�lido.", vbCritical, App.Title 'MELHORAR MSG
            End If

        Case "Filhos"
            If (Valida_filho_micro_seguro(mskDtNascimento.Text)) Then
                Me.Hide
                SEGP0794_4.Show
                Call SEGP0794_4.Continuar
            Else
                MsgBox "Sinistrado informado inv�lido.", vbCritical, App.Title 'MELHORAR MSG
            End If
        
        Case Else
                MsgBox "Escolher uma das op��es: Titular, C�njuge ou Filhos.", vbCritical, App.Title
        End Select
End Sub

Private Sub mskDtNascimento_LostFocus()
    If mskDtNascimento.Text <> "__/__/____" Then
        If Not IsDate(mskDtNascimento.Text) Then
            MsgBox "Data inv�lida.", vbCritical
            mskDtNascimento.SetFocus
        End If
    End If
End Sub

Private Sub cmdSair_Click()
    Me.Hide
    SEGP0794_4.Show
End Sub
'Valida��es

Public Function Valida_conjuge_micro_seguro(CPF As String) As Boolean
    Dim SQL, aux As String
    Dim rs As ADODB.Recordset
    Dim i As Long
    
    SQL = "Set nocount on create table #propostas_avisar( proposta_id int" & _
          "     , componente varchar(60)" & _
          "     , produto_id int )"
          
    aux = ""
    For i = 1 To SEGP0794_4.GrdResultadoPesquisa.Rows - 1
        If SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" Then
            aux = aux & " insert into #propostas_avisar (proposta_id, componente, produto_id)" & _
                        " values (" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 5) & ", '" & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 10) & "', " & SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 12) & ")"
        End If
    Next i
    
    SQL = SQL & aux
    
    SQL = SQL & "declare @count_titular int " & _
                "      , @count_cpf_invalido int " & _
                "select @count_titular = count(*) " & _
                "  from #propostas_avisar " & _
                " Where produto_id <> 1218 " & _
                "   and componente <> 'C�njuge' " & _
                "select @count_cpf_invalido = count(*) " & _
                "  from #propostas_avisar " & _
                " Where produto_id <> 1218 " & _
                "   and componente = 'C�njuge' " & _
                "   and not exists (select 1 " & _
                "                     From cliente_tb " & _
                "                     Join proposta_complementar_tb " & _
                "                       on cliente_tb.cliente_id = proposta_complementar_tb.prop_cliente_id " & _
                "                     Join proposta_tb " & _
                "                       on proposta_tb.proposta_id = proposta_complementar_tb.proposta_id " & _
                "                    where cpf_cnpj = '" & CPF & "'" & _
                "                      and proposta_complementar_tb.proposta_id = #propostas_avisar.proposta_id) " & _
                " select @count_titular as count_titular, @count_cpf_invalido count_cpf_invalido " & _
                " drop table #propostas_avisar"
    
    Set rs = ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, SQL, True)
    
    If rs(0) > 0 And rs(1) > 0 Then
        Valida_conjuge_micro_seguro = False
    Else
        Valida_conjuge_micro_seguro = True
    End If
    
End Function

Public Function Valida_filho_micro_seguro(data_nascimento_filho As Date) As Boolean

    Dim SQL, aux As String
    Dim rs As ADODB.Recordset
    Dim i As Long
    Dim validado As Boolean
    Dim data_nascimento_titular As Date

    validado = True
    For i = 1 To SEGP0794_4.GrdResultadoPesquisa.Rows - 1
        data_nascimento_titular = SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 20)
        If SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 1) = "AVSR" Then
            If SEGP0794_4.GrdResultadoPesquisa.TextMatrix(i, 12) <> 1218 Then
                validado = False
                Exit For
            ElseIf DateDiff("d", data_nascimento_titular, data_nascimento_filho) <= 0 Then
                validado = False
                Exit For
            End If
        End If
    Next i
    
    Valida_filho_micro_seguro = validado
    
End Function


'Fim valida��es
