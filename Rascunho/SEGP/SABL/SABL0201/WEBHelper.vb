Imports System
Imports System.IO
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports Alianca.Seguranca.BancoDados
Imports Alianca.Seguranca.Web
Imports Microsoft.Win32

'<Assembly: ApplicationName("SABL0201")> 
'<Assembly: ApplicationActivation(ActivationOption.Server)> 
'<Assembly: ApplicationAccessControl(False, AccessChecksLevel:=AccessChecksLevelOption.ApplicationComponent)> 

Namespace Alianca
    Namespace Seguranca
        Namespace Web
            Public Class LinkSeguro

#Region "Vari�veis"
                Private _Nome As String
                Private _Usuario_ID As Integer
                Private _Login_WEB As String
                Private _Login_REDE As String
                Private _Unidade_ID As Integer
                Private _CPF As String
                Private _AmbientecCon As Alianca.Seguranca.BancoDados.cCon.Ambientes
#End Region

#Region "Propriedades"

                Public ReadOnly Property Nome() As String
                    Get
                        Nome = _Nome
                    End Get
                End Property

                Public ReadOnly Property Usuario_ID() As Integer
                    Get
                        Usuario_ID = _Usuario_ID
                    End Get
                End Property

                Public ReadOnly Property Login_WEB() As String
                    Get
                        Login_WEB = _Login_WEB
                    End Get
                End Property

                Public ReadOnly Property Login_REDE() As String
                    Get
                        Login_REDE = _Login_REDE
                    End Get
                End Property

                Public ReadOnly Property Unidade_ID() As Integer
                    Get
                        Unidade_ID = _Unidade_ID
                    End Get
                End Property

                Public ReadOnly Property CPF() As String
                    Get
                        CPF = _CPF
                    End Get
                End Property

                Public ReadOnly Property Ambiente() As Alianca.Seguranca.BancoDados.cCon.Ambientes
                    Get
                        Ambiente = _AmbientecCon
                    End Get
                End Property
                Dim conexao As SqlConnection

#End Region

#Region "Construtor"

                Public Sub New()
                    '                    cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cCon.Ambientes.Produ��o)

                    Dim ConnectionString As String

                    Dim sAns As String
                    Dim sErr As String = ""

                    sAns = RegValue(RegistryHive.LocalMachine, _
                      "SOFTWARE\AliancaBrasil\SABL0201", _
                      "Conexao", sErr)
                    If sAns <> "" Then
                        ConnectionString = sAns
                    Else
                        ConnectionString = sErr
                    End If

                    ConnectionString = ConnectionString.Replace("Provider=SQLNCLI;Persist Security Info=False;User ID=", "uid=")

                    ConnectionString = ConnectionString.Replace("Initial Catalog=", "database=")

                    ConnectionString = ConnectionString.Replace("Data Source=", "server=")

                    ConnectionString = ConnectionString & ";Pooling=false;Application Name=SABL0201;Connect Timeout=0;"

                    ConnectionString = "uid=uSegbr;pwd=Tuti-Frut;database=controle_sistema_db;server=SISAB003;Pooling=false;Application Name=SABL0201;Connect Timeout=0;"

                    conexao = New SqlConnection(ConnectionString)

                End Sub

#End Region

#Region "Enumeradores"
                Public Enum TempoExpiracao
                    Curto = 20
                    Medio = 40
                    Grande = 100
                    Diario = 1440
                End Enum
#End Region

#Region "M�todos"
                ''' <summary>  
                '''<para>Gera parametro link seguro a ser enviado</para>  
                ''' </summary>  
                ''' <returns>String</returns>  
                ''' <exception cref="System.Exception">Retorna "" em caso de erro.</exception>  
                Public Function GerarParametro(ByVal URL As String, ByVal Tempo As TempoExpiracao, ByVal IP As String, ByVal usuario As String) As String
                    Try
                        Dim password As String
                        Dim ds_usuario As New DataSet
                        If IsNumeric(usuario) And usuario.Length <= 5 Then
                            Dim sqlcmd As New SqlCommand("exec segab_db..s_usuario_id_sps @usuario_id = " & usuario, conexao)
                            Dim da As New SqlDataAdapter(sqlcmd)
                            da.Fill(ds_usuario)
                            If ds_usuario.Tables.Count = 0 Then
                                Throw New Exception("Usuario n�o Encontrado")
                            Else
                                _Usuario_ID = CType(ds_usuario.Tables(0).Rows.Item(0)("usuario_id"), Integer)
                            End If
                        Else
                            Dim sqlcmd As New SqlCommand("exec segab_db..s_usuario_geral_sps @usuario = '" & usuario & "'", conexao)
                            Dim da As New SqlDataAdapter(sqlcmd)
                            da.Fill(ds_usuario)
                            If ds_usuario.Tables.Count = 0 Then
                                Throw New Exception("Usuario n�o Encontrado")
                            Else
                                _Usuario_ID = CType(ds_usuario.Tables(0).Rows.Item(0)("usuario_id"), Integer)
                            End If
                        End If
                        password = URL & "|!|" & Tempo & "|!|" & usuario & "|!|" & Now.ToUniversalTime.ToString("u") & "|!|" & CType(Usuario_ID, String)
                        Return (GeraPassword(password, Replace(IP, ".", "")))
                    Catch ex As Exception
                        Return ""
                    End Try
                End Function

                ''' <summary>  
                '''<para>Valida usu�rio segundo o par�metro gerado pelo m�todo GerarParametro</para>  
                ''' </summary>  
                ''' <returns>String</returns>  
                ''' <exception cref="System.Exception">Retorna "" em caso de erro.</exception>  
                Public Function LerUsuario(ByVal URL As String, ByVal IP As String, ByVal password As String) As String
                    Try
                        Dim URLOriginal As String
                        Dim valor() As String = Split(AbrePassword(password, Replace(IP, ".", "")), "|!|", , CompareMethod.Binary)
                        If valor(0) = Replace(IP, ".", "") Then
                            URLOriginal = valor(1)
                            Dim _tempo As TempoExpiracao = CType(valor(2), TempoExpiracao)
                            Dim _Usuario As String = valor(3)
                            Dim _datahora As Date = CType(valor(4), Date)
                            _Usuario_ID = CType(valor(5), Integer)
                            If valor.Length = 7 Then
                                _AmbientecCon = DirectCast([Enum].Parse(GetType(Alianca.Seguranca.BancoDados.cCon.Ambientes), valor(6).ToString(), True), Alianca.Seguranca.BancoDados.cCon.Ambientes)
                            End If
                            'If DateDiff(DateInterval.Minute, _datahora.ToUniversalTime, Now.ToUniversalTime) <= _tempo Then
                            'If URLOriginal <> "" And URL <> "" Then
                            'If CompararURL(URLOriginal.ToUpper, URL.ToUpper) Then
                            Try
                                carrega_usuario(_Usuario_ID)
                                Return _Usuario
                            Catch ex As Exception
                                Throw ex
                            End Try
                            'Else
                            'Return ""
                            'End If
                            'ElseIf URLOriginal = "" And URL = "" Then
                            '   Try
                            'carrega_usuario(_Usuario_ID)
                            'Return _Usuario
                            'Catch ex As Exception
                            'Throw ex
                            'End Try
                            'Else
                            'Return ""
                            'End If

                            'Else
                            'Return ""
                            'End If
                        End If
                        Return ""
                    Catch ex As Exception
                        Return ""
                    End Try
                End Function

                Private Sub carrega_usuario(ByVal valor As Integer)
                    Try
                        Dim ds_usuario As New DataSet
                        Dim sqlcmd As New SqlCommand("exec segab_db..s_usuario_id_sps @usuario_id = " & valor, conexao)
                        Dim da As New SqlDataAdapter(sqlcmd)
                        da.Fill(ds_usuario)


                        If ds_usuario.Tables.Count = 0 Then
                            Throw New Exception("Usuario n�o Encontrado")
                        Else
                            _Usuario_ID = CType(ds_usuario.Tables(0).Rows.Item(0)("usuario_id"), Integer)
                            _Login_REDE = CType(ds_usuario.Tables(0).Rows.Item(0)("login_rede"), String)
                            _Login_WEB = CType(ds_usuario.Tables(0).Rows.Item(0)("login_web"), String)
                            _Unidade_ID = CType(ds_usuario.Tables(0).Rows.Item(0)("unidade_id"), Integer)
                            _Nome = CType(ds_usuario.Tables(0).Rows.Item(0)("nome"), String)
                            _CPF = CType(ds_usuario.Tables(0).Rows.Item(0)("CPF"), String)
                        End If
                    Catch ex As Exception
                        Throw ex
                    End Try
                End Sub

                Private Function GeraPassword(ByVal inpt As String, ByVal IP As String) As String
                    Try
                        Dim temp As String = ""
                        Dim tempA As String
                        Dim Rand As Integer
                        Dim rad As Integer
                        Dim crntASC As String
                        Dim j As Integer
                        Dim eChave As String
Top:
                        inpt = IP & "|!|" & inpt
                        eChave = IP.ToString.Substring(CType((IP.Length / 2), Integer), IP.Length - CType((IP.Length / 2), Integer))
                        Rand = CType(eChave, Integer)
                        Do While Len(Rand.ToString) > 4 And Rand > 5267
                            Rand = CType(Rand / 2, Integer)
                        Loop

                        Dim contador As Integer = 2
                        Do While Rand > 5700

                            Rand = Rand - (contador * 2)
                            contador = contador + 1
                        Loop

                        rad = CType(Left(Rand.ToString, 1), Integer)
                        If Left(Rand.ToString, 1) = "-" Then
                            GoTo Top
                        End If

                        For j = 1 To Len(inpt)
                            crntASC = Asc(Mid(inpt, j, 1)).ToString
                            tempA = ((CInt((crntASC)) Xor (CInt(Rand) + j + CInt(rad))) + (j + CInt(rad))).ToString
                            temp = temp & New String(CType("0", Char), 4 - Len(tempA)) & tempA
                        Next j

                        GeraPassword = temp
                    Catch ex As Exception
                        Throw
                    End Try
                End Function
                Public Function GerarParametro(ByVal URL As String, ByVal Tempo As TempoExpiracao, ByVal IP As String, ByVal usuario As String, ByVal Ambiente As Alianca.Seguranca.BancoDados.cCon.Ambientes) As String
                    Try
                        Dim password As String
                        Dim ds_usuario As New DataSet
                        If IsNumeric(usuario) And usuario.Length <= 5 Then
                            Dim sqlcmd As New SqlCommand("exec segab_db..s_usuario_id_sps @usuario_id = " & usuario, conexao)
                            Dim da As New SqlDataAdapter(sqlcmd)
                            da.Fill(ds_usuario)

                            If ds_usuario.Tables.Count = 0 Then
                                Throw New Exception("Usuario n�o Encontrado")
                            Else
                                _Usuario_ID = CType(ds_usuario.Tables(0).Rows.Item(0)("usuario_id"), Integer)
                            End If
                        Else
                            Dim sqlcmd As New SqlCommand("exec segab_db..s_usuario_geral_sps @usuario = " & usuario, conexao)
                            Dim da As New SqlDataAdapter(sqlcmd)
                            da.Fill(ds_usuario)

                            If ds_usuario.Tables.Count = 0 Then
                                Throw New Exception("Usuario n�o Encontrado")
                            Else
                                _Usuario_ID = CType(ds_usuario.Tables(0).Rows.Item(0)("usuario_id"), Integer)
                            End If
                        End If

                        password = URL & "|!|" & Tempo & "|!|" & usuario & "|!|" & Now.ToUniversalTime.ToString("u") & "|!|" & CType(Usuario_ID, String) & "|!|" & Ambiente.ToString()
                        Return (GeraPassword(password, Replace(IP, ".", "")))
                    Catch ex As Exception
                        Return ""
                    End Try
                End Function

                Private Function AbrePassword(ByVal inpt As String, ByVal IP As String) As String
                    Try
                        Dim rand As Integer
                        Dim j As Integer
                        Dim z As Integer
                        Dim tempa As String
                        Dim temp As String = ""
                        'Dim tamanhochave As Integer

                        rand = CType(IP.ToString.Substring(CType((IP.Length / 2), Integer), IP.Length - CType((IP.Length / 2), Integer)), Integer)
                        Do While Len(rand.ToString) > 4 And rand > 5267
                            rand = CType(rand / 2, Integer)
                        Loop

                        Dim contador As Integer = 2
                        Do While rand > 5700
                            rand = rand - (contador * 2)
                            contador = contador + 1
                        Loop

                        For j = 1 To (Len(inpt) - 3) Step 4
                            z = z + 1
                            tempa = Mid(inpt, j, 4)
                            tempa = (((CInt(tempa) - (z + CInt(Left(rand.ToString, 1)))) Xor (CInt(rand) + z + CInt(Left(rand.ToString, 1))))).ToString
                            temp = temp & Chr(CInt(tempa))
                        Next j
                        AbrePassword = temp

                        Exit Function
errAbre:
                        AbrePassword = ""
                    Catch ex As Exception
                        Throw New Exception("SENHA")
                    End Try
                End Function

                Private Function CompararURL(ByVal URLOriginal As String, ByVal URLAtual As String) As Boolean
                    Try

                        If InStr(URLOriginal, "?") <> 0 Then
                            URLOriginal = Mid(URLOriginal, 1, InStr(URLOriginal, "?") - 1)
                        End If

                        If InStr(URLAtual, "?") <> 0 Then
                            URLAtual = Mid(URLAtual, 1, InStr(URLAtual, "?") - 1)
                        End If

                        Dim vtURLOriginal() As String = Split(URLOriginal, "/")
                        Dim vtURLAtual() As String = Split(URLAtual, "/")


                        If vtURLOriginal.Length = vtURLAtual.Length Then
                            Return (URLOriginal = URLAtual)
                        Else

                            If InStr(vtURLOriginal(vtURLOriginal.Length - 1), ".") = 0 Then
                                ReDim Preserve vtURLOriginal(vtURLOriginal.Length)
                                vtURLOriginal(vtURLOriginal.Length - 1) = "DEFAULT.ASPX"
                            End If

                            If vtURLOriginal.Length = 1 Then
                                ReDim Preserve vtURLOriginal(vtURLOriginal.Length)
                                vtURLOriginal(1) = "DEFAULT.ASPX"
                            End If

                            vtURLAtual(vtURLAtual.Length - 1) = UCase(Replace(vtURLAtual(vtURLAtual.Length - 1), "ASPX", "ASP", , , CompareMethod.Text))
                            vtURLOriginal(vtURLOriginal.Length - 1) = UCase(Replace(vtURLOriginal(vtURLOriginal.Length - 1), "ASPX", "ASP", , , CompareMethod.Binary))

                            Dim contador As Integer = vtURLAtual.Length - 1

                            Do While contador >= 0
                                If UCase(vtURLAtual(contador)) <> UCase(vtURLOriginal(vtURLOriginal.Length - 1 - ((vtURLAtual.Length - 1) - contador))) Then
                                    Return False
                                End If
                                contador -= 1
                            Loop
                            Return True
                        End If
                    Catch ex As Exception
                        Throw
                    End Try
                End Function
                Public Shared Function RegValue(ByVal Hive As RegistryHive, _
                    ByVal Key As String, ByVal ValueName As String, _
                    Optional ByRef ErrInfo As String = "") As String

                    'DEMO USAGE



                    Dim objParent As RegistryKey
                    Dim objSubkey As RegistryKey
                    Dim sAns As String
                    Select Case Hive
                        Case RegistryHive.ClassesRoot
                            objParent = Registry.ClassesRoot
                        Case RegistryHive.CurrentConfig
                            objParent = Registry.CurrentConfig
                        Case RegistryHive.CurrentUser
                            objParent = Registry.CurrentUser
                        Case RegistryHive.DynData
                            objParent = Registry.DynData
                        Case RegistryHive.LocalMachine
                            objParent = Registry.LocalMachine
                        Case RegistryHive.PerformanceData
                            objParent = Registry.PerformanceData
                        Case RegistryHive.Users
                            objParent = Registry.Users
                    End Select
                    Try
                        objSubkey = objParent.OpenSubKey(Key)
                        If Not objSubkey Is Nothing Then
                            sAns = Convert.ToString((objSubkey.GetValue(ValueName)))
                        End If
                    Catch ex As Exception
                        ErrInfo = ex.Message
                    Finally
                        If ErrInfo = "" And sAns = "" Then
                            ErrInfo = _
                               "No value found for requested registry key"
                        End If
                    End Try
                    Return sAns
                End Function
#End Region

            End Class
        End Namespace
    End Namespace
End Namespace
