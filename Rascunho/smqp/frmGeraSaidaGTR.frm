VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form frmGeraSaidaGTR 
   Caption         =   "SMQP0060 - GTR Importa��o"
   ClientHeight    =   6135
   ClientLeft      =   7515
   ClientTop       =   3675
   ClientWidth     =   11100
   Icon            =   "frmGeraSaidaGTR.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6135
   ScaleWidth      =   11100
   StartUpPosition =   2  'CenterScreen
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   5265
      Top             =   4455
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   4680
      Top             =   4455
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin VB.PictureBox Picture1 
      Height          =   570
      Index           =   1
      Left            =   2445
      Picture         =   "frmGeraSaidaGTR.frx":0442
      ScaleHeight     =   510
      ScaleWidth      =   585
      TabIndex        =   8
      Top             =   5310
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.PictureBox Picture1 
      Height          =   540
      Index           =   0
      Left            =   1770
      Picture         =   "frmGeraSaidaGTR.frx":0884
      ScaleHeight     =   480
      ScaleWidth      =   585
      TabIndex        =   7
      Top             =   5310
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.PictureBox picBotao 
      Height          =   390
      Index           =   1
      Left            =   4020
      Picture         =   "frmGeraSaidaGTR.frx":0CC6
      ScaleHeight     =   330
      ScaleWidth      =   360
      TabIndex        =   11
      Top             =   5370
      Visible         =   0   'False
      Width           =   420
   End
   Begin VB.PictureBox picBotao 
      Height          =   390
      Index           =   0
      Left            =   4035
      Picture         =   "frmGeraSaidaGTR.frx":0E50
      ScaleHeight     =   330
      ScaleWidth      =   360
      TabIndex        =   10
      Top             =   5370
      Width           =   420
   End
   Begin VB.CommandButton cmdGerarAgora 
      Caption         =   "&Gerar Agora"
      Height          =   525
      Left            =   90
      TabIndex        =   6
      Top             =   5325
      Width           =   1260
   End
   Begin VB.CommandButton cmdIniciar 
      Caption         =   "&Iniciar            "
      Height          =   525
      Left            =   3330
      TabIndex        =   9
      Top             =   5310
      Width           =   1260
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   525
      Left            =   4740
      TabIndex        =   12
      Top             =   5310
      Width           =   1260
   End
   Begin VB.Frame frmRemessas 
      Caption         =   " Carga de Remessas "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5085
      Left            =   105
      TabIndex        =   0
      Top             =   135
      Width           =   10890
      Begin VB.Timer tmrConsultaEmissao 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   3315
         Top             =   210
      End
      Begin MSFlexGridLib.MSFlexGrid flexImportacao 
         Height          =   4290
         Left            =   120
         TabIndex        =   5
         Top             =   675
         Width           =   10665
         _ExtentX        =   18812
         _ExtentY        =   7567
         _Version        =   393216
         Rows            =   1
         Cols            =   9
         FixedCols       =   0
         FormatString    =   "Status | Sequencial | Evento   | Sinistro BB               | Layout_id|Num_Remessa  |Cod_Transacao | Produto|Empresa"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   3735
         Top             =   135
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   24
         ImageHeight     =   22
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   4
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmGeraSaidaGTR.frx":0FDA
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmGeraSaidaGTR.frx":1174
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmGeraSaidaGTR.frx":172E
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmGeraSaidaGTR.frx":18C8
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "C�d. Trans.:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4185
         TabIndex        =   3
         Top             =   405
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblCodTrans 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "XXXX"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   5325
         TabIndex        =   4
         Top             =   405
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label lblTempo 
         Caption         =   "00:05:00 h"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2250
         TabIndex        =   2
         Top             =   405
         Width           =   1170
      End
      Begin VB.Label Label1 
         Caption         =   "Pr�xima carga em :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   180
         TabIndex        =   1
         Top             =   405
         Width           =   2070
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   13
      Top             =   5865
      Width           =   11100
      _ExtentX        =   19579
      _ExtentY        =   476
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Arquivo"
      Visible         =   0   'False
      Begin VB.Menu mnuAbrir 
         Caption         =   "&Abrir"
      End
      Begin VB.Menu mnuArquivoSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSair 
         Caption         =   "Sai&r"
      End
   End
End
Attribute VB_Name = "frmGeraSaidaGTR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Sinistros_Invalidos     As New Collection
Dim Sinistros_Bloqueados    As New Collection
Dim Sinistro_BB             As String
Dim VarAmbiente             As String

'Constantes do Grid de Saida
Const GRID_SAI_STATUS = 0
Const GRID_SAI_SEQ = 1
Const GRID_SAI_EVENTO = 2
Const GRID_SAI_SINISTRO_BB = 3
Const GRID_SAI_LAYOUT_ID = 4
Const GRID_SAI_NUM_REMESSA = 5
Const GRID_SAI_PRODUTO = 7

Const GRID_SAI_EMPRESA = 8
Const GRID_SAI_TRANSACAO = 6

Const Tempo_Default As String = "00:05:00"

Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long

'In�cio - Melhorias do SEGBR - Erika Lessa - 21/05/2013
Private Enum enStatusConexao
    cxFechada = 0
    cxAberta = 1
    cxTransacao = 2
End Enum
'Fim - Melhorias do SEGBR - Erika Lessa - 21/05/2013
Private Sub cmdGerarAgora_Click()

    tmrConsultaEmissao.Enabled = False
        
    Call LogExecution(1, "SMQP0060", "N")
    
   'Rodrigo Moreira 
   'If ValidaHorario = True Then
        Gera_Saida_GTR
    'End If
        
    Call LogExecution(2, "SMQP0060", "N")
    
    lblTempo = Tempo_Default & " h"
    
    If Left(cmdIniciar.Caption, 2) = "&P" Then
        tmrConsultaEmissao.Enabled = True
    End If
  
End Sub
Private Sub cmdSair_Click()
    Call TerminaSEGBR
End Sub

Private Sub cmdiniciar_Click()

    If cmdIniciar.Caption = "&Iniciar            " Then
        cmdIniciar.Caption = "&Parar              "
        picBotao(0).Visible = False
        picBotao(1).Visible = True
        Me.Icon = Picture1(0).Picture
        tmrConsultaEmissao.Enabled = True
    Else
        cmdIniciar.Caption = "&Iniciar            "
        picBotao(0).Visible = True
        picBotao(1).Visible = False
        Me.Icon = Picture1(1).Picture
        tmrConsultaEmissao.Enabled = False
    End If

End Sub
Private Sub Form_Load()
'Rodrigo Moreira
'On Error GoTo TrataErro
    'Rodrigo Moreira --in�cio
    'Verifica se o Programa j� est� rodando
    'If App.PrevInstance Then Call TerminaSEGBR
    'Dim Returno As String
    'Dim strComputer As String
    'Dim colProcesses As Variant
    'Dim objProcess As Variant
    'Dim strNameOfUser As String
    'Dim Aa As String

    'strComputer = "."
    'Set colProcesses = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & _
    '                        "\root\cimv2").ExecQuery("Select * from Win32_Process Where Name = 'SMQP0060.exe'")
    '                    
    'If colProcesses.Count > 1 Then 'J� tem um programa aberto
    '    MsgBox "Programa SMQP0060.exe aberto pelo usuario: " & strNameOfUser, vbExclamation
    '    Call GravarTrace("Programa SMQP0060.exe aberto pelo usuario: " & strNameOfUser)
    '    Call TerminaSEGBR
    'End If
    
     Me.Caption = "SMQP0060 - GTR Importa��o"  '"SEGP0459 - GTR Importa��o"
    
     'glAmbiente_id = Right(Command, 1) --fim
     'Rodrigo Moreira
     glAmbiente_id = 3
     cUserName = "SMQP0060" '"SEGP0459"
       
    'Abre Conex�o com o SEGBR
     Conexao
         
    'Habilitar esta linha quando a string de seguran�a estiver OK
    'Call Seguranca("SEGP0459", "GTR Importa��o")
    
    'Rodrigo Moreira
    'Call GravarTrace("In�cio - Execu��o SMQP0060")
    
    'Usu�rio � o GTR
    cUserName = "SMQP0060" '"SEGP0459"
    
    'Verifica se o GTR Controle est� em execu��o, se n�o estiver em execu��o tem que enviar e-mail e finalizar o programa
    'If GTR_Controle_em_Execucao = False Then
    '   Call Grava_Log_eMail_Finaliza_Aplicacao("Programa SEGP0456(GTR Controle) n�o est� sendo executado. " & vbCrLf & "O Programa SEGP0459(GTR Importa��o) n�o pode ser executado." & vbCrLf & "Favor verificar o programa SEGP0456(GTR Controle).", "PROC", "LBLTEMPO", "Erro de execu��o de Programa GTR.", MAPISession1, MAPIMessages1, True)
    '   Exit Sub
    'End If
    
    'Configura a tela
    CentraFrm Me
    Me.Icon = Picture1(1)
    lblTempo = Tempo_Default & " h"
    With flexImportacao
        .ColWidth(GRID_SAI_LAYOUT_ID) = 0
        .Rows = 1
        .Row = 0
    End With
    lblCodTrans = ""
    
   'Rodrigo Moreira
   'C�d. da Transa��o
   'lblCodTrans = ObtemConteudo("BB_COD_TRANS_SAIDA") '"SN10"
    
   'Abre conex�es do GTR
    Abre_Conexoes_GTR True
    
  ''Monta a rela��o entre os eventos e os layouts
   'MontaColecaoEventos "E"
     
   'Rodrigo Moreira 
   'Call GravarTrace("MontaColecaoLayout - Execu��o SMQP0060, Form_Load()")
    Call MontaColecaoLayout
    
    'Verifica se inicia autom�ticamente
    If Trim(Command()) <> "" Then
        cmdiniciar_Click
        Me.WindowState = vbMinimized
    End If
    
    'Coloca o Sistema no System Tray
    If Left(cmdIniciar.Caption, 5) = "&Inic" Then
        Call CarregaBandeja(traySegbr, Picture1(1), "GTR Importa��o - Parado" & Chr$(0))
    Else
        Call CarregaBandeja(traySegbr, Picture1(0), "GTR Importa��o - Executando" & Chr$(0))
    End If
    
    cmdiniciar_Click

'Rodrigo Moreira
    'Exit Sub

'TrataErro:
'    Dim msgErro As String
'    'msgErro = "SMQP0060 - Sub Form_Load() na linha - " & Erl() & " - " & Err.Description
'    Call GravarTrace(msgErro)
'    'MsgBox msgErro, vbCritical, Caption
'    End
End Sub

Private Sub Form_Resize()
        
    'Reposiciona a Lista de Mensagens
    If WindowState <> vbMinimized Then
        Call DescarregaBandeja(traySegbr)
    Else
        'Coloca o Sistema no System Tray
        If Left(cmdIniciar.Caption, 5) = "&Inic" Then
           Call CarregaBandeja(traySegbr, Picture1(1), "GTR Importa��o - Parado" & Chr$(0))
        Else
           Call CarregaBandeja(traySegbr, Picture1(0), "GTR Importa��o - Executando" & Chr$(0))
        End If
        Me.Hide
    End If
        
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    'Descarrega o Sistema
    Call DescarregaBandeja(traySegbr)

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    If cmdSair.Enabled = False Then
        Cancel = 1
    End If

End Sub

Private Sub lblTempo_Change()
'Rodrigo.Moreira
'On Error GoTo TrataErro

    If lblTempo.Caption = "00:00:00 h" Then
        lblTempo.Refresh
        tmrConsultaEmissao.Enabled = False
        
        'In�cio - Melhorias do SEGBR - Felipe Sbragia - 14/05/2013
        Call LogExecution(1, "SMQP0060", "N")
       
        'Rodrigo.Moreira
        'If ValidaHorario = True Then
            Gera_Saida_GTR
        'End If
        
        Call LogExecution(2, "SMQP0060", "N")
        'Fim - Melhorias do SEGBR - Felipe Sbragia - 14/05/2013
        
        lblTempo = Tempo_Default & " h"
        tmrConsultaEmissao.Enabled = True
    End If
'Rodrigo.Moreira
'Exit Sub
'TrataErro:
 '   MsgBox "Erro ao Atualiza label Tempo." & vbNewLine & vbNewLine & _
 '        "Linha: " & Erl() & vbNewLine & Err.Description, _
 '        vbCritical, _
 '        "Gera_Saida_GTR.lblTempo_Change"
 '        
 '        Call GravarTrace(Err.Description)
End
End Sub

Private Sub mnuAbrir_Click()
    'Exibe o Programa
    Me.WindowState = vbNormal
    Me.Show
    Me.SetFocus
End Sub

Private Sub mnuSair_Click()
    'Finaliza o Sistema
    Call TerminaSEGBR
End Sub

Private Sub picBotao_Click(Index As Integer)
    cmdiniciar_Click
End Sub

Private Sub Picture1_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
        
        Select Case x
                    
               Case TRAY_MSG_MOUSEMOVE
                    
               Case TRAY_MSG_LEFTBTN_DOWN
                    
               Case TRAY_MSG_LEFTBTN_UP
                    'aqui pode utilizar um menu
                    
               Case TRAY_MSG_LEFTBTN_DBLCLICK
                    Me.WindowState = vbNormal
                    Me.Show
                    Me.SetFocus
                    
               Case TRAY_MSG_RIGHTBTN_DOWN
                    
               Case TRAY_MSG_RIGHTBTN_UP
                    PopupMenu mnuArquivo
                    
               Case TRAY_MSG_RIGHTBTN_DBLCLICK
                    
        End Select
        
End Sub

Private Sub StatusBar1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    StatusBar1.ToolTipText = StatusBar1.SimpleText

End Sub

Private Sub tmrConsultaEmissao_Timer()

    Dim Tempo As String
    Dim Segundos As Integer, Minutos As Integer, Horas As Integer
    
    Tempo = Left(lblTempo.Caption, InStr(lblTempo.Caption, " h"))
    Segundos = Second(Tempo)
    Minutos = Minute(Tempo)
    Horas = Hour(Tempo)
    
    Segundos = Segundos - 1
    If Segundos = -1 Then
        Segundos = 59
        Minutos = Minutos - 1
        If Minutos = -1 Then
            Horas = Horas - 1
        End If
    End If
    lblTempo.Caption = Format(Horas & ":" & Minutos & ":" & Segundos, "hh:mm:ss") & " h"
    
End Sub

Private Sub Gera_Saida_GTR()

    '**************************************************************************************************
    'Data Altera��o: 14/05/2013 (�ltima Atualiza��o: 04/07/2013)
    'Projeto       : Melhorias do SEGBR
    'Autores       : Felipe Sbragia / Erika Lessa
    'Descri��o     : M�todo completamente reescrito para atender a nova l�gica
    '                   e melhorias determinadas no projeto.
    '                Toda a l�gica de filtragem de eventos foi transferida para a proc SEGS9398_SPS
    '                Os eventos que retornarem para o VB, devem ser enviados, sem novas valida��es
    '**************************************************************************************************

    Dim SQL As String, rs As rdoResultset
    Dim Erro_Aplicacao As Long, Situacao As String
       
    Dim j As Integer, i As Integer, registros() As String
    Dim Qtd_Registros As String, Saida_GTR_ID As String
    Dim Cont_Eventos As Long, Cont_Sucesso As Integer, Cont_Erros As Integer
    Dim Gera_Saida As Boolean
    
    'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
    Dim strSinistroBB As String
    Dim strEventoBB As String
    Dim strEventoID As String
    Dim SQL_log As String
    
    Dim LayoutId_ant As String
    
    On Error GoTo Erro
       
    cmdSair.Enabled = False
    cmdGerarAgora.Enabled = False
    cmdIniciar.Enabled = False
    picBotao(0).Enabled = False
    picBotao(1).Enabled = False

    Cont_Eventos = 0
    Cont_Sucesso = 0
    Cont_Erros = 0
    
    StatusBar1.SimpleText = "Procurando novos eventos ..."
   
  'Rodrigo.Moreira 
   'Call GravarTrace("Procurando novos eventos ...")
    
    'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
    SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 0, 'Preenchendo Grid ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
    rdocn_aux.Execute SQL_log
    
    'Rodrigo.Moreira - Apenas AB 
    If Not (Monta_Flex_Saida("AB")) THEN
    'And Not (Monta_Flex_Saida("ABS")) Then
        StatusBar1.SimpleText = "Nenhum evento novo encontrado !"
    Else

        For i = 1 To flexImportacao.Rows - 1
            
            strSinistroBB = flexImportacao.TextMatrix(i, GRID_SAI_SINISTRO_BB)
            strEventoBB = flexImportacao.TextMatrix(i, GRID_SAI_EVENTO)
            strEventoID = flexImportacao.TextMatrix(i, GRID_SAI_SEQ)
            
            Gera_Saida = True

            Cont_Eventos = Cont_Eventos + 1
            StatusBar1.SimpleText = "Processando eventos encontrados (" & Cont_Eventos & " de " & (flexImportacao.Rows - 1) & ") ..."
            
            Atualiza_FlexImportacao i, "TEMPO"
            
            'Se n�o existir, segue como "N" para transmiss�o
            
            'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
            SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 1, 'Gera��o registro sinistro_bb: " & CStr(strSinistroBB) & ", evento_bb: " & CStr(strEventoBB) & " evento_id: " & CStr(strEventoID) & " ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
            rdocn_aux.Execute SQL_log
            
            Erro_Aplicacao = Gera_Registros_Saida(flexImportacao.TextMatrix(i, GRID_SAI_SEQ), _
                                                  flexImportacao.TextMatrix(i, GRID_SAI_LAYOUT_ID), _
                                                  registros, _
                                                  flexImportacao.TextMatrix(i, GRID_SAI_EMPRESA), _
                                                  flexImportacao.TextMatrix(i, GRID_SAI_EVENTO), _
                                                  flexImportacao.TextMatrix(i, GRID_SAI_TRANSACAO))

            If Erro_Aplicacao > 0 Then
                'Trata_Erro_Carga
                Atualiza_FlexImportacao i, "ERRO"
                Cont_Erros = Cont_Erros + 1
                'HENRIQUE H HENRIQUES - CONFITEC SP - TRATAMENTO PARA EVITAR BARRAR SINISTROS QUE O ERRO N�O SEJA DO EVENTO - INI
                If Erro_Aplicacao <> 40002 Then
                    Call GravaLogSaida(flexImportacao.TextMatrix(i, GRID_SAI_SEQ), flexImportacao.TextMatrix(i, GRID_SAI_SINISTRO_BB), flexImportacao.TextMatrix(i, GRID_SAI_EVENTO), flexImportacao.TextMatrix(i, GRID_SAI_EMPRESA))
                End If
                'HENRIQUE H HENRIQUES - CONFITEC SP - TRATAMENTO PARA EVITAR BARRAR SINISTROS QUE O ERRO N�O SEJA DO EVENTO - FIM
            Else

                Qtd_Registros = 0

                'Grava os registros na tabela de sa�da do GTR.
                For j = LBound(registros) To UBound(registros)
                    If registros(j) <> "" Then
                        Qtd_Registros = Qtd_Registros + 1
                    End If
                Next j
                
                Situacao = "N"

                If (flexImportacao.TextMatrix(i, GRID_SAI_EVENTO) = "1100" Or _
                    flexImportacao.TextMatrix(i, GRID_SAI_EVENTO) = "1180") And _
                    flexImportacao.TextMatrix(i, GRID_SAI_PRODUTO) = "1140" Then
                    Situacao = "T"
                End If

                'Inclui na saida_gtr
                
                'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
                SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 2, 'Registro SAIDA_GTR_ATUAL sinistro_bb: " & CStr(strSinistroBB) & ", evento_bb: " & CStr(strEventoBB) & " evento_id: " & CStr(strEventoID) & " ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
                rdocn_aux.Execute SQL_log
                
                SQL = "EXEC interface_db.dbo.SEGS11265_SPI 'SAIDA', " & flexImportacao.TextMatrix(i, GRID_SAI_SEQ) & ", " & Qtd_Registros
                 
                'Tratamento do ALS
                If flexImportacao.TextMatrix(i, GRID_SAI_TRANSACAO) <> "ST10" And flexImportacao.TextMatrix(i, GRID_SAI_TRANSACAO) <> "ST00" Then
                     SQL = SQL & ", 'SN10' " ' Cod_Transacao
                Else
                     SQL = SQL & ", 'ST10' " ' Cod_Transacao
                End If
                
                If flexImportacao.TextMatrix(i, GRID_SAI_SINISTRO_BB) <> "" Then
                     SQL = SQL & ", " & flexImportacao.TextMatrix(i, GRID_SAI_SINISTRO_BB)   ' Sinistro_BB
                Else
                     SQL = SQL & ", null"
                End If
                
                SQL = SQL & ", " & flexImportacao.TextMatrix(i, GRID_SAI_EVENTO)             ' Evento_BB
                SQL = SQL & ", '" & Situacao & "'"
                SQL = SQL & ", '" & flexImportacao.TextMatrix(i, GRID_SAI_NUM_REMESSA) & "'"
                SQL = SQL & ", " & flexImportacao.TextMatrix(i, GRID_SAI_LAYOUT_ID)
                SQL = SQL & ", '" & IIf(flexImportacao.TextMatrix(i, GRID_SAI_EMPRESA) = 6 Or flexImportacao.TextMatrix(i, GRID_SAI_EMPRESA) = 7, "ABS", "AB") & "' " & vbNewLine
                SQL = SQL & ", " & i & vbNewLine
                
                Set rs = rdocn_Interface.OpenResultset(SQL)
                Saida_GTR_ID = rs(0)
                rs.Close
                                                   
                'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
                SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 3, 'Registro SAIDA_GTR_REGISTRO_ATUAL sinistro_bb: " & CStr(strSinistroBB) & ", evento_bb: " & CStr(strEventoBB) & " evento_id: " & CStr(strEventoID) & " ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
                rdocn_aux.Execute SQL_log
                
                For j = LBound(registros) To UBound(registros)
                     If registros(j) <> "" Then
                         SQL = "EXEC interface_db.dbo.SEGS11265_SPI 'SAIDA_REGISTRO', null, null, null, null, null, null, null, null, null, "
                         SQL = SQL & Saida_GTR_ID & ", "
                         SQL = SQL & "'" & registros(j) & "', "
                         SQL = SQL & "'" & Left(registros(j), 1) & "'"
                         
                         rdocn_Interface.Execute (SQL)
                     End If
                Next j
                
                Atualiza_FlexImportacao i, "OK"
                    
                Cont_Sucesso = Cont_Sucesso + 1
                
                'Rodrigo.Moreira
                'GravarTrace "Remessa: " & flexImportacao.TextMatrix(i, GRID_SAI_NUM_REMESSA) & " - " & _
                '            "Evento: " & flexImportacao.TextMatrix(i, GRID_SAI_EVENTO) & " - " & _
                '            "Sinistro_BB: " & flexImportacao.TextMatrix(i, GRID_SAI_SINISTRO_BB)
                'DoEvents

             End If
            
            StatusBar1.SimpleText = "Total de " & Cont_Sucesso & " remessa(s) processada(s) com sucesso ! (" & Cont_Erros & " com erro!)"
Continue:
        Next i
        i = i - 1 'trata nro da ultima linha
        
        strSinistroBB = ""
        strEventoBB = ""
        strEventoID = ""
        
        'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
        SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 0, 'Gravando na base de dados ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
        rdocn_aux.Execute SQL_log
       'Chama proc de inser��o
        SQL = " EXEC interface_db.dbo.SEGS11266_SPI "
        rdocn_Interface.Execute (SQL)
        
        'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
        SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 0, 'Atualizando dt_emissao ABS ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
        rdocn_aux.Execute SQL_log
        
        SQL = " EXEC interface_db.dbo.SEGS11537_SPU 'ABS' "
        rdocn_Seg2.Execute (SQL)
        
        'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
        SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 0, 'Atualizando dt_emissao AB ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
        rdocn_aux.Execute SQL_log
        
        SQL = " EXEC interface_db.dbo.SEGS11537_SPU 'AB' "
        rdocn_Interface.Execute (SQL)
        
        'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
        SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 0, 'Removendo tabelas tempor�rias ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
        rdocn_aux.Execute SQL_log
        
        SQL = ""
        SQL = SQL & " IF ISNULL(OBJECT_ID('TEMPDB..##SAIDA_REGISTRO_SMQP0060'), 0) > 0 " & vbNewLine
        SQL = SQL & "     BEGIN " & vbNewLine
        SQL = SQL & "         DROP TABLE ##SAIDA_REGISTRO_SMQP0060 " & vbNewLine
        SQL = SQL & "     END " & vbNewLine
        
        rdocn_Interface.Execute (SQL)
        
        SQL = ""
        SQL = SQL & " IF ISNULL(OBJECT_ID('TEMPDB..##SAIDA_SMQP0060'), 0) > 0 " & vbNewLine
        SQL = SQL & "     BEGIN " & vbNewLine
        SQL = SQL & "         DROP TABLE ##SAIDA_SMQP0060 " & vbNewLine
        SQL = SQL & "     END " & vbNewLine
        
        rdocn_Interface.Execute (SQL)
        
    End If
    
    'GENJUNIOR - FLOW 17860335 - CONTROLE DE LOG DE ERROS DO PROGRAMA
    SQL_log = " EXEC controle_sistema_db.dbo.log_erro_spi 0, 'Fim do Processamento ', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', 'GERA_SAIDA_GTR', ''"
    rdocn_aux.Execute SQL_log
    
    cmdSair.Enabled = True
    cmdGerarAgora.Enabled = True
    cmdIniciar.Enabled = True
    picBotao(0).Enabled = True
    picBotao(1).Enabled = True
    
    Exit Sub
    
Erro:
    Atualiza_FlexImportacao i, "ERRO"
    
  ''Erika 21/08/2013
    Call GravaLogErro(Err.Number, Err.Description, "Gera_Saida_GTR")
    
    If strSinistroBB <> "" Then
        Call GravaLogSaida(CDbl(strEventoID), CDbl(strSinistroBB), CDbl(strEventoBB), 2)
    End If
    
    'call GravaLogSaida
     
    If SQL = Empty Then
       'Call Enviar_Notificacao("SEGP0459", "SEGP0459 - Gera_Saida_GTR" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description)
        Call Enviar_Notificacao("SMQP0060", "SMQP0060 - Gera_Saida_GTR" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description & " Sinistro: " & CStr(strSinistroBB) & " Evento: " & CStr(strEventoBB))
    Else
       'Call Enviar_Notificacao("SEGP0459", "SEGP0459 - Gera_Saida_GTR" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description & vbCrLf & " SQL: " & SQL)
        Call Enviar_Notificacao("SMQP0060", "SMQP0060 - Gera_Saida_GTR" & vbCrLf & " Erro: " & Err.Number & " Descri��o: " & Err.Description & vbCrLf & " SQL: " & SQL & " Sinistro: " & CStr(strSinistroBB) & " Evento: " & CStr(strEventoBB))
    End If
    
    Call GravaMensagem("Rotina: Gera_Saida_GTR." & vbCrLf & "Descri��o Erro: " & Err.Description & " .", "VB", "Gera_Saida_GTR")
       
    If i < flexImportacao.Rows - 1 Then
        GoTo Continue
    Else
        cmdSair.Enabled = True
        cmdGerarAgora.Enabled = True
        cmdIniciar.Enabled = True
        picBotao(0).Enabled = True
        picBotao(1).Enabled = True
    End If
        
End Sub
Private Function Monta_Flex_Saida(strAmbiente As String) As Boolean

    '**************************************************************************************************
    'Data Altera��o: 14/05/2013 (�ltima Atualiza��o: 22/05/2013)
    'Projeto       : Melhorias do SEGBR
    'Autores       : Felipe Sbragia / Erika Lessa
    'Descri��o     : Cria��o da tabela tempor�ria ##retorno (Global) como Local em procedure (SEGS9398_SPS)
    '                Procedure SEGS9398_SPS passa a executar proc "seleciona_sinistro_encerrado_sps"
    '                Mudan�a de conex�o adaptada a ExecutarSQL
    '**************************************************************************************************

    Monta_Flex_Saida = False
    
    Dim SQL As String, rs As rdoResultset
    Dim Linha As String
    Dim linhas As Double
    
    If strAmbiente = "AB" Then
        flexImportacao.Rows = 1
    End If
    
On Error GoTo Erro
    'Rodrigo.Moreira 
    'GravarTrace "Procurando novos eventos ..."
    
    SQL = "EXEC seguros_db.dbo.SMQS00180_SPS "
    
    If strAmbiente = "AB" Then
        Set rs = rdocn.OpenResultset(SQL)
        'VarAmbiente = "AB"
        'Rodrigo.Moreira
        'Call GravarTrace("Tipo Processamento: " & " 1 - Ambiente: AB")
        'Print #num, 16/03/2016 18:17:55 - Tipo Processamento: " & tpLog & " -  Ambiente: " & Ambiente
    Else
        Set rs = rdocn_Seg2.OpenResultset(SQL)
        'VarAmbiente = "ABS"
        'Rodrigo.Moreira
        'Call GravarTrace("Tipo Processamento: " & " 2 - Ambiente: ABS")
    End If
    
    'Rodrigo.Moreira
    'linhas = 0
    
    If Not rs.EOF Then
        While Not rs.EOF
                        
            Linha = vbTab & rs("evento_id") & vbTab     'Sequencial
            Linha = Linha & rs("evento_bb_id") & vbTab  'Evento
            Linha = Linha & rs("sinistro_bb") & vbTab   'Sinistro BB
            Linha = Linha & rs("layout_id") & vbTab     'Layout_id
            Linha = Linha & rs("num_remessa") & vbTab   'Num_Remessa
            Linha = Linha & rs("cod_transacao") & vbTab 'Cod_Transa��o
            Linha = Linha & rs("produto_id") & vbTab    'Produto_id
            Linha = Linha & rs("empresa") & vbTab       'Empresa
            
          
         'Rodrigo.Moreira

       'Rodrigo Moreira -Processar apenas os sinistros dos produtos desejados
            
            If rs("evento_id") = 41766736 Then
            flexImportacao.AddItem Linha
            linhas = linhas + 1
            End If

            rs.MoveNext
            
        Wend
        
        Monta_Flex_Saida = True
        
    End If
    
    'Rodrigo.Moreira
    'GravarTrace "Qtde de Registros em " & strAmbiente & ": " & linhas
    
    rs.Close
    
    Exit Function

Erro:
 
 ''Erika 21/08/2013
  Call GravaLogErro(Err.Number, Err.Description, "Monta_Flex_Saida")

End Function
Private Sub Atualiza_Emissao(ByVal Evento_id As String, ByVal Layout_Id As String, ByVal Empresa As Integer)

    On Error GoTo Erro

    Dim SQL As String

    ' Marca a data de emiss�o na tabela para n�o processar mais.
    
    If Empresa = 2 Or Empresa = 3 Then
        SQL = "EXEC " & GTR_banco & "..GTR_emissao_layout_spu "
    ElseIf Empresa = 6 Or Empresa = 7 Then
        SQL = "EXEC ABSS." & GTR_banco & "..GTR_emissao_layout_spu "
    End If
      
    SQL = SQL & Evento_id           ' @evento_id
    SQL = SQL & ", " & Layout_Id    ' @layout_id
    SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'" ' @dt_emissao
    SQL = SQL & ", '" & cUserName & "'" ' @usuario
    
    rdocn.Execute (SQL)
     
    Exit Sub

Erro:
    Err.Raise Err.Number, "frmGeraSaidaGTR.Atualiza_Emissao", "Erro ao atualizar emiss�o."
End Sub

Private Sub Carrega_Registros(ByVal Entrada_GTR_ID As String, ByRef registros As Variant)

    Dim SQL As String, Rs_GTR As rdoResultset
    Dim i As Integer
    
    'In�cio - Melhorias do SEGBR - Erika Lessa - 17/05/2013
    'SQL = "SELECT * FROM entrada_gtr_registro_tb WITH (NOLOCK) "
    SQL = "SELECT * FROM ENTRADA_GTR_REGISTRO_ATUAL_TB WITH (NOLOCK) "
    'Fim - Melhorias do SEGBR - Erika Lessa - 17/05/2013
   
    SQL = SQL & " WHERE entrada_gtr_id = " & Entrada_GTR_ID

    i = 0
    Set Rs_GTR = rdocn_Interface.OpenResultset(SQL)
    While Not Rs_GTR.EOF
        registros(i) = Rs_GTR("registro")
        Rs_GTR.MoveNext
        i = i + 1
    Wend
    Rs_GTR.Close

End Sub

Private Sub Atualiza_FlexImportacao(Linha As Integer, Tipo As String)
'Rodrigo.Moreira
'On Error GoTo Erro

    flexImportacao.Col = 0
    flexImportacao.Row = Linha
    If flexImportacao.CellPicture = 1392844117 Then Exit Sub
    If Linha > 15 Then flexImportacao.TopRow = Linha - 5
    flexImportacao.CellPictureAlignment = 3
    If Tipo = "TEMPO" Then
        Set flexImportacao.CellPicture = ImageList1.ListImages(1).Picture
    ElseIf Tipo = "OK" Then
        Set flexImportacao.CellPicture = ImageList1.ListImages(2).Picture
    ElseIf Tipo = "ERRO" Then
        Set flexImportacao.CellPicture = ImageList1.ListImages(3).Picture
    ElseIf Tipo = "BLOQUEADO" Then
        Set flexImportacao.CellPicture = ImageList1.ListImages(4).Picture
    End If
    
    flexImportacao.Refresh

'Rodrigo.Moreira    
'Erro:
'    Err.Raise Err.Number, "frmGeraSaidaGTR.Atualiza_FlexImportacao", "Erro ao Atualiza Flex Importacao."
End Sub

Private Sub Grava_Log_GTR(Num_Arquivo As Integer, Data_Hora As Date, Entrada_GTR_ID As String, Evento_BB As String)

    Dim Linha As String
    
    Linha = Format(Data_Hora, "dd/mm/yyyy HH:MM:SS")
    Linha = Linha & " ---> entrada_gtr_id = " & Entrada_GTR_ID
    Linha = Linha & " "

    Print #Num_Arquivo, Linha

End Sub

Private Function Existe_Pgtos(ByVal OSinistro_BB As String, ByVal Empresa As String) As Boolean
    
    Dim SQL         As String
    Dim rc          As rdoResultset
    Dim AQtde_Pgtos As String
    Dim ASituacao   As Byte
    
    Existe_Pgtos = True
    
    '* ****************************************************************************** *'
    '* Obt�m a situa��o do sinistro -- Inclu�do por Junior em 21/03/2003              *'
    '* ****************************************************************************** *'
    
    '**************************************
    'Altera��o a pedido de Marcio Yoshimura
    'feito por Stefanini 23/12/2004
    '**************************************
    'SQL = "SELECT situacao"
    SQL = "SELECT situacao = case when dt_fim_vigencia is not null then 2 else situacao end"
    '**************************************
    SQL = SQL & " FROM sinistro_tb s WITH (NOLOCK) "
    SQL = SQL & "   INNER JOIN sinistro_BB_tb b WITH (NOLOCK) "
    SQL = SQL & "       ON (b.sinistro_id            = s.sinistro_id"
    SQL = SQL & "       AND b.apolice_id             = s.apolice_id"
    SQL = SQL & "       AND b.sucursal_seguradora_id = s.sucursal_seguradora_id"
    SQL = SQL & "       AND b.seguradora_cod_susep   = s.seguradora_cod_susep"
    SQL = SQL & "       AND b.ramo_id                = s.ramo_id)"
'   Alterado por Gustavo Machado em 25/08/2003
'   SQL = SQL & "       AND b.dt_fim_vigencia        IS NULL)"
    SQL = SQL & " WHERE b.sinistro_BB           = " & OSinistro_BB  'N� do SINISTRO BB
   
    If bUsarConexaoABS Then
        If Empresa = glAmbiente_id_seg2 Then
            Set rdocn = rdocn_Seg2
        Else
            Set rdocn = rdocn_Seg
        End If
    Else
        Set rdocn = rdocn_Seg
    End If
    
    Set rc = rdocn.OpenResultset(SQL)
    ASituacao = rc("situacao")
    
    rc.Close
    
    '* ****************************************************************************** *'
    '* Verifica se existe algum evento de cria��o do Recibo de Pagamento(1151)        *'
    '* que ainda n�o foi enviado e a situa��o do sinistro tem que estar encerrada (2) *'
    '* ****************************************************************************** *'
    SQL = "SELECT qtde_pgtos = COUNT(*)"
    
    'In�cio - Melhorias do SEGBR - Erika Lessa - 17/05/2013
    'SQL = SQL & " FROM evento_SEGBR_sinistro_tb a WITH (NOLOCK) "
    SQL = SQL & " FROM EVENTO_SEGBR_SINISTRO_ATUAL_TB a WITH (NOLOCK) "
    'Fim - Melhorias do SEGBR - Erika Lessa - 17/05/2013
 
    'In�cio - Melhorias do SEGBR - Erika Lessa - 17/05/2013
    'SQL = SQL & "   INNER JOIN(" & GTR_banco & "..GTR_emissao_layout_tb b WITH (NOLOCK) "
    'SQL = SQL & "       INNER JOIN " & GTR_banco & "..GTR_layout_tb c WITH (NOLOCK) "
    SQL = SQL & "   INNER JOIN(" & GTR_banco & "..GTR_emissao_layout_tb b WITH (NOLOCK) "
    SQL = SQL & "       INNER JOIN " & GTR_banco & "..GTR_layout_tb c WITH (NOLOCK) "
    'Fim - Melhorias do SEGBR - Erika Lessa - 17/05/2013
    
    SQL = SQL & "           ON b.layout_id = c.layout_id )"
    SQL = SQL & "       ON a.evento_id = b.evento_id"
    SQL = SQL & " WHERE c.entidade_id = 5"                  ' Entidade_destino = 5 (GTR)
    SQL = SQL & "   AND c.entrada_saida = 's'"              ' Sa�da
    SQL = SQL & "   AND a.entidade_id   = 1"                ' Alian�a do Brasil gerou o evento
    SQL = SQL & "   AND a.evento_bb_id  IS NOT NULL"        ' Que tem evento_BB_id
    SQL = SQL & "   AND b.dt_emissao    IS NULL"            ' Ainda n�o emitidos
    SQL = SQL & "   AND c.ativo         = 's'"              ' Apenas para os ativos
    SQL = SQL & "   AND a.evento_bb_id  = 1151"             ' Somente pagamentos
    SQL = SQL & "   AND a.sinistro_bb   = " & OSinistro_BB  'Pra este sinistro_BB
    
   
    If bUsarConexaoABS Then
        If Empresa = glAmbiente_id_seg2 Then
            Set rdocn = rdocn_Seg2
        Else
            Set rdocn = rdocn_Seg
        End If
    Else
        Set rdocn = rdocn_Seg
    End If
    
    Set rc = rdocn.OpenResultset(SQL)
    
    If Not rc.EOF Then
        AQtde_Pgtos = rc("qtde_pgtos")
    End If
    
    rc.Close
    
    'In�cio - Melhorias do SEGBR - Erika Lessa - 21/05/2013
    'If bUsarConexaoABS Then
    '    Set rdocn = rdocn_Seg
    'End If
    'Fim - Melhorias do SEGBR - Erika Lessa - 21/05/2013

    If (ASituacao = 2 Or ASituacao = 5) And AQtde_Pgtos = 0 Then Existe_Pgtos = False
    
End Function

Private Function Existe_Pgtos_OLD(ByVal OSinistro_BB As String) As Boolean
    
    Dim SQL As String, rc As rdoResultset
    Dim AData_Implantacao_GTR   As String
    Dim ASituacao   As Byte
    
    Existe_Pgtos_OLD = True
    
    AData_Implantacao_GTR = ObtemConteudo("DATA_IMPLANTACAO_GTR")
    
    '* Verifica se existem pagamentos pendentes no SEGBR
    SQL = "SELECT qtde_pgtos = COUNT(*)"
    SQL = SQL & " FROM pgto_sinistro_tb p WITH (NOLOCK) "
    SQL = SQL & "   INNER JOIN sinistro_BB_tb b WITH (NOLOCK) "
    SQL = SQL & "       ON (b.sinistro_id            = p.sinistro_id"
    SQL = SQL & "       AND b.apolice_id             = p.apolice_id"
    SQL = SQL & "       AND b.sucursal_seguradora_id = p.sucursal_seguradora_id"
    SQL = SQL & "       AND b.seguradora_cod_susep   = p.seguradora_cod_susep"
    SQL = SQL & "       AND b.ramo_id                = p.ramo_id"
    SQL = SQL & "       AND b.dt_fim_vigencia        IS NULL)"
    SQL = SQL & " WHERE b.sinistro_BB           = " & OSinistro_BB  'N� do SINISTRO BB
    SQL = SQL & "   AND p.item_val_estimativa   = 1"                'Somente INDENIZA��O
    SQL = SQL & "   AND p.situacao_op           NOT IN ('c', 'e')"  'Pagamentos que n�o est�o cancelados e nem estornados
    SQL = SQL & "   AND p.autorizado_SISBB      = 'n'"              'Ainda n�o aprovados pelo programa SEGP0420
    SQL = SQL & "   AND p.retorno_recibo_BB     <> 's'"             'Pagamentos que ainda n�o foram confirmados ou j� confirmados pelo Banco
    SQL = SQL & "   AND p.dt_solicitacao_pgto   >= '" & AData_Implantacao_GTR & "'" 'Data de implanta��o do GTR
    Set rc = rdocn.OpenResultset(SQL)
    If Not rc.EOF Then
       If rc("qtde_pgtos") = 0 Then
          rc.Close
          
          '* ****************************************************************************** *'
          '* Obt�m a situa��o do sinistro -- Inclu�do por Junior em 21/03/2003              *'
          '* ****************************************************************************** *'
          SQL = "SELECT situacao"
          SQL = SQL & " FROM sinistro_tb s WITH (NOLOCK) "
          SQL = SQL & "   INNER JOIN sinistro_BB_tb b WITH (NOLOCK) "
          SQL = SQL & "       ON (b.sinistro_id            = s.sinistro_id"
          SQL = SQL & "       AND b.apolice_id             = s.apolice_id"
          SQL = SQL & "       AND b.sucursal_seguradora_id = s.sucursal_seguradora_id"
          SQL = SQL & "       AND b.seguradora_cod_susep   = s.seguradora_cod_susep"
          SQL = SQL & "       AND b.ramo_id                = s.ramo_id"
          SQL = SQL & "       AND b.dt_fim_vigencia        IS NULL)"
          SQL = SQL & " WHERE b.sinistro_BB           = " & OSinistro_BB  'N� do SINISTRO BB
          Set rc = rdocn.OpenResultset(SQL)
             ASituacao = rc("situacao")
          rc.Close
          
          '* ****************************************************************************** *'
          '* Verifica se existe algum evento de cria��o do Recibo de Pagamento(1151)        *'
          '* que ainda n�o foi enviado e a situa��o do sinistro tem que estar encerrada (2) *'
          '* ****************************************************************************** *'
          SQL = "SELECT qtde_pgtos = COUNT(*)"
          SQL = SQL & " FROM evento_SEGBR_sinistro_tb a WITH (NOLOCK) "
          SQL = SQL & "   INNER JOIN(" & GTR_banco & "..GTR_emissao_layout_tb b WITH (NOLOCK) "
          SQL = SQL & "       INNER JOIN " & GTR_banco & "..GTR_layout_tb c WITH (NOLOCK) "
          SQL = SQL & "           ON b.layout_id = c.layout_id )"
          SQL = SQL & "       ON a.evento_id = b.evento_id"
          SQL = SQL & " WHERE c.entidade_id = 5"                  ' Entidade_destino = 5 (GTR)
          SQL = SQL & "   AND c.entrada_saida = 's'"              ' Sa�da
          SQL = SQL & "   AND a.entidade_id   = 1"                ' Alian�a do Brasil gerou o evento
          SQL = SQL & "   AND a.evento_bb_id  IS NOT NULL"        ' Que tem evento_BB_id
          SQL = SQL & "   AND b.dt_emissao    IS NULL"            ' Ainda n�o emitidos
          SQL = SQL & "   AND c.ativo         = 's'"              ' Apenas para os ativos
          SQL = SQL & "   AND a.evento_bb_id  = 1153"             ' Somente pagamentos
          SQL = SQL & "   AND a.sinistro_bb   = " & OSinistro_BB  'Pra este sinistro_BB
          Set rc = rdocn.OpenResultset(SQL)
          If Not rc.EOF Then
             Existe_Pgtos_OLD = IIf(rc("qtde_pgtos") > 0, True, False)
          End If
       End If
    End If
    rc.Close
    
End Function

Public Sub MontaColecaoLayout() ''(ByVal LayoutId1 As String, ByVal LayoutId2 As String)

    '**************************************************************************************************
    'Data Altera��o: 17/06/2013
    'Projeto       : Melhorias do SEGBR
    'Autores       : Erika Lessa
    'Descri��o     : Novo m�todo para criar Cole��o de Layouts
    '**************************************************************************************************

''  Dim rs As New ADODB.Recordset, SQL As String
    Dim Layout As clEvento_Layout
    
    Dim rs As rdoResultset
    Dim Evento As clEvento_Layout
    Dim sSQL As String
                          
On Error GoTo Erro

    sSQL = " SELECT DISTINCT B.layout_id, B.detalhe_id, B.seq_campo, B.nome_SEGBR as nome_campo, B.tabela_SEGBR as tabela, "
    sSQL = sSQL & " B.constante, B.tamanho, B.tipo, B.formato, "
    sSQL = sSQL & " B.separador_decimal, B.separador_milhar, 'N' as transacao "
    sSQL = sSQL & " FROM " & GTR_banco & "..GTR_layout_campo_tb B WITH (NOLOCK) "
    sSQL = sSQL & " INNER JOIN " & GTR_banco & "..GTR_layout_detalhe_tb A WITH (NOLOCK) "
    sSQL = sSQL & " ON A.layout_id = B.layout_id AND A.detalhe_id = B.detalhe_id "
   
   'Identifica transa��o
    sSQL = sSQL & " INNER JOIN INTERFACE_DB.DBO.GTR_EMISSAO_LAYOUT_TB C WITH (NOLOCK) "
    sSQL = sSQL & " ON C.LAYOUT_ID = A.LAYOUT_ID "
    sSQL = sSQL & " INNER JOIN SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB D WITH (NOLOCK) "
    sSQL = sSQL & " ON D.EVENTO_ID = C.EVENTO_ID "
   'Filtra transa��o <> de ST10/ST00
    'sSQL = sSQL & " WHERE (D.COD_TRANSACAO <> 'ST10' AND D.COD_TRANSACAO <> 'ST00') "
       
    sSQL = sSQL & " Union "
    
    sSQL = sSQL & " SELECT DISTINCT B.layout_id, B.registro_id, B.seq_campo, B.coluna_origem as nome_campo, B.tabela_origem as tabela, B.constante, B.tamanho, "
    sSQL = sSQL & " B.tp_dado, B.formato, null as separador_decimal, null as separador_milhar, 'S' as transacao "
    sSQL = sSQL & " From INTERFACE_DB.DBO.layout_campo_tb B WITH (NOLOCK) "
    sSQL = sSQL & " INNER JOIN interface_db.dbo.layout_registro_tb A WITH (NOLOCK) "
    sSQL = sSQL & " ON A.layout_id = B.layout_id AND A.registro_id = B.registro_id "

   'Identifica transa��o
    sSQL = sSQL & " INNER JOIN INTERFACE_DB.DBO.GTR_EMISSAO_LAYOUT_TB C WITH (NOLOCK) "
    sSQL = sSQL & " ON C.LAYOUT_ID = A.LAYOUT_ID "
    sSQL = sSQL & " INNER JOIN SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB D WITH (NOLOCK) "
    sSQL = sSQL & " ON D.EVENTO_ID = C.EVENTO_ID "
   'Filtra transa��o <> de ST10/ST00
    'sSQL = sSQL & " WHERE (D.COD_TRANSACAO = 'ST10' OR D.COD_TRANSACAO = 'ST00') "
   
    sSQL = sSQL & " ORDER BY b.layout_id, 2, B.seq_campo "
    
    Set rs = rdocn_Interface.OpenResultset(sSQL)
      
    If Not rs.EOF Then
        While Not rs.EOF
            Set Layout = New clEvento_Layout
            Layout.Layout_Id = rs("layout_id")
            Layout.Detalhe_id = rs("detalhe_id")
            Layout.Seq_Campo = rs("seq_campo")
            Layout.Nome_Campo = IIf(IsNull(rs("nome_campo")), "", rs("nome_campo"))
            Layout.Tabela = IIf(IsNull(rs("tabela")), "", rs("tabela"))
            Layout.Conteudo = IIf(IsNull(rs("constante")), "", rs("constante"))
            Layout.Tamanho = IIf(IsNull(rs("tamanho")), "", rs("tamanho"))
            Layout.Tipo = IIf(IsNull(rs("tipo")), "", rs("tipo"))
            Layout.Formato = IIf(IsNull(rs("formato")), "", rs("formato"))
            Layout.Separador_Decimal = IIf(IsNull(rs("separador_decimal")), "", rs("separador_decimal"))
            Layout.Separador_Milhar = IIf(IsNull(rs("separador_milhar")), "", rs("separador_milhar"))
            Layout.Transacao = rs("transacao")
            
            Eventos.Add Layout
            rs.MoveNext
        Wend
    End If
    
    rs.Close
    
    Exit Sub
    
Erro:

''Erika 21/08/2013
Call GravaLogErro(Err.Number, Err.Description, "MontaColecaoLayout")
    
End Sub
Public Function Gera_Registros_Saida(ByVal Evento_id As String, _
                                     ByVal Layout_Id As String, _
                                     ByRef registros() As String, _
                                     ByVal Empresa As String, _
                                     Optional ByVal Evento_BB As String, _
                                     Optional ByVal Transacao As String _
                                    ) As Long
                                    
'**************************************************************************************************
'Data Altera��o: 24/05/2013
'Projeto       : Melhorias do SEGBR
'Autores       : Erika Lessa
'Descri��o     : M�todo adaptado para gera��o de registros a partir da cole��o de layouts
'**************************************************************************************************
                                  
On Error GoTo Erro

    Dim rs As rdoResultset, SQL As String, i As Integer
    'Dim rs As New ADODB.Recordset, SQL As String, i As Integer
    Dim Query As String, Query_Select As String, Query_From As String
    Dim Formato() As String, Tamanho() As Integer, Tipo() As String
    Dim Sep_Milhar() As String, Sep_Decimal() As String
    Dim Detalhes() As String, Qtd_Registros As Integer
    
    'GENJUNIOR - FLOW 17860335 - LOG DE EXECU��O
    '21/10/2013
    Dim SQL_log As String
    
    Dim cLayout As clEvento_Layout
    Dim sDetalheId_ant As String
            
    i = 0
    Qtd_Registros = 0
    sDetalheId_ant = "" 'armazena detalhe anterior
                             
    For Each cLayout In Eventos
        If (cLayout.Layout_Id = Layout_Id And cLayout.Transacao = IIf(Transacao <> "ST10" And Transacao <> "ST00", "N", "S")) Then
            'Formata registros agrupados por detalhe:
            If sDetalheId_ant <> cLayout.Detalhe_id Then
                If sDetalheId_ant <> "" Then
                    Query = Query_Select
                    Query = Query & Query_From
                    Query = Query & " WHERE evento_SEGBR_sinistro_atual_tb.evento_id = " & Evento_id
                    
                    If bUsarConexaoABS Then
                        If ((Empresa = 6 Or Empresa = 7) And (glAmbiente_id_seg2 = 6 Or glAmbiente_id_seg2 = 7)) Then
                            Set rs = rdocn_Seg2.OpenResultset(Query)
                        Else
                            Set rs = rdocn_Seg.OpenResultset(Query)
                        End If
                    Else
                        Set rs = rdocn_Seg.OpenResultset(Query)
                    End If
                                      
                    If Not rs.EOF Then
                        While Not rs.EOF
                            Qtd_Registros = Qtd_Registros + 1
                            ReDim Preserve registros(Qtd_Registros) As String
                            If Transacao <> "ST10" And Transacao <> "ST00" Then
                                registros(Qtd_Registros) = Formata_Registro(rs, Tamanho, Formato, Tipo, Sep_Decimal, Sep_Milhar, Evento_BB)
                            Else
                                registros(Qtd_Registros) = Formata_Registro_ST10(rs, Tamanho, Formato, Tipo)
                            End If
                            rs.MoveNext
                        Wend
                    End If
                    rs.Close
                    
                End If
                'Redimensiona array
                ReDim Tamanho(0) As Integer
                ReDim Formato(0) As String
                ReDim Tipo(0) As String
                ReDim Sep_Decimal(0) As String
                ReDim Sep_Milhar(0) As String
                
                Query_From = " FROM evento_SEGBR_sinistro_atual_tb WITH(NOLOCK) "
                Query_Select = ""
            End If
            
            'Se for primeira do loop, "SELECT", sen�o coloca v�rgula pois � outro campo
            If Query_Select = "" Then
                Query_Select = "SELECT "
            Else
                Query_Select = Query_Select & ", "
            End If
    
            If cLayout.Tipo = "CT" Then
                'Se constante, basta copi�-lo na Query_Select.
                Query_Select = Query_Select & "'" & Trim(cLayout.Conteudo) & "'"
            Else
                'Se campo de tabela, busca <tabela>.<campo>
                Query_Select = Query_Select & cLayout.Tabela & "." & cLayout.Nome_Campo
            End If
    
            'Preenche as tabelas que far�o parte do FROM
            If InStr(LCase(Query_From), "" & LCase(cLayout.Tabela)) = 0 Then
                'Todas com left join, pois pode ser que n�o exista.
                Query_From = Query_From & " LEFT JOIN " & cLayout.Tabela & " WITH(NOLOCK) ON "
                Query_From = Query_From & " evento_SEGBR_sinistro_atual_tb.evento_id = " & cLayout.Tabela & ".evento_id "
            End If
    
            ReDim Preserve Tamanho(Val(cLayout.Seq_Campo)) As Integer
            ReDim Preserve Formato(Val(cLayout.Seq_Campo)) As String
            ReDim Preserve Tipo(Val(cLayout.Seq_Campo)) As String
            
            Tamanho(Val(cLayout.Seq_Campo)) = Val("0" & cLayout.Tamanho)
            Formato(Val(cLayout.Seq_Campo)) = "" & cLayout.Formato
            Tipo(Val(cLayout.Seq_Campo)) = "" & cLayout.Tipo
            
            If Transacao <> "ST10" And Transacao <> "ST00" Then
                
                ReDim Preserve Sep_Decimal(Val(cLayout.Seq_Campo)) As String
                ReDim Preserve Sep_Milhar(Val(cLayout.Seq_Campo)) As String
                
                Sep_Decimal(Val(cLayout.Seq_Campo)) = "" & Trim(cLayout.Separador_Decimal)
                Sep_Milhar(Val(cLayout.Seq_Campo)) = "" & Trim(cLayout.Separador_Milhar)
            End If
                                    
            sDetalheId_ant = cLayout.Detalhe_id
        End If
    Next
          
    'Formata registro do �ltimo detalhe
    Query = Query_Select
    Query = Query & Query_From
    Query = Query & " WHERE evento_SEGBR_sinistro_atual_tb.evento_id = " & Evento_id
    
    If bUsarConexaoABS Then
        If ((Empresa = 6 Or Empresa = 7) And (glAmbiente_id_seg2 = 6 Or glAmbiente_id_seg2 = 7)) Then
            Set rs = rdocn_Seg2.OpenResultset(Query)
        Else
            Set rs = rdocn_Seg.OpenResultset(Query)
        End If
    Else
        Set rs = rdocn_Seg.OpenResultset(Query)
    End If
  
    If Not rs.EOF Then
        While Not rs.EOF
            Qtd_Registros = Qtd_Registros + 1
            ReDim Preserve registros(Qtd_Registros) As String
            If Transacao <> "ST10" And Transacao <> "ST00" Then
                registros(Qtd_Registros) = Formata_Registro(rs, Tamanho, Formato, Tipo, Sep_Decimal, Sep_Milhar, Evento_BB)
            Else
                registros(Qtd_Registros) = Formata_Registro_ST10(rs, Tamanho, Formato, Tipo)
            End If
            rs.MoveNext
        Wend
    End If
    rs.Close

    Exit Function
  
Erro:
    Gera_Registros_Saida = Err.Number
  ''Erika 21/08/2013
    Call GravaLogErro(Err.Number, Err.Description & " EVENTO_ID: " & CStr(Evento_id), "Gera_Registro_Saida")
    
    If Evento_id >= 6384748 Then
        Debug.Print Err.Description
        Debug.Print Query
    End If
End Function
''Erika 21/08/2013 - grava log de erro
Sub GravaLogErro(nro_erro As Long, descr_erro As String, rotina As String)
Dim sSQL As String

    msgErro = "SMQP0060 - " & vbNewLine & nro_erro & descr_erro
    
    If Valida_Trace = True Then
        sSQL = "exec controle_sistema_db..log_erro_spi " & nro_erro & ", '" & MudaAspaSimples(descr_erro) & "', 'SMQP0060', 'SMQP0060', 'SMQP0060', 'VB', '" & rotina & "', '' "
        rdocn_aux.Execute (sSQL)
    End If
    
'Rodrigo.Moreira    
'GravarTrace msgErro
End Sub
''Erika 22/08/2013 - grava saidas nao geradas
Sub GravaLogSaida(Evento_id As Long, Sinistro_BB As String, Evento_BB As Integer, Empresa As Integer)
Dim sSQL As String
    
    'GENJUNIOR - FLOW 17860335
    'DATA: 02/10/2013
    'ALTERADO PARA INIBIR O EVENTO TANTO EM AB OU ABS
    
    If Empresa = 2 Then
        sSQL = "exec interface_db.DBO.SMQS00182_SPI " & Evento_id & ", " & IIf(Trim(Sinistro_BB) = "", "NULL", Sinistro_BB) & ", " & Evento_BB & ", 'SMQP0060' "
        rdocn_Interface.Execute (sSQL)
    Else
        sSQL = "exec interface_db.DBO.SMQS00182_SPI " & Evento_id & ", " & IIf(Trim(Sinistro_BB) = "", "NULL", Sinistro_BB) & ", " & Evento_BB & ", 'SMQP0060' "
        rdocn_Seg2.Execute (sSQL)
    End If
End Sub
 'Valida se � para gerar o controle de erro na log_erro_tb
Private Function Valida_Trace() As Boolean
    Dim SQL As String
    Dim rs As rdoResultset
         
    SQL = "select trace "
    SQL = SQL & "from interface_dados_db.dbo.parametrizacao_servico_tb With(nolock)"
    SQL = SQL & " where ServiceName = 'SMQP0060'"
    SQL = SQL & " and trace='TRUE'"
    Set rs = rdocn_Interface.OpenResultset(SQL)
    
    If Not rs.EOF Then
        Valida_Trace = True
    Else
        Valida_Trace = False
    End If
    
    rs.Close
End Function

Public Sub LogExecution(ByVal tpLog As Integer, ByVal serviceName As String, ByVal tpABS As String, Optional Ambiente As String)

Dim SQL As String
    SQL = ""
    SQL = SQL & " EXEC INTERFACE_DADOS_DB.DBO.SMQS0133_SPU " & tpLog & ", '" & serviceName & "','" & tpABS & "' " & vbNewLine

    rdocn_aux.Execute SQL
End Sub

Public Function ValidaHorario() As Boolean
    Dim horaAtual As Date
    Dim SQL As String, rs As rdoResultset
    Dim destinatario As String
    Dim TimeStart As String
    Dim TimeEnd As String
    
    horaAtual = Time()
    
    SQL = "select TimeStart, TimeEnd from  INTERFACE_DADOS_DB.DBO.PARAMETRIZACAO_SERVICO_TB  WHERE SERVICENAME = 'SMQP0060' and AmbienteBD = " & glAmbiente_id
    Set rs = rdocn.OpenResultset(SQL)
    If Not rs.EOF Then
        TimeStart = rs("TimeStart")
        TimeEnd = rs("TimeEnd")
    End If
    
    If horaAtual > TimeStart And horaAtual < TimeEnd Then
        Call GravarTrace("Inicio: " + TimeStart)
        Call GravarTrace("Fim: " + TimeEnd)
        Call GravarTrace("Hor�rio de execu��o permitido")
        Call GravarTrace("Iniciando Gera_Saida_GTR")
        ValidaHorario = True
    Else
        Call GravarTrace("Inicio: " + TimeStart)
        Call GravarTrace("Fim: " + TimeEnd)
        Call GravarTrace("Hor�rio de execu��o n�o permitido")
        ValidaHorario = False
    End If
End Function

Private Sub TerminateProcess(app_exe As String)
    Dim Process As Object
    For Each Process In GetObject("winmgmts:").ExecQuery("Select Name from Win32_Process Where Name = '" & app_exe & "'")
        Process.Terminate
    Next
End Sub
