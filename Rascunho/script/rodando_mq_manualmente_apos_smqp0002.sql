/*
O objetivo desse script � simular a resposta do BB quando os MQs n�o est�o funcionado.
ele avan�a do smqp0002 e smqp0060 (apos terem sido executados automaticamente) e executa simula o MQs 014, 019, 061 e 059.
SMQP0002 (tem que ser executado automaticamente)
SMQP0060 (tem que ser executado automaticamente)
SMQP0014 (executado por esse script)
SMQP0019 (executado por esse script)
SMQP0061 (executado por esse script)
SMQP0059 (executado por esse script)
*/
--VARIAVEIS
declare @dt as SMALLDATETIME
declare @usurio as VARCHAR(20)

set @dt = '2019-11-13 11:00:00'
set @usurio = 'ntendencia_bsp'

--consulta grava��o de dados no tabel�o
SELECT evento_id
	,sinistro_id
	,proposta_id
	,apolice_id
	,seguradora_cod_susep
	,ramo_id
	,evento_segbr_id
	,entidade_id
	,evento_bb_id
	,localizacao
	,produto_id
	,proposta_id
	,solicitante_cpf
	,usuario
FROM seguros_db.dbo.evento_SEGBR_sinistro_atual_tb WITH (NOLOCK)
WHERE dt_inclusao > @dt
	AND usuario = @usurio

SELECT *
FROM seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb WITH (NOLOCK)
WHERE dt_inclusao > @dt
	AND usuario = @usurio

--conferindo dados apos rodar SMQP0002 (dt_coa, dt_, msg_id) preenchidos
SELECT *
FROM interface_db.dbo.saida_gtr_atual_tb WITH (NOLOCK)
WHERE dt_inclusao > @dt
GO

CREATE PROCEDURE dbo.atualiza_sinistro_bb_saida_gtr_atual_tb_BSP (
	@sinistro_id AS BIGINT
	,@apolice_id AS BIGINT
	,@ramo_id AS INTEGER
	)
AS
BEGIN
	DECLARE @sinistro_bb AS BIGINT

	SELECT @sinistro_bb = MAX(sinistro_bb) + 1
	FROM seguros_db.dbo.sinistro_bb_tb a WITH (NOLOCK)
	WHERE a.apolice_id = @apolice_id
		AND ramo_id = @ramo_id

	WHILE EXISTS (
			SELECT 1
			FROM seguros_db.dbo.sinistro_bb_tb a
			WHERE a.sinistro_bb = @sinistro_bb
			)
	BEGIN
		SET @sinistro_bb = @sinistro_bb + 1
	END

	UPDATE a
	SET sinistro_bb = @sinistro_bb
	FROM interface_db.dbo.saida_gtr_atual_tb a
	INNER JOIN SEGUROS_DB.DBO.evento_SEGBR_sinistro_atual_tb b
		ON a.cod_remessa = b.num_remessa
	WHERE sinistro_id = @sinistro_id
		AND proposta_id = @apolice_id

	UPDATE SEGUROS_DB.DBO.evento_SEGBR_sinistro_atual_tb
	SET sinistro_bb = @sinistro_bb
	WHERE sinistro_id = @sinistro_id
		AND proposta_id = @apolice_id
END
GO

--Inserir evento 2003
CREATE PROCEDURE DBO.INSERE_EVENTO_2003_BSP @sinistro_id AS NUMERIC(11)
AS
--insere na tabela evento_segbr_sinistro_atual_tb o evento 2003    
IF NOT EXISTS (
		SELECT sinistro_id
		FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb
		WHERE sinistro_id = @sinistro_id
			AND evento_bb_id = 2003
		)
BEGIN
	BEGIN
		INSERT seguros_db.dbo.evento_segbr_sinistro_atual_tb (
			sinistro_id
			,apolice_id
			,seguradora_cod_susep
			,sucursal_seguradora_id
			,ramo_id
			,evento_SEGBR_id
			,usuario
			,dt_evento
			,entidade_id
			,evento_bb_id
			,localizacao
			,produto_id
			,sinistro_bb
			,proposta_id
			,agencia_aviso_id
			,dt_aviso_sinistro
			,dt_ocorrencia_sinistro
			,endereco_sinistro
			,num_recibo
			,banco_aviso_id
			,moeda_id
			,pgto_nossa_parte
			,situacao_sinistro
			,processa_reintegracao_IS
			,dt_inclusao
			,enviado_eMail_tecnico
			,ind_reanalise
			,CD_PRD
			,CD_MDLD
			,CD_ITEM_MDLD
			,cod_transacao
			)
		SELECT sinistro_id
			,apolice_id
			,seguradora_cod_susep
			,sucursal_seguradora_id
			,ramo_id
			,10110 AS evento_SEGBR_id
			,'VFOSANTOS' AS usuario
			,GETDATE() AS dt_evento
			,entidade_id
			,2003 AS evento_bb_id
			,'SEGBR' AS localizacao
			,produto_id
			,sinistro_bb
			,proposta_id
			,agencia_aviso_id
			,dt_aviso_sinistro
			,dt_ocorrencia_sinistro
			,endereco_sinistro
			,0 AS num_recibo
			,banco_aviso_id
			,moeda_id
			,'n' AS pgto_nossa_parte
			,0 AS situacao_sinistro
			,processa_reintegracao_IS
			,GETDATE() AS dt_inclusao
			,'s' AS enviado_eMail_tecnico
			,'N' AS ind_reanalise
			,CD_PRD
			,CD_MDLD
			,CD_ITEM_MDLD
			,'ST00' AS cod_transacao
		FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb
		WHERE sinistro_id = @sinistro_id
			AND evento_bb_id = 1100
	END

	/*in�cio - l�gica para gerar o evento 1110*/
	--insere na tabela documentos_recebidos_GTR_tb para que seja poss�vel gravar posteriormente o evento 1110    
	IF NOT EXISTS (
			SELECT 1
			FROM INTERFACE_DB..documentos_recebidos_GTR_tb a
			INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
				ON b.sinistro_bb = a.sinistro_bb
			WHERE sinistro_id = @sinistro_id
				AND evento_bb_id = 1100
			)
	BEGIN
		INSERT INTERFACE_DB.dbo.documentos_recebidos_GTR_tb (
			cod_remessa
			,sinistro_bb
			,evento_BB
			,usuario
			,dt_inclusao
			)
		SELECT DISTINCT ISNULL(a.cod_remessa, b.num_remessa)
			,b.sinistro_bb
			,'2003'
			,'Firstone'
			,GETDATE()
		FROM INTERFACE_DB..entrada_GTR_atual_tb a
		INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
			ON b.sinistro_bb = a.sinistro_bb
		WHERE b.sinistro_id = @sinistro_id
			--    INSERT INTERFACE_DB..documentos_recebidos_GTR_tb    
			--           (cod_remessa, sinistro_bb, evento_BB, usuario, dt_inclusao)    
			--    SELECT num_remessa, sinistro_bb, '2003', 'Firstone', GETDATE()    
			--      FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb    
			--     WHERE sinistro_id = @sinistro_id    
			--       AND evento_bb_id = 1100    
	END

	IF NOT EXISTS (
			SELECT 1
			FROM INTERFACE_DB..entrada_GTR_tb a
			INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
				ON b.sinistro_bb = a.sinistro_bb
			WHERE b.sinistro_id = @sinistro_id
				AND b.evento_bb_id = 1100
				AND a.evento_BB = 2003
			)
	BEGIN
		INSERT INTERFACE_DB..entrada_GTR_atual_tb (
			cod_remessa
			,qtd_registros
			,cod_transacao
			,evento_BB
			,sinistro_bb
			,situacao
			,cod_erro
			,usuario
			,dt_inclusao
			,situacao_MQ
			)
		SELECT DISTINCT a.cod_remessa
			,1
			,a.cod_transacao
			,'2003'
			,a.sinistro_bb
			,a.situacao
			,a.cod_erro
			,'VFOSANTOS'
			,a.dt_inclusao
			,'T'
		FROM INTERFACE_DB..entrada_GTR_atual_tb a
		INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
			ON b.sinistro_bb = a.sinistro_bb
		WHERE b.sinistro_id = @sinistro_id
	END

	IF NOT EXISTS (
			SELECT 1
			FROM INTERFACE_DB..entrada_GTR_tb a
			INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
				ON b.sinistro_bb = a.sinistro_bb
			INNER JOIN INTERFACE_DB..entrada_GTR_registro_tb c
				ON c.entrada_gtr_id = a.entrada_gtr_id
			WHERE b.sinistro_id = @sinistro_id
				AND b.evento_bb_id = 1100
				AND a.evento_BB = 2003
			)
	BEGIN
		DECLARE @entrada_gtr_id NUMERIC(15)

		SELECT @entrada_gtr_id = a.entrada_gtr_id
		FROM INTERFACE_DB..entrada_GTR_tb a
		INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
			ON b.sinistro_bb = a.sinistro_bb
		WHERE b.sinistro_id = @sinistro_id
			AND b.evento_bb_id = 1100
			AND a.evento_BB = 2003

		INSERT INTERFACE_DB..entrada_GTR_registro_atual_tb (
			entrada_gtr_id
			,registro_id
			,registro
			,reg_cd_tip
			,usuario
			,dt_inclusao
			)
		VALUES (
			@entrada_gtr_id
			,1
			,'P000500991'
			,'P'
			,'Firstone'
			,GETDATE()
			)
	END

	--inser��o do detalhe para aparecer no programa SEGP1285    
	IF NOT EXISTS (
			SELECT 1
			FROM seguros_db.dbo.evento_Segbr_sinistro_atual_tb a
			INNER JOIN seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb b
				ON a.evento_id = b.evento_id
			WHERE a.sinistro_id = @sinistro_id
				AND a.evento_bb_id = 2003
			)
	BEGIN
		INSERT seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb (
			evento_id
			,tp_detalhe
			,seq_detalhe
			,descricao
			,usuario
			)
		SELECT evento_id
			,'ANOTACAO'
			,1
			,'AVISO HOMOLOGADO PELA SEGURADORA'
			,'ALS_IMP'
		FROM seguros_db.dbo.evento_Segbr_sinistro_atual_tb
		WHERE sinistro_id = @sinistro_id
			AND evento_bb_id = 2003
	END

	IF NOT EXISTS (
			SELECT 1
			FROM seguros_db.dbo.evento_Segbr_sinistro_atual_tb a
			INNER JOIN seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb b
				ON a.evento_id = b.evento_id
			INNER JOIN seguros_db.dbo.sinistro_linha_detalhamento_tb c
				ON a.sinistro_id = c.sinistro_id
			WHERE a.sinistro_id = @sinistro_id
				AND a.evento_bb_id = 2003
				AND c.linha = 'AVISO HOMOLOGADO PELA SEGURADORA'
			)
	BEGIN
		DECLARE @detalhamento_id INT

		SELECT @detalhamento_id = MAX(detalhamento_id) + 1
		FROM seguros_db.dbo.sinistro_linha_detalhamento_tb
		WHERE sinistro_id = @sinistro_id

		INSERT Seguros_Db.Dbo.Sinistro_Detalhamento_Tb (
			sinistro_id
			,apolice_id
			,sucursal_seguradora_id
			,seguradora_cod_susep
			,ramo_id
			,detalhamento_id
			,dt_detalhamento
			,tp_detalhamento
			,restrito
			,usuario
			,dt_inclusao
			)
		SELECT a.sinistro_id
			,a.apolice_id
			,a.sucursal_seguradora_id
			,a.seguradora_cod_susep
			,a.ramo_id
			,@detalhamento_id
			,GETDATE()
			,0
			,'n'
			,b.usuario
			,a.dt_inclusao
		FROM seguros_db.dbo.evento_Segbr_sinistro_atual_tb a
		INNER JOIN seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb b
			ON a.evento_id = b.evento_id
		WHERE a.sinistro_id = @sinistro_id
			AND a.evento_bb_id = 2003

		INSERT seguros_db.dbo.sinistro_linha_detalhamento_tb (
			sinistro_id
			,apolice_id
			,sucursal_seguradora_id
			,seguradora_cod_susep
			,ramo_id
			,detalhamento_id
			,linha_id
			,linha
			,usuario
			)
		SELECT a.sinistro_id
			,a.apolice_id
			,a.sucursal_seguradora_id
			,a.seguradora_cod_susep
			,a.ramo_id
			,@detalhamento_id
			,1
			,b.descricao
			,b.usuario
		FROM seguros_db.dbo.evento_Segbr_sinistro_atual_tb a
		INNER JOIN seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb b
			ON a.evento_id = b.evento_id
		WHERE a.sinistro_id = @sinistro_id
			AND a.evento_bb_id = 2003
	END

	/*fim - l�gica para gerar o evento 1110*/
	SELECT 'seguros_db.dbo.evento_segbr_sinistro_atual_tb'

	SELECT sinistro_id
		,apolice_id
		,seguradora_cod_susep
		,sucursal_seguradora_id
		,ramo_id
		,evento_SEGBR_id
		,usuario
		,dt_evento
		,entidade_id
		,evento_bb_id
		,localizacao
		,produto_id
		,sinistro_bb
		,proposta_id
		,agencia_aviso_id
		,dt_aviso_sinistro
		,dt_ocorrencia_sinistro
		,endereco_sinistro
		,num_recibo
		,banco_aviso_id
		,moeda_id
		,pgto_nossa_parte
		,situacao_sinistro
		,processa_reintegracao_IS
		,dt_inclusao
		,enviado_eMail_tecnico
		,ind_reanalise
		,CD_PRD
		,CD_MDLD
		,CD_ITEM_MDLD
		,cod_transacao
	FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb
	WHERE sinistro_id = @sinistro_id

	SELECT 'INTERFACE_DB..documentos_recebidos_GTR_tb'

	SELECT a.*
	FROM INTERFACE_DB..documentos_recebidos_GTR_tb a
	INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
		ON b.sinistro_bb = a.sinistro_bb
	WHERE b.sinistro_id = @sinistro_id
		AND b.evento_bb_id = 1100

	SELECT 'INTERFACE_DB..entrada_GTR_tb'

	SELECT a.*
	FROM INTERFACE_DB..entrada_GTR_tb a
	INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
		ON b.sinistro_bb = a.sinistro_bb
	WHERE b.sinistro_id = @sinistro_id
		AND b.evento_bb_id = 1100
		AND a.evento_BB = 2003

	SELECT 'INTERFACE_DB..entrada_GTR_registro_tb'

	SELECT c.*
	FROM INTERFACE_DB..entrada_GTR_tb a
	INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb b
		ON b.sinistro_bb = a.sinistro_bb
	INNER JOIN INTERFACE_DB..entrada_GTR_registro_tb c
		ON c.entrada_gtr_id = a.entrada_gtr_id
	WHERE b.sinistro_id = @sinistro_id
		AND b.evento_bb_id = 1100
		AND a.evento_BB = 2003

	SELECT 'seguros_db..evento_SEGBR_sinistro_detalhe_atual_tb'

	SELECT b.*
	FROM seguros_db.dbo.evento_Segbr_sinistro_atual_tb a
	INNER JOIN seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb b
		ON a.evento_id = b.evento_id
	WHERE a.sinistro_id = @sinistro_id
		AND a.evento_bb_id = 2003

	SELECT 'seguros_db.dbo.sinistro_linha_detalhamento_tb'

	SELECT c.*
	FROM seguros_db.dbo.evento_Segbr_sinistro_atual_tb a
	INNER JOIN seguros_db.dbo.evento_SEGBR_sinistro_detalhe_atual_tb b
		ON a.evento_id = b.evento_id
	INNER JOIN seguros_db.dbo.sinistro_linha_detalhamento_tb c
		ON a.sinistro_id = c.sinistro_id
	WHERE a.sinistro_id = @sinistro_id
		AND a.evento_bb_id = 2003
		AND c.linha = 'AVISO HOMOLOGADO PELA SEGURADORA'
END
GO

BEGIN TRAN
  DECLARE @dt AS SMALLDATETIME
  DECLARE @usuario AS VARCHAR(20)

  SELECT @dt = DATEADD(hh, - 1, GETDATE())
  SET @usuario = 'ntendencia_bsp'

  IF @@trancount > 0 	EXEC desenv_db.dbo.atualiza_sinistro_bb_saida_gtr_atual_tb_BSP @sinistro_id = 93201914492		,@apolice_id = 5143803		,@ramo_id = 93

  IF @@trancount > 0 	EXEC desenv_db.DBO.INSERE_EVENTO_2003_BSP @sinistro_id = 93201914491

  IF @@trancount > 0 	EXEC interface_db.dbo.SMQS00170_SPI @usuario = @usuario

  SELECT solicitante_id
	  ,nome
	  ,endereco
	  ,usuario
	  ,solicitante_cpf
  FROM seguros_db.dbo.solicitante_sinistro_tb WITH (NOLOCK)
  WHERE dt_inclusao > @dt
	  AND usuario = @usuario
  GO
--ROLLBACK
--COMMIT

