﻿Imports Alianca
Imports System.IO
Imports System.Data.SqlClient
Imports IBM.WMQ
Imports System.Configuration
Imports Alianca.Seguranca.BancoDados
Imports SMQL0001

Public Class Mensagem

#Region "Atributos"
    Dim _CodTransacao As String
    Dim _Evento_BB As Integer
    Dim _Qtd_Registros As Integer
    Dim _Registro As String
    Dim _Grupo_evento_als_id As Integer
    Dim _Correlation_Id As String
    Shared _hostName As String
    Shared _channel As String
#End Region

#Region "Propriedades"
    Shared Property HostName() As String
        Get
            Return _hostName
        End Get
        Set(ByVal value As String)
            _hostName = value
        End Set
    End Property

    Shared Property Channel() As String
        Get
            Return _channel
        End Get
        Set(ByVal value As String)
            _channel = value
        End Set
    End Property

    Property CorrelationID()
        Get
            Return _Correlation_Id
        End Get
        Set(ByVal value)
            _Correlation_Id = value
        End Set
    End Property

    Property Grupo_evento_als_id()
        Get
            Return _Grupo_evento_als_id
        End Get
        Set(ByVal value)
            _Grupo_evento_als_id = value
        End Set
    End Property
    Property CodTransacao()
        Get
            Return _CodTransacao
        End Get
        Set(ByVal value)
            _CodTransacao = value
        End Set
    End Property

    Property EventoBB()
        Get
            Return _Evento_BB
        End Get
        Set(ByVal value)
            _Evento_BB = value
        End Set
    End Property

    Property QtdRegistros()
        Get
            Return _Qtd_Registros
        End Get
        Set(ByVal value)
            _Qtd_Registros = value
        End Set
    End Property

    Property Registro()
        Get
            Return _Registro
        End Get
        Set(ByVal value)
            _Registro = value
        End Set
    End Property
#End Region

#Region "Métodos"

#Region "Recuperar mensagem da fila do MQ"
    Public Function GetMessageFromMQ(ByVal qMgr As MQQueueManager, ByVal queue As MQQueue) As MQMessage
        Dim mqObject As New MQ()
        Dim mqMessage As New MQMessage
        Try
            mqMessage = mqObject.GetMessage(qMgr, queue)
        Catch ex As Exception
            TraceFile.Save("Classe: Mensagem")
            TraceFile.Save("Método: GetMessageFromMQ")
            TraceFile.Erro("Falha ao recuperar mensagem da fila do MQ.")
            ErrorFile.SaveException(ex)
            Return Nothing
        End Try
        Return mqMessage
    End Function
#End Region

#Region "Colocar mensagem na fila do MQ"
    Public Sub PutMessageInMQ(ByVal qMgr As MQQueueManager, ByVal queue As MQQueue, ByVal message As MQMessage)
        Try
            Dim myobject As New MQ()
            myobject.PutMessage(queue, qMgr, message)
        Catch ex As Exception
            TraceFile.Save("Classe: Mensagem")
            TraceFile.Save("Método: PutMessageInMQ")
            TraceFile.Erro("Falha ao colocar a mensagem na fila do MQ")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#Region "Ler Arquivo"
    'Método somente para teste para não utilizar a mensagem recuperada do MQ Server, 
    'recuperando a mensagem de um arquivo(.text) de backup do MQ
    Public Shared Function LerArquivo(ByVal caminhoArquivo As String) As String()
        Dim remessa As String = File.ReadAllText(caminhoArquivo, System.Text.Encoding.UTF7)
        'Troca todos os ENTER do texto por ""
        remessa = remessa.Replace(Chr(0), "")
        'Troca todos os acentos por crase
        remessa = remessa.Replace("´", "`")
        'A cada quebra de linha é uma nova mensagem
        Dim linhas As String() = remessa.Split(New String() {Environment.NewLine}, StringSplitOptions.None)

        'Remove as msgs em branco do array
        Dim list As New List(Of String)
        list.AddRange(linhas)
        list.Remove("")
        linhas = list.ToArray()

        Return linhas
    End Function
#End Region

#Region "Interpretar"
    Public Function Interpretar(ByVal structCodTransacao As CodTransacao, ByVal structEventoBB As EventoBB, ByVal structQtdRegistros As QtdRegistros, ByVal msg As String, ByVal correlationId As Byte(), ByVal grupo_evento_als_id As Integer) As Boolean

        Dim c As New Text.StringBuilder

        Try

            'Pegando as strings de acordo com sua posicao na mensagem e setando o valor no 
            'objeto mensagem que será persistido no Banco de Dados
            Me._CodTransacao = msg.Substring(structCodTransacao.posicao_inicial, structCodTransacao.qtd_caracteres)
            Me._Evento_BB = msg.Substring(structEventoBB.posicao_inicial, structEventoBB.qtd_caracteres)
            Me._Qtd_Registros = Convert.ToInt32(msg.Substring(structQtdRegistros.posicao_inicial, structQtdRegistros.qtd_caracteres))
            Me._Registro = msg
            Me._Grupo_evento_als_id = Convert.ToInt32(grupo_evento_als_id)
            For Each b As Byte In correlationId
                c.Append(Right("00" + Hex(b), 2))
            Next
            Me._Correlation_Id = c.ToString()

            Return True

        Catch ex As Exception
            TraceFile.Save("Classe: Mensagem")
            TraceFile.Save("Método: Interpretar")
            TraceFile.Erro("mensagem com erro: " + msg)
            ErrorFile.SaveException(ex)
            Return False

        End Try
    End Function
#End Region

#Region "Salvar"
    Public Function Salvar() As Boolean
        Try

            Dim params As SqlParameter() = New SqlParameter() { _
                                                                New SqlParameter("@grupo_evento_als_id", Me._Grupo_evento_als_id), _
                                                                New SqlParameter("@mensagem", Me._Registro), _
                                                                New SqlParameter("@cod_transacao", Me._CodTransacao), _
                                                                New SqlParameter("@evento_BB", Me._Evento_BB), _
                                                                New SqlParameter("@qtd_registros", Me._Qtd_Registros), _
                                                                New SqlParameter("@usuario", "SMQP"), _
                                                                New SqlParameter("@msg_id", Me._Correlation_Id) _
                                                                }
            cCon.InternalExecuteNonQuery(CommandType.StoredProcedure, "interface_dados_db..importacao_MQ_spi", params)

            Return True

        Catch ex As Exception
            TraceFile.Save("Classe: Mensagem")
            TraceFile.Save("Método: Salvar")
            TraceFile.Erro("Falha ao salvar mensagem.")
            ErrorFile.SaveException(ex)

            Return False
        End Try
    End Function
#End Region

#Region "Salvar Log"
    Public Sub SalvarLog(ByVal linha As String, ByVal caminho As String, ByVal secao As String)
        Try
            'Cria o nome do arquivo
            Dim data As String = Date.Today.Year.ToString() + Date.Today.Month.ToString() + Date.Today.Day.ToString()
            Dim nomeArquivo As String = caminho + ConfigurationManager.AppSettings.Get("ServiceName").ToString() + "." + secao + "." + data + ".Text"
            Dim writer As StreamWriter

            'Verifica se o arquivo existe, se não existir é criado
            If Not File.Exists(nomeArquivo) Then
                File.Create(nomeArquivo).Close()
                writer = New StreamWriter(nomeArquivo)
            Else
                writer = File.AppendText(nomeArquivo)
            End If

            'Grava as linhas no arquivo
            writer.WriteLine(linha)

            'Fecha as streams
            writer.Flush()
            writer.Close()
        Catch ex As Exception
            TraceFile.Save("Classe: Mensagem")
            TraceFile.Save("Método: SalvarLog")
            TraceFile.Erro("Falha ao salvar o arquivo de log.")
            ErrorFile.SaveException(ex)
        End Try
    End Sub
#End Region

#End Region

End Class
