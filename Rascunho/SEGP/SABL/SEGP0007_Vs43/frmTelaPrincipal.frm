VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrincipal 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGBR"
   ClientHeight    =   8325
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   11940
   Icon            =   "frmTelaPrincipal.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MouseIcon       =   "frmTelaPrincipal.frx":030A
   ScaleHeight     =   8325
   ScaleWidth      =   11940
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Usuario 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1920
      ScaleHeight     =   255
      ScaleWidth      =   2175
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
   Begin VB.PictureBox Padrao 
      AutoRedraw      =   -1  'True
      Height          =   345
      Left            =   90
      Picture         =   "frmTelaPrincipal.frx":045C
      ScaleHeight     =   285
      ScaleWidth      =   405
      TabIndex        =   24
      Top             =   1830
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Frame fraIcones 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   1850
      Left            =   240
      TabIndex        =   7
      Top             =   6420
      Width           =   11000
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "A&juda do SEGBR"
         Height          =   195
         Index           =   19
         Left            =   3225
         TabIndex        =   23
         Top             =   600
         Width           =   1245
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   12
         Left            =   8370
         Picture         =   "frmTelaPrincipal.frx":1996
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Administra��o"
         Height          =   195
         Index           =   0
         Left            =   1860
         TabIndex        =   22
         Top             =   600
         Width           =   990
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   0
         Left            =   2220
         Picture         =   "frmTelaPrincipal.frx":1CA0
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Central de Atendimento"
         Height          =   195
         Index           =   1
         Left            =   4920
         TabIndex        =   21
         Top             =   600
         Width           =   1650
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   1
         Left            =   5550
         Picture         =   "frmTelaPrincipal.frx":1FAA
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Co&mercial"
         Height          =   195
         Index           =   2
         Left            =   8235
         TabIndex        =   20
         Top             =   600
         Width           =   690
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   2
         Left            =   8340
         Picture         =   "frmTelaPrincipal.frx":22B4
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Co&ntabilidade"
         Height          =   195
         Index           =   3
         Left            =   6900
         TabIndex        =   19
         Top             =   600
         Width           =   960
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   3
         Left            =   7200
         Picture         =   "frmTelaPrincipal.frx":25BE
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cosse&guro"
         Height          =   195
         Index           =   4
         Left            =   9390
         TabIndex        =   18
         Top             =   630
         Width           =   750
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   4
         Left            =   9480
         Picture         =   "frmTelaPrincipal.frx":28C8
         Top             =   30
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "&Dados B�sicos"
         Height          =   435
         Index           =   5
         Left            =   90
         TabIndex        =   17
         Top             =   1440
         Width           =   615
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   5
         Left            =   165
         Picture         =   "frmTelaPrincipal.frx":2BD2
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Emiss�o"
         Height          =   195
         Index           =   6
         Left            =   1200
         TabIndex        =   16
         Top             =   1560
         Width           =   615
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   6
         Left            =   1230
         Picture         =   "frmTelaPrincipal.frx":2EDC
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Financeiro"
         Height          =   195
         Index           =   7
         Left            =   2160
         TabIndex        =   15
         Top             =   1560
         Width           =   765
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   7
         Left            =   2385
         Picture         =   "frmTelaPrincipal.frx":31E6
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Pr&odu��o"
         Height          =   195
         Index           =   8
         Left            =   3360
         TabIndex        =   14
         Top             =   1560
         Width           =   690
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Produto"
         Height          =   195
         Index           =   9
         Left            =   4680
         TabIndex        =   13
         Top             =   1560
         Width           =   585
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   9
         Left            =   4695
         Picture         =   "frmTelaPrincipal.frx":34F0
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Re&lat�rio"
         Height          =   195
         Index           =   10
         Left            =   5760
         TabIndex        =   12
         Top             =   1560
         Width           =   630
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   10
         Left            =   5850
         Picture         =   "frmTelaPrincipal.frx":37FA
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Resseguro"
         Height          =   195
         Index           =   11
         Left            =   6840
         TabIndex        =   11
         Top             =   1560
         Width           =   795
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   11
         Left            =   7005
         Picture         =   "frmTelaPrincipal.frx":3B04
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&Sinistro"
         Height          =   195
         Index           =   13
         Left            =   9450
         TabIndex        =   10
         Top             =   1560
         Width           =   510
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   13
         Left            =   9480
         Picture         =   "frmTelaPrincipal.frx":3E0E
         Top             =   960
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "&T�cnica"
         Height          =   195
         Index           =   14
         Left            =   10320
         TabIndex        =   9
         Top             =   1560
         Width           =   615
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   14
         Left            =   10395
         Picture         =   "frmTelaPrincipal.frx":4118
         Top             =   960
         Width           =   480
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   8
         Left            =   3540
         Picture         =   "frmTelaPrincipal.frx":4422
         Top             =   960
         Width           =   480
      End
      Begin VB.Image Imagem 
         Height          =   480
         Index           =   19
         Left            =   3630
         Picture         =   "frmTelaPrincipal.frx":472C
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lblMenus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "A&utProd"
         Height          =   195
         Index           =   12
         Left            =   8265
         TabIndex        =   8
         Top             =   1560
         Width           =   570
      End
   End
   Begin MSComctlLib.ImageList ImageListLigado 
      Left            =   600
      Top             =   810
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   16
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":4A36
            Key             =   "icone3"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":4D52
            Key             =   "icone5"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":562E
            Key             =   "icone6"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":594A
            Key             =   "icone19"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":5C66
            Key             =   "icone2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":5F82
            Key             =   "icone7"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":629E
            Key             =   "icone1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":65BA
            Key             =   "icone11"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":68D6
            Key             =   "icone8"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":6BF2
            Key             =   "icone13"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":6F0E
            Key             =   "icone10"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":722A
            Key             =   "icone9"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":7546
            Key             =   "icone14"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":7862
            Key             =   "icone4"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":7B7E
            Key             =   "icone0"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":7E9A
            Key             =   "icone12"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageListDesligado 
      Left            =   30
      Top             =   810
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   16
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":81B6
            Key             =   "icone3"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":84D2
            Key             =   "icone6"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":87EE
            Key             =   "icone19"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":8B0A
            Key             =   "icone2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":8E26
            Key             =   "icone1"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":9142
            Key             =   "icone11"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":945E
            Key             =   "icone8"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":977A
            Key             =   "icone13"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":9A96
            Key             =   "icone10"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":9DB2
            Key             =   "icone9"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":A0CE
            Key             =   "icone14"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":A3EA
            Key             =   "icone4"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":A706
            Key             =   "icone0"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":AA22
            Key             =   "icone7"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":AD3E
            Key             =   "icone5"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTelaPrincipal.frx":B05A
            Key             =   "icone12"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Data 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   2040
      ScaleHeight     =   255
      ScaleWidth      =   2175
      TabIndex        =   2
      Top             =   1080
      Width           =   2175
   End
   Begin VB.PictureBox OpcaoExecucao 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   3510
      ScaleHeight     =   495
      ScaleWidth      =   3495
      TabIndex        =   6
      Top             =   780
      Width           =   3495
   End
   Begin VB.PictureBox Status 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   4200
      ScaleHeight     =   495
      ScaleWidth      =   3495
      TabIndex        =   4
      Top             =   720
      Width           =   3495
   End
   Begin VB.PictureBox LabelData 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   0
      ScaleHeight     =   255
      ScaleWidth      =   1815
      TabIndex        =   3
      Top             =   1080
      Width           =   1815
   End
   Begin VB.PictureBox LabelUsuario 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1290
      ScaleHeight     =   255
      ScaleWidth      =   1815
      TabIndex        =   1
      Top             =   150
      Width           =   1815
   End
   Begin VB.Timer tmrPrincipal 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   2040
      Top             =   480
   End
   Begin VB.PictureBox pcbSegbr 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   1440
      Picture         =   "frmTelaPrincipal.frx":B376
      ScaleHeight     =   495
      ScaleWidth      =   495
      TabIndex        =   5
      Top             =   480
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Image Imagem 
      Height          =   3345
      Index           =   20
      Left            =   840
      Picture         =   "frmTelaPrincipal.frx":B680
      Stretch         =   -1  'True
      Top             =   510
      Width           =   5100
   End
   Begin VB.Menu mnuSEGBR 
      Caption         =   "SEGBR"
      Visible         =   0   'False
      Begin VB.Menu itmGenerico 
         Caption         =   "Central de Atendimento"
         Index           =   0
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Comercial"
         Index           =   1
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Contabilidade"
         Index           =   2
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Cosseguro"
         Index           =   3
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Dados B�sicos"
         Index           =   4
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Emiss�o"
         Index           =   5
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Financeiro"
         Index           =   6
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Produto"
         Index           =   7
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Produ��o"
         Index           =   8
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Relat�rio"
         Index           =   9
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Resseguro"
         Index           =   10
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "AutProd"
         Index           =   11
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Sinistro"
         Index           =   12
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "T�cnica"
         Index           =   13
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Usu�rios em Sess�o"
         Index           =   14
      End
      Begin VB.Menu itmGenerico 
         Caption         =   "Ajuda do SEGBR"
         Index           =   19
      End
      Begin VB.Menu MenuSair 
         Caption         =   "Sair"
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public GAmbienteGlobal As Object
Dim iIndiceIcone As Integer

Sub AssociaControles()
  Imagem(0).ToolTipText = "M�dulo: Administra��o"
  Imagem(1).ToolTipText = "M�dulo: Central de Atendimento"
  Imagem(2).ToolTipText = "M�dulo: Comercial"
  Imagem(3).ToolTipText = "M�dulo: Contabilidade"
  Imagem(4).ToolTipText = "M�dulo: Cosseguro"
  Imagem(5).ToolTipText = "M�dulo: Dados B�sicos"
  Imagem(6).ToolTipText = "M�dulo: Emiss�o"
  Imagem(7).ToolTipText = "M�dulo: Financeiro"
  Imagem(8).ToolTipText = "M�dulo: Produ��o"
  Imagem(9).ToolTipText = "M�dulo: Produto"
  Imagem(10).ToolTipText = "M�dulo: Relat�rio"
  Imagem(11).ToolTipText = "M�dulo: Resseguro "
  Imagem(12).ToolTipText = "M�dulo: AutProd"
  Imagem(13).ToolTipText = "M�dulo: Sinistro"
  Imagem(14).ToolTipText = "M�dulo: T�cnica"
  Imagem(19).ToolTipText = "Ajuda do SEGBR"
  Dim i As Byte
  For i = 0 To 14
    lblMenus(i).ToolTipText = Imagem(i).ToolTipText
    itmGenerico(i).Caption = lblMenus(i).Caption
  Next i
  lblMenus(19).ToolTipText = Imagem(19).ToolTipText
  itmGenerico(19).Caption = lblMenus(19).Caption
End Sub
Sub MudaMouse()
  MousePointer = vbCustom ' � o mouseicon do formul�rio
End Sub

Sub VoltaMouse()
  MousePointer = vbDefault ' � o mouseicon do formul�rio
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

  If Shift = vbAltMask Then
  
    Select Case KeyCode
    
      Case vbKeyA
        Call Imagem_Click(0)
      Case vbKeyC
        Call Imagem_Click(1)
      Case vbKeyN
        Call Imagem_Click(3)
      Case vbKeyM
        Call Imagem_Click(2)
      Case vbKeyG
        Call Imagem_Click(4)
      Case vbKeyD
        Call Imagem_Click(5)
      Case vbKeyE
        Call Imagem_Click(6)
      Case vbKeyF
        Call Imagem_Click(7)
      Case vbKeyO
        Call Imagem_Click(8)
      Case vbKeyP
        Call Imagem_Click(9)
      Case vbKeyL
        Call Imagem_Click(10)
      Case vbKeyR
        Call Imagem_Click(11)
      Case vbKeyU
        Call Imagem_Click(12)
      Case vbKeyS
        Call Imagem_Click(13)
      Case vbKeyT
        Call Imagem_Click(14)
  
  End Select
  
    

'    Debug.Print KeyCode & " - " & Shift
'    If KeyCode = vbKeyA Then
'      Beep
'    End If
  End If
End Sub

Private Sub Form_Load()
  
  On Local Error GoTo TrataErro
  tmrPrincipal.Enabled = False
  bUsuarioSolicitaTermino = False
  Set GAmbienteGlobal = CreateObject("Ambiente.ClasseAmbiente")
  
  If App.PrevInstance Then AtivaInstanciaCriada
  
  Call Conexao(, True)
'  Call Seguranca("FRMCONPROPBASICA", "Menu Principal", True)
  Call CriaTabelaTemporaria(GAmbienteGlobal)
  sDataHoraMensagem = DataHoraBanco(GAmbienteGlobal)
  Call VerificaMensagem("dt_validade", GAmbienteGlobal)
  sDataHoraMensagem = DataHoraBanco(GAmbienteGlobal)
  tmrPrincipal.Interval = glINTERVALOATUALIZACAO
  tmrPrincipal.Enabled = True
  Call AssociaMenus
  Call AssociaControles
  Call CarregaBandeja(traySegbr, pcbSegbr, "SEGBR (" & cUserName & ")")
  Call RegisterDataSource(GAmbienteGlobal)

  'Coloca o efeito 3D nos labels e Textos na Tela
  Call Coloca_Efeito_3D(LabelUsuario, "Usu�rio:", 0, 0, &H8000000F, 11, Padrao.Point(100, 100))
  Call Coloca_Efeito_3D(LabelData, "Data SEGBR:", 0, 0, &H8000000F, 11, Padrao.Point(100, 100))
  Call Coloca_Efeito_3D(Usuario, cUserName, 0, 0, &H8000000F, 11, Padrao.Point(100, 100))
  Call Coloca_Efeito_3D(Data, Format(Data_Sistema, "dd/mm/yyyy"), 0, 0, &H8000000F, 11, Padrao.Point(100, 100))
  Call Coloca_Efeito_3D(OpcaoExecucao, "Op��es em execu��o: 0", 0, 0, &H8000000F, 11, Padrao.Point(100, 100))
  fraIcones.BackColor = Padrao.Point(100, 100)
  
  iIndiceIcone = 20
  
  'Verifica se o Sistema est� bloqueado
  If UCase(Status_Sistema) <> UCase("Dispon�vel") Then
    'Se o sistema est� bloqueado imprime na Tela
    Call Coloca_Efeito_3D(Status, "*** Sistema Bloqueado ***", 0, 0, &HC0&, 14, Padrao.Point(100, 100))
    Status.Visible = True
  Else
    Status.Visible = False
  End If
  
  'Exibe Ambiente
  Me.Caption = gsSIGLASISTEMA & " - " & Ambiente
  
  Exit Sub
TrataErro:
  Call TrataErroGeral("Load", Me.name)
  Call TerminaSEGBR(frmPrincipal)
End Sub
Private Sub Form_Resize()
  If WindowState = vbMinimized Then
    Visible = False
  Else
    Me.Visible = False
    Call Imagem(20).Move(0, 0)
    Imagem(20).Width = Me.Width
    Imagem(20).Height = Me.Height - fraIcones.Height / 2
    
    Call LabelUsuario.Move(100, 200)
    Call LabelData.Move(Me.Width - 2500, 200)
    Call Usuario.Move(1000, 200)
    Call Data.Move(Me.Width - 1200, 200)
    Call Status.Move((Me.Width / 2 - (Status.Width / 2.2)), (Me.Height \ 8))
    Call OpcaoExecucao.Move((Me.Width / 2 - (OpcaoExecucao.Width / 2.2)), 200)
    
    Call PosicionaMoldura(Me, fraIcones)
    Me.Visible = True
    Restringe
  End If
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  tmrPrincipal_Timer
  If Meio(OpcaoExecucao.Tag, ":") <> ": 0" And _
     ((UnloadMode = vbFormControlMenu) Or bUsuarioSolicitaTermino) Then
    ' jogar foco no principal
    frmPrincipal.WindowState = vbMaximized
    frmPrincipal.Visible = True
    frmPrincipal.SetFocus
    Dim Pergunta As String, Resposta
    Pergunta = "Deseja fechar todas as aplica��es (" & _
               Trim(Meio(Meio(OpcaoExecucao.Tag, ":"), " ")) & ") do " & _
               gsSIGLASISTEMA & "?"
    Call Pause(1)
Volta:
    Resposta = MsgBox(Pergunta, vbYesNo, gsSIGLASISTEMA)
    frmPrincipal.SetFocus
    If Not EstaContido(Resposta, vbYes, vbNo) Then GoTo Volta
    Cancel = Resposta = vbNo
  End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
  On Local Error GoTo TrataErro
  Call DescarregaBandeja(traySegbr)
  Call VerificaProgramasAbertos(True, GAmbienteGlobal)
  Set GAmbienteGlobal = Nothing
  Call TerminaSEGBR
  Exit Sub
TrataErro:
  Resume Next
End Sub
Private Sub Fundo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MousePointer = vbDefault
End Sub

Private Sub Imagem_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Index < 20 Then
    MudaMouse
    If iIndiceIcone <> Index And iIndiceIcone < 20 Then
      Imagem(iIndiceIcone).Picture = ImageListDesligado.ListImages.Item("icone" & CStr(iIndiceIcone)).Picture
    End If
    Imagem(Index).Picture = ImageListLigado.ListImages("icone" & CStr(Index)).Picture
    iIndiceIcone = Index
  Else
    If iIndiceIcone < 20 Then
      Imagem(iIndiceIcone).Picture = ImageListDesligado.ListImages.Item("icone" & CStr(iIndiceIcone)).Picture
      VoltaMouse
    End If
  End If
End Sub
Private Sub lblMenus_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  MudaMouse
End Sub
Private Sub Imagem_Click(Index As Integer)
  If Index < 20 Then lblMenus_Click Index
End Sub
Private Sub lblMenus_Click(Index As Integer)
  tmrPrincipal_Timer
  If Index = 19 Then ' ajuda do SEGBR
    Call ExibeAjudaSEGBR
  ElseIf ExecutaPrograma(sMenus(Index), sMenus(Index), "", vbMaximizedFocus) Then
    Call Coloca_Efeito_3D(OpcaoExecucao, "Op��es em execu��o: " & FechaProgramaSolicitado, 0, 0, &H8000000F, 11, Padrao.Point(100, 100))
    frmPrincipal.Visible = False
    WindowState = vbMinimized
  End If
End Sub
Private Sub itmGenerico_Click(Index As Integer)
  lblMenus_Click Index
End Sub

Private Sub MenuSair_Click()
  bUsuarioSolicitaTermino = True
  Unload Me
  bUsuarioSolicitaTermino = False
End Sub
Private Sub pcbSegbr_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  ' clique do bot�o esquerdo
  If Hex(X) = "1E1E" Then
  End If
  ' duplo clique do bot�o esquerdo
  If Hex(X) = "1E2D" Then
    frmPrincipal.WindowState = vbMaximized
    frmPrincipal.Visible = True
    frmPrincipal.SetFocus
  End If
  ' clique do bot�o direito
  If Hex(X) = "1E4B" Then
    PopupMenu mnuSEGBR
  End If
  ' duplo clique do bot�o direito
  If Hex(X) = "1E5A" Then
    Call MenuSair_Click
  End If
End Sub
Private Sub tmrPrincipal_Timer()
  Call VerificaMensagem
  Call VerificaProgramasAbertos(False, GAmbienteGlobal)
  Call Coloca_Efeito_3D(OpcaoExecucao, "Op��es em execu��o: " & FechaProgramaSolicitado, 0, 0, &H8000000F, 11, Padrao.Point(100, 100))
End Sub
