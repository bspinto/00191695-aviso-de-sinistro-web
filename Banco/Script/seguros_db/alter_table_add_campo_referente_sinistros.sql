/*
	Schoralick (ntendencia) - 07/11/2019
	Demanda: 00191695-aviso-de-sinistro-web
	Nome do script: alter_table_add_campo_referente_sinistros
	Descri��o: incluir campo solicitante_cpf na tabela evento_SEGBR_sinistro_atual_tb e solicitante_sinistro_tb
	Objetivo: estrutura
	Banco: seguros_db
	Ambiente: AB e ABS
*/

ALTER TABLE seguros_db.dbo.evento_SEGBR_sinistro_atual_tb
ADD solicitante_cpf VARCHAR(11) NULL
GO

ALTER TABLE seguros_db.dbo.solicitante_sinistro_tb
ADD solicitante_cpf VARCHAR(11) NULL
GO
