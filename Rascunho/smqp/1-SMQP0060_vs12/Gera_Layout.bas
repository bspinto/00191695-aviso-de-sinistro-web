Attribute VB_Name = "modGera_Layout"
Option Explicit

Private Const MOD_OK_APLICACAO = 0
'andre luvison
Public Function Gera_Registros_ST10(ByVal Evento_id As String, _
                                    ByVal Layout_Id As String, _
                                    ByRef registros() As String, _
                                    ByVal empresa As String) As Long


    Dim rs As rdoResultset, SQL As String, i As Integer
    Dim Query As String, Query_Select As String, Query_From As String
    Dim Formato() As String, Tamanho() As Integer, Tipo() As String
'    Dim Sep_Milhar() As String, Sep_Decimal() As String
    Dim Detalhes() As String, Qtd_Registros As Integer

    On Error GoTo Erro

    SQL = "SELECT registro_id "
    SQL = SQL & " FROM interface_db.dbo.layout_registro_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE layout_id = " & Layout_Id
    SQL = SQL & " ORDER BY registro_id"
    i = 0
    Set rs = rdocn.OpenResultset(SQL)
    While Not rs.EOF
        i = i + 1
        ReDim Preserve Detalhes(i) As String
        Detalhes(i) = rs(0)
        rs.MoveNext
    Wend
    rs.Close

    Qtd_Registros = 0
    For i = 1 To UBound(Detalhes)
        ' Limpa os arrays a cada passo do loop.
        ReDim Tamanho(0) As Integer
        ReDim Formato(0) As String
        ReDim Tipo(0) As String
''''''''        ReDim Sep_Decimal(0) As String
''''''''        ReDim Sep_Milhar(0) As String

        'ANDRE LUVISON
            SQL = "SELECT seq_campo, coluna_origem, tabela_origem, constante, tamanho, "
            SQL = SQL & "tp_dado, formato "
            SQL = SQL & "From INTERFACE_DB.DBO.layout_campo_tb "
            SQL = SQL & " WHERE layout_id = " & Layout_Id
            SQL = SQL & " AND registro_id = " & Detalhes(i)
            SQL = SQL & " ORDER BY seq_campo "
        
        ' A tabela evento_SEGBR_sinistro_tb sempre faz parte da query
            Query_From = " FROM evento_SEGBR_sinistro_tb WITH (NOLOCK) "
        
        Query_Select = ""
        
        Set rs = rdocn.OpenResultset(SQL)
        While Not rs.EOF
            ' Se for primeira do loop, "SELECT", sen�o coloca v�rgula
            ' pois � outro campo
            If Query_Select = "" Then
                Query_Select = "SELECT "
            Else
                Query_Select = Query_Select & ", "
            End If

            If UCase(rs("tp_dado")) = "CT" Then
                ' Se constante, basta copi�-lo na Query_Select.
                Query_Select = Query_Select & "'" & Trim(rs("constante")) & "'"
                
            Else
                ' Se campo de tabela, busca <tabela>.<campo>
                Query_Select = Query_Select & rs("tabela_origem") & _
                        "." & rs("coluna_origem")
            End If

            ' Preenche as tabelas que far�o parte do FROM
            If InStr(LCase(Query_From), "" & LCase(rs("tabela_origem"))) = 0 Then
                ' Todas com left join, pois pode ser que n�o exista.
                Query_From = Query_From & " Left JOIN " & rs("tabela_origem") & " ON "
                Query_From = Query_From & " evento_SEGBR_sinistro_tb.evento_id = " & rs("tabela_origem") & ".evento_id "

            End If

            ReDim Preserve Tamanho(Val(rs("seq_campo"))) As Integer
            ReDim Preserve Formato(Val(rs("seq_campo"))) As String
            ReDim Preserve Tipo(Val(rs("seq_campo"))) As String
            'ReDim Preserve Sep_Decimal(Val(rs("seq_campo"))) As String
            'ReDim Preserve Sep_Milhar(Val(rs("seq_campo"))) As String

            Tamanho(Val(rs("seq_campo"))) = Val("0" & rs("tamanho"))
            Formato(Val(rs("seq_campo"))) = "" & rs("formato")
            Tipo(Val(rs("seq_campo"))) = "" & rs("tp_dado")
            'Sep_Decimal(Val(rs("seq_campo"))) = "" & Trim(rs("separador_decimal"))
            'Sep_Milhar(Val(rs("seq_campo"))) = "" & Trim(rs("separador_milhar"))
        rs.MoveNext
        Wend
        rs.Close

        Query = Query_Select
        Query = Query & Query_From
        
        Query = Query & " WHERE evento_SEGBR_sinistro_tb.evento_id = " & Evento_id
        
        If bUsarConexaoABS Then
            If empresa = glAmbiente_id_seg2 Then
                Set rs = rdocn_Seg2.OpenResultset(Query)
            Else
                Set rs = rdocn_Seg.OpenResultset(Query)
            End If
        Else
            Set rs = rdocn_Seg.OpenResultset(Query)
        End If
        
        While Not rs.EOF
            'if
            Qtd_Registros = Qtd_Registros + 1
            ReDim Preserve registros(Qtd_Registros) As String
            registros(Qtd_Registros) = Formata_Registro_ST10(rs, Tamanho, _
                        Formato, Tipo)
            rs.MoveNext
        Wend
        rs.Close

    Next i

    Exit Function

Erro:
    Gera_Registros_ST10 = Err.Number
    
    If Evento_id >= 6384748 Then
        Debug.Print Err.Description
        Debug.Print Query
    
    End If
    
   
'*******************************************************************************************
End Function

Public Function Gera_Registros(ByVal Evento_id As String, _
                               ByVal Layout_Id As String, _
                               ByRef registros() As String, _
                               ByVal empresa As String, _
                               Optional ByVal Evento_BB As String _
                               ) As Long

    Dim rs As rdoResultset, SQL As String, i As Integer
    Dim Query As String, Query_Select As String, Query_From As String
    Dim Formato() As String, Tamanho() As Integer, Tipo() As String
    Dim Sep_Milhar() As String, Sep_Decimal() As String
    Dim Detalhes() As String, Qtd_Registros As Integer

    On Error GoTo Erro



    SQL = "SELECT detalhe_id "
    SQL = SQL & " FROM " & GTR_banco & "..GTR_layout_detalhe_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE layout_id = " & Layout_Id
    'Inclu�do por Gustavo Machado em 25/03/2004
    SQL = SQL & " ORDER BY DETALHE_ID"

    i = 0
    If bUsarConexaoABS Then
        If empresa = glAmbiente_id_seg2 Then
            Set rdocn = rdocn_Seg2
        Else
            Set rdocn = rdocn_Seg
        End If
    Else
        Set rdocn = rdocn_Seg
    End If
        
    
    Set rs = rdocn.OpenResultset(SQL)
    While Not rs.EOF
        i = i + 1
        ReDim Preserve Detalhes(i) As String
        Detalhes(i) = rs(0)
        rs.MoveNext
    Wend
    rs.Close

    Qtd_Registros = 0
    For i = 1 To UBound(Detalhes)
        ' Limpa os arrays a cada passo do loop.
        ReDim Tamanho(0) As Integer
        ReDim Formato(0) As String
        ReDim Tipo(0) As String
        ReDim Sep_Decimal(0) As String
        ReDim Sep_Milhar(0) As String

        SQL = "SELECT seq_campo, nome_SEGBR, tabela_SEGBR, "
        SQL = SQL & " constante, tamanho, tipo, formato, "
        SQL = SQL & " separador_decimal, separador_milhar "
        SQL = SQL & " FROM " & GTR_banco & "..GTR_layout_campo_tb WITH (NOLOCK) "
        SQL = SQL & " WHERE layout_id = " & Layout_Id
        SQL = SQL & "   AND detalhe_id = " & Detalhes(i)
        SQL = SQL & " ORDER BY seq_campo "

        ' A tabela evento_SEGBR_sinistro_tb sempre faz parte da query
        Query_From = " FROM evento_SEGBR_sinistro_tb WITH (NOLOCK) "

        Query_Select = ""

        Set rs = rdocn.OpenResultset(SQL)
        While Not rs.EOF
            ' Se for primeira do loop, "SELECT", sen�o coloca v�rgula
            ' pois � outro campo
            If Query_Select = "" Then
                Query_Select = "SELECT "
            Else
                Query_Select = Query_Select & ", "
            End If

            If UCase(rs("tipo")) = "CT" Then
                ' Se constante, basta copi�-lo na Query_Select.
                Query_Select = Query_Select & "'" & rs("constante") & "'"
            Else
                ' Se campo de tabela, busca <tabela>.<campo>
                Query_Select = Query_Select & rs("tabela_SEGBR") & _
                        "." & rs("nome_SEGBR")
            End If

            ' Preenche as tabelas que far�o parte do FROM
            If InStr(LCase(Query_From), "" & LCase(rs("tabela_SEGBR"))) = 0 Then
                ' Todas com left join, pois pode ser que n�o exista.
                Query_From = Query_From & " LEFT JOIN " & rs("tabela_SEGBR") & " ON "
                Query_From = Query_From & " evento_SEGBR_sinistro_tb.evento_id = " & rs("tabela_SEGBR") & ".evento_id "
            End If

            ReDim Preserve Tamanho(Val(rs("seq_campo"))) As Integer
            ReDim Preserve Formato(Val(rs("seq_campo"))) As String
            ReDim Preserve Tipo(Val(rs("seq_campo"))) As String
            ReDim Preserve Sep_Decimal(Val(rs("seq_campo"))) As String
            ReDim Preserve Sep_Milhar(Val(rs("seq_campo"))) As String

            Tamanho(Val(rs("seq_campo"))) = Val("0" & rs("tamanho"))
            Formato(Val(rs("seq_campo"))) = "" & rs("formato")
            Tipo(Val(rs("seq_campo"))) = "" & rs("tipo")
            Sep_Decimal(Val(rs("seq_campo"))) = "" & Trim(rs("separador_decimal"))
            Sep_Milhar(Val(rs("seq_campo"))) = "" & Trim(rs("separador_milhar"))

            rs.MoveNext
        Wend
        rs.Close

        Query = Query_Select
        Query = Query & Query_From
        
        Query = Query & " WHERE evento_SEGBR_sinistro_tb.evento_id = " & Evento_id
        

        Set rs = rdocn.OpenResultset(Query)
        While Not rs.EOF
            Qtd_Registros = Qtd_Registros + 1
            ReDim Preserve registros(Qtd_Registros) As String
            registros(Qtd_Registros) = Formata_Registro(rs, Tamanho, _
                        Formato, Tipo, Sep_Decimal, Sep_Milhar, Evento_BB)
            rs.MoveNext
        Wend
        rs.Close

    Next i

    If bUsarConexaoABS Then
        Set rdocn = rdocn_Seg
    End If
    
    Exit Function

Erro:
    Gera_Registros = Err.Number
    
    If Evento_id >= 6384748 Then
        Debug.Print Err.Description
        Debug.Print Query
    
    End If

End Function

Public Function Formata_Registro_ST10(ByVal rs As rdoResultset, _
                                  ByRef Tamanho() As Integer, _
                                  ByRef Formato() As String, _
                                  ByRef Tipo() As String) As String

    Dim Linha As String, i As Integer
    Dim Inteiro As Integer, Fracao As Integer
    Dim Formato_Cadastrado As String, Parte_Inteira As String
    Dim Parte_Decimal As String
    Linha = ""
    For i = 0 To rs.rdoColumns.Count - 1
        
        Select Case Tipo(i + 1) ' No array, o primeiro �ndice ser� o 1.
        Case "CT"
            Linha = rs(i)
        Case "AN"
            Linha = Left(Tira_Caracter_Especial(rs(i) & "") & Space(Tamanho(i + 1)), Tamanho(i + 1)) '& Chr(9)
        Case "NM", "C1", "C2"
            ' O formato cont�m "<Inteiro>,<Fra��o>"
            Inteiro = Val(Left(Formato(i + 1), InStr(Formato(i + 1), ",") - 1))
            Fracao = Val(Mid(Formato(i + 1), InStr(Formato(i + 1), ",") + 1))
            Parte_Inteira = ""
            While Inteiro > 0
                If Parte_Inteira = "" Then
                    Parte_Inteira = Parte_Inteira & Left(String(Inteiro, "0"), 3)
                Else
                    Parte_Inteira = Left(String(Inteiro, "0"), 3) & "," & Parte_Inteira
                End If
                Inteiro = Inteiro - 3
            Wend
            If Fracao > 0 Then
                Parte_Decimal = "." & String(Fracao, "0")
            Else
                Parte_Decimal = ""
            End If
            Formato_Cadastrado = Parte_Inteira & Parte_Decimal
            Linha = Format(CDbl("0" & TrocaPontoPorVirgula(IIf(IsNull(rs(i)), "", rs(i)))), Formato_Cadastrado)
            
'            If Fracao > 0 Then
'                Parte_Decimal = "." & String(Fracao, "0")
'            Else
'                Parte_Decimal = ""
'            End If
'            Formato_Cadastrado = Parte_Inteira & Parte_Decimal
'            Linha = Format(CDbl("0" & rs(i)), Formato_Cadastrado)
'            Linha = Replace(Linha, ",", ";")
'            Linha = Replace(Linha, ".", Sep_Milhar(i + 1))
'            Linha = Replace(Linha, ";", Sep_Decimal(i + 1))
            'Linha = Format(CDbl("0" & Format(rs(i), "#,##0.00")))
            Linha = Replace(Linha, ",", ";")
            Linha = Replace(Linha, ".", "")
            Linha = Replace(Linha, ";", "")
            Linha = Trim(Linha)
''''''            Linha = Replace(Linha, ".", Sep_Milhar(i + 1))
''''''            Linha = Replace(Linha, ";", Sep_Decimal(i + 1))
        Case "DT"
            'Alterado por Gustavo em 26/03/2004
            If IsNull(rs(i)) Then
                Linha = Format("01/01/1900", Formato(i + 1))
            Else
                Linha = Format(CDate(rs(i)), Formato(i + 1))
            End If
        End Select

        Formata_Registro_ST10 = Formata_Registro_ST10 & Linha

    Next i
'*********************************************************************************************

End Function
                                  
Public Function Formata_Registro(ByVal rs As rdoResultset, _
                                  ByRef Tamanho() As Integer, _
                                  ByRef Formato() As String, _
                                  ByRef Tipo() As String, _
                                  ByRef Sep_Decimal() As String, _
                                  ByRef Sep_Milhar() As String, _
                                  Optional ByVal Evento_BB As String) As String

    Dim Linha As String, i As Integer
    Dim Inteiro As Integer, Fracao As Integer
    Dim Formato_Cadastrado As String, Parte_Inteira As String
    Dim Parte_Decimal As String

    Linha = ""
    For i = 0 To rs.rdoColumns.Count - 1
        Select Case Tipo(i + 1) ' No array, o primeiro �ndice ser� o 1.
        Case "CT"
            Linha = rs(i)
        Case "AN"
            'mfujino 20/05/2005
            'Tira todos os caracteres especial excluindo o ''
            'Nao exclui os caracteres acentuados
            'Tratamento n�o aceita valor nulo
            'Cleber 08/06/2005
            Linha = Left(Tira_Caracter_Especial(rs(i) & "") & Space(Tamanho(i + 1)), Tamanho(i + 1)) ' & Chr(9)
        Case "NM", "C1", "C2"
            ' O formato cont�m "<Inteiro>,<Fra��o>"
            Inteiro = Val(Left(Formato(i + 1), InStr(Formato(i + 1), ",") - 1))
            Fracao = Val(Mid(Formato(i + 1), InStr(Formato(i + 1), ",") + 1))
            Parte_Inteira = ""
            While Inteiro > 0
                If Parte_Inteira = "" Then
                    Parte_Inteira = Parte_Inteira & Left(String(Inteiro, "0"), 3)
                Else
                    Parte_Inteira = Left(String(Inteiro, "0"), 3) & "," & Parte_Inteira
                End If
                Inteiro = Inteiro - 3
            Wend
            If Fracao > 0 Then
                Parte_Decimal = "." & String(Fracao, "0")
            Else
                Parte_Decimal = ""
            End If
            Formato_Cadastrado = Parte_Inteira & Parte_Decimal
            Linha = Format(CDbl("0" & rs(i)), Formato_Cadastrado)
            Linha = Replace(Linha, ",", ";")
            Linha = Replace(Linha, ".", Sep_Milhar(i + 1))
            Linha = Replace(Linha, ";", Sep_Decimal(i + 1))
        Case "DT"
            'Alterado por Gustavo em 26/03/2004
            If IsNull(rs(i)) Then
                Linha = Format("01/01/1900", Formato(i + 1))
            Else
                Linha = Format(CDate(rs(i)), Formato(i + 1))
            End If
        End Select

        Formata_Registro = Formata_Registro & Linha

    Next i
    
    'Emaior - 06/03/2010 - Tratamento para trocar "/" por "." nas datas do registro.
    If Evento_BB = 1300 Or Evento_BB = 1301 Then
        Formata_Registro = Replace(Formata_Registro, "/", ".")
    End If

End Function

Public Function Tira_Caracter_Especial(sTxt As String) As String
    'Tira os caracteres especial menos o ''
    
    'Cleber - 08/06/2005 - Alterado a pedido do Marcio para retirar caracteres
    'especiais de acordo com tabela enviada por email
    
    Dim sTxt1 As String
    Dim i  As Integer
    Dim sCaracter As String
    
    sTxt = Trim(sTxt)
    For i = 1 To Len(sTxt)
        Select Case Asc(Mid(sTxt, i, 1))
            Case 0 To 31, 94, 96, 127, 128, 129, 131 To 149, 151 To 191, 198, 208, 209, 215, 216, 221 To 223, 230, 231, 240, 241, 248, 253 To 255    'tirar
                sCaracter = ""
            Case 192 To 197 'substitui para A
                sCaracter = "A"
            Case 199        'substitui para C
                sCaracter = "C"
            Case 200 To 203 'substitui para E
                sCaracter = "E"
            Case 204 To 207 'substitui para I
                sCaracter = "I"
            Case 210 To 214 'substitui para O
                sCaracter = "O"
            Case 217 To 220 'substitui para U
                sCaracter = "U"
            Case 224 To 229 'substitui para a
                sCaracter = "a"
            Case 38, 232 To 235 'substitui para e
                sCaracter = "e"
            Case 236 To 239 'substitui para i
                sCaracter = "i"
            Case 242 To 246 'substitui para o
                sCaracter = "o"
            Case 249 To 252 'substitui para u
                sCaracter = "u"
            Case Else
                sCaracter = Mid(sTxt, i, 1)
        End Select
        
        sTxt1 = sTxt1 + sCaracter
        
'comentado
'        If (Asc(Mid(sTxt, i, 1)) > 32 And Asc(Mid(sTxt, i, 1)) < 255) Or (Asc(Mid(sTxt, i, 1)) = 0) Then
'            sTxt1 = sTxt1 + Mid(sTxt, i, 1)
'        End If
    Next
    Tira_Caracter_Especial = sTxt1
End Function


