/*
	(ntendencia) - 21/11/2019
	Demanda: 00191695-aviso-de-sinistro-web
	Nome do script: view_evento_SEGBR_sinistro_tb.sql
	Descri��o: script para alterar a view evento_SEGBR_sinistro_tb
	Objetivo: alterar view
	Banco: seguros_db
	Ambiente: AB e ABS
*/
ALTER VIEW dbo.evento_SEGBR_sinistro_tb  
AS  
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],        
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],         
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider], [num_recibo],         
[item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto], [val_correcao],        
[val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB], [perc_indenizacao_SISBB],         
[tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial], [chave_autorizacao_sinistro], [chave_usuario_SISBB],         
[proposta_bb], [banco_aviso_id], [moeda_id], [pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro],        
[nome_beneficiario], [situacao_op], [situacao_sinistro], [motivo_encerramento], [cod_produto_bb],         
[dt_acerto_contas_sinistro], [dt_avaliacao], [regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao],         
[dt_alteracao], [lock], [enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante],         
[SITUACAO_AVISO], [ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4],         
[solicitante_grau_parentesco], [solicitante_email], [solicitante_bairro], [solicitante_municipio_id],         
[solicitante_estado], [solicitante_municipio], [solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3],         
[ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2], [tp_telefone3], [tp_telefone4], [cpf_sinistrado],         
[sexo_sinistrado], [dt_nascimento_sinistrado], [evento_sinistro_id], [descricao_evento], [nome_sinistrado],         
[num_remessa], [num_proc_remessa], [endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario],         
[uf_beneficiario], [cep_beneficiario], [ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario],         
[nome_orgao_expedidor], [sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor],[num_remessa_original],        
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],         
[sub_grupo_id],[motivo_reanalise_sinistro_id], tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,cod_cli_cdc, num_ctr_cdc, tipo_pagamento, conta_pagamento_id, dv_conta_pagamento, agencia_pagamento_id, dv_agencia_pagamento    
,banco_pagamento_id, dt_pagamento, val_saldo_devedor_cota, variacao_poupanca, controle_evento_entrada_id  
,cod_segmento ,num_grupo ,num_cota ,num_parcela_consorcio ,dt_vencimento ,val_parcela ,val_adiantamento     
,pos_adiantamento , val_saldo_devedor_vencido ,situacao_cobranca ,num_aviso_newcon ,val_total_adiantado     
,val_corrigido_adiantamentos ,dt_liquidacao ,num_total_parcelas ,val_pld ,val_devolucao ,avaliacao_consorcio_id ,val_total_parcelas     
,val_desp_ressarciveis ,val_desp_nao_ressarciveis ,val_indice_correcao ,val_devolucao_atualizado ,val_pld_parcela ,tp_pessoa  , proposta_bb_consorcio  
,NR_PTC_AVISO -- LPS 
,solicitante_cpf --21/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_DB].[dbo].[evento_SEGBR_sinistro_atual_tb]        
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],         
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb],[nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB],[val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial], [chave_autorizacao_sinistro],         
[chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id], [pgto_nossa_parte], [seq_estimativa],         
[val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op], [situacao_sinistro], [motivo_encerramento],         
[cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao], [regulador_id], [cod_ramo], [processa_reintegracao_IS],         
[dt_inclusao], [dt_alteracao], [lock], [enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise],         
[eMail_reclamante], [SITUACAO_AVISO], [ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3],         
[ddd4], [telefone4], [solicitante_grau_parentesco], [solicitante_email], [solicitante_bairro],         
[solicitante_municipio_id], [solicitante_estado], [solicitante_municipio], [solicitante_CEP], [ramal], [ramal1],         
[ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2], [tp_telefone3], [tp_telefone4],         
[cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado], [evento_sinistro_id], [descricao_evento],         
[nome_sinistrado], [num_remessa], [num_proc_remessa], [endereco_beneficiario], [bairro_beneficiario],         
[cidade_beneficiario], [uf_beneficiario], [cep_beneficiario], [ddd_beneficiario], [telefone_beneficiario],         
[tp_documento_beneficiario], [nome_orgao_expedidor], [sigla_orgao_expedidor], [dt_expedicao], [numero_documento],         
[nome_tutor], [num_remessa_original], [evento_BB_original], [cod_erro_remessa], [descr_erro_remessa],         
[canal_comunicacao], [ind_tp_sinistrado], [cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro],         
[municipio_sinistro], [municipio_id_sinistro], [estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD],         
[CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO], [NR_VRS_EDS], [val_informado], [num_contr_seguro],         
[num_ver_endosso], [subevento_sinistro_id],sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,        
dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL  
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2003_tb] WITH (NOLOCK)        
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],         
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb],[nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB],[val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial], [chave_autorizacao_sinistro],         
[chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id], [pgto_nossa_parte], [seq_estimativa],         
[val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op], [situacao_sinistro], [motivo_encerramento],         
[cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao], [regulador_id], [cod_ramo], [processa_reintegracao_IS],         
[dt_inclusao], [dt_alteracao], [lock], [enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise],         
[eMail_reclamante], [SITUACAO_AVISO], [ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3],         
[ddd4], [telefone4], [solicitante_grau_parentesco], [solicitante_email], [solicitante_bairro],         
[solicitante_municipio_id], [solicitante_estado], [solicitante_municipio], [solicitante_CEP], [ramal], [ramal1],         
[ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2], [tp_telefone3], [tp_telefone4],         
[cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado], [evento_sinistro_id], [descricao_evento],         
[nome_sinistrado], [num_remessa], [num_proc_remessa], [endereco_beneficiario], [bairro_beneficiario],         
[cidade_beneficiario], [uf_beneficiario], [cep_beneficiario], [ddd_beneficiario], [telefone_beneficiario],         
[tp_documento_beneficiario], [nome_orgao_expedidor], [sigla_orgao_expedidor], [dt_expedicao], [numero_documento],         
[nome_tutor], [num_remessa_original], [evento_BB_original], [cod_erro_remessa], [descr_erro_remessa],         
[canal_comunicacao], [ind_tp_sinistrado], [cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro],         
[municipio_sinistro], [municipio_id_sinistro], [estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD],         
[CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO], [NR_VRS_EDS], [val_informado], [num_contr_seguro],         
[num_ver_endosso], [subevento_sinistro_id],sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,        
dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL , NULL  
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2004_tb] WITH (NOLOCK)        
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],        
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial],         
[chave_autorizacao_sinistro], [chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id],         
[pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op],         
[situacao_sinistro], [motivo_encerramento], [cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao],         
[regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao], [dt_alteracao], [lock],         
[enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante], [SITUACAO_AVISO],         
[ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4], [solicitante_grau_parentesco],         
[solicitante_email], [solicitante_bairro], [solicitante_municipio_id], [solicitante_estado], [solicitante_municipio],         
[solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2],         
[tp_telefone3], [tp_telefone4], [cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado],         
[evento_sinistro_id], [descricao_evento], [nome_sinistrado], [num_remessa], [num_proc_remessa],         
[endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario], [uf_beneficiario], [cep_beneficiario],         
[ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario], [nome_orgao_expedidor],         
[sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor], [num_remessa_original],         
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],        
sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL   
,NULL -- LPS 
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web 
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2005_tb] WITH (NOLOCK)  
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],        
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial],         
[chave_autorizacao_sinistro], [chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id],         
[pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op],         
[situacao_sinistro], [motivo_encerramento], [cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao],         
[regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao], [dt_alteracao], [lock],         
[enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante], [SITUACAO_AVISO],         
[ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4], [solicitante_grau_parentesco],         
[solicitante_email], [solicitante_bairro], [solicitante_municipio_id], [solicitante_estado], [solicitante_municipio],         
[solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2],         
[tp_telefone3], [tp_telefone4], [cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado],         
[evento_sinistro_id], [descricao_evento], [nome_sinistrado], [num_remessa], [num_proc_remessa],         
[endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario], [uf_beneficiario], [cep_beneficiario],         
[ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario], [nome_orgao_expedidor],         
[sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor], [num_remessa_original],         
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],        
sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL   
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2006_tb] WITH (NOLOCK)  
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],        
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial],         
[chave_autorizacao_sinistro], [chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id],         
[pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op],         
[situacao_sinistro], [motivo_encerramento], [cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao],         
[regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao], [dt_alteracao], [lock],         
[enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante], [SITUACAO_AVISO],         
[ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4], [solicitante_grau_parentesco],         
[solicitante_email], [solicitante_bairro], [solicitante_municipio_id], [solicitante_estado], [solicitante_municipio],         
[solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2],         
[tp_telefone3], [tp_telefone4], [cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado],         
[evento_sinistro_id], [descricao_evento], [nome_sinistrado], [num_remessa], [num_proc_remessa],         
[endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario], [uf_beneficiario], [cep_beneficiario],         
[ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario], [nome_orgao_expedidor],         
[sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor], [num_remessa_original],         
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],        
sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL   
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2007_tb] WITH (NOLOCK)  
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],        
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial],         
[chave_autorizacao_sinistro], [chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id],         
[pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op],         
[situacao_sinistro], [motivo_encerramento], [cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao],         
[regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao], [dt_alteracao], [lock],         
[enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante], [SITUACAO_AVISO],         
[ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4], [solicitante_grau_parentesco],         
[solicitante_email], [solicitante_bairro], [solicitante_municipio_id], [solicitante_estado], [solicitante_municipio],         
[solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2],         
[tp_telefone3], [tp_telefone4], [cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado],         
[evento_sinistro_id], [descricao_evento], [nome_sinistrado], [num_remessa], [num_proc_remessa],         
[endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario], [uf_beneficiario], [cep_beneficiario],         
[ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario], [nome_orgao_expedidor],         
[sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor], [num_remessa_original],         
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],        
sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL   
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2008_tb] WITH (NOLOCK)  
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],        
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial],         
[chave_autorizacao_sinistro], [chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id],         
[pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op],         
[situacao_sinistro], [motivo_encerramento], [cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao],         
[regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao], [dt_alteracao], [lock],         
[enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante], [SITUACAO_AVISO],         
[ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4], [solicitante_grau_parentesco],         
[solicitante_email], [solicitante_bairro], [solicitante_municipio_id], [solicitante_estado], [solicitante_municipio],         
[solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2],         
[tp_telefone3], [tp_telefone4], [cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado],         
[evento_sinistro_id], [descricao_evento], [nome_sinistrado], [num_remessa], [num_proc_remessa],         
[endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario], [uf_beneficiario], [cep_beneficiario],         
[ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario], [nome_orgao_expedidor],         
[sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor], [num_remessa_original],         
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],        
sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL   
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2009_tb] WITH (NOLOCK)  
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],        
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial],         
[chave_autorizacao_sinistro], [chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id],         
[pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op],         
[situacao_sinistro], [motivo_encerramento], [cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao],         
[regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao], [dt_alteracao], [lock],         
[enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante], [SITUACAO_AVISO],         
[ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4], [solicitante_grau_parentesco],         
[solicitante_email], [solicitante_bairro], [solicitante_municipio_id], [solicitante_estado], [solicitante_municipio],         
[solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2],         
[tp_telefone3], [tp_telefone4], [cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado],         
[evento_sinistro_id], [descricao_evento], [nome_sinistrado], [num_remessa], [num_proc_remessa],         
[endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario], [uf_beneficiario], [cep_beneficiario],         
[ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario], [nome_orgao_expedidor],         
[sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor], [num_remessa_original],         
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],        
sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL   
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2010_tb] WITH (NOLOCK)  
UNION ALL        
SELECT [evento_id], [sinistro_id], [apolice_id], [seguradora_cod_susep], [sucursal_seguradora_id], [ramo_id],         
[evento_SEGBR_id], [dt_evento], [usuario], [entidade_id], [evento_bb_id], [localizacao], [produto_id], [sinistro_bb],         
[proposta_id], [agencia_aviso_id], [dt_aviso_sinistro], [cliente_id], [nome_segurado], [cpf_cgc_segurado],         
[dt_nascimento_segurado], [sexo_segurado], [dt_ocorrencia_sinistro], [endereco_sinistro], [nome_solicitante],        
[endereco_solicitante], [ddd_solicitante], [telefone_solicitante], [endosso_id], [sinistro_id_lider],         
[num_recibo], [item_val_estimativa], [tp_cobertura_id], [cod_cobertura_bb], [nome_cobertura], [val_acerto],         
[val_correcao], [val_prejuizo_SISBB], [val_depreciacao_SISBB], [val_salvado_SISBB], [val_franquia_SISBB],         
[perc_indenizacao_SISBB], [tp_beneficiario], [dt_nascimento_benef], [pgto_ordem_judicial],         
[chave_autorizacao_sinistro], [chave_usuario_SISBB], [proposta_bb], [banco_aviso_id], [moeda_id],         
[pgto_nossa_parte], [seq_estimativa], [val_cosseguro], [val_resseguro], [nome_beneficiario], [situacao_op],         
[situacao_sinistro], [motivo_encerramento], [cod_produto_bb], [dt_acerto_contas_sinistro], [dt_avaliacao],         
[regulador_id], [cod_ramo], [processa_reintegracao_IS], [dt_inclusao], [dt_alteracao], [lock],         
[enviado_eMail_tecnico], [canal_venda_id], [ind_reanalise], [eMail_reclamante], [SITUACAO_AVISO],         
[ddd1], [telefone1], [ddd2], [telefone2], [ddd3], [telefone3], [ddd4], [telefone4], [solicitante_grau_parentesco],         
[solicitante_email], [solicitante_bairro], [solicitante_municipio_id], [solicitante_estado], [solicitante_municipio],         
[solicitante_CEP], [ramal], [ramal1], [ramal2], [ramal3], [ramal4], [tp_telefone], [tp_telefone1], [tp_telefone2],         
[tp_telefone3], [tp_telefone4], [cpf_sinistrado], [sexo_sinistrado], [dt_nascimento_sinistrado],         
[evento_sinistro_id], [descricao_evento], [nome_sinistrado], [num_remessa], [num_proc_remessa],         
[endereco_beneficiario], [bairro_beneficiario], [cidade_beneficiario], [uf_beneficiario], [cep_beneficiario],         
[ddd_beneficiario], [telefone_beneficiario], [tp_documento_beneficiario], [nome_orgao_expedidor],         
[sigla_orgao_expedidor], [dt_expedicao], [numero_documento], [nome_tutor], [num_remessa_original],         
[evento_BB_original], [cod_erro_remessa], [descr_erro_remessa], [canal_comunicacao], [ind_tp_sinistrado],         
[cpf_beneficiario], [hora_ocorrencia_sinistro], [bairro_sinistro], [municipio_sinistro], [municipio_id_sinistro],         
[estado_sinistro], [tp_ramo_id], [cep_sinistro], [CD_PRD], [CD_MDLD], [CD_ITEM_MDLD], [cod_transacao], [NR_CTC_SGRO],         
[NR_VRS_EDS], [val_informado], [num_contr_seguro], [num_ver_endosso], [subevento_sinistro_id],        
sub_grupo_id,motivo_reanalise_sinistro_id, tp_log_sinistro_id,dt_ocorrencia_sinistro_fim    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL    
,NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL   
,NULL -- LPS  
,NULL --28/11/2019 - ntendencia - 00191695-aviso-de-sinistro-web
FROM [SEGUROS_HIST_DB].[dbo].[evento_SEGBR_sinistro_hist_2011_tb] WITH (NOLOCK)  
  
  
  
  
  